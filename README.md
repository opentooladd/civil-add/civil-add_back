# Civil-Add

[![pipeline status](https://gitlab.com/opentooladd/civil-add/badges/master/pipeline.svg)](https://gitlab.com/opentooladd/civil-add/-/commits/master)
[![coverage report](https://gitlab.com/opentooladd/civil-add/badges/master/coverage.svg)](https://gitlab.com/opentooladd/civil-add/-/commits/master)

[Civil-Add documentation home page](https://opentooladd.gitlab.io/civil-add/)


## Goals

Civil-Add has to

* be a tool helping production coordination and trade standardisation.
* be compatible with current economic system
* take high inspiration from agile management tools, process and philosophy that
bring virtue to humans being.
* bring trade and collaboration tools to persons into their social network.
* define "common good" and to defend it.

**TODO** add wiki link 

## Usage

### Setup local development environment

Get dev environment from gitlab registry : `docker login` **TODO**

Build dev environment : `make docker-build` or `make docker-login`

Setup Keycloak data : `make env-reset-dev`


> :point_up: This last step set default local user
>
> | username | password |
> | :------: | :------: |
> | ta_user  | ta_pwd   |

### Postman setup

Configure postman : [import](https://learning.postman.com/docs/postman/collections/data-formats/#importing-postman-data)
`infra/postman` directory. Then use `CIVIL-ADD_LOCAL` [environment](https://learning.postman.com/docs/postman/collections/data-formats/#importing-postman-data)

> :point_up: You may set your own variable values

In order to login, you need to [authenticate in postman](https://learning.postman.com/docs/postman/sending-api-requests/authorization/#oauth-20)
If you are properly authenticated, you may find your connection info sending
`service > authentication > Session details` request.

If you are not properly logged in, all private requests may redirect you to login
page.

Once you are logged in, you should see `user_session` cookie.

> :point_up: If you need to test some login function, you can remove this cookie.


### Development process

* ```make dev-start```
* ```make dev-run```
* tests & developments
* ```make dev-stop```

> :point_up: according `infra/docker/dev.docker-compose.yaml`, you can listen
> civil-add service at [http://localhost:8000](http://localhost:8000)


**TODO** Describe docker setup process
 
> 1. get up to date develop/master branch
> 2. create your branch
> 3. build and deploy your development docker image (you can use CI)
> 4. dev-start
> 5. dev-run
> 6. developer stuff
> 7. make dev-stop
> 8. push your changes

> cf. Makefile
>> ```
>> ## default docker registry uses git origin :
>> ## git@gitlab.com:my_path/my_project.git	=> registry.gitlab.com/my_path/
>> ```
>> ```
>> ## default docker image uses project name and branches as tag
>> ## git@gitlab.com:my_path/my_project.git	=> my_project:current_branche
>> ```

### debug and custom usage

**TODO**

* ```make shell-dev```
* ```cargo run --bin show_settings```
* ```cargo run --bin write_task```
* ```cargo run --bin show_tasks```
* ```cargo run --bin delete_task {a key word contained in targeted tasks}```
* ```cargo run --bin show_tasks```
* ...


### Clean Docker environment

You may need to clean all your local Docker environment :
```sh
docker stop $(docker ps -a -q)
docker rm -v $(docker ps -a -q)
docker volume rm $(docker volume ls -qf dangling=true)
docker rmi $(docker images -a -q) -f
```

## Project layout

### Versioning

[https://semver.org/](https://semver.org/)


**TODO** add **Semantic git flow** wiki link 

### Structure

[https://doc.rust-lang.org/cargo/guide/project-layout.html](https://doc.rust-lang.org/cargo/guide/project-layout.html)


## Stack

**TODO**

Docker architecture description

* rust
    * cargo
    * rocket
    * diesel
    * oxide-auth
* PostgreSQL
* Docker
* git


## Contribute

### Update [gitlab Docker registry](https://docs.gitlab.com/ee/user/packages/container_registry/)

You can use CI `deploy docker image` stage to publish manually an image.

To publish from your local, you'll need your username and a dedicated token.
Then you can use `make docker-*` commands

```sh
make docker-login
make docker-deploy
```

### Database

**TODO**

If you want to create a new db migration (https://diesel.rs/guides/getting-started/)
`diesel migration generate create_posts`

If you want to reset your db
`diesel --database-url ${CVAD_BACK_DATABASE_URL} migration redo`

**TODO**

[problem with diesel](https://github.com/diesel-rs/diesel/issues/1661)