-- Initial tables
-------------------

CREATE TABLE user_profiles (
  id VARCHAR(36) NOT NULL,
  name VARCHAR(63) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE tasks (
  id BIGSERIAL PRIMARY KEY,
  title VARCHAR(127) NOT NULL,
  body TEXT NOT NULL
);

CREATE TABLE task_status (
  id SERIAL PRIMARY KEY,
  name VARCHAR(63) NOT NULL
);

CREATE TABLE team_roles (
  id SERIAL PRIMARY KEY,
  name VARCHAR(63) NOT NULL
);

CREATE TABLE task_teams (
  task_id BIGINT NOT NULL REFERENCES tasks(id),
  user_id VARCHAR(36) NOT NULL REFERENCES user_profiles(id),
  team_role_id INT NOT NULL REFERENCES team_roles(id),
  PRIMARY KEY (task_id, user_id, team_role_id)
);

CREATE TABLE task_status_history (
  task_id BIGINT NOT NULL REFERENCES tasks(id),
  status_id  INT NOT NULL REFERENCES task_status(id),
  author_id VARCHAR(36) NOT NULL,
  transition_timestamp TIMESTAMP NOT NULL,
  -- the PRIMARY KEY is here to satisfy diesel constraints and avoid :
  -- `Diesel only supports tables with primary keys. Table task_status_history has no primary key`
  PRIMARY KEY (task_id, status_id, author_id, transition_timestamp)
);


-- Default values
-------------------

INSERT INTO task_status (name) VALUES
('DRAFT'), ('PUBLISHED'), ('TEAM_ESTABLISHED'), ('UNDER_WORK'), ('DONE'), ('ARCHIVED');

INSERT INTO team_roles (name) VALUES
('OWNER'), ('CONTRIBUTOR');
