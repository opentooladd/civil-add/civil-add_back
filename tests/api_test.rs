// WARN : notice this macro is mandatory for common::base_test
// TODO : how to add this macro to target source code file
// https://doc.rust-lang.org/book/ch19-06-macros.html
#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;

extern crate civil_add;

mod common;

#[test]
fn it_returns_proper_home_page_message() {

    let api_mock = common::base_test::ApiMock::init();

    let mut response = api_mock.client.get("/").dispatch();

    assert_eq!(response.status(), Status::Ok);

    assert_eq!(response.content_type(), Some(ContentType::Plain));
    // assert!(response.headers().get_one("X-Special").is_some());
    use rocket::http::{ContentType, Status};
    assert_eq!(response.body_string(), Some("Hello, world ! \nDon't worry, Tool-Add is here !".into()));
}
