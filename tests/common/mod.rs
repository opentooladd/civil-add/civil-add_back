
pub mod base_test {

    extern crate civil_add;

    use rocket::local::Client;

    pub struct ApiMock {
        pub client: Client
    }

    impl ApiMock {
        pub fn init() -> ApiMock {

            let rocket_server = rocket::ignite().mount("/", routes![civil_add::routes::base::index]);
            let rocket_client = Client::new(rocket_server).expect("valid rocket instance");

            ApiMock{ client: rocket_client, }
        }
    }
}