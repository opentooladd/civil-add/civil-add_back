
## variables & setup
#####################

# DOCKER
ifndef DOCKER_COMPOSE
DOCKER_COMPOSE := docker-compose \
	-f infra/docker/postgres.docker-compose.yaml \
	-f infra/docker/keycloak.docker-compose.yaml \
	-f infra/docker/dev.docker-compose.yaml
endif

ifndef EXEC
EXEC := docker exec -it dev-civil-add sh -c
endif

export DOCKER_WORKING_DIR=/usr/src/civil-add

ifndef DOCKER_REGISTRY
## default docker registry uses git origin :
## git@gitlab.com:my_path/my_project.git	git						=> registry.gitlab.com/my_path/
## https://gitlab-ci-token:[...]@gitlab.com:my_path/my_project.git	=> registry.gitlab.com/my_path/
export DOCKER_REGISTRY=$(shell git remote get-url origin | sed -e 's/^.*@gitlab\.com[:\/]/registry.gitlab.com\//' -e 's|\(.*\)/.*|\1|')
endif

ifndef CI_COMMIT_REF_NAME
## variable used to make CI / local environment consistent
export CI_COMMIT_REF_NAME=$(shell git rev-parse --abbrev-ref HEAD)
endif

ifndef DOCKER_IMG
## default docker image uses project name and branches as tag
## git@gitlab.com:my_path/my_project.git	=> my_project:current_branche
# export DOCKER_IMG=$(DOCKER_REGISTRY)/$(shell git remote get-url origin | sed -e 's|.*\/||' -e 's/\.git//'):$(CI_COMMIT_REF_NAME)
export DOCKER_IMG=$(DOCKER_REGISTRY)/$(shell git remote get-url origin | sed -e 's|.*/||' -e 's/.git//'):master
endif


# RUST
export CARGO_INCREMENTAL=0
export RUSTFLAGS="-Zprofile -Ccodegen-units=1 -Cinline-threshold=0 -Clink-dead-code -Coverflow-checks=off -Zno-landing-pads"
export RUST_BACKTRACE=full
#export RUST_BACKTRACE=1


# POSTGRES
export PG_ADMIN_USR=aUser
export PG_ADMIN_PWD=aPwd
export PG_ADMIN_DB=postgres

export PG_KEYCLOAK_USR=kUser
export PG_KEYCLOAK_PWD=kPwd
export PG_KEYCLOAK_DB=keycloak_db

export PG_CIVILADD_USR=caUser
export PG_CIVILADD_PWD=caPwd
export PG_CIVILADD_DB=civil_add_db

# CIVIL-ADD
export CVAD_PATH_LOCAL=$(shell realpath ./infra/docker/dev)
export CVAD_PATH=/var/civil-add
export DATABASE_URL=postgres://${PG_CIVILADD_USR}:${PG_CIVILADD_PWD}@postgres-civil-add:5432/${PG_CIVILADD_DB}
# TODO : review environment management; shouldn't we use a .env file ?
#echo DATABASE_URL=postgres://username:password@localhost/diesel_demo > .env

## Dev commands
################

dev-start: infra-up
	$(EXEC) 'cargo build'
	$(EXEC) "diesel --database-url ${DATABASE_URL} setup"

dev-run:
	$(EXEC) 'cargo watch -x run'

dev-test:
	$(EXEC) 'cargo test'

dev-coverage:
	$(EXEC) 'cargo tarpaulin -v --out Html --output-dir ./public/coverage'

dev-stop: clean-project infra-stop

dev-doc:
	$(EXEC) 'cargo doc'
	$(EXEC) 'mv target/doc public/doc'


## DB commands
###############

# TODO
#db-gen-migration:
#	$(EXEC) 'diesel migration generate {Migration Name}'

db-migrate:
	$(EXEC) "diesel --database-url ${DATABASE_URL} migration run"

db-redo-migration:
	$(EXEC) "diesel --database-url ${DATABASE_URL} migration redo"


## Docker
##########

docker-info:
	docker info
	docker-compose --version
	@echo " == DOCKER_WORKING_DIR : ${DOCKER_WORKING_DIR}"
	@echo " == DOCKER_REGISTRY    : ${DOCKER_REGISTRY}"
	@echo " == DOCKER_IMG         : ${DOCKER_IMG}"

## TODO : make CI/local docker login consistent
docker-login:
	docker login $(DOCKER_REGISTRY)

docker-login-ci:
	docker login -u gitlab-ci-token -p $(CI_BUILD_TOKEN) $(DOCKER_REGISTRY)

#aaaa
#	docker login -u $(DOCKER_REGISTRY_USER) -p $(CI_BUILD_TOKEN) $(DOCKER_REGISTRY)
#	docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com


docker-logout:
	docker logout $(DOCKER_REGISTRY)

docker-build:
	docker build --build-arg working_dir=${DOCKER_WORKING_DIR} -t ${DOCKER_IMG} -f ./infra/docker/Dockerfile .

docker-publish:
	docker push $(DOCKER_IMG)

docker-deploy: docker-build docker-publish

## Infra
##########

infra-docker-build:
	docker build --build-arg working_dir=${DOCKER_WORKING_DIR} -t ${DOCKER_IMG} -f ./infra/docker/Dockerfile .

infra-up: infra-gen-conf
	$(DOCKER_COMPOSE) up -d

infra-stop:
	$(DOCKER_COMPOSE) stop

infra-down:
	- $(DOCKER_COMPOSE) down --volumes

infra-ps:
	$(DOCKER_COMPOSE) ps

infra-log:
	$(DOCKER_COMPOSE) logs -f
# $(DOCKER_COMPOSE) logs -f keycloak-civil-add
#	$(DOCKER_COMPOSE) logs -f dev-civil-add
#	$(DOCKER_COMPOSE) logs -f postgres-civil-add

# TODO : define a script for this generation
# TODO : use variables properly
infra-gen-conf:
	@mkdir -p ${CVAD_PATH_LOCAL}/configuration
	@touch ${CVAD_PATH_LOCAL}/configuration/civil-add.toml
	@echo '[database]' > ${CVAD_PATH_LOCAL}/configuration/civil-add.toml
	@echo "url='${DATABASE_URL}'" >> ${CVAD_PATH_LOCAL}/configuration/civil-add.toml
	@echo "" >> ${CVAD_PATH_LOCAL}/configuration/civil-add.toml
	@echo '[keycloak]' >> ${CVAD_PATH_LOCAL}/configuration/civil-add.toml
	@echo "provider_token_uri='http://keycloak-civil-add:8080/auth/realms/civil-add/protocol/openid-connect/token'" >> ${CVAD_PATH_LOCAL}/configuration/civil-add.toml
	@echo "provider_auth_uri='http://localhost:8888/auth/realms/civil-add/protocol/openid-connect/auth'" >> ${CVAD_PATH_LOCAL}/configuration/civil-add.toml
	@echo "client_id='civil-add_back'" >> ${CVAD_PATH_LOCAL}/configuration/civil-add.toml
	@echo "client_secret='d5fd9f3f-13b4-4a92-9f31-9ff0a3dd7ac1'" >> ${CVAD_PATH_LOCAL}/configuration/civil-add.toml
	@echo "redirect_uri='http://localhost:8000/auth/redirect'" >> ${CVAD_PATH_LOCAL}/configuration/civil-add.toml


## Environment admin
#####################

# TODO :
# * define env (dev / test / preprod / prod ?)
# * enhance env setup scripts

env-reset:
	$(DOCKER_COMPOSE) stop keycloak-civil-add
	docker exec -it postgres-civil-add sh -c '/docker/env/cmd/reset-db.sh'
	$(DOCKER_COMPOSE) start keycloak-civil-add

env-reset-dev:
	$(DOCKER_COMPOSE) stop keycloak-civil-add
	docker exec -it postgres-civil-add sh -c '/docker/env/cmd/populate-db.sh'
	$(DOCKER_COMPOSE) start keycloak-civil-add

env-dump:
	docker exec -it postgres-civil-add sh -c '/docker/env/cmd/dump-db.sh'


## Docker container shell access'
##################################

shell-dev:
	docker exec -it dev-civil-add sh -c '/bin/bash'

shell-postgres:
	docker exec -it postgres-civil-add sh -c '/bin/bash'

shell-psql:
	docker exec -it postgres-civil-add sh -c 'psql -U "$(PG_CIVILADD_USR)" -d "$(PG_CIVILADD_DB)"'

shell-keycloak:
	docker exec -it keycloak-civil-add sh -c '/bin/bash'


## Clean commands
##################

clean-infra-volumes:
	- $(DOCKER_COMPOSE) down -v --remove-orphans

clean-infra-images:
	- $(DOCKER_COMPOSE) down --rmi all --remove-orphans

clean-civil-add:
	- @find ${CVAD_PATH_LOCAL}/* ! -name .gitignore | sort -nr | xargs rm -r

clean-project: clean-civil-add
	- $(EXEC) 'cargo clean'
	- $(EXEC) 'rm -r public/doc public/coverage'

clean-all: clean-project clean-infra-volumes clean-infra-images

