#!/bin/bash
set -e

## $1 : db_usr
## $2 : db_pwd
## $3 : db_name
function init_db() {

  psql \
    -U "$PG_ADMIN_USR" \
    -d "$PG_ADMIN_DB" \
    -f /docker/env/sql/create-user.sql \
    -v db_usr="$1" \
    -v db_pwd="$2"

  psql \
    -U "$PG_ADMIN_USR" \
    -d "$PG_ADMIN_DB" \
    -f /docker/env/sql/init-db.sql \
    -v db_usr="$1" \
    -v db_name="$3"

}

init_db "$PG_KEYCLOAK_USR" "$PG_KEYCLOAK_PWD" "$PG_KEYCLOAK_DB"
init_db "$PG_CIVILADD_USR" "$PG_CIVILADD_PWD" "$PG_CIVILADD_DB"
