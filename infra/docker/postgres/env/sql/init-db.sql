\set quoted_db_name '"' :db_name '"'
\set quoted_db_usr '"' :db_usr '"'

-- init database
DROP DATABASE IF EXISTS :quoted_db_name;
CREATE DATABASE :quoted_db_name;
GRANT ALL PRIVILEGES ON DATABASE :quoted_db_name TO :quoted_db_usr;