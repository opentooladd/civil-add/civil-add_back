\set quoted_db_usr '"' :db_usr '"'
\set quoted_db_pwd '\'' :db_pwd '\''

-- create user
CREATE USER :quoted_db_usr
WITH
    ENCRYPTED PASSWORD :quoted_db_pwd
    NOSUPERUSER
    NOCREATEDB
    NOCREATEROLE
    LOGIN
;