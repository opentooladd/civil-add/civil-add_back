\set double_quoted_db_name '"' :db_name '"'
\set single_quoted_db_name '\'' :db_name '\''

-- isolate database (no connections)
REVOKE CONNECT ON DATABASE :double_quoted_db_name FROM public;
SELECT pid, pg_terminate_backend(pid)
FROM pg_stat_activity
WHERE datname = :single_quoted_db_name AND pid <> pg_backend_pid();