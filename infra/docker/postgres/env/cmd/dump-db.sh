#!/bin/bash
set -e

pg_dump -U "$PG_ADMIN_USR" "$PG_KEYCLOAK_DB" > /docker/env/data/local/keycloak_dump.sql
pg_dump -U "$PG_ADMIN_USR" "$PG_CIVILADD_DB" > /docker/env/data/local/civil-add_dump.sql