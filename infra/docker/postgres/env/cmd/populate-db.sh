#!/bin/bash
set -e

## $1 : db_usr
## $2 : db_name
function reset_db() {

  psql \
    -U "$PG_ADMIN_USR" \
    -d "$PG_ADMIN_DB" \
    -f /docker/env/sql/isolate-db.sql \
    -v db_name="$2"

  psql \
    -U "$PG_ADMIN_USR" \
    -d "$PG_ADMIN_DB" \
    -f /docker/env/sql/init-db.sql \
    -v db_usr="$1" \
    -v db_name="$2"

}


reset_db "$PG_KEYCLOAK_USR" "$PG_KEYCLOAK_DB"
psql -U "$PG_ADMIN_USR" -d "$PG_KEYCLOAK_DB" -f /docker/env/data/dev/keycloak_dump.sql

reset_db "$PG_CIVILADD_USR" "$PG_CIVILADD_DB"
psql -U "$PG_ADMIN_USR" -d "$PG_CIVILADD_DB" -f /docker/env/data/dev/civil-add_dump.sql