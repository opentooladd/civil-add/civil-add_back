--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Debian 12.2-2.pgdg100+1)
-- Dumped by pg_dump version 12.2 (Debian 12.2-2.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: diesel_manage_updated_at(regclass); Type: FUNCTION; Schema: public; Owner: caUser
--

CREATE FUNCTION public.diesel_manage_updated_at(_tbl regclass) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    EXECUTE format('CREATE TRIGGER set_updated_at BEFORE UPDATE ON %s
                    FOR EACH ROW EXECUTE PROCEDURE diesel_set_updated_at()', _tbl);
END;
$$;


ALTER FUNCTION public.diesel_manage_updated_at(_tbl regclass) OWNER TO "caUser";

--
-- Name: diesel_set_updated_at(); Type: FUNCTION; Schema: public; Owner: caUser
--

CREATE FUNCTION public.diesel_set_updated_at() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (
        NEW IS DISTINCT FROM OLD AND
        NEW.updated_at IS NOT DISTINCT FROM OLD.updated_at
    ) THEN
        NEW.updated_at := current_timestamp;
    END IF;
    RETURN NEW;
END;
$$;


ALTER FUNCTION public.diesel_set_updated_at() OWNER TO "caUser";

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: __diesel_schema_migrations; Type: TABLE; Schema: public; Owner: caUser
--

CREATE TABLE public.__diesel_schema_migrations (
    version character varying(50) NOT NULL,
    run_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.__diesel_schema_migrations OWNER TO "caUser";

--
-- Name: task_status; Type: TABLE; Schema: public; Owner: caUser
--

CREATE TABLE public.task_status (
    id integer NOT NULL,
    name character varying(63) NOT NULL
);


ALTER TABLE public.task_status OWNER TO "caUser";

--
-- Name: task_status_history; Type: TABLE; Schema: public; Owner: caUser
--

CREATE TABLE public.task_status_history (
    task_id bigint NOT NULL,
    status_id integer NOT NULL,
    author_id character varying(36) NOT NULL,
    transition_timestamp timestamp without time zone NOT NULL
);


ALTER TABLE public.task_status_history OWNER TO "caUser";

--
-- Name: task_status_id_seq; Type: SEQUENCE; Schema: public; Owner: caUser
--

CREATE SEQUENCE public.task_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.task_status_id_seq OWNER TO "caUser";

--
-- Name: task_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: caUser
--

ALTER SEQUENCE public.task_status_id_seq OWNED BY public.task_status.id;


--
-- Name: task_teams; Type: TABLE; Schema: public; Owner: caUser
--

CREATE TABLE public.task_teams (
    task_id bigint NOT NULL,
    user_id character varying(36) NOT NULL,
    team_role_id integer NOT NULL
);


ALTER TABLE public.task_teams OWNER TO "caUser";

--
-- Name: tasks; Type: TABLE; Schema: public; Owner: caUser
--

CREATE TABLE public.tasks (
    id bigint NOT NULL,
    title character varying(127) NOT NULL,
    body text NOT NULL
);


ALTER TABLE public.tasks OWNER TO "caUser";

--
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: caUser
--

CREATE SEQUENCE public.tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_id_seq OWNER TO "caUser";

--
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: caUser
--

ALTER SEQUENCE public.tasks_id_seq OWNED BY public.tasks.id;


--
-- Name: team_roles; Type: TABLE; Schema: public; Owner: caUser
--

CREATE TABLE public.team_roles (
    id integer NOT NULL,
    name character varying(63) NOT NULL
);


ALTER TABLE public.team_roles OWNER TO "caUser";

--
-- Name: team_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: caUser
--

CREATE SEQUENCE public.team_roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.team_roles_id_seq OWNER TO "caUser";

--
-- Name: team_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: caUser
--

ALTER SEQUENCE public.team_roles_id_seq OWNED BY public.team_roles.id;


--
-- Name: task_status id; Type: DEFAULT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.task_status ALTER COLUMN id SET DEFAULT nextval('public.task_status_id_seq'::regclass);


--
-- Name: tasks id; Type: DEFAULT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.tasks ALTER COLUMN id SET DEFAULT nextval('public.tasks_id_seq'::regclass);


--
-- Name: team_roles id; Type: DEFAULT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.team_roles ALTER COLUMN id SET DEFAULT nextval('public.team_roles_id_seq'::regclass);


--
-- Data for Name: __diesel_schema_migrations; Type: TABLE DATA; Schema: public; Owner: caUser
--

COPY public.__diesel_schema_migrations (version, run_on) FROM stdin;
00000000000000	2020-04-30 12:31:10.30747
20200220111666	2020-04-30 12:31:10.314167
\.


--
-- Data for Name: task_status; Type: TABLE DATA; Schema: public; Owner: caUser
--

COPY public.task_status (id, name) FROM stdin;
1	DRAFT
2	PUBLISHED
3	TEAM_ESTABLISHED
4	UNDER_WORK
5	DONE
6	ARCHIVED
\.


--
-- Data for Name: task_status_history; Type: TABLE DATA; Schema: public; Owner: caUser
--

COPY public.task_status_history (task_id, status_id, author_id, transition_timestamp) FROM stdin;
\.


--
-- Data for Name: task_teams; Type: TABLE DATA; Schema: public; Owner: caUser
--

COPY public.task_teams (task_id, user_id, team_role_id) FROM stdin;
\.


--
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: caUser
--

COPY public.tasks (id, title, body) FROM stdin;
\.


--
-- Data for Name: team_roles; Type: TABLE DATA; Schema: public; Owner: caUser
--

COPY public.team_roles (id, name) FROM stdin;
1	OWNER
2	CONTRIBUTOR
\.


--
-- Name: task_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: caUser
--

SELECT pg_catalog.setval('public.task_status_id_seq', 6, true);


--
-- Name: tasks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: caUser
--

SELECT pg_catalog.setval('public.tasks_id_seq', 1, false);


--
-- Name: team_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: caUser
--

SELECT pg_catalog.setval('public.team_roles_id_seq', 2, true);


--
-- Name: __diesel_schema_migrations __diesel_schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.__diesel_schema_migrations
    ADD CONSTRAINT __diesel_schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: task_status_history task_status_history_pkey; Type: CONSTRAINT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.task_status_history
    ADD CONSTRAINT task_status_history_pkey PRIMARY KEY (task_id, status_id, author_id, transition_timestamp);


--
-- Name: task_status task_status_pkey; Type: CONSTRAINT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.task_status
    ADD CONSTRAINT task_status_pkey PRIMARY KEY (id);


--
-- Name: task_teams task_teams_pkey; Type: CONSTRAINT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.task_teams
    ADD CONSTRAINT task_teams_pkey PRIMARY KEY (task_id, user_id, team_role_id);


--
-- Name: tasks tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- Name: team_roles team_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.team_roles
    ADD CONSTRAINT team_roles_pkey PRIMARY KEY (id);


--
-- Name: task_status_history task_status_history_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.task_status_history
    ADD CONSTRAINT task_status_history_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.task_status(id);


--
-- Name: task_status_history task_status_history_task_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.task_status_history
    ADD CONSTRAINT task_status_history_task_id_fkey FOREIGN KEY (task_id) REFERENCES public.tasks(id);


--
-- Name: task_teams task_teams_task_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.task_teams
    ADD CONSTRAINT task_teams_task_id_fkey FOREIGN KEY (task_id) REFERENCES public.tasks(id);


--
-- Name: task_teams task_teams_team_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: caUser
--

ALTER TABLE ONLY public.task_teams
    ADD CONSTRAINT task_teams_team_role_id_fkey FOREIGN KEY (team_role_id) REFERENCES public.team_roles(id);


--
-- PostgreSQL database dump complete
--

