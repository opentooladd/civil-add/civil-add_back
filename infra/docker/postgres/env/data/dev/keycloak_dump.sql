--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Debian 12.2-2.pgdg100+1)
-- Dumped by pg_dump version 12.2 (Debian 12.2-2.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: admin_event_entity; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.admin_event_entity (
    id character varying(36) NOT NULL,
    admin_event_time bigint,
    realm_id character varying(255),
    operation_type character varying(255),
    auth_realm_id character varying(255),
    auth_client_id character varying(255),
    auth_user_id character varying(255),
    ip_address character varying(255),
    resource_path character varying(2550),
    representation text,
    error character varying(255),
    resource_type character varying(64)
);


ALTER TABLE public.admin_event_entity OWNER TO "kUser";

--
-- Name: associated_policy; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.associated_policy (
    policy_id character varying(36) NOT NULL,
    associated_policy_id character varying(36) NOT NULL
);


ALTER TABLE public.associated_policy OWNER TO "kUser";

--
-- Name: authentication_execution; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.authentication_execution (
    id character varying(36) NOT NULL,
    alias character varying(255),
    authenticator character varying(36),
    realm_id character varying(36),
    flow_id character varying(36),
    requirement integer,
    priority integer,
    authenticator_flow boolean DEFAULT false NOT NULL,
    auth_flow_id character varying(36),
    auth_config character varying(36)
);


ALTER TABLE public.authentication_execution OWNER TO "kUser";

--
-- Name: authentication_flow; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.authentication_flow (
    id character varying(36) NOT NULL,
    alias character varying(255),
    description character varying(255),
    realm_id character varying(36),
    provider_id character varying(36) DEFAULT 'basic-flow'::character varying NOT NULL,
    top_level boolean DEFAULT false NOT NULL,
    built_in boolean DEFAULT false NOT NULL
);


ALTER TABLE public.authentication_flow OWNER TO "kUser";

--
-- Name: authenticator_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.authenticator_config (
    id character varying(36) NOT NULL,
    alias character varying(255),
    realm_id character varying(36)
);


ALTER TABLE public.authenticator_config OWNER TO "kUser";

--
-- Name: authenticator_config_entry; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.authenticator_config_entry (
    authenticator_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.authenticator_config_entry OWNER TO "kUser";

--
-- Name: broker_link; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.broker_link (
    identity_provider character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL,
    broker_user_id character varying(255),
    broker_username character varying(255),
    token text,
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.broker_link OWNER TO "kUser";

--
-- Name: client; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client (
    id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    full_scope_allowed boolean DEFAULT false NOT NULL,
    client_id character varying(255),
    not_before integer,
    public_client boolean DEFAULT false NOT NULL,
    secret character varying(255),
    base_url character varying(255),
    bearer_only boolean DEFAULT false NOT NULL,
    management_url character varying(255),
    surrogate_auth_required boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    protocol character varying(255),
    node_rereg_timeout integer DEFAULT 0,
    frontchannel_logout boolean DEFAULT false NOT NULL,
    consent_required boolean DEFAULT false NOT NULL,
    name character varying(255),
    service_accounts_enabled boolean DEFAULT false NOT NULL,
    client_authenticator_type character varying(255),
    root_url character varying(255),
    description character varying(255),
    registration_token character varying(255),
    standard_flow_enabled boolean DEFAULT true NOT NULL,
    implicit_flow_enabled boolean DEFAULT false NOT NULL,
    direct_access_grants_enabled boolean DEFAULT false NOT NULL,
    always_display_in_console boolean DEFAULT false NOT NULL
);


ALTER TABLE public.client OWNER TO "kUser";

--
-- Name: client_attributes; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_attributes (
    client_id character varying(36) NOT NULL,
    value character varying(4000),
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_attributes OWNER TO "kUser";

--
-- Name: client_auth_flow_bindings; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_auth_flow_bindings (
    client_id character varying(36) NOT NULL,
    flow_id character varying(36),
    binding_name character varying(255) NOT NULL
);


ALTER TABLE public.client_auth_flow_bindings OWNER TO "kUser";

--
-- Name: client_default_roles; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_default_roles (
    client_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.client_default_roles OWNER TO "kUser";

--
-- Name: client_initial_access; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_initial_access (
    id character varying(36) NOT NULL,
    realm_id character varying(36) NOT NULL,
    "timestamp" integer,
    expiration integer,
    count integer,
    remaining_count integer
);


ALTER TABLE public.client_initial_access OWNER TO "kUser";

--
-- Name: client_node_registrations; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_node_registrations (
    client_id character varying(36) NOT NULL,
    value integer,
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_node_registrations OWNER TO "kUser";

--
-- Name: client_scope; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_scope (
    id character varying(36) NOT NULL,
    name character varying(255),
    realm_id character varying(36),
    description character varying(255),
    protocol character varying(255)
);


ALTER TABLE public.client_scope OWNER TO "kUser";

--
-- Name: client_scope_attributes; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_scope_attributes (
    scope_id character varying(36) NOT NULL,
    value character varying(2048),
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_scope_attributes OWNER TO "kUser";

--
-- Name: client_scope_client; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_scope_client (
    client_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE public.client_scope_client OWNER TO "kUser";

--
-- Name: client_scope_role_mapping; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_scope_role_mapping (
    scope_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.client_scope_role_mapping OWNER TO "kUser";

--
-- Name: client_session; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_session (
    id character varying(36) NOT NULL,
    client_id character varying(36),
    redirect_uri character varying(255),
    state character varying(255),
    "timestamp" integer,
    session_id character varying(36),
    auth_method character varying(255),
    realm_id character varying(255),
    auth_user_id character varying(36),
    current_action character varying(36)
);


ALTER TABLE public.client_session OWNER TO "kUser";

--
-- Name: client_session_auth_status; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_session_auth_status (
    authenticator character varying(36) NOT NULL,
    status integer,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_auth_status OWNER TO "kUser";

--
-- Name: client_session_note; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_session_note (
    name character varying(255) NOT NULL,
    value character varying(255),
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_note OWNER TO "kUser";

--
-- Name: client_session_prot_mapper; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_session_prot_mapper (
    protocol_mapper_id character varying(36) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_prot_mapper OWNER TO "kUser";

--
-- Name: client_session_role; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_session_role (
    role_id character varying(255) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_role OWNER TO "kUser";

--
-- Name: client_user_session_note; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_user_session_note (
    name character varying(255) NOT NULL,
    value character varying(2048),
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_user_session_note OWNER TO "kUser";

--
-- Name: component; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.component (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_id character varying(36),
    provider_id character varying(36),
    provider_type character varying(255),
    realm_id character varying(36),
    sub_type character varying(255)
);


ALTER TABLE public.component OWNER TO "kUser";

--
-- Name: component_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.component_config (
    id character varying(36) NOT NULL,
    component_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(4000)
);


ALTER TABLE public.component_config OWNER TO "kUser";

--
-- Name: composite_role; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.composite_role (
    composite character varying(36) NOT NULL,
    child_role character varying(36) NOT NULL
);


ALTER TABLE public.composite_role OWNER TO "kUser";

--
-- Name: credential; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.credential (
    id character varying(36) NOT NULL,
    salt bytea,
    type character varying(255),
    user_id character varying(36),
    created_date bigint,
    user_label character varying(255),
    secret_data text,
    credential_data text,
    priority integer
);


ALTER TABLE public.credential OWNER TO "kUser";

--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO "kUser";

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO "kUser";

--
-- Name: default_client_scope; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.default_client_scope (
    realm_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE public.default_client_scope OWNER TO "kUser";

--
-- Name: event_entity; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.event_entity (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    details_json character varying(2550),
    error character varying(255),
    ip_address character varying(255),
    realm_id character varying(255),
    session_id character varying(255),
    event_time bigint,
    type character varying(255),
    user_id character varying(255)
);


ALTER TABLE public.event_entity OWNER TO "kUser";

--
-- Name: fed_user_attribute; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_attribute (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    value character varying(2024)
);


ALTER TABLE public.fed_user_attribute OWNER TO "kUser";

--
-- Name: fed_user_consent; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE public.fed_user_consent OWNER TO "kUser";

--
-- Name: fed_user_consent_cl_scope; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_consent_cl_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.fed_user_consent_cl_scope OWNER TO "kUser";

--
-- Name: fed_user_credential; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_credential (
    id character varying(36) NOT NULL,
    salt bytea,
    type character varying(255),
    created_date bigint,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    user_label character varying(255),
    secret_data text,
    credential_data text,
    priority integer
);


ALTER TABLE public.fed_user_credential OWNER TO "kUser";

--
-- Name: fed_user_group_membership; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_group_membership OWNER TO "kUser";

--
-- Name: fed_user_required_action; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_required_action (
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_required_action OWNER TO "kUser";

--
-- Name: fed_user_role_mapping; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_role_mapping (
    role_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_role_mapping OWNER TO "kUser";

--
-- Name: federated_identity; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.federated_identity (
    identity_provider character varying(255) NOT NULL,
    realm_id character varying(36),
    federated_user_id character varying(255),
    federated_username character varying(255),
    token text,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.federated_identity OWNER TO "kUser";

--
-- Name: federated_user; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.federated_user (
    id character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.federated_user OWNER TO "kUser";

--
-- Name: group_attribute; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.group_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.group_attribute OWNER TO "kUser";

--
-- Name: group_role_mapping; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.group_role_mapping (
    role_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.group_role_mapping OWNER TO "kUser";

--
-- Name: identity_provider; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.identity_provider (
    internal_id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    provider_alias character varying(255),
    provider_id character varying(255),
    store_token boolean DEFAULT false NOT NULL,
    authenticate_by_default boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    add_token_role boolean DEFAULT true NOT NULL,
    trust_email boolean DEFAULT false NOT NULL,
    first_broker_login_flow_id character varying(36),
    post_broker_login_flow_id character varying(36),
    provider_display_name character varying(255),
    link_only boolean DEFAULT false NOT NULL
);


ALTER TABLE public.identity_provider OWNER TO "kUser";

--
-- Name: identity_provider_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.identity_provider_config (
    identity_provider_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.identity_provider_config OWNER TO "kUser";

--
-- Name: identity_provider_mapper; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.identity_provider_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    idp_alias character varying(255) NOT NULL,
    idp_mapper_name character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.identity_provider_mapper OWNER TO "kUser";

--
-- Name: idp_mapper_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.idp_mapper_config (
    idp_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.idp_mapper_config OWNER TO "kUser";

--
-- Name: keycloak_group; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.keycloak_group (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_group character varying(36) NOT NULL,
    realm_id character varying(36)
);


ALTER TABLE public.keycloak_group OWNER TO "kUser";

--
-- Name: keycloak_role; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.keycloak_role (
    id character varying(36) NOT NULL,
    client_realm_constraint character varying(255),
    client_role boolean DEFAULT false NOT NULL,
    description character varying(255),
    name character varying(255),
    realm_id character varying(255),
    client character varying(36),
    realm character varying(36)
);


ALTER TABLE public.keycloak_role OWNER TO "kUser";

--
-- Name: migration_model; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.migration_model (
    id character varying(36) NOT NULL,
    version character varying(36),
    update_time bigint DEFAULT 0 NOT NULL
);


ALTER TABLE public.migration_model OWNER TO "kUser";

--
-- Name: offline_client_session; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.offline_client_session (
    user_session_id character varying(36) NOT NULL,
    client_id character varying(255) NOT NULL,
    offline_flag character varying(4) NOT NULL,
    "timestamp" integer,
    data text,
    client_storage_provider character varying(36) DEFAULT 'local'::character varying NOT NULL,
    external_client_id character varying(255) DEFAULT 'local'::character varying NOT NULL
);


ALTER TABLE public.offline_client_session OWNER TO "kUser";

--
-- Name: offline_user_session; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.offline_user_session (
    user_session_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    created_on integer NOT NULL,
    offline_flag character varying(4) NOT NULL,
    data text,
    last_session_refresh integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.offline_user_session OWNER TO "kUser";

--
-- Name: policy_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.policy_config (
    policy_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value text
);


ALTER TABLE public.policy_config OWNER TO "kUser";

--
-- Name: protocol_mapper; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.protocol_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    protocol character varying(255) NOT NULL,
    protocol_mapper_name character varying(255) NOT NULL,
    client_id character varying(36),
    client_scope_id character varying(36)
);


ALTER TABLE public.protocol_mapper OWNER TO "kUser";

--
-- Name: protocol_mapper_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.protocol_mapper_config (
    protocol_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.protocol_mapper_config OWNER TO "kUser";

--
-- Name: realm; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm (
    id character varying(36) NOT NULL,
    access_code_lifespan integer,
    user_action_lifespan integer,
    access_token_lifespan integer,
    account_theme character varying(255),
    admin_theme character varying(255),
    email_theme character varying(255),
    enabled boolean DEFAULT false NOT NULL,
    events_enabled boolean DEFAULT false NOT NULL,
    events_expiration bigint,
    login_theme character varying(255),
    name character varying(255),
    not_before integer,
    password_policy character varying(2550),
    registration_allowed boolean DEFAULT false NOT NULL,
    remember_me boolean DEFAULT false NOT NULL,
    reset_password_allowed boolean DEFAULT false NOT NULL,
    social boolean DEFAULT false NOT NULL,
    ssl_required character varying(255),
    sso_idle_timeout integer,
    sso_max_lifespan integer,
    update_profile_on_soc_login boolean DEFAULT false NOT NULL,
    verify_email boolean DEFAULT false NOT NULL,
    master_admin_client character varying(36),
    login_lifespan integer,
    internationalization_enabled boolean DEFAULT false NOT NULL,
    default_locale character varying(255),
    reg_email_as_username boolean DEFAULT false NOT NULL,
    admin_events_enabled boolean DEFAULT false NOT NULL,
    admin_events_details_enabled boolean DEFAULT false NOT NULL,
    edit_username_allowed boolean DEFAULT false NOT NULL,
    otp_policy_counter integer DEFAULT 0,
    otp_policy_window integer DEFAULT 1,
    otp_policy_period integer DEFAULT 30,
    otp_policy_digits integer DEFAULT 6,
    otp_policy_alg character varying(36) DEFAULT 'HmacSHA1'::character varying,
    otp_policy_type character varying(36) DEFAULT 'totp'::character varying,
    browser_flow character varying(36),
    registration_flow character varying(36),
    direct_grant_flow character varying(36),
    reset_credentials_flow character varying(36),
    client_auth_flow character varying(36),
    offline_session_idle_timeout integer DEFAULT 0,
    revoke_refresh_token boolean DEFAULT false NOT NULL,
    access_token_life_implicit integer DEFAULT 0,
    login_with_email_allowed boolean DEFAULT true NOT NULL,
    duplicate_emails_allowed boolean DEFAULT false NOT NULL,
    docker_auth_flow character varying(36),
    refresh_token_max_reuse integer DEFAULT 0,
    allow_user_managed_access boolean DEFAULT false NOT NULL,
    sso_max_lifespan_remember_me integer DEFAULT 0 NOT NULL,
    sso_idle_timeout_remember_me integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.realm OWNER TO "kUser";

--
-- Name: realm_attribute; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_attribute (
    name character varying(255) NOT NULL,
    value character varying(255),
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_attribute OWNER TO "kUser";

--
-- Name: realm_default_groups; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_default_groups (
    realm_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_default_groups OWNER TO "kUser";

--
-- Name: realm_default_roles; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_default_roles (
    realm_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_default_roles OWNER TO "kUser";

--
-- Name: realm_enabled_event_types; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_enabled_event_types (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_enabled_event_types OWNER TO "kUser";

--
-- Name: realm_events_listeners; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_events_listeners (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_events_listeners OWNER TO "kUser";

--
-- Name: realm_required_credential; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_required_credential (
    type character varying(255) NOT NULL,
    form_label character varying(255),
    input boolean DEFAULT false NOT NULL,
    secret boolean DEFAULT false NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_required_credential OWNER TO "kUser";

--
-- Name: realm_smtp_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_smtp_config (
    realm_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.realm_smtp_config OWNER TO "kUser";

--
-- Name: realm_supported_locales; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_supported_locales (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_supported_locales OWNER TO "kUser";

--
-- Name: redirect_uris; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.redirect_uris (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.redirect_uris OWNER TO "kUser";

--
-- Name: required_action_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.required_action_config (
    required_action_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.required_action_config OWNER TO "kUser";

--
-- Name: required_action_provider; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.required_action_provider (
    id character varying(36) NOT NULL,
    alias character varying(255),
    name character varying(255),
    realm_id character varying(36),
    enabled boolean DEFAULT false NOT NULL,
    default_action boolean DEFAULT false NOT NULL,
    provider_id character varying(255),
    priority integer
);


ALTER TABLE public.required_action_provider OWNER TO "kUser";

--
-- Name: resource_attribute; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    resource_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_attribute OWNER TO "kUser";

--
-- Name: resource_policy; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_policy (
    resource_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_policy OWNER TO "kUser";

--
-- Name: resource_scope; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_scope (
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_scope OWNER TO "kUser";

--
-- Name: resource_server; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_server (
    id character varying(36) NOT NULL,
    allow_rs_remote_mgmt boolean DEFAULT false NOT NULL,
    policy_enforce_mode character varying(15) NOT NULL,
    decision_strategy smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.resource_server OWNER TO "kUser";

--
-- Name: resource_server_perm_ticket; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_server_perm_ticket (
    id character varying(36) NOT NULL,
    owner character varying(255) NOT NULL,
    requester character varying(255) NOT NULL,
    created_timestamp bigint NOT NULL,
    granted_timestamp bigint,
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36),
    resource_server_id character varying(36) NOT NULL,
    policy_id character varying(36)
);


ALTER TABLE public.resource_server_perm_ticket OWNER TO "kUser";

--
-- Name: resource_server_policy; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_server_policy (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    type character varying(255) NOT NULL,
    decision_strategy character varying(20),
    logic character varying(20),
    resource_server_id character varying(36) NOT NULL,
    owner character varying(255)
);


ALTER TABLE public.resource_server_policy OWNER TO "kUser";

--
-- Name: resource_server_resource; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_server_resource (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255),
    icon_uri character varying(255),
    owner character varying(255) NOT NULL,
    resource_server_id character varying(36) NOT NULL,
    owner_managed_access boolean DEFAULT false NOT NULL,
    display_name character varying(255)
);


ALTER TABLE public.resource_server_resource OWNER TO "kUser";

--
-- Name: resource_server_scope; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_server_scope (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    icon_uri character varying(255),
    resource_server_id character varying(36) NOT NULL,
    display_name character varying(255)
);


ALTER TABLE public.resource_server_scope OWNER TO "kUser";

--
-- Name: resource_uris; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_uris (
    resource_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.resource_uris OWNER TO "kUser";

--
-- Name: role_attribute; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.role_attribute (
    id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255)
);


ALTER TABLE public.role_attribute OWNER TO "kUser";

--
-- Name: scope_mapping; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.scope_mapping (
    client_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.scope_mapping OWNER TO "kUser";

--
-- Name: scope_policy; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.scope_policy (
    scope_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE public.scope_policy OWNER TO "kUser";

--
-- Name: user_attribute; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_attribute (
    name character varying(255) NOT NULL,
    value character varying(255),
    user_id character varying(36) NOT NULL,
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL
);


ALTER TABLE public.user_attribute OWNER TO "kUser";

--
-- Name: user_consent; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    user_id character varying(36) NOT NULL,
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE public.user_consent OWNER TO "kUser";

--
-- Name: user_consent_client_scope; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_consent_client_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.user_consent_client_scope OWNER TO "kUser";

--
-- Name: user_entity; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_entity (
    id character varying(36) NOT NULL,
    email character varying(255),
    email_constraint character varying(255),
    email_verified boolean DEFAULT false NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    federation_link character varying(255),
    first_name character varying(255),
    last_name character varying(255),
    realm_id character varying(255),
    username character varying(255),
    created_timestamp bigint,
    service_account_client_link character varying(255),
    not_before integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.user_entity OWNER TO "kUser";

--
-- Name: user_federation_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_federation_config (
    user_federation_provider_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_federation_config OWNER TO "kUser";

--
-- Name: user_federation_mapper; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_federation_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    federation_provider_id character varying(36) NOT NULL,
    federation_mapper_type character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.user_federation_mapper OWNER TO "kUser";

--
-- Name: user_federation_mapper_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_federation_mapper_config (
    user_federation_mapper_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_federation_mapper_config OWNER TO "kUser";

--
-- Name: user_federation_provider; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_federation_provider (
    id character varying(36) NOT NULL,
    changed_sync_period integer,
    display_name character varying(255),
    full_sync_period integer,
    last_sync integer,
    priority integer,
    provider_name character varying(255),
    realm_id character varying(36)
);


ALTER TABLE public.user_federation_provider OWNER TO "kUser";

--
-- Name: user_group_membership; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.user_group_membership OWNER TO "kUser";

--
-- Name: user_required_action; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_required_action (
    user_id character varying(36) NOT NULL,
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL
);


ALTER TABLE public.user_required_action OWNER TO "kUser";

--
-- Name: user_role_mapping; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_role_mapping (
    role_id character varying(255) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.user_role_mapping OWNER TO "kUser";

--
-- Name: user_session; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_session (
    id character varying(36) NOT NULL,
    auth_method character varying(255),
    ip_address character varying(255),
    last_session_refresh integer,
    login_username character varying(255),
    realm_id character varying(255),
    remember_me boolean DEFAULT false NOT NULL,
    started integer,
    user_id character varying(255),
    user_session_state integer,
    broker_session_id character varying(255),
    broker_user_id character varying(255)
);


ALTER TABLE public.user_session OWNER TO "kUser";

--
-- Name: user_session_note; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_session_note (
    user_session character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(2048)
);


ALTER TABLE public.user_session_note OWNER TO "kUser";

--
-- Name: username_login_failure; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.username_login_failure (
    realm_id character varying(36) NOT NULL,
    username character varying(255) NOT NULL,
    failed_login_not_before integer,
    last_failure bigint,
    last_ip_failure character varying(255),
    num_failures integer
);


ALTER TABLE public.username_login_failure OWNER TO "kUser";

--
-- Name: web_origins; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.web_origins (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.web_origins OWNER TO "kUser";

--
-- Data for Name: admin_event_entity; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.admin_event_entity (id, admin_event_time, realm_id, operation_type, auth_realm_id, auth_client_id, auth_user_id, ip_address, resource_path, representation, error, resource_type) FROM stdin;
\.


--
-- Data for Name: associated_policy; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.associated_policy (policy_id, associated_policy_id) FROM stdin;
483cfade-75f6-42f8-aa9b-f262a04952c7	c5509373-62f1-438d-b14d-2bfbd629dec0
\.


--
-- Data for Name: authentication_execution; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.authentication_execution (id, alias, authenticator, realm_id, flow_id, requirement, priority, authenticator_flow, auth_flow_id, auth_config) FROM stdin;
1fcc5c88-39f8-4866-9f36-4c8044d74587	\N	auth-cookie	master	458aca5a-6431-444b-8154-3a1ba163176b	2	10	f	\N	\N
44aa5d22-ca8f-4b97-a573-b72bace7cb45	\N	auth-spnego	master	458aca5a-6431-444b-8154-3a1ba163176b	3	20	f	\N	\N
c267d1d8-edad-4752-936a-ca0ce0255354	\N	identity-provider-redirector	master	458aca5a-6431-444b-8154-3a1ba163176b	2	25	f	\N	\N
7cb4a977-0edc-49ab-b0ca-e74c42e3e973	\N	\N	master	458aca5a-6431-444b-8154-3a1ba163176b	2	30	t	b7b62927-1be6-441a-8a40-2b8f513f5e03	\N
2ff25729-f32f-462f-bed9-6833a0122e35	\N	auth-username-password-form	master	b7b62927-1be6-441a-8a40-2b8f513f5e03	0	10	f	\N	\N
52a09878-f24a-4388-a19b-b46c83de9036	\N	\N	master	b7b62927-1be6-441a-8a40-2b8f513f5e03	1	20	t	d3db95c4-ee98-4f78-94ca-82cb8009802a	\N
06dcec1a-e7c8-421d-b6b3-7bbd767bb0ca	\N	conditional-user-configured	master	d3db95c4-ee98-4f78-94ca-82cb8009802a	0	10	f	\N	\N
763f6bd0-5422-4f50-85ed-499294b7b9a4	\N	auth-otp-form	master	d3db95c4-ee98-4f78-94ca-82cb8009802a	0	20	f	\N	\N
bcfa70ac-5748-49e9-8376-e2b07a9a476c	\N	direct-grant-validate-username	master	d0ba8d6e-e5c5-4131-937c-ca6dfd6501c2	0	10	f	\N	\N
38903ede-74ac-454f-8e4c-45cb3d4c6f90	\N	direct-grant-validate-password	master	d0ba8d6e-e5c5-4131-937c-ca6dfd6501c2	0	20	f	\N	\N
3e344659-498c-4a2b-8e23-6cb9a428f798	\N	\N	master	d0ba8d6e-e5c5-4131-937c-ca6dfd6501c2	1	30	t	a48b4c46-f7d3-41dc-a01e-d6389b6d01ba	\N
2c5f7c4f-4718-40f1-817a-6c19be36201c	\N	conditional-user-configured	master	a48b4c46-f7d3-41dc-a01e-d6389b6d01ba	0	10	f	\N	\N
4515e93f-e72a-4cd6-b9c8-409b13b7d2c5	\N	direct-grant-validate-otp	master	a48b4c46-f7d3-41dc-a01e-d6389b6d01ba	0	20	f	\N	\N
0ac024a8-f482-4de3-8137-ea49a3b41853	\N	registration-page-form	master	1336a465-dc8d-4fc9-a22b-6a51b81d1d1d	0	10	t	c452934f-cfbd-4c0f-8734-896cf09280b1	\N
35a1b60e-5244-493e-bd60-3924124da295	\N	registration-user-creation	master	c452934f-cfbd-4c0f-8734-896cf09280b1	0	20	f	\N	\N
fa0b4238-4f52-48d8-bd1a-7e5a9abfdc7d	\N	registration-profile-action	master	c452934f-cfbd-4c0f-8734-896cf09280b1	0	40	f	\N	\N
b4721bf5-c4ef-497e-8daa-b87ff431418e	\N	registration-password-action	master	c452934f-cfbd-4c0f-8734-896cf09280b1	0	50	f	\N	\N
23ea7f73-65be-4fcc-a8af-21e24717420c	\N	registration-recaptcha-action	master	c452934f-cfbd-4c0f-8734-896cf09280b1	3	60	f	\N	\N
261336ba-8603-46b8-b80a-441d6dde7317	\N	reset-credentials-choose-user	master	18abeaf9-9775-4c62-9195-2aba10a59dd8	0	10	f	\N	\N
424156f6-5441-468a-b482-04d8ea95cc65	\N	reset-credential-email	master	18abeaf9-9775-4c62-9195-2aba10a59dd8	0	20	f	\N	\N
e7595ea8-fd7e-4c46-bf51-46f50230debb	\N	reset-password	master	18abeaf9-9775-4c62-9195-2aba10a59dd8	0	30	f	\N	\N
67969e26-4006-43df-babf-053c36346216	\N	\N	master	18abeaf9-9775-4c62-9195-2aba10a59dd8	1	40	t	9d9ee932-2239-4f1d-a34f-17c358f4e0fe	\N
8d716a31-99b0-4beb-ae61-9b8317d320ce	\N	conditional-user-configured	master	9d9ee932-2239-4f1d-a34f-17c358f4e0fe	0	10	f	\N	\N
5dac2aa6-1bef-4335-ae2a-f377411943a2	\N	reset-otp	master	9d9ee932-2239-4f1d-a34f-17c358f4e0fe	0	20	f	\N	\N
d426d1f2-444b-4bfc-b141-411b84d8bdc4	\N	client-secret	master	4893e889-9bff-41a0-9509-e018fe690ffc	2	10	f	\N	\N
2bc6d4aa-6688-4415-878f-9e9ac5517dda	\N	client-jwt	master	4893e889-9bff-41a0-9509-e018fe690ffc	2	20	f	\N	\N
5bef1993-987d-4065-a499-6cb601e832d0	\N	client-secret-jwt	master	4893e889-9bff-41a0-9509-e018fe690ffc	2	30	f	\N	\N
ce681470-516a-41eb-b7d3-275bcd86764c	\N	client-x509	master	4893e889-9bff-41a0-9509-e018fe690ffc	2	40	f	\N	\N
18c0783c-716d-4912-8fa0-16b4a59671b2	\N	idp-review-profile	master	926208f4-bfaa-4beb-8ec2-8c9e5bd81b19	0	10	f	\N	a012d705-0d3a-42b9-9006-594c791c32e5
2d2304dc-3f44-47e9-968f-8e8678c1baed	\N	\N	master	926208f4-bfaa-4beb-8ec2-8c9e5bd81b19	0	20	t	7bb82e7f-52f8-4760-9eaa-ed0cdcabd4d2	\N
4f6a93ea-9e9a-49ad-858a-1471a4817bdd	\N	idp-create-user-if-unique	master	7bb82e7f-52f8-4760-9eaa-ed0cdcabd4d2	2	10	f	\N	bb960ea7-e970-4cd5-9ae3-508d2fa4806a
23cc67db-f822-48b4-8dba-b466407331f1	\N	\N	master	7bb82e7f-52f8-4760-9eaa-ed0cdcabd4d2	2	20	t	2c74ef2c-bcf8-4749-b40f-cf286162d01c	\N
f3f73293-8657-4318-89cb-552d8644c165	\N	idp-confirm-link	master	2c74ef2c-bcf8-4749-b40f-cf286162d01c	0	10	f	\N	\N
1f0471e8-cca0-4c10-9bc3-0fa98eba5f1d	\N	\N	master	2c74ef2c-bcf8-4749-b40f-cf286162d01c	0	20	t	49f424f9-3205-45d5-b26d-7f9e2533d166	\N
fb83b09c-1401-4b24-89b4-81cff4446023	\N	idp-email-verification	master	49f424f9-3205-45d5-b26d-7f9e2533d166	2	10	f	\N	\N
638bb10e-df9c-4156-94e4-ef4d9c9ad57d	\N	\N	master	49f424f9-3205-45d5-b26d-7f9e2533d166	2	20	t	90b49964-638d-4b6d-986a-f24d6ef05ace	\N
8d601213-7ac6-4758-aa3a-42ecdecc877c	\N	idp-username-password-form	master	90b49964-638d-4b6d-986a-f24d6ef05ace	0	10	f	\N	\N
6e2711f5-54dd-4798-991e-540bc07e1865	\N	\N	master	90b49964-638d-4b6d-986a-f24d6ef05ace	1	20	t	46c71ba4-5020-413b-bf4a-629dd6308f66	\N
a1fedd3f-7ac0-460c-a24b-9a2f7695b4fe	\N	conditional-user-configured	master	46c71ba4-5020-413b-bf4a-629dd6308f66	0	10	f	\N	\N
727c7d5d-d783-496f-844d-f0f67769fb88	\N	auth-otp-form	master	46c71ba4-5020-413b-bf4a-629dd6308f66	0	20	f	\N	\N
622f6abd-8977-4e26-bbd9-c653ee295288	\N	http-basic-authenticator	master	8a24245b-85fc-498e-833b-e94be35158d7	0	10	f	\N	\N
7da67a38-1432-4cd0-8e63-8ec3ef18f76b	\N	docker-http-basic-authenticator	master	9349cbfc-d917-4665-b9fa-30648e034f7b	0	10	f	\N	\N
2cfff20a-0079-463a-a469-6abe93265928	\N	no-cookie-redirect	master	3914df03-6bce-47cb-93ac-0d51d331089b	0	10	f	\N	\N
7e193518-b2b0-4d68-82ff-0b9b94dda0aa	\N	\N	master	3914df03-6bce-47cb-93ac-0d51d331089b	0	20	t	56337207-cabb-44fd-87ea-dfffa1dc771a	\N
7f13cc8d-338b-48db-bba3-eb56352ecfb9	\N	basic-auth	master	56337207-cabb-44fd-87ea-dfffa1dc771a	0	10	f	\N	\N
90b9fc90-9136-460a-a134-271f63657416	\N	basic-auth-otp	master	56337207-cabb-44fd-87ea-dfffa1dc771a	3	20	f	\N	\N
319328f7-e0f7-4905-a2e6-4a8cddb81361	\N	auth-spnego	master	56337207-cabb-44fd-87ea-dfffa1dc771a	3	30	f	\N	\N
cfa6d831-0afb-4223-b0e8-524cb691749f	\N	auth-cookie	civil-add	394d6d73-3f25-40e5-b7c5-b2c66c51e421	2	10	f	\N	\N
469275fa-154d-4f62-8a61-0a037b4cd279	\N	auth-spnego	civil-add	394d6d73-3f25-40e5-b7c5-b2c66c51e421	3	20	f	\N	\N
3bf18dc9-7352-4bd3-94bb-a0841e8675ad	\N	identity-provider-redirector	civil-add	394d6d73-3f25-40e5-b7c5-b2c66c51e421	2	25	f	\N	\N
8c6901d9-af65-4336-a41f-6c7579b9ad03	\N	\N	civil-add	394d6d73-3f25-40e5-b7c5-b2c66c51e421	2	30	t	9c831899-99cf-43c0-9da9-93a79dfcd9c9	\N
e551fa68-6b84-48ca-a1f2-14259f801ebf	\N	auth-username-password-form	civil-add	9c831899-99cf-43c0-9da9-93a79dfcd9c9	0	10	f	\N	\N
ea40a684-2736-4fac-8c78-1cf7d3c3ea8a	\N	\N	civil-add	9c831899-99cf-43c0-9da9-93a79dfcd9c9	1	20	t	472d4bfb-6f1b-4d1c-9695-be522f847691	\N
3668a41a-eed8-4dd3-9a3f-b000136476aa	\N	conditional-user-configured	civil-add	472d4bfb-6f1b-4d1c-9695-be522f847691	0	10	f	\N	\N
4ea785b3-fcdf-413a-9085-98540ff79d19	\N	auth-otp-form	civil-add	472d4bfb-6f1b-4d1c-9695-be522f847691	0	20	f	\N	\N
e0e08f68-237d-4e2e-b5aa-570fd42fda1d	\N	direct-grant-validate-username	civil-add	e144ee07-cc75-47f9-a2b5-e60a34f0dbfa	0	10	f	\N	\N
babf1dd6-2ea7-4e5d-8b31-80f59bb4caee	\N	direct-grant-validate-password	civil-add	e144ee07-cc75-47f9-a2b5-e60a34f0dbfa	0	20	f	\N	\N
a85ba148-2717-4f0e-9a11-46dba4d602aa	\N	\N	civil-add	e144ee07-cc75-47f9-a2b5-e60a34f0dbfa	1	30	t	1831c6fe-ca02-4f43-b4b9-eda2048213c9	\N
50c92f9d-594e-4892-8b3a-50edd11e1ee9	\N	conditional-user-configured	civil-add	1831c6fe-ca02-4f43-b4b9-eda2048213c9	0	10	f	\N	\N
00284b1e-2cf2-47b6-8b23-626c80bce74d	\N	direct-grant-validate-otp	civil-add	1831c6fe-ca02-4f43-b4b9-eda2048213c9	0	20	f	\N	\N
5038f783-ac24-414c-8192-b0967eba5997	\N	registration-page-form	civil-add	63fd6b7b-80d8-4268-a849-325d4f1f0e99	0	10	t	74558c9b-b2e3-4a41-91b1-b50af1b25a8f	\N
c1234659-7ba7-44fb-abb6-66fd98cde478	\N	registration-user-creation	civil-add	74558c9b-b2e3-4a41-91b1-b50af1b25a8f	0	20	f	\N	\N
cb97e9bf-ea3a-404c-b7fb-b43be3689c27	\N	registration-profile-action	civil-add	74558c9b-b2e3-4a41-91b1-b50af1b25a8f	0	40	f	\N	\N
4d7d4b6a-04fd-410e-acb3-362d222841d1	\N	registration-password-action	civil-add	74558c9b-b2e3-4a41-91b1-b50af1b25a8f	0	50	f	\N	\N
5253395a-f4a1-47d2-a916-ef178bca77ab	\N	registration-recaptcha-action	civil-add	74558c9b-b2e3-4a41-91b1-b50af1b25a8f	3	60	f	\N	\N
02fbb388-f04e-4c5a-88e3-d4fd7d0e237f	\N	reset-credentials-choose-user	civil-add	2b030fd4-db8d-4b8a-b320-5939e3e1725c	0	10	f	\N	\N
9dc00a04-88ee-48ac-81e8-57a5028b6bf9	\N	reset-credential-email	civil-add	2b030fd4-db8d-4b8a-b320-5939e3e1725c	0	20	f	\N	\N
2c385dd7-89cd-4064-a923-9afe69c00cbd	\N	reset-password	civil-add	2b030fd4-db8d-4b8a-b320-5939e3e1725c	0	30	f	\N	\N
213ab3b7-9394-45d8-a5fb-5db5eb32d173	\N	\N	civil-add	2b030fd4-db8d-4b8a-b320-5939e3e1725c	1	40	t	cf536d9b-31fe-4a45-9c88-5adeec22ef72	\N
af3cd368-eb6b-4707-b899-9b44759b2d6f	\N	conditional-user-configured	civil-add	cf536d9b-31fe-4a45-9c88-5adeec22ef72	0	10	f	\N	\N
e6e7beb0-d601-4c01-a99a-882bcf83d2c1	\N	reset-otp	civil-add	cf536d9b-31fe-4a45-9c88-5adeec22ef72	0	20	f	\N	\N
7b205636-98ca-4c66-99c0-765046abfcab	\N	client-secret	civil-add	7561b83b-9a60-45f2-8383-fd1af2f0be69	2	10	f	\N	\N
1473c5b1-5bd0-4589-ae30-7820260fdb9e	\N	client-jwt	civil-add	7561b83b-9a60-45f2-8383-fd1af2f0be69	2	20	f	\N	\N
9ffb4c2b-42f6-446b-ae89-41e2cc9b9dbb	\N	client-secret-jwt	civil-add	7561b83b-9a60-45f2-8383-fd1af2f0be69	2	30	f	\N	\N
710e43f7-3105-416b-af7d-42ebc802ef54	\N	client-x509	civil-add	7561b83b-9a60-45f2-8383-fd1af2f0be69	2	40	f	\N	\N
5b25d386-9f09-4589-b2d0-fbe2d9ef884d	\N	idp-review-profile	civil-add	517a2bdb-1db7-4808-9b99-ebfe67b7bac7	0	10	f	\N	79079aa2-4edc-4a7f-ac82-213b9bbab2fa
8295317b-03ba-43d6-b43c-50c24b74e084	\N	\N	civil-add	517a2bdb-1db7-4808-9b99-ebfe67b7bac7	0	20	t	2833900c-62ee-43d4-8c55-4254985b6f54	\N
ffc1b51d-047f-4eb9-a3ab-c94706f8cf15	\N	idp-create-user-if-unique	civil-add	2833900c-62ee-43d4-8c55-4254985b6f54	2	10	f	\N	fbd5b791-7977-4919-bd83-a7b100ce7c6f
a4de7086-2b58-42b3-bc9d-5dda1552c258	\N	\N	civil-add	2833900c-62ee-43d4-8c55-4254985b6f54	2	20	t	3f607735-48d9-4f18-a1a3-0d89077ac9a5	\N
5e4196a9-f3f8-4951-b029-7e99c356e42c	\N	idp-confirm-link	civil-add	3f607735-48d9-4f18-a1a3-0d89077ac9a5	0	10	f	\N	\N
b16d44b8-7801-42a2-95dc-fa5a1297d500	\N	\N	civil-add	3f607735-48d9-4f18-a1a3-0d89077ac9a5	0	20	t	566ae722-2b2e-4c8f-a754-03a5533fcfee	\N
881530b0-3bea-44bd-9d92-f6a257182919	\N	idp-email-verification	civil-add	566ae722-2b2e-4c8f-a754-03a5533fcfee	2	10	f	\N	\N
9459179c-91a3-4e00-b0fd-f70914aaeeb2	\N	\N	civil-add	566ae722-2b2e-4c8f-a754-03a5533fcfee	2	20	t	4448a7f5-0585-4b2d-b7fa-15ab66e21502	\N
493b6c0d-580f-4822-97e4-f079a0e77815	\N	idp-username-password-form	civil-add	4448a7f5-0585-4b2d-b7fa-15ab66e21502	0	10	f	\N	\N
0100d20b-d194-4f57-babd-5bb17d542dad	\N	\N	civil-add	4448a7f5-0585-4b2d-b7fa-15ab66e21502	1	20	t	e88e41fe-3ef7-4541-a769-ec219721bfcf	\N
14cbeb86-3e68-4881-993f-58b702dcb8cd	\N	conditional-user-configured	civil-add	e88e41fe-3ef7-4541-a769-ec219721bfcf	0	10	f	\N	\N
b8266e95-3e33-4b75-bf28-4e2d7523ce14	\N	auth-otp-form	civil-add	e88e41fe-3ef7-4541-a769-ec219721bfcf	0	20	f	\N	\N
a16644bc-492b-420e-93bc-19aac23f5bb8	\N	http-basic-authenticator	civil-add	627d51f3-54df-4e18-bf79-27fedc1cc36e	0	10	f	\N	\N
2b858356-f082-4d85-a5f1-aa4ff6c19f44	\N	docker-http-basic-authenticator	civil-add	d1bcce02-7635-4067-a40c-6f75fb03c4a7	0	10	f	\N	\N
bdc8bb90-13a2-4281-b393-fda3b94d5a69	\N	no-cookie-redirect	civil-add	a41bcdba-9cb9-4ad3-9f46-f240dc41c29f	0	10	f	\N	\N
e86c5bc6-ae68-4f49-8935-f14e9dd2eb27	\N	\N	civil-add	a41bcdba-9cb9-4ad3-9f46-f240dc41c29f	0	20	t	17a737a8-de80-4778-a01c-9d1a8e0d28cf	\N
e4dd8d94-e069-4a94-91f0-83bac29afb14	\N	basic-auth	civil-add	17a737a8-de80-4778-a01c-9d1a8e0d28cf	0	10	f	\N	\N
227ecc6b-3930-4012-9018-5c4d77d6fdeb	\N	basic-auth-otp	civil-add	17a737a8-de80-4778-a01c-9d1a8e0d28cf	3	20	f	\N	\N
21a52816-e922-4d23-8eab-d9e978bc400b	\N	auth-spnego	civil-add	17a737a8-de80-4778-a01c-9d1a8e0d28cf	3	30	f	\N	\N
\.


--
-- Data for Name: authentication_flow; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.authentication_flow (id, alias, description, realm_id, provider_id, top_level, built_in) FROM stdin;
458aca5a-6431-444b-8154-3a1ba163176b	browser	browser based authentication	master	basic-flow	t	t
b7b62927-1be6-441a-8a40-2b8f513f5e03	forms	Username, password, otp and other auth forms.	master	basic-flow	f	t
d3db95c4-ee98-4f78-94ca-82cb8009802a	Browser - Conditional OTP	Flow to determine if the OTP is required for the authentication	master	basic-flow	f	t
d0ba8d6e-e5c5-4131-937c-ca6dfd6501c2	direct grant	OpenID Connect Resource Owner Grant	master	basic-flow	t	t
a48b4c46-f7d3-41dc-a01e-d6389b6d01ba	Direct Grant - Conditional OTP	Flow to determine if the OTP is required for the authentication	master	basic-flow	f	t
1336a465-dc8d-4fc9-a22b-6a51b81d1d1d	registration	registration flow	master	basic-flow	t	t
c452934f-cfbd-4c0f-8734-896cf09280b1	registration form	registration form	master	form-flow	f	t
18abeaf9-9775-4c62-9195-2aba10a59dd8	reset credentials	Reset credentials for a user if they forgot their password or something	master	basic-flow	t	t
9d9ee932-2239-4f1d-a34f-17c358f4e0fe	Reset - Conditional OTP	Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.	master	basic-flow	f	t
4893e889-9bff-41a0-9509-e018fe690ffc	clients	Base authentication for clients	master	client-flow	t	t
926208f4-bfaa-4beb-8ec2-8c9e5bd81b19	first broker login	Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account	master	basic-flow	t	t
7bb82e7f-52f8-4760-9eaa-ed0cdcabd4d2	User creation or linking	Flow for the existing/non-existing user alternatives	master	basic-flow	f	t
2c74ef2c-bcf8-4749-b40f-cf286162d01c	Handle Existing Account	Handle what to do if there is existing account with same email/username like authenticated identity provider	master	basic-flow	f	t
49f424f9-3205-45d5-b26d-7f9e2533d166	Account verification options	Method with which to verity the existing account	master	basic-flow	f	t
90b49964-638d-4b6d-986a-f24d6ef05ace	Verify Existing Account by Re-authentication	Reauthentication of existing account	master	basic-flow	f	t
46c71ba4-5020-413b-bf4a-629dd6308f66	First broker login - Conditional OTP	Flow to determine if the OTP is required for the authentication	master	basic-flow	f	t
8a24245b-85fc-498e-833b-e94be35158d7	saml ecp	SAML ECP Profile Authentication Flow	master	basic-flow	t	t
9349cbfc-d917-4665-b9fa-30648e034f7b	docker auth	Used by Docker clients to authenticate against the IDP	master	basic-flow	t	t
3914df03-6bce-47cb-93ac-0d51d331089b	http challenge	An authentication flow based on challenge-response HTTP Authentication Schemes	master	basic-flow	t	t
56337207-cabb-44fd-87ea-dfffa1dc771a	Authentication Options	Authentication options.	master	basic-flow	f	t
394d6d73-3f25-40e5-b7c5-b2c66c51e421	browser	browser based authentication	civil-add	basic-flow	t	t
9c831899-99cf-43c0-9da9-93a79dfcd9c9	forms	Username, password, otp and other auth forms.	civil-add	basic-flow	f	t
472d4bfb-6f1b-4d1c-9695-be522f847691	Browser - Conditional OTP	Flow to determine if the OTP is required for the authentication	civil-add	basic-flow	f	t
e144ee07-cc75-47f9-a2b5-e60a34f0dbfa	direct grant	OpenID Connect Resource Owner Grant	civil-add	basic-flow	t	t
1831c6fe-ca02-4f43-b4b9-eda2048213c9	Direct Grant - Conditional OTP	Flow to determine if the OTP is required for the authentication	civil-add	basic-flow	f	t
63fd6b7b-80d8-4268-a849-325d4f1f0e99	registration	registration flow	civil-add	basic-flow	t	t
74558c9b-b2e3-4a41-91b1-b50af1b25a8f	registration form	registration form	civil-add	form-flow	f	t
2b030fd4-db8d-4b8a-b320-5939e3e1725c	reset credentials	Reset credentials for a user if they forgot their password or something	civil-add	basic-flow	t	t
cf536d9b-31fe-4a45-9c88-5adeec22ef72	Reset - Conditional OTP	Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.	civil-add	basic-flow	f	t
7561b83b-9a60-45f2-8383-fd1af2f0be69	clients	Base authentication for clients	civil-add	client-flow	t	t
517a2bdb-1db7-4808-9b99-ebfe67b7bac7	first broker login	Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account	civil-add	basic-flow	t	t
2833900c-62ee-43d4-8c55-4254985b6f54	User creation or linking	Flow for the existing/non-existing user alternatives	civil-add	basic-flow	f	t
3f607735-48d9-4f18-a1a3-0d89077ac9a5	Handle Existing Account	Handle what to do if there is existing account with same email/username like authenticated identity provider	civil-add	basic-flow	f	t
566ae722-2b2e-4c8f-a754-03a5533fcfee	Account verification options	Method with which to verity the existing account	civil-add	basic-flow	f	t
4448a7f5-0585-4b2d-b7fa-15ab66e21502	Verify Existing Account by Re-authentication	Reauthentication of existing account	civil-add	basic-flow	f	t
e88e41fe-3ef7-4541-a769-ec219721bfcf	First broker login - Conditional OTP	Flow to determine if the OTP is required for the authentication	civil-add	basic-flow	f	t
627d51f3-54df-4e18-bf79-27fedc1cc36e	saml ecp	SAML ECP Profile Authentication Flow	civil-add	basic-flow	t	t
d1bcce02-7635-4067-a40c-6f75fb03c4a7	docker auth	Used by Docker clients to authenticate against the IDP	civil-add	basic-flow	t	t
a41bcdba-9cb9-4ad3-9f46-f240dc41c29f	http challenge	An authentication flow based on challenge-response HTTP Authentication Schemes	civil-add	basic-flow	t	t
17a737a8-de80-4778-a01c-9d1a8e0d28cf	Authentication Options	Authentication options.	civil-add	basic-flow	f	t
\.


--
-- Data for Name: authenticator_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.authenticator_config (id, alias, realm_id) FROM stdin;
a012d705-0d3a-42b9-9006-594c791c32e5	review profile config	master
bb960ea7-e970-4cd5-9ae3-508d2fa4806a	create unique user config	master
79079aa2-4edc-4a7f-ac82-213b9bbab2fa	review profile config	civil-add
fbd5b791-7977-4919-bd83-a7b100ce7c6f	create unique user config	civil-add
\.


--
-- Data for Name: authenticator_config_entry; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.authenticator_config_entry (authenticator_id, value, name) FROM stdin;
a012d705-0d3a-42b9-9006-594c791c32e5	missing	update.profile.on.first.login
bb960ea7-e970-4cd5-9ae3-508d2fa4806a	false	require.password.update.after.registration
79079aa2-4edc-4a7f-ac82-213b9bbab2fa	missing	update.profile.on.first.login
fbd5b791-7977-4919-bd83-a7b100ce7c6f	false	require.password.update.after.registration
\.


--
-- Data for Name: broker_link; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.broker_link (identity_provider, storage_provider_id, realm_id, broker_user_id, broker_username, token, user_id) FROM stdin;
\.


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client (id, enabled, full_scope_allowed, client_id, not_before, public_client, secret, base_url, bearer_only, management_url, surrogate_auth_required, realm_id, protocol, node_rereg_timeout, frontchannel_logout, consent_required, name, service_accounts_enabled, client_authenticator_type, root_url, description, registration_token, standard_flow_enabled, implicit_flow_enabled, direct_access_grants_enabled, always_display_in_console) FROM stdin;
b268de25-483d-4179-a250-b7597b3ddbe1	t	t	master-realm	0	f	1166b54b-b19f-4be7-a42e-6ca53123775b	\N	t	\N	f	master	\N	0	f	f	master Realm	f	client-secret	\N	\N	\N	t	f	f	f
3ea086ad-f833-40cc-8aff-3612b7138d17	t	f	account	0	f	0c1795ef-72dc-4bc1-a826-7cb95dbfd9fa	/realms/master/account/	f	\N	f	master	openid-connect	0	f	f	${client_account}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
249c9f55-d8be-43d5-8e3d-7c744ded79f1	t	f	account-console	0	t	68d0f347-e9e9-437b-b932-62710509d60e	/realms/master/account/	f	\N	f	master	openid-connect	0	f	f	${client_account-console}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
4fa01505-dac2-40df-a136-31ebc1b58f4b	t	f	broker	0	f	138f3105-0e65-4f13-bb85-a49ba3438887	\N	f	\N	f	master	openid-connect	0	f	f	${client_broker}	f	client-secret	\N	\N	\N	t	f	f	f
b03c1fb3-9455-4b87-a0eb-c25e1be32912	t	f	security-admin-console	0	t	68d13bfa-47fe-4d2d-8dd8-d1a59836462e	/admin/master/console/	f	\N	f	master	openid-connect	0	f	f	${client_security-admin-console}	f	client-secret	${authAdminUrl}	\N	\N	t	f	f	f
d4d85a4c-f93b-4c86-82cc-0e9bec076997	t	f	admin-cli	0	t	2a027d9d-a155-4d8f-b713-6e8926aa24c7	\N	f	\N	f	master	openid-connect	0	f	f	${client_admin-cli}	f	client-secret	\N	\N	\N	f	f	t	f
b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	t	civil-add-realm	0	f	ef4e88a1-a010-4dbe-b8b3-0469ce1491ca	\N	t	\N	f	master	\N	0	f	f	civil-add Realm	f	client-secret	\N	\N	\N	t	f	f	f
a04e902f-9e19-4c75-9014-608c2de644e4	t	f	realm-management	0	f	db4d7d66-2bce-4ca9-98c9-a9e4d70aecf8	\N	t	\N	f	civil-add	openid-connect	0	f	f	${client_realm-management}	f	client-secret	\N	\N	\N	t	f	f	f
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	t	f	account	0	f	7a6717a9-dad6-494c-ae3c-a52957831e0f	/realms/civil-add/account/	f	\N	f	civil-add	openid-connect	0	f	f	${client_account}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	t	f	account-console	0	t	4405a722-c40d-45f0-a284-9a31b16795d0	/realms/civil-add/account/	f	\N	f	civil-add	openid-connect	0	f	f	${client_account-console}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
243fbfa0-43bd-4c06-80e5-ce0619e3ca7c	t	f	broker	0	f	6949aee7-a5d3-4f28-87ba-f73d65b5d58d	\N	f	\N	f	civil-add	openid-connect	0	f	f	${client_broker}	f	client-secret	\N	\N	\N	t	f	f	f
a9b463b8-18bb-49f5-a3bd-9e399734c104	t	f	security-admin-console	0	t	71a6aa42-b0c9-4e1b-8df0-2ebaca80c829	/admin/civil-add/console/	f	\N	f	civil-add	openid-connect	0	f	f	${client_security-admin-console}	f	client-secret	${authAdminUrl}	\N	\N	t	f	f	f
530817f0-b05b-4b8c-99ef-494a7fa3b4c0	t	f	admin-cli	0	t	a2f9a0e3-4f10-4be0-8700-8de2a3008e2e	\N	f	\N	f	civil-add	openid-connect	0	f	f	${client_admin-cli}	f	client-secret	\N	\N	\N	f	f	t	f
1d826890-822d-4456-8d3b-6fdbe01475ba	t	t	civil-add_back	0	f	d5fd9f3f-13b4-4a92-9f31-9ff0a3dd7ac1	\N	f	\N	f	civil-add	openid-connect	-1	f	f	\N	t	client-secret	\N	\N	\N	t	f	t	f
\.


--
-- Data for Name: client_attributes; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_attributes (client_id, value, name) FROM stdin;
249c9f55-d8be-43d5-8e3d-7c744ded79f1	S256	pkce.code.challenge.method
b03c1fb3-9455-4b87-a0eb-c25e1be32912	S256	pkce.code.challenge.method
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	S256	pkce.code.challenge.method
a9b463b8-18bb-49f5-a3bd-9e399734c104	S256	pkce.code.challenge.method
1d826890-822d-4456-8d3b-6fdbe01475ba	false	saml.server.signature
1d826890-822d-4456-8d3b-6fdbe01475ba	false	saml.server.signature.keyinfo.ext
1d826890-822d-4456-8d3b-6fdbe01475ba	false	saml.assertion.signature
1d826890-822d-4456-8d3b-6fdbe01475ba	false	saml.client.signature
1d826890-822d-4456-8d3b-6fdbe01475ba	false	saml.encrypt
1d826890-822d-4456-8d3b-6fdbe01475ba	false	saml.authnstatement
1d826890-822d-4456-8d3b-6fdbe01475ba	false	saml.onetimeuse.condition
1d826890-822d-4456-8d3b-6fdbe01475ba	false	saml_force_name_id_format
1d826890-822d-4456-8d3b-6fdbe01475ba	false	saml.multivalued.roles
1d826890-822d-4456-8d3b-6fdbe01475ba	false	saml.force.post.binding
1d826890-822d-4456-8d3b-6fdbe01475ba	false	exclude.session.state.from.auth.response
1d826890-822d-4456-8d3b-6fdbe01475ba	false	tls.client.certificate.bound.access.tokens
1d826890-822d-4456-8d3b-6fdbe01475ba	false	display.on.consent.screen
\.


--
-- Data for Name: client_auth_flow_bindings; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_auth_flow_bindings (client_id, flow_id, binding_name) FROM stdin;
\.


--
-- Data for Name: client_default_roles; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_default_roles (client_id, role_id) FROM stdin;
3ea086ad-f833-40cc-8aff-3612b7138d17	59641392-3138-44db-b74f-e4876f3e0def
3ea086ad-f833-40cc-8aff-3612b7138d17	1635476e-d04b-41db-8429-6c164d2b0255
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	c5c2aee8-1c25-45b3-8535-49bf502325f0
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	093235eb-d1b2-4732-bdaa-8db103645f63
\.


--
-- Data for Name: client_initial_access; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_initial_access (id, realm_id, "timestamp", expiration, count, remaining_count) FROM stdin;
\.


--
-- Data for Name: client_node_registrations; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_node_registrations (client_id, value, name) FROM stdin;
\.


--
-- Data for Name: client_scope; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_scope (id, name, realm_id, description, protocol) FROM stdin;
c29b50b9-5f8f-4115-abc8-6019fd964792	offline_access	master	OpenID Connect built-in scope: offline_access	openid-connect
37aaf91a-24ef-4fe6-a2ea-95dfcad81dd5	role_list	master	SAML role list	saml
ba301432-ee9e-4b1b-b737-d10ef9c47df4	profile	master	OpenID Connect built-in scope: profile	openid-connect
6f69599a-8291-4901-b425-021ec1e2c2fd	email	master	OpenID Connect built-in scope: email	openid-connect
65152274-9681-4374-89b0-d76bdcb64a77	address	master	OpenID Connect built-in scope: address	openid-connect
ba52b31e-dae3-4fd0-9320-a695e933234e	phone	master	OpenID Connect built-in scope: phone	openid-connect
1493365a-d059-4c99-882a-08f95c1d65d3	roles	master	OpenID Connect scope for add user roles to the access token	openid-connect
b78dd573-d751-49f3-9fef-6687c7c96184	web-origins	master	OpenID Connect scope for add allowed web origins to the access token	openid-connect
1b99148c-91d4-41cf-a653-35f6272b5038	microprofile-jwt	master	Microprofile - JWT built-in scope	openid-connect
e6f99d68-36ce-42bc-ba8c-7d359d4ed001	offline_access	civil-add	OpenID Connect built-in scope: offline_access	openid-connect
d133efd5-8929-4e2e-814a-f1a40ab8886f	role_list	civil-add	SAML role list	saml
dd1036f7-c11d-49da-829b-5c6337b19452	profile	civil-add	OpenID Connect built-in scope: profile	openid-connect
bc79864e-90bd-487f-b474-73a864e85aaf	email	civil-add	OpenID Connect built-in scope: email	openid-connect
37cd1341-8446-43d0-ba1e-7b1b1b310bd7	address	civil-add	OpenID Connect built-in scope: address	openid-connect
b2e26561-f132-4e2d-b4f9-1d860548791a	phone	civil-add	OpenID Connect built-in scope: phone	openid-connect
eec61978-bb6d-4959-8df6-bbadf6d1bee7	roles	civil-add	OpenID Connect scope for add user roles to the access token	openid-connect
e83e4acd-b6d4-4dbf-9e14-579fb796de1b	web-origins	civil-add	OpenID Connect scope for add allowed web origins to the access token	openid-connect
e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b	microprofile-jwt	civil-add	Microprofile - JWT built-in scope	openid-connect
\.


--
-- Data for Name: client_scope_attributes; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_scope_attributes (scope_id, value, name) FROM stdin;
c29b50b9-5f8f-4115-abc8-6019fd964792	true	display.on.consent.screen
c29b50b9-5f8f-4115-abc8-6019fd964792	${offlineAccessScopeConsentText}	consent.screen.text
37aaf91a-24ef-4fe6-a2ea-95dfcad81dd5	true	display.on.consent.screen
37aaf91a-24ef-4fe6-a2ea-95dfcad81dd5	${samlRoleListScopeConsentText}	consent.screen.text
ba301432-ee9e-4b1b-b737-d10ef9c47df4	true	display.on.consent.screen
ba301432-ee9e-4b1b-b737-d10ef9c47df4	${profileScopeConsentText}	consent.screen.text
ba301432-ee9e-4b1b-b737-d10ef9c47df4	true	include.in.token.scope
6f69599a-8291-4901-b425-021ec1e2c2fd	true	display.on.consent.screen
6f69599a-8291-4901-b425-021ec1e2c2fd	${emailScopeConsentText}	consent.screen.text
6f69599a-8291-4901-b425-021ec1e2c2fd	true	include.in.token.scope
65152274-9681-4374-89b0-d76bdcb64a77	true	display.on.consent.screen
65152274-9681-4374-89b0-d76bdcb64a77	${addressScopeConsentText}	consent.screen.text
65152274-9681-4374-89b0-d76bdcb64a77	true	include.in.token.scope
ba52b31e-dae3-4fd0-9320-a695e933234e	true	display.on.consent.screen
ba52b31e-dae3-4fd0-9320-a695e933234e	${phoneScopeConsentText}	consent.screen.text
ba52b31e-dae3-4fd0-9320-a695e933234e	true	include.in.token.scope
1493365a-d059-4c99-882a-08f95c1d65d3	true	display.on.consent.screen
1493365a-d059-4c99-882a-08f95c1d65d3	${rolesScopeConsentText}	consent.screen.text
1493365a-d059-4c99-882a-08f95c1d65d3	false	include.in.token.scope
b78dd573-d751-49f3-9fef-6687c7c96184	false	display.on.consent.screen
b78dd573-d751-49f3-9fef-6687c7c96184		consent.screen.text
b78dd573-d751-49f3-9fef-6687c7c96184	false	include.in.token.scope
1b99148c-91d4-41cf-a653-35f6272b5038	false	display.on.consent.screen
1b99148c-91d4-41cf-a653-35f6272b5038	true	include.in.token.scope
e6f99d68-36ce-42bc-ba8c-7d359d4ed001	true	display.on.consent.screen
e6f99d68-36ce-42bc-ba8c-7d359d4ed001	${offlineAccessScopeConsentText}	consent.screen.text
d133efd5-8929-4e2e-814a-f1a40ab8886f	true	display.on.consent.screen
d133efd5-8929-4e2e-814a-f1a40ab8886f	${samlRoleListScopeConsentText}	consent.screen.text
dd1036f7-c11d-49da-829b-5c6337b19452	true	display.on.consent.screen
dd1036f7-c11d-49da-829b-5c6337b19452	${profileScopeConsentText}	consent.screen.text
dd1036f7-c11d-49da-829b-5c6337b19452	true	include.in.token.scope
bc79864e-90bd-487f-b474-73a864e85aaf	true	display.on.consent.screen
bc79864e-90bd-487f-b474-73a864e85aaf	${emailScopeConsentText}	consent.screen.text
bc79864e-90bd-487f-b474-73a864e85aaf	true	include.in.token.scope
37cd1341-8446-43d0-ba1e-7b1b1b310bd7	true	display.on.consent.screen
37cd1341-8446-43d0-ba1e-7b1b1b310bd7	${addressScopeConsentText}	consent.screen.text
37cd1341-8446-43d0-ba1e-7b1b1b310bd7	true	include.in.token.scope
b2e26561-f132-4e2d-b4f9-1d860548791a	true	display.on.consent.screen
b2e26561-f132-4e2d-b4f9-1d860548791a	${phoneScopeConsentText}	consent.screen.text
b2e26561-f132-4e2d-b4f9-1d860548791a	true	include.in.token.scope
eec61978-bb6d-4959-8df6-bbadf6d1bee7	true	display.on.consent.screen
eec61978-bb6d-4959-8df6-bbadf6d1bee7	${rolesScopeConsentText}	consent.screen.text
eec61978-bb6d-4959-8df6-bbadf6d1bee7	false	include.in.token.scope
e83e4acd-b6d4-4dbf-9e14-579fb796de1b	false	display.on.consent.screen
e83e4acd-b6d4-4dbf-9e14-579fb796de1b		consent.screen.text
e83e4acd-b6d4-4dbf-9e14-579fb796de1b	false	include.in.token.scope
e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b	false	display.on.consent.screen
e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b	true	include.in.token.scope
\.


--
-- Data for Name: client_scope_client; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_scope_client (client_id, scope_id, default_scope) FROM stdin;
3ea086ad-f833-40cc-8aff-3612b7138d17	37aaf91a-24ef-4fe6-a2ea-95dfcad81dd5	t
249c9f55-d8be-43d5-8e3d-7c744ded79f1	37aaf91a-24ef-4fe6-a2ea-95dfcad81dd5	t
d4d85a4c-f93b-4c86-82cc-0e9bec076997	37aaf91a-24ef-4fe6-a2ea-95dfcad81dd5	t
4fa01505-dac2-40df-a136-31ebc1b58f4b	37aaf91a-24ef-4fe6-a2ea-95dfcad81dd5	t
b268de25-483d-4179-a250-b7597b3ddbe1	37aaf91a-24ef-4fe6-a2ea-95dfcad81dd5	t
b03c1fb3-9455-4b87-a0eb-c25e1be32912	37aaf91a-24ef-4fe6-a2ea-95dfcad81dd5	t
3ea086ad-f833-40cc-8aff-3612b7138d17	ba301432-ee9e-4b1b-b737-d10ef9c47df4	t
3ea086ad-f833-40cc-8aff-3612b7138d17	6f69599a-8291-4901-b425-021ec1e2c2fd	t
3ea086ad-f833-40cc-8aff-3612b7138d17	1493365a-d059-4c99-882a-08f95c1d65d3	t
3ea086ad-f833-40cc-8aff-3612b7138d17	b78dd573-d751-49f3-9fef-6687c7c96184	t
3ea086ad-f833-40cc-8aff-3612b7138d17	c29b50b9-5f8f-4115-abc8-6019fd964792	f
3ea086ad-f833-40cc-8aff-3612b7138d17	65152274-9681-4374-89b0-d76bdcb64a77	f
3ea086ad-f833-40cc-8aff-3612b7138d17	ba52b31e-dae3-4fd0-9320-a695e933234e	f
3ea086ad-f833-40cc-8aff-3612b7138d17	1b99148c-91d4-41cf-a653-35f6272b5038	f
249c9f55-d8be-43d5-8e3d-7c744ded79f1	ba301432-ee9e-4b1b-b737-d10ef9c47df4	t
249c9f55-d8be-43d5-8e3d-7c744ded79f1	6f69599a-8291-4901-b425-021ec1e2c2fd	t
249c9f55-d8be-43d5-8e3d-7c744ded79f1	1493365a-d059-4c99-882a-08f95c1d65d3	t
249c9f55-d8be-43d5-8e3d-7c744ded79f1	b78dd573-d751-49f3-9fef-6687c7c96184	t
249c9f55-d8be-43d5-8e3d-7c744ded79f1	c29b50b9-5f8f-4115-abc8-6019fd964792	f
249c9f55-d8be-43d5-8e3d-7c744ded79f1	65152274-9681-4374-89b0-d76bdcb64a77	f
249c9f55-d8be-43d5-8e3d-7c744ded79f1	ba52b31e-dae3-4fd0-9320-a695e933234e	f
249c9f55-d8be-43d5-8e3d-7c744ded79f1	1b99148c-91d4-41cf-a653-35f6272b5038	f
d4d85a4c-f93b-4c86-82cc-0e9bec076997	ba301432-ee9e-4b1b-b737-d10ef9c47df4	t
d4d85a4c-f93b-4c86-82cc-0e9bec076997	6f69599a-8291-4901-b425-021ec1e2c2fd	t
d4d85a4c-f93b-4c86-82cc-0e9bec076997	1493365a-d059-4c99-882a-08f95c1d65d3	t
d4d85a4c-f93b-4c86-82cc-0e9bec076997	b78dd573-d751-49f3-9fef-6687c7c96184	t
d4d85a4c-f93b-4c86-82cc-0e9bec076997	c29b50b9-5f8f-4115-abc8-6019fd964792	f
d4d85a4c-f93b-4c86-82cc-0e9bec076997	65152274-9681-4374-89b0-d76bdcb64a77	f
d4d85a4c-f93b-4c86-82cc-0e9bec076997	ba52b31e-dae3-4fd0-9320-a695e933234e	f
d4d85a4c-f93b-4c86-82cc-0e9bec076997	1b99148c-91d4-41cf-a653-35f6272b5038	f
4fa01505-dac2-40df-a136-31ebc1b58f4b	ba301432-ee9e-4b1b-b737-d10ef9c47df4	t
4fa01505-dac2-40df-a136-31ebc1b58f4b	6f69599a-8291-4901-b425-021ec1e2c2fd	t
4fa01505-dac2-40df-a136-31ebc1b58f4b	1493365a-d059-4c99-882a-08f95c1d65d3	t
4fa01505-dac2-40df-a136-31ebc1b58f4b	b78dd573-d751-49f3-9fef-6687c7c96184	t
4fa01505-dac2-40df-a136-31ebc1b58f4b	c29b50b9-5f8f-4115-abc8-6019fd964792	f
4fa01505-dac2-40df-a136-31ebc1b58f4b	65152274-9681-4374-89b0-d76bdcb64a77	f
4fa01505-dac2-40df-a136-31ebc1b58f4b	ba52b31e-dae3-4fd0-9320-a695e933234e	f
4fa01505-dac2-40df-a136-31ebc1b58f4b	1b99148c-91d4-41cf-a653-35f6272b5038	f
b268de25-483d-4179-a250-b7597b3ddbe1	ba301432-ee9e-4b1b-b737-d10ef9c47df4	t
b268de25-483d-4179-a250-b7597b3ddbe1	6f69599a-8291-4901-b425-021ec1e2c2fd	t
b268de25-483d-4179-a250-b7597b3ddbe1	1493365a-d059-4c99-882a-08f95c1d65d3	t
b268de25-483d-4179-a250-b7597b3ddbe1	b78dd573-d751-49f3-9fef-6687c7c96184	t
b268de25-483d-4179-a250-b7597b3ddbe1	c29b50b9-5f8f-4115-abc8-6019fd964792	f
b268de25-483d-4179-a250-b7597b3ddbe1	65152274-9681-4374-89b0-d76bdcb64a77	f
b268de25-483d-4179-a250-b7597b3ddbe1	ba52b31e-dae3-4fd0-9320-a695e933234e	f
b268de25-483d-4179-a250-b7597b3ddbe1	1b99148c-91d4-41cf-a653-35f6272b5038	f
b03c1fb3-9455-4b87-a0eb-c25e1be32912	ba301432-ee9e-4b1b-b737-d10ef9c47df4	t
b03c1fb3-9455-4b87-a0eb-c25e1be32912	6f69599a-8291-4901-b425-021ec1e2c2fd	t
b03c1fb3-9455-4b87-a0eb-c25e1be32912	1493365a-d059-4c99-882a-08f95c1d65d3	t
b03c1fb3-9455-4b87-a0eb-c25e1be32912	b78dd573-d751-49f3-9fef-6687c7c96184	t
b03c1fb3-9455-4b87-a0eb-c25e1be32912	c29b50b9-5f8f-4115-abc8-6019fd964792	f
b03c1fb3-9455-4b87-a0eb-c25e1be32912	65152274-9681-4374-89b0-d76bdcb64a77	f
b03c1fb3-9455-4b87-a0eb-c25e1be32912	ba52b31e-dae3-4fd0-9320-a695e933234e	f
b03c1fb3-9455-4b87-a0eb-c25e1be32912	1b99148c-91d4-41cf-a653-35f6272b5038	f
b5b9bb1d-8598-4e60-b13b-39da766bda6b	37aaf91a-24ef-4fe6-a2ea-95dfcad81dd5	t
b5b9bb1d-8598-4e60-b13b-39da766bda6b	ba301432-ee9e-4b1b-b737-d10ef9c47df4	t
b5b9bb1d-8598-4e60-b13b-39da766bda6b	6f69599a-8291-4901-b425-021ec1e2c2fd	t
b5b9bb1d-8598-4e60-b13b-39da766bda6b	1493365a-d059-4c99-882a-08f95c1d65d3	t
b5b9bb1d-8598-4e60-b13b-39da766bda6b	b78dd573-d751-49f3-9fef-6687c7c96184	t
b5b9bb1d-8598-4e60-b13b-39da766bda6b	c29b50b9-5f8f-4115-abc8-6019fd964792	f
b5b9bb1d-8598-4e60-b13b-39da766bda6b	65152274-9681-4374-89b0-d76bdcb64a77	f
b5b9bb1d-8598-4e60-b13b-39da766bda6b	ba52b31e-dae3-4fd0-9320-a695e933234e	f
b5b9bb1d-8598-4e60-b13b-39da766bda6b	1b99148c-91d4-41cf-a653-35f6272b5038	f
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	d133efd5-8929-4e2e-814a-f1a40ab8886f	t
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	d133efd5-8929-4e2e-814a-f1a40ab8886f	t
530817f0-b05b-4b8c-99ef-494a7fa3b4c0	d133efd5-8929-4e2e-814a-f1a40ab8886f	t
243fbfa0-43bd-4c06-80e5-ce0619e3ca7c	d133efd5-8929-4e2e-814a-f1a40ab8886f	t
a04e902f-9e19-4c75-9014-608c2de644e4	d133efd5-8929-4e2e-814a-f1a40ab8886f	t
a9b463b8-18bb-49f5-a3bd-9e399734c104	d133efd5-8929-4e2e-814a-f1a40ab8886f	t
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	dd1036f7-c11d-49da-829b-5c6337b19452	t
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	bc79864e-90bd-487f-b474-73a864e85aaf	t
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	eec61978-bb6d-4959-8df6-bbadf6d1bee7	t
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	e83e4acd-b6d4-4dbf-9e14-579fb796de1b	t
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	e6f99d68-36ce-42bc-ba8c-7d359d4ed001	f
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	37cd1341-8446-43d0-ba1e-7b1b1b310bd7	f
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	b2e26561-f132-4e2d-b4f9-1d860548791a	f
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b	f
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	dd1036f7-c11d-49da-829b-5c6337b19452	t
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	bc79864e-90bd-487f-b474-73a864e85aaf	t
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	eec61978-bb6d-4959-8df6-bbadf6d1bee7	t
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	e83e4acd-b6d4-4dbf-9e14-579fb796de1b	t
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	e6f99d68-36ce-42bc-ba8c-7d359d4ed001	f
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	37cd1341-8446-43d0-ba1e-7b1b1b310bd7	f
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	b2e26561-f132-4e2d-b4f9-1d860548791a	f
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b	f
530817f0-b05b-4b8c-99ef-494a7fa3b4c0	dd1036f7-c11d-49da-829b-5c6337b19452	t
530817f0-b05b-4b8c-99ef-494a7fa3b4c0	bc79864e-90bd-487f-b474-73a864e85aaf	t
530817f0-b05b-4b8c-99ef-494a7fa3b4c0	eec61978-bb6d-4959-8df6-bbadf6d1bee7	t
530817f0-b05b-4b8c-99ef-494a7fa3b4c0	e83e4acd-b6d4-4dbf-9e14-579fb796de1b	t
530817f0-b05b-4b8c-99ef-494a7fa3b4c0	e6f99d68-36ce-42bc-ba8c-7d359d4ed001	f
530817f0-b05b-4b8c-99ef-494a7fa3b4c0	37cd1341-8446-43d0-ba1e-7b1b1b310bd7	f
530817f0-b05b-4b8c-99ef-494a7fa3b4c0	b2e26561-f132-4e2d-b4f9-1d860548791a	f
530817f0-b05b-4b8c-99ef-494a7fa3b4c0	e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b	f
243fbfa0-43bd-4c06-80e5-ce0619e3ca7c	dd1036f7-c11d-49da-829b-5c6337b19452	t
243fbfa0-43bd-4c06-80e5-ce0619e3ca7c	bc79864e-90bd-487f-b474-73a864e85aaf	t
243fbfa0-43bd-4c06-80e5-ce0619e3ca7c	eec61978-bb6d-4959-8df6-bbadf6d1bee7	t
243fbfa0-43bd-4c06-80e5-ce0619e3ca7c	e83e4acd-b6d4-4dbf-9e14-579fb796de1b	t
243fbfa0-43bd-4c06-80e5-ce0619e3ca7c	e6f99d68-36ce-42bc-ba8c-7d359d4ed001	f
243fbfa0-43bd-4c06-80e5-ce0619e3ca7c	37cd1341-8446-43d0-ba1e-7b1b1b310bd7	f
243fbfa0-43bd-4c06-80e5-ce0619e3ca7c	b2e26561-f132-4e2d-b4f9-1d860548791a	f
243fbfa0-43bd-4c06-80e5-ce0619e3ca7c	e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b	f
a04e902f-9e19-4c75-9014-608c2de644e4	dd1036f7-c11d-49da-829b-5c6337b19452	t
a04e902f-9e19-4c75-9014-608c2de644e4	bc79864e-90bd-487f-b474-73a864e85aaf	t
a04e902f-9e19-4c75-9014-608c2de644e4	eec61978-bb6d-4959-8df6-bbadf6d1bee7	t
a04e902f-9e19-4c75-9014-608c2de644e4	e83e4acd-b6d4-4dbf-9e14-579fb796de1b	t
a04e902f-9e19-4c75-9014-608c2de644e4	e6f99d68-36ce-42bc-ba8c-7d359d4ed001	f
a04e902f-9e19-4c75-9014-608c2de644e4	37cd1341-8446-43d0-ba1e-7b1b1b310bd7	f
a04e902f-9e19-4c75-9014-608c2de644e4	b2e26561-f132-4e2d-b4f9-1d860548791a	f
a04e902f-9e19-4c75-9014-608c2de644e4	e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b	f
a9b463b8-18bb-49f5-a3bd-9e399734c104	dd1036f7-c11d-49da-829b-5c6337b19452	t
a9b463b8-18bb-49f5-a3bd-9e399734c104	bc79864e-90bd-487f-b474-73a864e85aaf	t
a9b463b8-18bb-49f5-a3bd-9e399734c104	eec61978-bb6d-4959-8df6-bbadf6d1bee7	t
a9b463b8-18bb-49f5-a3bd-9e399734c104	e83e4acd-b6d4-4dbf-9e14-579fb796de1b	t
a9b463b8-18bb-49f5-a3bd-9e399734c104	e6f99d68-36ce-42bc-ba8c-7d359d4ed001	f
a9b463b8-18bb-49f5-a3bd-9e399734c104	37cd1341-8446-43d0-ba1e-7b1b1b310bd7	f
a9b463b8-18bb-49f5-a3bd-9e399734c104	b2e26561-f132-4e2d-b4f9-1d860548791a	f
a9b463b8-18bb-49f5-a3bd-9e399734c104	e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b	f
1d826890-822d-4456-8d3b-6fdbe01475ba	d133efd5-8929-4e2e-814a-f1a40ab8886f	t
1d826890-822d-4456-8d3b-6fdbe01475ba	dd1036f7-c11d-49da-829b-5c6337b19452	t
1d826890-822d-4456-8d3b-6fdbe01475ba	bc79864e-90bd-487f-b474-73a864e85aaf	t
1d826890-822d-4456-8d3b-6fdbe01475ba	eec61978-bb6d-4959-8df6-bbadf6d1bee7	t
1d826890-822d-4456-8d3b-6fdbe01475ba	e83e4acd-b6d4-4dbf-9e14-579fb796de1b	t
1d826890-822d-4456-8d3b-6fdbe01475ba	e6f99d68-36ce-42bc-ba8c-7d359d4ed001	f
1d826890-822d-4456-8d3b-6fdbe01475ba	37cd1341-8446-43d0-ba1e-7b1b1b310bd7	f
1d826890-822d-4456-8d3b-6fdbe01475ba	b2e26561-f132-4e2d-b4f9-1d860548791a	f
1d826890-822d-4456-8d3b-6fdbe01475ba	e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b	f
\.


--
-- Data for Name: client_scope_role_mapping; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_scope_role_mapping (scope_id, role_id) FROM stdin;
c29b50b9-5f8f-4115-abc8-6019fd964792	43df3975-e020-4a1b-8b70-3be499933685
e6f99d68-36ce-42bc-ba8c-7d359d4ed001	159d8b16-c522-4128-8e3a-c29b28e80d34
\.


--
-- Data for Name: client_session; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_session (id, client_id, redirect_uri, state, "timestamp", session_id, auth_method, realm_id, auth_user_id, current_action) FROM stdin;
\.


--
-- Data for Name: client_session_auth_status; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_session_auth_status (authenticator, status, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_note; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_session_note (name, value, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_prot_mapper; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_session_prot_mapper (protocol_mapper_id, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_role; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_session_role (role_id, client_session) FROM stdin;
\.


--
-- Data for Name: client_user_session_note; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_user_session_note (name, value, client_session) FROM stdin;
\.


--
-- Data for Name: component; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.component (id, name, parent_id, provider_id, provider_type, realm_id, sub_type) FROM stdin;
8f7a5514-897c-4589-bbf6-e6fe07230a39	Trusted Hosts	master	trusted-hosts	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
38f91150-7dde-48c0-93fd-ed2b37e27337	Consent Required	master	consent-required	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
8a6ab596-6735-4d16-b6b1-ab73bd1d420d	Full Scope Disabled	master	scope	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
b37a89e6-2fcc-4663-b51b-cce90c164e2f	Max Clients Limit	master	max-clients	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
067a7fd6-84fb-49b8-bf18-deedd60db8c2	Allowed Protocol Mapper Types	master	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
979dea8b-c033-4fdb-8b9b-a13548b30b85	Allowed Client Scopes	master	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
dff04bd7-20d8-417e-b9c8-4d0ec2ed8fbd	Allowed Protocol Mapper Types	master	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	authenticated
7b0a741b-eb89-4f76-9b4d-de6a60cc65f8	Allowed Client Scopes	master	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	authenticated
aaf27ede-ab00-40ae-a947-1634575dd49f	rsa-generated	master	rsa-generated	org.keycloak.keys.KeyProvider	master	\N
4b54a913-7e06-407b-9f91-2bb41a726723	hmac-generated	master	hmac-generated	org.keycloak.keys.KeyProvider	master	\N
96228279-96d4-4972-b8fc-bdb4e0ecd761	aes-generated	master	aes-generated	org.keycloak.keys.KeyProvider	master	\N
4fa489cb-ff07-437a-ba07-38f50e268881	rsa-generated	civil-add	rsa-generated	org.keycloak.keys.KeyProvider	civil-add	\N
947b091d-a576-4f12-bbf7-7657a0873021	hmac-generated	civil-add	hmac-generated	org.keycloak.keys.KeyProvider	civil-add	\N
c9f0bbba-4c55-41b1-9bd0-5ec05b84edab	aes-generated	civil-add	aes-generated	org.keycloak.keys.KeyProvider	civil-add	\N
534a691e-1d89-4160-87dc-d1558884d266	Trusted Hosts	civil-add	trusted-hosts	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	civil-add	anonymous
46606055-76bc-4167-8bc8-87698fe4f9c1	Consent Required	civil-add	consent-required	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	civil-add	anonymous
bbc46e7c-b500-497d-8f3f-cc55819e25c7	Full Scope Disabled	civil-add	scope	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	civil-add	anonymous
6f7948c1-ee71-4db8-83af-c9fea93c399e	Max Clients Limit	civil-add	max-clients	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	civil-add	anonymous
f868f45d-a2aa-4d51-8722-c8b46087febd	Allowed Protocol Mapper Types	civil-add	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	civil-add	anonymous
385b044b-cc78-4b9b-8661-07d75b6fb7f0	Allowed Client Scopes	civil-add	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	civil-add	anonymous
8cff5b36-b7ab-4d95-a3d9-9ae2c2bd5b35	Allowed Protocol Mapper Types	civil-add	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	civil-add	authenticated
93f7a861-d717-44ff-9294-42d824ec2b6f	Allowed Client Scopes	civil-add	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	civil-add	authenticated
\.


--
-- Data for Name: component_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.component_config (id, component_id, name, value) FROM stdin;
e76fcccb-0cf7-4e14-b508-92c26d44129f	b37a89e6-2fcc-4663-b51b-cce90c164e2f	max-clients	200
cb856874-0550-463f-8014-a4af1db8e2f9	dff04bd7-20d8-417e-b9c8-4d0ec2ed8fbd	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
0dd8f1f6-90e1-4f81-a909-dae477b7b977	dff04bd7-20d8-417e-b9c8-4d0ec2ed8fbd	allowed-protocol-mapper-types	saml-user-property-mapper
ee89f3b1-af59-44a6-9a3e-c392d2a06e33	dff04bd7-20d8-417e-b9c8-4d0ec2ed8fbd	allowed-protocol-mapper-types	oidc-address-mapper
9ea1ec9c-eb35-4472-bd5e-cf1ad2995008	dff04bd7-20d8-417e-b9c8-4d0ec2ed8fbd	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
1296e6f6-892b-4d8e-a426-9ede79f53615	dff04bd7-20d8-417e-b9c8-4d0ec2ed8fbd	allowed-protocol-mapper-types	saml-role-list-mapper
c3bc2d02-fe5a-43f8-aef8-76763ecbcb4e	dff04bd7-20d8-417e-b9c8-4d0ec2ed8fbd	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
b37d56d5-1086-47a4-8877-4d47b49ce21f	dff04bd7-20d8-417e-b9c8-4d0ec2ed8fbd	allowed-protocol-mapper-types	saml-user-attribute-mapper
e3f394e4-1940-4a93-9c44-e99d954594d7	dff04bd7-20d8-417e-b9c8-4d0ec2ed8fbd	allowed-protocol-mapper-types	oidc-full-name-mapper
680cdcaf-a236-4fc1-af1b-ad0b1b116579	067a7fd6-84fb-49b8-bf18-deedd60db8c2	allowed-protocol-mapper-types	saml-role-list-mapper
eae1f3b9-b019-4ca3-8fc9-b77e6d118965	067a7fd6-84fb-49b8-bf18-deedd60db8c2	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
966dc9a4-fee9-4cb8-92d5-f84c2d70b87d	067a7fd6-84fb-49b8-bf18-deedd60db8c2	allowed-protocol-mapper-types	oidc-full-name-mapper
2063dedb-0c0b-4686-a85c-7bbed986d7c8	067a7fd6-84fb-49b8-bf18-deedd60db8c2	allowed-protocol-mapper-types	saml-user-property-mapper
f4b5572a-1869-4d68-acd4-1243bb9ede37	067a7fd6-84fb-49b8-bf18-deedd60db8c2	allowed-protocol-mapper-types	saml-user-attribute-mapper
5110b9f3-75c0-4587-a2c3-6d7570566012	067a7fd6-84fb-49b8-bf18-deedd60db8c2	allowed-protocol-mapper-types	oidc-address-mapper
6142f50d-f7f2-44f3-b3b2-5d5a150d7461	067a7fd6-84fb-49b8-bf18-deedd60db8c2	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
e81889ea-3e7d-417e-899e-0efd80927905	067a7fd6-84fb-49b8-bf18-deedd60db8c2	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
580488fe-26a2-4124-9921-eab897044ad7	8f7a5514-897c-4589-bbf6-e6fe07230a39	host-sending-registration-request-must-match	true
1b9afa7b-b4e3-46d1-a24c-b1b918159aeb	8f7a5514-897c-4589-bbf6-e6fe07230a39	client-uris-must-match	true
2e691698-cddc-4079-afc7-100216a0ff37	979dea8b-c033-4fdb-8b9b-a13548b30b85	allow-default-scopes	true
5a55c684-fe57-4091-9a50-b9866ae3a783	7b0a741b-eb89-4f76-9b4d-de6a60cc65f8	allow-default-scopes	true
c1a0a3e0-a3c1-4826-a11d-ece95ed5d7b2	aaf27ede-ab00-40ae-a947-1634575dd49f	certificate	MIICmzCCAYMCBgFxl4Kv4zANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjAwNDIwMTIxMjM2WhcNMzAwNDIwMTIxNDE2WjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCz/00FnJnIv9yFhPcT5T3CHPu9T3Yhup762PjgDe7f4YOxPFX5rG6/SyPjCW5peHaD3kNyKAXeNZ7Ac17+mDPk13g6pmCtHNeF1lMrzqACmbXrbHCmfYiKWWpBG3tFI+WadPDEJFEob5hkEFM8RIr8qBbWqh7HS8wkDVxjRN8zR9DXH7HGNJ1Gsf3ALdnwA5L+5mR9qSvIbGDp423X+2Tre7OS0prUX68bXK0O/OSR3fafVNtzn/7Gd1eNMrf+HwUk0bgFAib/RgdaPt88/69ej59G+iHVxDfQ+ZetAwYYd/vNIH4aDbQwYOHrdri+qeT8SWUGljin2kKXYzzH1p35AgMBAAEwDQYJKoZIhvcNAQELBQADggEBACeSGOvbRqtH4gj0Rsk6jgtde0rbduQIHL5vZ69cPAvo3wfNPtrHIRhsV61y7yhkORaa7Z0k7sEVvH7ySa0USB8IOqRvgSLU6hgzSqka+U5W7vClSUKc2aGTUzzPSWkKyJAyoyohVJ3z0/TAErHyVloKzYu5tmRUeZ+YRMvLP8whiOmCekKtzIF9C+yYtF/IjY0pfYVWxDuk7bnzxYReNQ7Tqa4M65Ntfwsd+S1NTdkNrffmOa9HHrSxxKYr4pMufywU/YiorKqM9VrgtGeWZa+8ECTWBb+eShOuc4UtvkRM16AMghoRyGs+MFHiI6Zs15FJ3712yhis8wYxRj66MOs=
56484280-1b88-41ae-a559-5966a4fb74fd	aaf27ede-ab00-40ae-a947-1634575dd49f	priority	100
4397d529-d41a-4744-ae3b-4e24b1ddd24b	aaf27ede-ab00-40ae-a947-1634575dd49f	privateKey	MIIEpQIBAAKCAQEAs/9NBZyZyL/chYT3E+U9whz7vU92Ibqe+tj44A3u3+GDsTxV+axuv0sj4wluaXh2g95DcigF3jWewHNe/pgz5Nd4OqZgrRzXhdZTK86gApm162xwpn2IillqQRt7RSPlmnTwxCRRKG+YZBBTPESK/KgW1qoex0vMJA1cY0TfM0fQ1x+xxjSdRrH9wC3Z8AOS/uZkfakryGxg6eNt1/tk63uzktKa1F+vG1ytDvzkkd32n1Tbc5/+xndXjTK3/h8FJNG4BQIm/0YHWj7fPP+vXo+fRvoh1cQ30PmXrQMGGHf7zSB+Gg20MGDh63a4vqnk/EllBpY4p9pCl2M8x9ad+QIDAQABAoIBAQCDXXIDBHWf85anw5DnbkkSYEDBuzqGB0N749pO+xw5PMb2FQjSPSxeZCJ+0iVIyGgGebJToC4YLJ396rWLrd1ByJewhjjMXt5UiUUOFbDl9PS2KuBUQg3imrtXZQ0kOryZa3tESxUII9nePaPhukyNdMemR2ZLIWCNd0rUEDGtpCLzKwaO8aFKO66BavnTQJwuSn2qC0zbcDBE3T5uqvSjizqnbWpX2T5tcOp7M61zyTWYjUo6KjQkwB0qUcNrZuN5zmdV0L+hi8Olz6Xo/g/SzG7YeDdGTGfcSWP/x2pRGoA2+PgpFpgHBMbefYGxcXAkpK2KDixlFoiHhY0lMgThAoGBAO9Eso9wjBtKgG3t5qKNxWQS3UAep2YJn5lxJJT7ebqcHbmYVOJkUNlWczoqJfWG1Tm0psYc94dC/f6L4rSyUqCsvmBcvSA014c3ra7UcfrAArEAfGJDCFbDCEduKW9gGEfofdLwGNDqg29kDDSwveAkrN1dlIfqXPXvTMi6qeBdAoGBAMCVjVs6xyzUuHp1uqtVpCs5X2kXdos66fYglc89CHCLonPeNY/nRuzw4Q6MbfuJyJP5FWkVViAl/VMrN0wibywbEGn4vweSE0UMS3CGgU4mh4bf//iNB7Lkglvlgz6/SNzfgGhrqEIpDgECxgy5sTiLf1vlXE1foHHSlZ7CPIpNAoGAT4b+7qxS9VM7As7bJNaIcG3Qulo1+7VhJeg0Z4x6dmsjeifGtwTu1n5kiEWwHj61qpt//0Nrhrren3exX6wRyLQCBxdsCUNnC9uW0BBu3hBqFJj7vtFW+TeUDeHwxcMj7uq4v57sMKxyCVkwsWa5hcHK7z0GfHKKeXgThnh8tEUCgYEAl82Kxi230GvBGq5wPRvds0CfJ4yaIbeHsj4POHMIkwZOQH0YxVztuFlLXZduHkeGdrVxngp2gBLR81vuU7IU5bRTpOSxV7taOyQs1v7/L+YJEFTDRV2NgwLCisOIR86qFYiLhEPQNo57/mn8SQ2aV1MGSh6qOkc1hmMTEzhSVkUCgYEA165NnBP4ilv02RgSh/rP5+wbzbwP2GVvsvGIwB/FO3P/+SzuSRyznfQLygyhhB8QQzmk6S8gw4NUedx/s3A2UfjxyWnJXD/wSiEdTErJ+I0/ZK2LI+s7lRNGJcauxj0TlZ7Bo1+7WfSJhkRAJhpIpe1SgV8RVMl+FfNAZus45eg=
b87dae84-65f0-45c4-9c0b-17b3fe6ae558	96228279-96d4-4972-b8fc-bdb4e0ecd761	kid	5045cbb6-3434-4b37-8886-fb2428f80e84
45e16fe9-c7ea-4552-9588-9996ea58856a	96228279-96d4-4972-b8fc-bdb4e0ecd761	secret	iMdHBLwdNdGdug1RYEcHyw
5c984ab4-d3b2-4a69-ae2b-e18c0fe7d87b	96228279-96d4-4972-b8fc-bdb4e0ecd761	priority	100
ce4a032a-7540-4942-911b-f0b32393c74b	4b54a913-7e06-407b-9f91-2bb41a726723	kid	ce75ab4b-ad29-4e66-88da-c15760ecabf5
410a69a6-94a2-4659-bcf2-a7c8da8a0872	4b54a913-7e06-407b-9f91-2bb41a726723	secret	7t1ym5qIRcrodNwrqNeAq0cBf7LWrd6fcK6PaC5ob8uAA0ROr0tdKzGEWYuhNpZDdc6f_oaost_D5pF45EAhHw
38879440-498e-4cfd-bb14-7ea7e2867761	4b54a913-7e06-407b-9f91-2bb41a726723	algorithm	HS256
d264d1cc-5415-43b8-8b20-5280f88dc04e	4b54a913-7e06-407b-9f91-2bb41a726723	priority	100
385aeb38-ed51-4f62-94cf-f8004a04f21a	c9f0bbba-4c55-41b1-9bd0-5ec05b84edab	secret	JIgJcm8cR8PlFkKe0DR7WA
54832f88-b753-4a52-bf54-d014b0079212	c9f0bbba-4c55-41b1-9bd0-5ec05b84edab	kid	d1d262e5-1e98-4254-9fd7-8697fa055d9a
a0965846-be80-4a93-8100-6486e1a9c356	c9f0bbba-4c55-41b1-9bd0-5ec05b84edab	priority	100
7c5e3e0a-a67d-4d1e-80ea-f7a7737a308d	947b091d-a576-4f12-bbf7-7657a0873021	priority	100
62a23f05-2a31-4d39-b13e-7403b4bbd2b8	947b091d-a576-4f12-bbf7-7657a0873021	kid	41262899-2642-4041-89a4-75bdc91e95ce
2db911e5-e99f-4a55-b7c1-b6d46648ce85	947b091d-a576-4f12-bbf7-7657a0873021	secret	vxPiTxA3V6oi6nF5ehPfMs-tLW_OwTwfLp1Yn4J4F7K3hN0X0KsEGE-ESDyoXUz18xnjFr2lf7dfG_AH9YzIVg
361b3a4e-851c-4a8f-ae18-f704e7f3eb83	947b091d-a576-4f12-bbf7-7657a0873021	algorithm	HS256
3beeae65-e52d-464c-a2ca-2eac482e6e65	4fa489cb-ff07-437a-ba07-38f50e268881	privateKey	MIIEowIBAAKCAQEAm+rJCVp/diKLm51ef+rMF/sDb/U2auH3gLpw9HVZTIe9yF7Z0H+VGhWShyXSXHUMJbW8qvgWJdV+O6p+KZAY5lIDRgrUGXA9MiAj9n2JFsl0HbeaMFbhXhGZFl7X0urp+Gm5x8ScQkXvVdtvORG9IpHIup5P7TbcKWhJ1DgRSwkjg9qw5EiM8+oVySY2PzLi2YYV/DpzE+JJ6sq713eyamVaq00nRwvniCpMZwEKGlE3+70jJudAS+qqIE1walWeJpZWHtUovkfSrgND0oH2IhrKpzp5VK6GgJPA5naGr7SXFdZvAc7dEaTkiNZ8dAE/XEiLNEGgIXdHhJCWgvZB9wIDAQABAoIBAQCVFQH095s4kf97ZBWfqhpMM68ttgG6YSYO6DZpCGvCSWil1kvemJ8wub/JidiCn0hmJkeoEl7a/jAdTQAlLashTvniVyhGypM5ApJgddhG+YkVgSDlZ61GDP2b7kPfYM8mv7Dr2oqus8ANECxhHR0DmPGdvxIdd387+IPfb1zZqP0TtYnvpcHQ+VLsIp8dXZVtuNklFKmcBswMGR/PJs0Pw2j5gmCCwhjkfJilUUaf/+ICdnh6d6RbXKioEaL4qAxB2nZY+EV+k2ysOlk6d/E3g4QbHayQdGAHgjsidhXzWUqsLHevT6VKuPNZf0r6hmJVW44OI3Z7HbWpp8wMRDGBAoGBAM4u5mYD2p+L0695q42uPMK66UoOTLv+caCKagmgzgHern6bqW+bVU4R3U0RIm5qsj6Ri4u1Dl1VE4fwWEvEXucEN15CxhwSA3RaYR7d8zDAy8MRX9dGPPBtiD/rKmEQVe6SvG3XrlcdMuhv3zdqxS4kCndAPIF2ddYbeILYK8KfAoGBAMGWw/9sxKj5pDElly1BlvneK46onlXZ7I9IwpBBa8WycNnqqYbSj9LK5IYkUMBI8IIddC+cAb6C4S7qAhsHxPPyubnY7UuGjEnvNHDCKCqZZ5/j5FdJfyW6yyykbE36r+G3ZYzVQQX2JuOLDH6uHGk3umuAg8b8wm2v9aOdptmpAoGAGUQYCcJJJSn+W89+HVOPsJE1C1UrRBJPHEw56EojVIlUOsGahsmzL2jzBIGoBX3fr9koD4u1YeyOAyeJumcq9gZHPq7SFhiDAdwdcKnkJkJJ9fKIGxpJV3EktCh+PfzBewXSP2ehvp0eYutqoCDeXuQdwDVH9NluYTlmkCorWqMCgYBBRyhyMeginoN6OxW+IfbZ7Mu9QCicBG8bZcqNwDGqKWCaauGdX9U1nErw3MAKBs5q4amzACUqa5GpMylgmf8Uz41HGNnzdLxLlgiV2TKYIsBpjvwJXvQr8c/fDtaieNzj/OaWVbamXvmAEvw0n1tfM3Z+F4jYCZU6TXJEKL4BeQKBgCO0YgaC5o8AmMo6KZRFbaDdJ4qKrwBanB7pLFJodJEl1SHh5AsfV+7wq+7n8YPucPeeHnEAK+qoOPaHLrm0Alt4MaGzrmVKvLjI3hcoAHwTeavdo3DI6cUxVXhLTbRAv0rYE9EcDeZsZSbZEFR48ABQ36M5NLl+gXSHKm2IIuUG
198e6172-fdc3-449e-92e4-65cf55db1dfd	4fa489cb-ff07-437a-ba07-38f50e268881	certificate	MIICoTCCAYkCBgFxmVX2DDANBgkqhkiG9w0BAQsFADAUMRIwEAYDVQQDDAljaXZpbC1hZGQwHhcNMjAwNDIwMjA0MjU5WhcNMzAwNDIwMjA0NDM5WjAUMRIwEAYDVQQDDAljaXZpbC1hZGQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCb6skJWn92IoubnV5/6swX+wNv9TZq4feAunD0dVlMh73IXtnQf5UaFZKHJdJcdQwltbyq+BYl1X47qn4pkBjmUgNGCtQZcD0yICP2fYkWyXQdt5owVuFeEZkWXtfS6un4abnHxJxCRe9V2285Eb0ikci6nk/tNtwpaEnUOBFLCSOD2rDkSIzz6hXJJjY/MuLZhhX8OnMT4knqyrvXd7JqZVqrTSdHC+eIKkxnAQoaUTf7vSMm50BL6qogTXBqVZ4mllYe1Si+R9KuA0PSgfYiGsqnOnlUroaAk8DmdoavtJcV1m8Bzt0RpOSI1nx0AT9cSIs0QaAhd0eEkJaC9kH3AgMBAAEwDQYJKoZIhvcNAQELBQADggEBAG8MHFdaf3+Rruf+dJ2ggZ5oMQYdgWe4JVx+Teo3lFEKvyytufRo+mKMi1FqebQ0XbOV1NArg6GvqH64ca2MkXh8fvR3lVlOA5EvA/y1BPCHjcuSGxT7W0E7BSHLYeBLcuJg/1+z4wFpuC4ZiM+gAedkubonpp7xjM/GcUtZZF2DjJ9a8U78i3vgNZOzk2U3E5gvHxIHuLYQ+pk7bWOYdlfPDIZS8R848PnckedcLD9Zf6P6UZ2JfAOLEBHv9FPbz5wgdelsYyVr4T/9uu5wlzUtuNQ8fhdDy6j6eIMh2NGLUgLS0yGUAQm1NdNcq2H65Q+B8/3Sfbycv4z9UrUhfuc=
b45c8be0-3ec8-425d-a98d-78422290cc83	4fa489cb-ff07-437a-ba07-38f50e268881	priority	100
e69ddace-7581-46ae-be20-b311713e016f	8cff5b36-b7ab-4d95-a3d9-9ae2c2bd5b35	allowed-protocol-mapper-types	oidc-address-mapper
1f1fbc17-3bcc-46c3-8ec6-f1f397136c95	8cff5b36-b7ab-4d95-a3d9-9ae2c2bd5b35	allowed-protocol-mapper-types	saml-user-attribute-mapper
d6d400a6-9ef2-49c7-95e4-56912c08574e	8cff5b36-b7ab-4d95-a3d9-9ae2c2bd5b35	allowed-protocol-mapper-types	oidc-full-name-mapper
25c37697-936a-4050-9f24-e1351528e458	8cff5b36-b7ab-4d95-a3d9-9ae2c2bd5b35	allowed-protocol-mapper-types	saml-user-property-mapper
23439b7c-35b4-4bc4-abee-574fc7cf659b	8cff5b36-b7ab-4d95-a3d9-9ae2c2bd5b35	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
a431b272-8d3e-4107-9d56-74507b2fa2be	8cff5b36-b7ab-4d95-a3d9-9ae2c2bd5b35	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
10ba70c9-b34f-49b0-9f92-085926dc4e61	8cff5b36-b7ab-4d95-a3d9-9ae2c2bd5b35	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
4b9bf40e-46d8-4aa8-bd99-2a4f92fa7b92	8cff5b36-b7ab-4d95-a3d9-9ae2c2bd5b35	allowed-protocol-mapper-types	saml-role-list-mapper
3023049c-a66e-45d1-9dea-10bb72dbea98	385b044b-cc78-4b9b-8661-07d75b6fb7f0	allow-default-scopes	true
86d90454-8591-4898-be90-0832d57dd739	f868f45d-a2aa-4d51-8722-c8b46087febd	allowed-protocol-mapper-types	oidc-full-name-mapper
a6df15b1-cad4-48a4-a874-e3b7ae1db4d6	f868f45d-a2aa-4d51-8722-c8b46087febd	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
64b7e885-afad-4606-a3e1-721876a88a29	f868f45d-a2aa-4d51-8722-c8b46087febd	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
a92f69d0-f55f-4719-b382-f299ea525f36	f868f45d-a2aa-4d51-8722-c8b46087febd	allowed-protocol-mapper-types	saml-role-list-mapper
9a3449b7-98aa-444d-837b-9e66706fc610	f868f45d-a2aa-4d51-8722-c8b46087febd	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
d27cdfb2-3beb-4509-924d-7b8d144717c8	f868f45d-a2aa-4d51-8722-c8b46087febd	allowed-protocol-mapper-types	saml-user-property-mapper
4389abae-4649-450b-a378-a223b0306e35	f868f45d-a2aa-4d51-8722-c8b46087febd	allowed-protocol-mapper-types	oidc-address-mapper
d0e6244e-aebe-4c31-a266-ebaf63903f5c	f868f45d-a2aa-4d51-8722-c8b46087febd	allowed-protocol-mapper-types	saml-user-attribute-mapper
ba0b34a9-df60-4cff-aa92-1cfada735523	6f7948c1-ee71-4db8-83af-c9fea93c399e	max-clients	200
d5a88ba0-d77f-4b07-97ab-aa69f99723dd	534a691e-1d89-4160-87dc-d1558884d266	client-uris-must-match	true
0493a4ca-7659-447d-8889-e2a85f353aa4	534a691e-1d89-4160-87dc-d1558884d266	host-sending-registration-request-must-match	true
5b336fdd-2159-40b7-8164-aa016b89a643	93f7a861-d717-44ff-9294-42d824ec2b6f	allow-default-scopes	true
\.


--
-- Data for Name: composite_role; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.composite_role (composite, child_role) FROM stdin;
7cef0a8f-b179-4cc3-8e59-92899865ef50	a0d1c070-b1f5-45d8-a7f1-e17ec5378025
7cef0a8f-b179-4cc3-8e59-92899865ef50	a09ea775-ac86-4680-8643-d6c3432974d8
7cef0a8f-b179-4cc3-8e59-92899865ef50	06fed710-9e97-4135-8696-4d3ae6a904da
7cef0a8f-b179-4cc3-8e59-92899865ef50	07d4a5a4-ce28-4a2f-a46d-670585d403da
7cef0a8f-b179-4cc3-8e59-92899865ef50	6ed0eb1a-2d28-4609-8916-1a4d4279ef3e
7cef0a8f-b179-4cc3-8e59-92899865ef50	a3b5e1b4-175f-4d40-b4a0-02e51c0fd67a
7cef0a8f-b179-4cc3-8e59-92899865ef50	424cba18-97bd-44a9-acd9-16bd76df0fec
7cef0a8f-b179-4cc3-8e59-92899865ef50	b5495d72-d02d-461a-8d01-3be369d13e47
7cef0a8f-b179-4cc3-8e59-92899865ef50	6a1d56a0-64ce-4363-9451-f78c9d63dcc0
7cef0a8f-b179-4cc3-8e59-92899865ef50	a04208ec-2c8b-476c-8373-a64597f1b8f5
7cef0a8f-b179-4cc3-8e59-92899865ef50	64919610-fde0-41d2-a8b7-555896c164c2
7cef0a8f-b179-4cc3-8e59-92899865ef50	257c8cc1-3697-4763-9056-7faf28eef3ec
7cef0a8f-b179-4cc3-8e59-92899865ef50	2a5a24f3-4ca3-413f-b074-1fc6619a97b1
7cef0a8f-b179-4cc3-8e59-92899865ef50	3ff78db0-9e54-4bf8-80d4-c971c0a23c0d
7cef0a8f-b179-4cc3-8e59-92899865ef50	a0446c4d-26dc-4eb5-acff-40091f5c18df
7cef0a8f-b179-4cc3-8e59-92899865ef50	42f11daa-2167-48f8-be43-52b1bebdff3c
7cef0a8f-b179-4cc3-8e59-92899865ef50	ce48e73a-a4c0-4977-9611-8a5bea8e1f1f
7cef0a8f-b179-4cc3-8e59-92899865ef50	c01a15d2-e609-4829-96b0-155c876e5384
07d4a5a4-ce28-4a2f-a46d-670585d403da	a0446c4d-26dc-4eb5-acff-40091f5c18df
07d4a5a4-ce28-4a2f-a46d-670585d403da	c01a15d2-e609-4829-96b0-155c876e5384
6ed0eb1a-2d28-4609-8916-1a4d4279ef3e	42f11daa-2167-48f8-be43-52b1bebdff3c
1635476e-d04b-41db-8429-6c164d2b0255	38074eaa-34f9-49aa-a6bd-cc80942d7c20
11406917-baf9-4811-bd5f-39854459d050	6f18f67d-524c-4e23-a042-9720db256328
7cef0a8f-b179-4cc3-8e59-92899865ef50	9086d304-b1b8-4f1f-b700-b35b040be43a
7cef0a8f-b179-4cc3-8e59-92899865ef50	5fc97272-2e73-4c96-8c03-5581550d2e41
7cef0a8f-b179-4cc3-8e59-92899865ef50	aee714d4-45b4-407b-8f48-36c9bba52804
7cef0a8f-b179-4cc3-8e59-92899865ef50	e3a4f637-24ff-4c76-be6f-fc6446d88eed
7cef0a8f-b179-4cc3-8e59-92899865ef50	747a23aa-339e-431b-a2e3-d222c5c15905
7cef0a8f-b179-4cc3-8e59-92899865ef50	32e2fa3c-8ff9-44a2-8936-3d160e373510
7cef0a8f-b179-4cc3-8e59-92899865ef50	569488f0-238a-47cb-bba3-572810e5b616
7cef0a8f-b179-4cc3-8e59-92899865ef50	4ea1c0f5-d026-4242-b3da-c7b1ba0fb50f
7cef0a8f-b179-4cc3-8e59-92899865ef50	91b8a153-2d3a-48fb-9599-750fbb98874e
7cef0a8f-b179-4cc3-8e59-92899865ef50	749b1778-1c48-4816-b601-78888db90000
7cef0a8f-b179-4cc3-8e59-92899865ef50	e47332c8-ed27-42ac-9b65-c2db290647bc
7cef0a8f-b179-4cc3-8e59-92899865ef50	1aa21623-e153-442b-91db-7457347d49b4
7cef0a8f-b179-4cc3-8e59-92899865ef50	afd53ce1-06e8-4924-9621-f1a59b53b019
7cef0a8f-b179-4cc3-8e59-92899865ef50	a9baa2fb-2c02-4795-b1c4-41f16e431a5a
7cef0a8f-b179-4cc3-8e59-92899865ef50	ab6b990b-e56c-4bfb-b2e2-a9d6e9f853e7
7cef0a8f-b179-4cc3-8e59-92899865ef50	f8512649-3f9d-4911-b30d-b3a5694a0c00
7cef0a8f-b179-4cc3-8e59-92899865ef50	05941097-62d6-49e6-8585-2681ceb06fe5
7cef0a8f-b179-4cc3-8e59-92899865ef50	2d6bf747-965d-4a05-a6b2-470fe20ee58c
e3a4f637-24ff-4c76-be6f-fc6446d88eed	ab6b990b-e56c-4bfb-b2e2-a9d6e9f853e7
e3a4f637-24ff-4c76-be6f-fc6446d88eed	2d6bf747-965d-4a05-a6b2-470fe20ee58c
747a23aa-339e-431b-a2e3-d222c5c15905	f8512649-3f9d-4911-b30d-b3a5694a0c00
73c48bc2-3261-4c56-978f-bb707f760001	5fcf72d0-9dc9-4d91-8bdd-99b960f49b20
73c48bc2-3261-4c56-978f-bb707f760001	6f23685d-67c5-4457-8ed2-6c8d418a4ae6
73c48bc2-3261-4c56-978f-bb707f760001	f9477a23-1588-4e81-a01b-8041230e2318
73c48bc2-3261-4c56-978f-bb707f760001	ee9e4447-b7b0-43d4-9347-1f78a1ae7654
73c48bc2-3261-4c56-978f-bb707f760001	640256b2-6004-4982-9cb2-ecf3a57ceea7
73c48bc2-3261-4c56-978f-bb707f760001	aab6bdf7-5e9a-445b-8c8c-44996dbba625
73c48bc2-3261-4c56-978f-bb707f760001	66721053-ab64-49d1-af76-04b7604237b9
73c48bc2-3261-4c56-978f-bb707f760001	f3e983cd-7117-49c2-9afd-04188de55c98
73c48bc2-3261-4c56-978f-bb707f760001	ae117f03-7437-49dd-b495-32245321f847
73c48bc2-3261-4c56-978f-bb707f760001	afd698c9-848d-4658-b87d-a4687c3c5a99
73c48bc2-3261-4c56-978f-bb707f760001	da8dfcdb-1fec-4e19-8cdd-52eef1764304
73c48bc2-3261-4c56-978f-bb707f760001	1f84f9da-aaa7-4f22-b33c-f2b31326316d
73c48bc2-3261-4c56-978f-bb707f760001	ace074e7-3b3b-4282-a2c2-c03ce9c39351
73c48bc2-3261-4c56-978f-bb707f760001	847672da-59a7-4e1c-816c-8fe59249b84a
73c48bc2-3261-4c56-978f-bb707f760001	a2aaaddb-6073-4860-966c-8e5406b9abda
73c48bc2-3261-4c56-978f-bb707f760001	ec342ed6-4607-4761-b10c-ca1bfaa9ad5d
73c48bc2-3261-4c56-978f-bb707f760001	42ee34e3-1753-4305-869e-6e6b683a50a7
f9477a23-1588-4e81-a01b-8041230e2318	42ee34e3-1753-4305-869e-6e6b683a50a7
f9477a23-1588-4e81-a01b-8041230e2318	847672da-59a7-4e1c-816c-8fe59249b84a
ee9e4447-b7b0-43d4-9347-1f78a1ae7654	a2aaaddb-6073-4860-966c-8e5406b9abda
093235eb-d1b2-4732-bdaa-8db103645f63	a51d1d10-fb23-472a-b6aa-f0b088708df3
0733e130-b91f-41e0-b72f-9db2aaeb89bd	41c887b5-a089-4480-bf6b-0fe4ace67495
7cef0a8f-b179-4cc3-8e59-92899865ef50	468fced7-d260-46a7-aa2b-58851a8d27fa
73c48bc2-3261-4c56-978f-bb707f760001	e45d2bb7-eb10-4b00-b1a9-e2041f34e5df
\.


--
-- Data for Name: credential; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.credential (id, salt, type, user_id, created_date, user_label, secret_data, credential_data, priority) FROM stdin;
c71bbfac-f9a9-4f60-bd31-274f067546a6	\N	password	785cc4e2-b971-4a82-bcb6-b997646aaec5	1587384857812	\N	{"value":"SdhOs3OROIdPD3rJ55/8jCKCXagxWToqowwewMzUud60FuWg+pPp8Gg5v6rC/VqB3G6EkQ0n/MZlJx0DI9emNw==","salt":"pYwo0862AdWLCgk8EkHKOA=="}	{"hashIterations":27500,"algorithm":"pbkdf2-sha256"}	10
4e6d8886-5c0d-42f2-ae19-884837574c35	\N	password	b7f5370c-02f9-4a55-8a44-43ab6c54f781	1587416089366	\N	{"value":"IvVrc/bUOiVj3z973eTBSRwLqJfZaS2dn4LAcg/1jl1N/xTkVn/Z5+IQhWrk7h3WVRszawBZLtNOcXwh0e/EmA==","salt":"STJYGT5H4YDg5ITPrVGgSw=="}	{"hashIterations":27500,"algorithm":"pbkdf2-sha256"}	10
\.


--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
1.0.0.Final-KEYCLOAK-5461	sthorger@redhat.com	META-INF/jpa-changelog-1.0.0.Final.xml	2020-04-20 12:14:03.578073	1	EXECUTED	7:4e70412f24a3f382c82183742ec79317	createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...		\N	3.5.4	\N	\N	7384842764
1.0.0.Final-KEYCLOAK-5461	sthorger@redhat.com	META-INF/db2-jpa-changelog-1.0.0.Final.xml	2020-04-20 12:14:03.623551	2	MARK_RAN	7:cb16724583e9675711801c6875114f28	createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...		\N	3.5.4	\N	\N	7384842764
1.1.0.Beta1	sthorger@redhat.com	META-INF/jpa-changelog-1.1.0.Beta1.xml	2020-04-20 12:14:03.716425	3	EXECUTED	7:0310eb8ba07cec616460794d42ade0fa	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=CLIENT_ATTRIBUTES; createTable tableName=CLIENT_SESSION_NOTE; createTable tableName=APP_NODE_REGISTRATIONS; addColumn table...		\N	3.5.4	\N	\N	7384842764
1.1.0.Final	sthorger@redhat.com	META-INF/jpa-changelog-1.1.0.Final.xml	2020-04-20 12:14:03.72716	4	EXECUTED	7:5d25857e708c3233ef4439df1f93f012	renameColumn newColumnName=EVENT_TIME, oldColumnName=TIME, tableName=EVENT_ENTITY		\N	3.5.4	\N	\N	7384842764
1.2.0.Beta1	psilva@redhat.com	META-INF/jpa-changelog-1.2.0.Beta1.xml	2020-04-20 12:14:03.887924	5	EXECUTED	7:c7a54a1041d58eb3817a4a883b4d4e84	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...		\N	3.5.4	\N	\N	7384842764
1.2.0.Beta1	psilva@redhat.com	META-INF/db2-jpa-changelog-1.2.0.Beta1.xml	2020-04-20 12:14:03.911146	6	MARK_RAN	7:2e01012df20974c1c2a605ef8afe25b7	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...		\N	3.5.4	\N	\N	7384842764
1.2.0.RC1	bburke@redhat.com	META-INF/jpa-changelog-1.2.0.CR1.xml	2020-04-20 12:14:04.040567	7	EXECUTED	7:0f08df48468428e0f30ee59a8ec01a41	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...		\N	3.5.4	\N	\N	7384842764
1.2.0.RC1	bburke@redhat.com	META-INF/db2-jpa-changelog-1.2.0.CR1.xml	2020-04-20 12:14:04.051091	8	MARK_RAN	7:a77ea2ad226b345e7d689d366f185c8c	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...		\N	3.5.4	\N	\N	7384842764
1.2.0.Final	keycloak	META-INF/jpa-changelog-1.2.0.Final.xml	2020-04-20 12:14:04.059238	9	EXECUTED	7:a3377a2059aefbf3b90ebb4c4cc8e2ab	update tableName=CLIENT; update tableName=CLIENT; update tableName=CLIENT		\N	3.5.4	\N	\N	7384842764
1.3.0	bburke@redhat.com	META-INF/jpa-changelog-1.3.0.xml	2020-04-20 12:14:04.232824	10	EXECUTED	7:04c1dbedc2aa3e9756d1a1668e003451	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=ADMI...		\N	3.5.4	\N	\N	7384842764
1.4.0	bburke@redhat.com	META-INF/jpa-changelog-1.4.0.xml	2020-04-20 12:14:04.330611	11	EXECUTED	7:36ef39ed560ad07062d956db861042ba	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	3.5.4	\N	\N	7384842764
1.4.0	bburke@redhat.com	META-INF/db2-jpa-changelog-1.4.0.xml	2020-04-20 12:14:04.334415	12	MARK_RAN	7:d909180b2530479a716d3f9c9eaea3d7	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	3.5.4	\N	\N	7384842764
1.5.0	bburke@redhat.com	META-INF/jpa-changelog-1.5.0.xml	2020-04-20 12:14:04.355072	13	EXECUTED	7:cf12b04b79bea5152f165eb41f3955f6	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	3.5.4	\N	\N	7384842764
1.6.1_from15	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2020-04-20 12:14:04.381437	14	EXECUTED	7:7e32c8f05c755e8675764e7d5f514509	addColumn tableName=REALM; addColumn tableName=KEYCLOAK_ROLE; addColumn tableName=CLIENT; createTable tableName=OFFLINE_USER_SESSION; createTable tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_US_SES_PK2, tableName=...		\N	3.5.4	\N	\N	7384842764
1.6.1_from16-pre	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2020-04-20 12:14:04.384072	15	MARK_RAN	7:980ba23cc0ec39cab731ce903dd01291	delete tableName=OFFLINE_CLIENT_SESSION; delete tableName=OFFLINE_USER_SESSION		\N	3.5.4	\N	\N	7384842764
1.6.1_from16	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2020-04-20 12:14:04.386572	16	MARK_RAN	7:2fa220758991285312eb84f3b4ff5336	dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_US_SES_PK, tableName=OFFLINE_USER_SESSION; dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_CL_SES_PK, tableName=OFFLINE_CLIENT_SESSION; addColumn tableName=OFFLINE_USER_SESSION; update tableName=OF...		\N	3.5.4	\N	\N	7384842764
1.6.1	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2020-04-20 12:14:04.389015	17	EXECUTED	7:d41d8cd98f00b204e9800998ecf8427e	empty		\N	3.5.4	\N	\N	7384842764
1.7.0	bburke@redhat.com	META-INF/jpa-changelog-1.7.0.xml	2020-04-20 12:14:04.459666	18	EXECUTED	7:91ace540896df890cc00a0490ee52bbc	createTable tableName=KEYCLOAK_GROUP; createTable tableName=GROUP_ROLE_MAPPING; createTable tableName=GROUP_ATTRIBUTE; createTable tableName=USER_GROUP_MEMBERSHIP; createTable tableName=REALM_DEFAULT_GROUPS; addColumn tableName=IDENTITY_PROVIDER; ...		\N	3.5.4	\N	\N	7384842764
1.8.0	mposolda@redhat.com	META-INF/jpa-changelog-1.8.0.xml	2020-04-20 12:14:04.542556	19	EXECUTED	7:c31d1646dfa2618a9335c00e07f89f24	addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...		\N	3.5.4	\N	\N	7384842764
1.8.0-2	keycloak	META-INF/jpa-changelog-1.8.0.xml	2020-04-20 12:14:04.55142	20	EXECUTED	7:df8bc21027a4f7cbbb01f6344e89ce07	dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL		\N	3.5.4	\N	\N	7384842764
authz-3.4.0.CR1-resource-server-pk-change-part1	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2020-04-20 12:14:05.395654	45	EXECUTED	7:6a48ce645a3525488a90fbf76adf3bb3	addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_RESOURCE; addColumn tableName=RESOURCE_SERVER_SCOPE		\N	3.5.4	\N	\N	7384842764
1.8.0	mposolda@redhat.com	META-INF/db2-jpa-changelog-1.8.0.xml	2020-04-20 12:14:04.561323	21	MARK_RAN	7:f987971fe6b37d963bc95fee2b27f8df	addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...		\N	3.5.4	\N	\N	7384842764
1.8.0-2	keycloak	META-INF/db2-jpa-changelog-1.8.0.xml	2020-04-20 12:14:04.56618	22	MARK_RAN	7:df8bc21027a4f7cbbb01f6344e89ce07	dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL		\N	3.5.4	\N	\N	7384842764
1.9.0	mposolda@redhat.com	META-INF/jpa-changelog-1.9.0.xml	2020-04-20 12:14:04.614843	23	EXECUTED	7:ed2dc7f799d19ac452cbcda56c929e47	update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=REALM; update tableName=REALM; customChange; dr...		\N	3.5.4	\N	\N	7384842764
1.9.1	keycloak	META-INF/jpa-changelog-1.9.1.xml	2020-04-20 12:14:04.621933	24	EXECUTED	7:80b5db88a5dda36ece5f235be8757615	modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=PUBLIC_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM		\N	3.5.4	\N	\N	7384842764
1.9.1	keycloak	META-INF/db2-jpa-changelog-1.9.1.xml	2020-04-20 12:14:04.625351	25	MARK_RAN	7:1437310ed1305a9b93f8848f301726ce	modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM		\N	3.5.4	\N	\N	7384842764
1.9.2	keycloak	META-INF/jpa-changelog-1.9.2.xml	2020-04-20 12:14:04.68719	26	EXECUTED	7:b82ffb34850fa0836be16deefc6a87c4	createIndex indexName=IDX_USER_EMAIL, tableName=USER_ENTITY; createIndex indexName=IDX_USER_ROLE_MAPPING, tableName=USER_ROLE_MAPPING; createIndex indexName=IDX_USER_GROUP_MAPPING, tableName=USER_GROUP_MEMBERSHIP; createIndex indexName=IDX_USER_CO...		\N	3.5.4	\N	\N	7384842764
authz-2.0.0	psilva@redhat.com	META-INF/jpa-changelog-authz-2.0.0.xml	2020-04-20 12:14:04.885905	27	EXECUTED	7:9cc98082921330d8d9266decdd4bd658	createTable tableName=RESOURCE_SERVER; addPrimaryKey constraintName=CONSTRAINT_FARS, tableName=RESOURCE_SERVER; addUniqueConstraint constraintName=UK_AU8TT6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER; createTable tableName=RESOURCE_SERVER_RESOU...		\N	3.5.4	\N	\N	7384842764
authz-2.5.1	psilva@redhat.com	META-INF/jpa-changelog-authz-2.5.1.xml	2020-04-20 12:14:04.900925	28	EXECUTED	7:03d64aeed9cb52b969bd30a7ac0db57e	update tableName=RESOURCE_SERVER_POLICY		\N	3.5.4	\N	\N	7384842764
2.1.0-KEYCLOAK-5461	bburke@redhat.com	META-INF/jpa-changelog-2.1.0.xml	2020-04-20 12:14:05.030341	29	EXECUTED	7:f1f9fd8710399d725b780f463c6b21cd	createTable tableName=BROKER_LINK; createTable tableName=FED_USER_ATTRIBUTE; createTable tableName=FED_USER_CONSENT; createTable tableName=FED_USER_CONSENT_ROLE; createTable tableName=FED_USER_CONSENT_PROT_MAPPER; createTable tableName=FED_USER_CR...		\N	3.5.4	\N	\N	7384842764
2.2.0	bburke@redhat.com	META-INF/jpa-changelog-2.2.0.xml	2020-04-20 12:14:05.050444	30	EXECUTED	7:53188c3eb1107546e6f765835705b6c1	addColumn tableName=ADMIN_EVENT_ENTITY; createTable tableName=CREDENTIAL_ATTRIBUTE; createTable tableName=FED_CREDENTIAL_ATTRIBUTE; modifyDataType columnName=VALUE, tableName=CREDENTIAL; addForeignKeyConstraint baseTableName=FED_CREDENTIAL_ATTRIBU...		\N	3.5.4	\N	\N	7384842764
2.3.0	bburke@redhat.com	META-INF/jpa-changelog-2.3.0.xml	2020-04-20 12:14:05.072899	31	EXECUTED	7:d6e6f3bc57a0c5586737d1351725d4d4	createTable tableName=FEDERATED_USER; addPrimaryKey constraintName=CONSTR_FEDERATED_USER, tableName=FEDERATED_USER; dropDefaultValue columnName=TOTP, tableName=USER_ENTITY; dropColumn columnName=TOTP, tableName=USER_ENTITY; addColumn tableName=IDE...		\N	3.5.4	\N	\N	7384842764
2.4.0	bburke@redhat.com	META-INF/jpa-changelog-2.4.0.xml	2020-04-20 12:14:05.078007	32	EXECUTED	7:454d604fbd755d9df3fd9c6329043aa5	customChange		\N	3.5.4	\N	\N	7384842764
2.5.0	bburke@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2020-04-20 12:14:05.084115	33	EXECUTED	7:57e98a3077e29caf562f7dbf80c72600	customChange; modifyDataType columnName=USER_ID, tableName=OFFLINE_USER_SESSION		\N	3.5.4	\N	\N	7384842764
2.5.0-unicode-oracle	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2020-04-20 12:14:05.086836	34	MARK_RAN	7:e4c7e8f2256210aee71ddc42f538b57a	modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...		\N	3.5.4	\N	\N	7384842764
2.5.0-unicode-other-dbs	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2020-04-20 12:14:05.11875	35	EXECUTED	7:09a43c97e49bc626460480aa1379b522	modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...		\N	3.5.4	\N	\N	7384842764
2.5.0-duplicate-email-support	slawomir@dabek.name	META-INF/jpa-changelog-2.5.0.xml	2020-04-20 12:14:05.125778	36	EXECUTED	7:26bfc7c74fefa9126f2ce702fb775553	addColumn tableName=REALM		\N	3.5.4	\N	\N	7384842764
2.5.0-unique-group-names	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2020-04-20 12:14:05.133985	37	EXECUTED	7:a161e2ae671a9020fff61e996a207377	addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	7384842764
2.5.1	bburke@redhat.com	META-INF/jpa-changelog-2.5.1.xml	2020-04-20 12:14:05.138956	38	EXECUTED	7:37fc1781855ac5388c494f1442b3f717	addColumn tableName=FED_USER_CONSENT		\N	3.5.4	\N	\N	7384842764
3.0.0	bburke@redhat.com	META-INF/jpa-changelog-3.0.0.xml	2020-04-20 12:14:05.144007	39	EXECUTED	7:13a27db0dae6049541136adad7261d27	addColumn tableName=IDENTITY_PROVIDER		\N	3.5.4	\N	\N	7384842764
3.2.0-fix	keycloak	META-INF/jpa-changelog-3.2.0.xml	2020-04-20 12:14:05.146699	40	MARK_RAN	7:550300617e3b59e8af3a6294df8248a3	addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS		\N	3.5.4	\N	\N	7384842764
3.2.0-fix-with-keycloak-5416	keycloak	META-INF/jpa-changelog-3.2.0.xml	2020-04-20 12:14:05.149732	41	MARK_RAN	7:e3a9482b8931481dc2772a5c07c44f17	dropIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS; addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS; createIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS		\N	3.5.4	\N	\N	7384842764
3.2.0-fix-offline-sessions	hmlnarik	META-INF/jpa-changelog-3.2.0.xml	2020-04-20 12:14:05.156825	42	EXECUTED	7:72b07d85a2677cb257edb02b408f332d	customChange		\N	3.5.4	\N	\N	7384842764
3.2.0-fixed	keycloak	META-INF/jpa-changelog-3.2.0.xml	2020-04-20 12:14:05.383049	43	EXECUTED	7:a72a7858967bd414835d19e04d880312	addColumn tableName=REALM; dropPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_PK2, tableName=OFFLINE_CLIENT_SESSION; dropColumn columnName=CLIENT_SESSION_ID, tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_P...		\N	3.5.4	\N	\N	7384842764
3.3.0	keycloak	META-INF/jpa-changelog-3.3.0.xml	2020-04-20 12:14:05.389893	44	EXECUTED	7:94edff7cf9ce179e7e85f0cd78a3cf2c	addColumn tableName=USER_ENTITY		\N	3.5.4	\N	\N	7384842764
authz-3.4.0.CR1-resource-server-pk-change-part2-KEYCLOAK-6095	hmlnarik@redhat.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2020-04-20 12:14:05.401808	46	EXECUTED	7:e64b5dcea7db06077c6e57d3b9e5ca14	customChange		\N	3.5.4	\N	\N	7384842764
authz-3.4.0.CR1-resource-server-pk-change-part3-fixed	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2020-04-20 12:14:05.404616	47	MARK_RAN	7:fd8cf02498f8b1e72496a20afc75178c	dropIndex indexName=IDX_RES_SERV_POL_RES_SERV, tableName=RESOURCE_SERVER_POLICY; dropIndex indexName=IDX_RES_SRV_RES_RES_SRV, tableName=RESOURCE_SERVER_RESOURCE; dropIndex indexName=IDX_RES_SRV_SCOPE_RES_SRV, tableName=RESOURCE_SERVER_SCOPE		\N	3.5.4	\N	\N	7384842764
authz-3.4.0.CR1-resource-server-pk-change-part3-fixed-nodropindex	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2020-04-20 12:14:05.460182	48	EXECUTED	7:542794f25aa2b1fbabb7e577d6646319	addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_POLICY; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_RESOURCE; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, ...		\N	3.5.4	\N	\N	7384842764
authn-3.4.0.CR1-refresh-token-max-reuse	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2020-04-20 12:14:05.466868	49	EXECUTED	7:edad604c882df12f74941dac3cc6d650	addColumn tableName=REALM		\N	3.5.4	\N	\N	7384842764
3.4.0	keycloak	META-INF/jpa-changelog-3.4.0.xml	2020-04-20 12:14:05.552755	50	EXECUTED	7:0f88b78b7b46480eb92690cbf5e44900	addPrimaryKey constraintName=CONSTRAINT_REALM_DEFAULT_ROLES, tableName=REALM_DEFAULT_ROLES; addPrimaryKey constraintName=CONSTRAINT_COMPOSITE_ROLE, tableName=COMPOSITE_ROLE; addPrimaryKey constraintName=CONSTR_REALM_DEFAULT_GROUPS, tableName=REALM...		\N	3.5.4	\N	\N	7384842764
3.4.0-KEYCLOAK-5230	hmlnarik@redhat.com	META-INF/jpa-changelog-3.4.0.xml	2020-04-20 12:14:05.611642	51	EXECUTED	7:d560e43982611d936457c327f872dd59	createIndex indexName=IDX_FU_ATTRIBUTE, tableName=FED_USER_ATTRIBUTE; createIndex indexName=IDX_FU_CONSENT, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CONSENT_RU, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CREDENTIAL, t...		\N	3.5.4	\N	\N	7384842764
3.4.1	psilva@redhat.com	META-INF/jpa-changelog-3.4.1.xml	2020-04-20 12:14:05.617538	52	EXECUTED	7:c155566c42b4d14ef07059ec3b3bbd8e	modifyDataType columnName=VALUE, tableName=CLIENT_ATTRIBUTES		\N	3.5.4	\N	\N	7384842764
3.4.2	keycloak	META-INF/jpa-changelog-3.4.2.xml	2020-04-20 12:14:05.622589	53	EXECUTED	7:b40376581f12d70f3c89ba8ddf5b7dea	update tableName=REALM		\N	3.5.4	\N	\N	7384842764
3.4.2-KEYCLOAK-5172	mkanis@redhat.com	META-INF/jpa-changelog-3.4.2.xml	2020-04-20 12:14:05.627117	54	EXECUTED	7:a1132cc395f7b95b3646146c2e38f168	update tableName=CLIENT		\N	3.5.4	\N	\N	7384842764
4.0.0-KEYCLOAK-6335	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2020-04-20 12:14:05.646851	55	EXECUTED	7:d8dc5d89c789105cfa7ca0e82cba60af	createTable tableName=CLIENT_AUTH_FLOW_BINDINGS; addPrimaryKey constraintName=C_CLI_FLOW_BIND, tableName=CLIENT_AUTH_FLOW_BINDINGS		\N	3.5.4	\N	\N	7384842764
4.0.0-CLEANUP-UNUSED-TABLE	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2020-04-20 12:14:05.655035	56	EXECUTED	7:7822e0165097182e8f653c35517656a3	dropTable tableName=CLIENT_IDENTITY_PROV_MAPPING		\N	3.5.4	\N	\N	7384842764
4.0.0-KEYCLOAK-6228	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2020-04-20 12:14:05.688377	57	EXECUTED	7:c6538c29b9c9a08f9e9ea2de5c2b6375	dropUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHOGM8UEWRT, tableName=USER_CONSENT; dropNotNullConstraint columnName=CLIENT_ID, tableName=USER_CONSENT; addColumn tableName=USER_CONSENT; addUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHO...		\N	3.5.4	\N	\N	7384842764
4.0.0-KEYCLOAK-5579-fixed	mposolda@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2020-04-20 12:14:05.875425	58	EXECUTED	7:6d4893e36de22369cf73bcb051ded875	dropForeignKeyConstraint baseTableName=CLIENT_TEMPLATE_ATTRIBUTES, constraintName=FK_CL_TEMPL_ATTR_TEMPL; renameTable newTableName=CLIENT_SCOPE_ATTRIBUTES, oldTableName=CLIENT_TEMPLATE_ATTRIBUTES; renameColumn newColumnName=SCOPE_ID, oldColumnName...		\N	3.5.4	\N	\N	7384842764
authz-4.0.0.CR1	psilva@redhat.com	META-INF/jpa-changelog-authz-4.0.0.CR1.xml	2020-04-20 12:14:05.927224	59	EXECUTED	7:57960fc0b0f0dd0563ea6f8b2e4a1707	createTable tableName=RESOURCE_SERVER_PERM_TICKET; addPrimaryKey constraintName=CONSTRAINT_FAPMT, tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRHO213XCX4WNKOG82SSPMT...		\N	3.5.4	\N	\N	7384842764
authz-4.0.0.Beta3	psilva@redhat.com	META-INF/jpa-changelog-authz-4.0.0.Beta3.xml	2020-04-20 12:14:05.937964	60	EXECUTED	7:2b4b8bff39944c7097977cc18dbceb3b	addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRPO2128CX4WNKOG82SSRFY, referencedTableName=RESOURCE_SERVER_POLICY		\N	3.5.4	\N	\N	7384842764
authz-4.2.0.Final	mhajas@redhat.com	META-INF/jpa-changelog-authz-4.2.0.Final.xml	2020-04-20 12:14:05.953083	61	EXECUTED	7:2aa42a964c59cd5b8ca9822340ba33a8	createTable tableName=RESOURCE_URIS; addForeignKeyConstraint baseTableName=RESOURCE_URIS, constraintName=FK_RESOURCE_SERVER_URIS, referencedTableName=RESOURCE_SERVER_RESOURCE; customChange; dropColumn columnName=URI, tableName=RESOURCE_SERVER_RESO...		\N	3.5.4	\N	\N	7384842764
authz-4.2.0.Final-KEYCLOAK-9944	hmlnarik@redhat.com	META-INF/jpa-changelog-authz-4.2.0.Final.xml	2020-04-20 12:14:05.962114	62	EXECUTED	7:9ac9e58545479929ba23f4a3087a0346	addPrimaryKey constraintName=CONSTRAINT_RESOUR_URIS_PK, tableName=RESOURCE_URIS		\N	3.5.4	\N	\N	7384842764
4.2.0-KEYCLOAK-6313	wadahiro@gmail.com	META-INF/jpa-changelog-4.2.0.xml	2020-04-20 12:14:05.96879	63	EXECUTED	7:14d407c35bc4fe1976867756bcea0c36	addColumn tableName=REQUIRED_ACTION_PROVIDER		\N	3.5.4	\N	\N	7384842764
4.3.0-KEYCLOAK-7984	wadahiro@gmail.com	META-INF/jpa-changelog-4.3.0.xml	2020-04-20 12:14:05.973368	64	EXECUTED	7:241a8030c748c8548e346adee548fa93	update tableName=REQUIRED_ACTION_PROVIDER		\N	3.5.4	\N	\N	7384842764
4.6.0-KEYCLOAK-7950	psilva@redhat.com	META-INF/jpa-changelog-4.6.0.xml	2020-04-20 12:14:05.979017	65	EXECUTED	7:7d3182f65a34fcc61e8d23def037dc3f	update tableName=RESOURCE_SERVER_RESOURCE		\N	3.5.4	\N	\N	7384842764
4.6.0-KEYCLOAK-8377	keycloak	META-INF/jpa-changelog-4.6.0.xml	2020-04-20 12:14:06.006014	66	EXECUTED	7:b30039e00a0b9715d430d1b0636728fa	createTable tableName=ROLE_ATTRIBUTE; addPrimaryKey constraintName=CONSTRAINT_ROLE_ATTRIBUTE_PK, tableName=ROLE_ATTRIBUTE; addForeignKeyConstraint baseTableName=ROLE_ATTRIBUTE, constraintName=FK_ROLE_ATTRIBUTE_ID, referencedTableName=KEYCLOAK_ROLE...		\N	3.5.4	\N	\N	7384842764
4.6.0-KEYCLOAK-8555	gideonray@gmail.com	META-INF/jpa-changelog-4.6.0.xml	2020-04-20 12:14:06.016735	67	EXECUTED	7:3797315ca61d531780f8e6f82f258159	createIndex indexName=IDX_COMPONENT_PROVIDER_TYPE, tableName=COMPONENT		\N	3.5.4	\N	\N	7384842764
4.7.0-KEYCLOAK-1267	sguilhen@redhat.com	META-INF/jpa-changelog-4.7.0.xml	2020-04-20 12:14:06.024241	68	EXECUTED	7:c7aa4c8d9573500c2d347c1941ff0301	addColumn tableName=REALM		\N	3.5.4	\N	\N	7384842764
4.7.0-KEYCLOAK-7275	keycloak	META-INF/jpa-changelog-4.7.0.xml	2020-04-20 12:14:06.042157	69	EXECUTED	7:b207faee394fc074a442ecd42185a5dd	renameColumn newColumnName=CREATED_ON, oldColumnName=LAST_SESSION_REFRESH, tableName=OFFLINE_USER_SESSION; addNotNullConstraint columnName=CREATED_ON, tableName=OFFLINE_USER_SESSION; addColumn tableName=OFFLINE_USER_SESSION; customChange; createIn...		\N	3.5.4	\N	\N	7384842764
4.8.0-KEYCLOAK-8835	sguilhen@redhat.com	META-INF/jpa-changelog-4.8.0.xml	2020-04-20 12:14:06.051705	70	EXECUTED	7:ab9a9762faaba4ddfa35514b212c4922	addNotNullConstraint columnName=SSO_MAX_LIFESPAN_REMEMBER_ME, tableName=REALM; addNotNullConstraint columnName=SSO_IDLE_TIMEOUT_REMEMBER_ME, tableName=REALM		\N	3.5.4	\N	\N	7384842764
authz-7.0.0-KEYCLOAK-10443	psilva@redhat.com	META-INF/jpa-changelog-authz-7.0.0.xml	2020-04-20 12:14:06.057292	71	EXECUTED	7:b9710f74515a6ccb51b72dc0d19df8c4	addColumn tableName=RESOURCE_SERVER		\N	3.5.4	\N	\N	7384842764
8.0.0-adding-credential-columns	keycloak	META-INF/jpa-changelog-8.0.0.xml	2020-04-20 12:14:06.065331	72	EXECUTED	7:ec9707ae4d4f0b7452fee20128083879	addColumn tableName=CREDENTIAL; addColumn tableName=FED_USER_CREDENTIAL		\N	3.5.4	\N	\N	7384842764
8.0.0-updating-credential-data-not-oracle	keycloak	META-INF/jpa-changelog-8.0.0.xml	2020-04-20 12:14:06.076028	73	EXECUTED	7:03b3f4b264c3c68ba082250a80b74216	update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL		\N	3.5.4	\N	\N	7384842764
8.0.0-updating-credential-data-oracle	keycloak	META-INF/jpa-changelog-8.0.0.xml	2020-04-20 12:14:06.079106	74	MARK_RAN	7:64c5728f5ca1f5aa4392217701c4fe23	update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL		\N	3.5.4	\N	\N	7384842764
8.0.0-credential-cleanup-fixed	keycloak	META-INF/jpa-changelog-8.0.0.xml	2020-04-20 12:14:06.108357	75	EXECUTED	7:b48da8c11a3d83ddd6b7d0c8c2219345	dropDefaultValue columnName=COUNTER, tableName=CREDENTIAL; dropDefaultValue columnName=DIGITS, tableName=CREDENTIAL; dropDefaultValue columnName=PERIOD, tableName=CREDENTIAL; dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; dropColumn ...		\N	3.5.4	\N	\N	7384842764
8.0.0-resource-tag-support	keycloak	META-INF/jpa-changelog-8.0.0.xml	2020-04-20 12:14:06.12431	76	EXECUTED	7:a73379915c23bfad3e8f5c6d5c0aa4bd	addColumn tableName=MIGRATION_MODEL; createIndex indexName=IDX_UPDATE_TIME, tableName=MIGRATION_MODEL		\N	3.5.4	\N	\N	7384842764
9.0.0-always-display-client	keycloak	META-INF/jpa-changelog-9.0.0.xml	2020-04-20 12:14:06.129586	77	EXECUTED	7:39e0073779aba192646291aa2332493d	addColumn tableName=CLIENT		\N	3.5.4	\N	\N	7384842764
9.0.0-drop-constraints-for-column-increase	keycloak	META-INF/jpa-changelog-9.0.0.xml	2020-04-20 12:14:06.133066	78	MARK_RAN	7:81f87368f00450799b4bf42ea0b3ec34	dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5PMT, tableName=RESOURCE_SERVER_PERM_TICKET; dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER_RESOURCE; dropPrimaryKey constraintName=CONSTRAINT_O...		\N	3.5.4	\N	\N	7384842764
9.0.0-increase-column-size-federated-fk	keycloak	META-INF/jpa-changelog-9.0.0.xml	2020-04-20 12:14:06.161098	79	EXECUTED	7:20b37422abb9fb6571c618148f013a15	modifyDataType columnName=CLIENT_ID, tableName=FED_USER_CONSENT; modifyDataType columnName=CLIENT_REALM_CONSTRAINT, tableName=KEYCLOAK_ROLE; modifyDataType columnName=OWNER, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=CLIENT_ID, ta...		\N	3.5.4	\N	\N	7384842764
9.0.0-recreate-constraints-after-column-increase	keycloak	META-INF/jpa-changelog-9.0.0.xml	2020-04-20 12:14:06.164495	80	MARK_RAN	7:1970bb6cfb5ee800736b95ad3fb3c78a	addNotNullConstraint columnName=CLIENT_ID, tableName=OFFLINE_CLIENT_SESSION; addNotNullConstraint columnName=OWNER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNullConstraint columnName=REQUESTER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNull...		\N	3.5.4	\N	\N	7384842764
9.0.1-add-index-to-client.client_id	keycloak	META-INF/jpa-changelog-9.0.1.xml	2020-04-20 12:14:06.173387	81	EXECUTED	7:45d9b25fc3b455d522d8dcc10a0f4c80	createIndex indexName=IDX_CLIENT_ID, tableName=CLIENT		\N	3.5.4	\N	\N	7384842764
9.0.1-KEYCLOAK-12579-drop-constraints	keycloak	META-INF/jpa-changelog-9.0.1.xml	2020-04-20 12:14:06.176256	82	MARK_RAN	7:890ae73712bc187a66c2813a724d037f	dropUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	7384842764
9.0.1-KEYCLOAK-12579-add-not-null-constraint	keycloak	META-INF/jpa-changelog-9.0.1.xml	2020-04-20 12:14:06.181659	83	EXECUTED	7:0a211980d27fafe3ff50d19a3a29b538	addNotNullConstraint columnName=PARENT_GROUP, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	7384842764
9.0.1-KEYCLOAK-12579-recreate-constraints	keycloak	META-INF/jpa-changelog-9.0.1.xml	2020-04-20 12:14:06.184894	84	MARK_RAN	7:a161e2ae671a9020fff61e996a207377	addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	7384842764
9.0.1-add-index-to-events	keycloak	META-INF/jpa-changelog-9.0.1.xml	2020-04-20 12:14:06.193174	85	EXECUTED	7:01c49302201bdf815b0a18d1f98a55dc	createIndex indexName=IDX_EVENT_TIME, tableName=EVENT_ENTITY		\N	3.5.4	\N	\N	7384842764
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
1000	f	\N	\N
1001	f	\N	\N
\.


--
-- Data for Name: default_client_scope; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.default_client_scope (realm_id, scope_id, default_scope) FROM stdin;
master	c29b50b9-5f8f-4115-abc8-6019fd964792	f
master	37aaf91a-24ef-4fe6-a2ea-95dfcad81dd5	t
master	ba301432-ee9e-4b1b-b737-d10ef9c47df4	t
master	6f69599a-8291-4901-b425-021ec1e2c2fd	t
master	65152274-9681-4374-89b0-d76bdcb64a77	f
master	ba52b31e-dae3-4fd0-9320-a695e933234e	f
master	1493365a-d059-4c99-882a-08f95c1d65d3	t
master	b78dd573-d751-49f3-9fef-6687c7c96184	t
master	1b99148c-91d4-41cf-a653-35f6272b5038	f
civil-add	e6f99d68-36ce-42bc-ba8c-7d359d4ed001	f
civil-add	d133efd5-8929-4e2e-814a-f1a40ab8886f	t
civil-add	dd1036f7-c11d-49da-829b-5c6337b19452	t
civil-add	bc79864e-90bd-487f-b474-73a864e85aaf	t
civil-add	37cd1341-8446-43d0-ba1e-7b1b1b310bd7	f
civil-add	b2e26561-f132-4e2d-b4f9-1d860548791a	f
civil-add	eec61978-bb6d-4959-8df6-bbadf6d1bee7	t
civil-add	e83e4acd-b6d4-4dbf-9e14-579fb796de1b	t
civil-add	e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b	f
\.


--
-- Data for Name: event_entity; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.event_entity (id, client_id, details_json, error, ip_address, realm_id, session_id, event_time, type, user_id) FROM stdin;
\.


--
-- Data for Name: fed_user_attribute; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_attribute (id, name, user_id, realm_id, storage_provider_id, value) FROM stdin;
\.


--
-- Data for Name: fed_user_consent; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_consent (id, client_id, user_id, realm_id, storage_provider_id, created_date, last_updated_date, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: fed_user_consent_cl_scope; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_consent_cl_scope (user_consent_id, scope_id) FROM stdin;
\.


--
-- Data for Name: fed_user_credential; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_credential (id, salt, type, created_date, user_id, realm_id, storage_provider_id, user_label, secret_data, credential_data, priority) FROM stdin;
\.


--
-- Data for Name: fed_user_group_membership; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_group_membership (group_id, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: fed_user_required_action; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_required_action (required_action, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: fed_user_role_mapping; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_role_mapping (role_id, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: federated_identity; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.federated_identity (identity_provider, realm_id, federated_user_id, federated_username, token, user_id) FROM stdin;
\.


--
-- Data for Name: federated_user; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.federated_user (id, storage_provider_id, realm_id) FROM stdin;
\.


--
-- Data for Name: group_attribute; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.group_attribute (id, name, value, group_id) FROM stdin;
\.


--
-- Data for Name: group_role_mapping; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.group_role_mapping (role_id, group_id) FROM stdin;
\.


--
-- Data for Name: identity_provider; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.identity_provider (internal_id, enabled, provider_alias, provider_id, store_token, authenticate_by_default, realm_id, add_token_role, trust_email, first_broker_login_flow_id, post_broker_login_flow_id, provider_display_name, link_only) FROM stdin;
\.


--
-- Data for Name: identity_provider_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.identity_provider_config (identity_provider_id, value, name) FROM stdin;
\.


--
-- Data for Name: identity_provider_mapper; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.identity_provider_mapper (id, name, idp_alias, idp_mapper_name, realm_id) FROM stdin;
\.


--
-- Data for Name: idp_mapper_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.idp_mapper_config (idp_mapper_id, value, name) FROM stdin;
\.


--
-- Data for Name: keycloak_group; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.keycloak_group (id, name, parent_group, realm_id) FROM stdin;
\.


--
-- Data for Name: keycloak_role; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.keycloak_role (id, client_realm_constraint, client_role, description, name, realm_id, client, realm) FROM stdin;
7cef0a8f-b179-4cc3-8e59-92899865ef50	master	f	${role_admin}	admin	master	\N	master
a0d1c070-b1f5-45d8-a7f1-e17ec5378025	master	f	${role_create-realm}	create-realm	master	\N	master
a09ea775-ac86-4680-8643-d6c3432974d8	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_create-client}	create-client	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
06fed710-9e97-4135-8696-4d3ae6a904da	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_view-realm}	view-realm	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
07d4a5a4-ce28-4a2f-a46d-670585d403da	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_view-users}	view-users	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
6ed0eb1a-2d28-4609-8916-1a4d4279ef3e	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_view-clients}	view-clients	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
a3b5e1b4-175f-4d40-b4a0-02e51c0fd67a	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_view-events}	view-events	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
424cba18-97bd-44a9-acd9-16bd76df0fec	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_view-identity-providers}	view-identity-providers	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
b5495d72-d02d-461a-8d01-3be369d13e47	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_view-authorization}	view-authorization	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
6a1d56a0-64ce-4363-9451-f78c9d63dcc0	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_manage-realm}	manage-realm	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
a04208ec-2c8b-476c-8373-a64597f1b8f5	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_manage-users}	manage-users	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
64919610-fde0-41d2-a8b7-555896c164c2	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_manage-clients}	manage-clients	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
257c8cc1-3697-4763-9056-7faf28eef3ec	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_manage-events}	manage-events	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
2a5a24f3-4ca3-413f-b074-1fc6619a97b1	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_manage-identity-providers}	manage-identity-providers	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
3ff78db0-9e54-4bf8-80d4-c971c0a23c0d	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_manage-authorization}	manage-authorization	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
a0446c4d-26dc-4eb5-acff-40091f5c18df	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_query-users}	query-users	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
42f11daa-2167-48f8-be43-52b1bebdff3c	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_query-clients}	query-clients	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
ce48e73a-a4c0-4977-9611-8a5bea8e1f1f	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_query-realms}	query-realms	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
c01a15d2-e609-4829-96b0-155c876e5384	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_query-groups}	query-groups	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
59641392-3138-44db-b74f-e4876f3e0def	3ea086ad-f833-40cc-8aff-3612b7138d17	t	${role_view-profile}	view-profile	master	3ea086ad-f833-40cc-8aff-3612b7138d17	\N
1635476e-d04b-41db-8429-6c164d2b0255	3ea086ad-f833-40cc-8aff-3612b7138d17	t	${role_manage-account}	manage-account	master	3ea086ad-f833-40cc-8aff-3612b7138d17	\N
38074eaa-34f9-49aa-a6bd-cc80942d7c20	3ea086ad-f833-40cc-8aff-3612b7138d17	t	${role_manage-account-links}	manage-account-links	master	3ea086ad-f833-40cc-8aff-3612b7138d17	\N
dc72c8b9-3b21-4898-bcf4-80d7b5423bae	3ea086ad-f833-40cc-8aff-3612b7138d17	t	${role_view-applications}	view-applications	master	3ea086ad-f833-40cc-8aff-3612b7138d17	\N
6f18f67d-524c-4e23-a042-9720db256328	3ea086ad-f833-40cc-8aff-3612b7138d17	t	${role_view-consent}	view-consent	master	3ea086ad-f833-40cc-8aff-3612b7138d17	\N
11406917-baf9-4811-bd5f-39854459d050	3ea086ad-f833-40cc-8aff-3612b7138d17	t	${role_manage-consent}	manage-consent	master	3ea086ad-f833-40cc-8aff-3612b7138d17	\N
75d22fac-f7d2-4cb2-8627-ae7b0e0d4aaf	4fa01505-dac2-40df-a136-31ebc1b58f4b	t	${role_read-token}	read-token	master	4fa01505-dac2-40df-a136-31ebc1b58f4b	\N
9086d304-b1b8-4f1f-b700-b35b040be43a	b268de25-483d-4179-a250-b7597b3ddbe1	t	${role_impersonation}	impersonation	master	b268de25-483d-4179-a250-b7597b3ddbe1	\N
43df3975-e020-4a1b-8b70-3be499933685	master	f	${role_offline-access}	offline_access	master	\N	master
05d5f5eb-1c19-4920-8002-7e42aaaa8a5e	master	f	${role_uma_authorization}	uma_authorization	master	\N	master
5fc97272-2e73-4c96-8c03-5581550d2e41	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_create-client}	create-client	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
aee714d4-45b4-407b-8f48-36c9bba52804	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_view-realm}	view-realm	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
e3a4f637-24ff-4c76-be6f-fc6446d88eed	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_view-users}	view-users	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
747a23aa-339e-431b-a2e3-d222c5c15905	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_view-clients}	view-clients	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
32e2fa3c-8ff9-44a2-8936-3d160e373510	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_view-events}	view-events	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
569488f0-238a-47cb-bba3-572810e5b616	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_view-identity-providers}	view-identity-providers	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
4ea1c0f5-d026-4242-b3da-c7b1ba0fb50f	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_view-authorization}	view-authorization	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
91b8a153-2d3a-48fb-9599-750fbb98874e	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_manage-realm}	manage-realm	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
749b1778-1c48-4816-b601-78888db90000	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_manage-users}	manage-users	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
e47332c8-ed27-42ac-9b65-c2db290647bc	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_manage-clients}	manage-clients	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
1aa21623-e153-442b-91db-7457347d49b4	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_manage-events}	manage-events	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
afd53ce1-06e8-4924-9621-f1a59b53b019	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_manage-identity-providers}	manage-identity-providers	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
a9baa2fb-2c02-4795-b1c4-41f16e431a5a	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_manage-authorization}	manage-authorization	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
ab6b990b-e56c-4bfb-b2e2-a9d6e9f853e7	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_query-users}	query-users	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
f8512649-3f9d-4911-b30d-b3a5694a0c00	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_query-clients}	query-clients	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
05941097-62d6-49e6-8585-2681ceb06fe5	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_query-realms}	query-realms	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
2d6bf747-965d-4a05-a6b2-470fe20ee58c	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_query-groups}	query-groups	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
73c48bc2-3261-4c56-978f-bb707f760001	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_realm-admin}	realm-admin	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
5fcf72d0-9dc9-4d91-8bdd-99b960f49b20	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_create-client}	create-client	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
6f23685d-67c5-4457-8ed2-6c8d418a4ae6	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_view-realm}	view-realm	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
f9477a23-1588-4e81-a01b-8041230e2318	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_view-users}	view-users	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
ee9e4447-b7b0-43d4-9347-1f78a1ae7654	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_view-clients}	view-clients	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
640256b2-6004-4982-9cb2-ecf3a57ceea7	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_view-events}	view-events	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
aab6bdf7-5e9a-445b-8c8c-44996dbba625	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_view-identity-providers}	view-identity-providers	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
66721053-ab64-49d1-af76-04b7604237b9	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_view-authorization}	view-authorization	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
f3e983cd-7117-49c2-9afd-04188de55c98	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_manage-realm}	manage-realm	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
ae117f03-7437-49dd-b495-32245321f847	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_manage-users}	manage-users	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
afd698c9-848d-4658-b87d-a4687c3c5a99	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_manage-clients}	manage-clients	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
da8dfcdb-1fec-4e19-8cdd-52eef1764304	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_manage-events}	manage-events	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
1f84f9da-aaa7-4f22-b33c-f2b31326316d	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_manage-identity-providers}	manage-identity-providers	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
ace074e7-3b3b-4282-a2c2-c03ce9c39351	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_manage-authorization}	manage-authorization	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
847672da-59a7-4e1c-816c-8fe59249b84a	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_query-users}	query-users	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
a2aaaddb-6073-4860-966c-8e5406b9abda	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_query-clients}	query-clients	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
ec342ed6-4607-4761-b10c-ca1bfaa9ad5d	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_query-realms}	query-realms	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
42ee34e3-1753-4305-869e-6e6b683a50a7	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_query-groups}	query-groups	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
c5c2aee8-1c25-45b3-8535-49bf502325f0	406cedd9-b0f8-4d0d-83f2-a5195cdfa910	t	${role_view-profile}	view-profile	civil-add	406cedd9-b0f8-4d0d-83f2-a5195cdfa910	\N
093235eb-d1b2-4732-bdaa-8db103645f63	406cedd9-b0f8-4d0d-83f2-a5195cdfa910	t	${role_manage-account}	manage-account	civil-add	406cedd9-b0f8-4d0d-83f2-a5195cdfa910	\N
a51d1d10-fb23-472a-b6aa-f0b088708df3	406cedd9-b0f8-4d0d-83f2-a5195cdfa910	t	${role_manage-account-links}	manage-account-links	civil-add	406cedd9-b0f8-4d0d-83f2-a5195cdfa910	\N
38daba30-921d-49d6-aa19-f79047687577	406cedd9-b0f8-4d0d-83f2-a5195cdfa910	t	${role_view-applications}	view-applications	civil-add	406cedd9-b0f8-4d0d-83f2-a5195cdfa910	\N
41c887b5-a089-4480-bf6b-0fe4ace67495	406cedd9-b0f8-4d0d-83f2-a5195cdfa910	t	${role_view-consent}	view-consent	civil-add	406cedd9-b0f8-4d0d-83f2-a5195cdfa910	\N
0733e130-b91f-41e0-b72f-9db2aaeb89bd	406cedd9-b0f8-4d0d-83f2-a5195cdfa910	t	${role_manage-consent}	manage-consent	civil-add	406cedd9-b0f8-4d0d-83f2-a5195cdfa910	\N
468fced7-d260-46a7-aa2b-58851a8d27fa	b5b9bb1d-8598-4e60-b13b-39da766bda6b	t	${role_impersonation}	impersonation	master	b5b9bb1d-8598-4e60-b13b-39da766bda6b	\N
e45d2bb7-eb10-4b00-b1a9-e2041f34e5df	a04e902f-9e19-4c75-9014-608c2de644e4	t	${role_impersonation}	impersonation	civil-add	a04e902f-9e19-4c75-9014-608c2de644e4	\N
2e30b0e5-a70a-4955-81b3-baedce8c0a69	243fbfa0-43bd-4c06-80e5-ce0619e3ca7c	t	${role_read-token}	read-token	civil-add	243fbfa0-43bd-4c06-80e5-ce0619e3ca7c	\N
159d8b16-c522-4128-8e3a-c29b28e80d34	civil-add	f	${role_offline-access}	offline_access	civil-add	\N	civil-add
9eeeb252-a3f2-485e-a87b-3dcdd240fe33	civil-add	f	${role_uma_authorization}	uma_authorization	civil-add	\N	civil-add
01a23297-519f-483d-882f-df599f518e58	1d826890-822d-4456-8d3b-6fdbe01475ba	t	\N	uma_protection	civil-add	1d826890-822d-4456-8d3b-6fdbe01475ba	\N
8b9961b3-5023-4a48-9bf7-3808fe878610	1d826890-822d-4456-8d3b-6fdbe01475ba	t	\N	ca-user	civil-add	1d826890-822d-4456-8d3b-6fdbe01475ba	\N
6a226a88-7fa5-4d9e-ab87-7f3342ac05c1	civil-add	f	\N	ca-user	civil-add	\N	civil-add
\.


--
-- Data for Name: migration_model; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.migration_model (id, version, update_time) FROM stdin;
k64tl	9.0.2	1587384853
\.


--
-- Data for Name: offline_client_session; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.offline_client_session (user_session_id, client_id, offline_flag, "timestamp", data, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: offline_user_session; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.offline_user_session (user_session_id, user_id, realm_id, created_on, offline_flag, data, last_session_refresh) FROM stdin;
\.


--
-- Data for Name: policy_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.policy_config (policy_id, name, value) FROM stdin;
c5509373-62f1-438d-b14d-2bfbd629dec0	code	// by default, grants any permission associated with this policy\n$evaluation.grant();\n
483cfade-75f6-42f8-aa9b-f262a04952c7	defaultResourceType	urn:civil-add_back:resources:default
\.


--
-- Data for Name: protocol_mapper; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.protocol_mapper (id, name, protocol, protocol_mapper_name, client_id, client_scope_id) FROM stdin;
f491cd39-5684-4576-9f8d-699502a6cdc6	audience resolve	openid-connect	oidc-audience-resolve-mapper	249c9f55-d8be-43d5-8e3d-7c744ded79f1	\N
177b4619-5036-4586-8cb0-cf840881d801	locale	openid-connect	oidc-usermodel-attribute-mapper	b03c1fb3-9455-4b87-a0eb-c25e1be32912	\N
9a19c552-37b9-4ab0-863b-fd8d618d9123	role list	saml	saml-role-list-mapper	\N	37aaf91a-24ef-4fe6-a2ea-95dfcad81dd5
705adc67-98f2-4077-929b-ac379bbe7ed1	full name	openid-connect	oidc-full-name-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
a75f897c-2f40-4f9d-8b91-33a4b21428f8	family name	openid-connect	oidc-usermodel-property-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
d569c3bb-c5d5-4e8a-a486-a1f199bf23ac	given name	openid-connect	oidc-usermodel-property-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
45419442-4a67-4123-9e7d-1f1171cf5abd	middle name	openid-connect	oidc-usermodel-attribute-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
01ac5506-ad2c-4fef-93d7-00767745acf0	nickname	openid-connect	oidc-usermodel-attribute-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
f1721fe2-4438-4154-af47-fa3efd86dfbd	username	openid-connect	oidc-usermodel-property-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
d88a49f0-54c2-4474-b3d0-6aae4744fbdc	profile	openid-connect	oidc-usermodel-attribute-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
d4f34541-4cc2-4aa9-a6c6-882221ddb53a	picture	openid-connect	oidc-usermodel-attribute-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
3802f904-159f-4282-8c41-5945f0354ffe	website	openid-connect	oidc-usermodel-attribute-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
e62338cf-544f-40b5-a93e-68622c1e5bbd	gender	openid-connect	oidc-usermodel-attribute-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
b2d69612-a218-4107-b040-1d8944e63c99	birthdate	openid-connect	oidc-usermodel-attribute-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
da9f7275-584e-4657-b84e-3374cfe68f97	zoneinfo	openid-connect	oidc-usermodel-attribute-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
2e9f5627-4779-4bb2-9c00-e87d797d8456	locale	openid-connect	oidc-usermodel-attribute-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
b6969ced-57c5-4e65-8d03-9086898f4138	updated at	openid-connect	oidc-usermodel-attribute-mapper	\N	ba301432-ee9e-4b1b-b737-d10ef9c47df4
5ecf31d6-4b71-4b07-8089-04f4094fd14f	email	openid-connect	oidc-usermodel-property-mapper	\N	6f69599a-8291-4901-b425-021ec1e2c2fd
34e79634-3cc2-438b-bcd2-221a48adbb91	email verified	openid-connect	oidc-usermodel-property-mapper	\N	6f69599a-8291-4901-b425-021ec1e2c2fd
6c08166d-03b7-4147-9200-97493f09fb07	address	openid-connect	oidc-address-mapper	\N	65152274-9681-4374-89b0-d76bdcb64a77
17abe44b-600c-4560-8bbd-c88362548d8d	phone number	openid-connect	oidc-usermodel-attribute-mapper	\N	ba52b31e-dae3-4fd0-9320-a695e933234e
3beb283c-72a6-41b8-a527-f81d26bafb61	phone number verified	openid-connect	oidc-usermodel-attribute-mapper	\N	ba52b31e-dae3-4fd0-9320-a695e933234e
b69739b4-ed30-42b8-ae81-73d6b62c22ca	realm roles	openid-connect	oidc-usermodel-realm-role-mapper	\N	1493365a-d059-4c99-882a-08f95c1d65d3
83ae4f91-bb5d-4533-9a33-7feba1a1cedf	client roles	openid-connect	oidc-usermodel-client-role-mapper	\N	1493365a-d059-4c99-882a-08f95c1d65d3
41bc9efe-3536-42a1-b206-ce4a29532841	audience resolve	openid-connect	oidc-audience-resolve-mapper	\N	1493365a-d059-4c99-882a-08f95c1d65d3
22829e89-96ee-4e69-b184-6499e24d1342	allowed web origins	openid-connect	oidc-allowed-origins-mapper	\N	b78dd573-d751-49f3-9fef-6687c7c96184
d24a6fff-911b-498b-a4a0-f11916faedf8	upn	openid-connect	oidc-usermodel-property-mapper	\N	1b99148c-91d4-41cf-a653-35f6272b5038
d616d411-ff3b-48ec-ad4d-ba5060d57740	groups	openid-connect	oidc-usermodel-realm-role-mapper	\N	1b99148c-91d4-41cf-a653-35f6272b5038
b7812849-7da3-4db5-b65a-ec5bf9bce7eb	audience resolve	openid-connect	oidc-audience-resolve-mapper	9059b8a4-352b-4ab8-ba9d-28b739cd80b6	\N
c5e74724-df92-4595-9097-17430bbdf5d8	role list	saml	saml-role-list-mapper	\N	d133efd5-8929-4e2e-814a-f1a40ab8886f
5786e340-844e-4f2d-b65c-0ae16847d745	full name	openid-connect	oidc-full-name-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
78362876-15d6-4e15-b5c6-5e4da359ec4a	family name	openid-connect	oidc-usermodel-property-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
bee9b862-76a4-4aed-8a9c-d41673cfc900	given name	openid-connect	oidc-usermodel-property-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
86d95dc5-706a-423f-a6ee-b3af9161c2e9	middle name	openid-connect	oidc-usermodel-attribute-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
274dd6c2-f4c9-4a27-97c3-7109b4520b84	nickname	openid-connect	oidc-usermodel-attribute-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
cdb5b38d-cff2-4286-800e-54347fefe6f7	username	openid-connect	oidc-usermodel-property-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
21b8a595-85fb-4db0-aef3-0c5515f8bcfb	profile	openid-connect	oidc-usermodel-attribute-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
93b2fb27-d425-45b0-97c3-46a903b8b50c	picture	openid-connect	oidc-usermodel-attribute-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
7a56defd-abbd-4283-98c8-77063c3cdf68	website	openid-connect	oidc-usermodel-attribute-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
74bb5f01-487e-47b3-9e1e-d2c407a469b0	gender	openid-connect	oidc-usermodel-attribute-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
62c6edbb-fa4b-4237-a454-3c9f2c3d59a4	birthdate	openid-connect	oidc-usermodel-attribute-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
a3530b02-0a4a-4209-903c-df35dc141587	zoneinfo	openid-connect	oidc-usermodel-attribute-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
e0a94abe-fd58-4ed8-a766-0b606ded4fc1	locale	openid-connect	oidc-usermodel-attribute-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
df2d5cf6-b340-4aac-8711-c759479bf5b6	updated at	openid-connect	oidc-usermodel-attribute-mapper	\N	dd1036f7-c11d-49da-829b-5c6337b19452
60a25bee-37f3-4144-a5a9-8963e48498fa	email	openid-connect	oidc-usermodel-property-mapper	\N	bc79864e-90bd-487f-b474-73a864e85aaf
9481b23d-c41a-4649-8b20-563d97bca837	email verified	openid-connect	oidc-usermodel-property-mapper	\N	bc79864e-90bd-487f-b474-73a864e85aaf
5fff92cf-008c-4265-a68f-d8027799d064	address	openid-connect	oidc-address-mapper	\N	37cd1341-8446-43d0-ba1e-7b1b1b310bd7
e9ecd319-62a7-47b3-aaac-1d2f75ca7dd5	phone number	openid-connect	oidc-usermodel-attribute-mapper	\N	b2e26561-f132-4e2d-b4f9-1d860548791a
19f29476-f5b2-45aa-9e9b-7115136c4718	phone number verified	openid-connect	oidc-usermodel-attribute-mapper	\N	b2e26561-f132-4e2d-b4f9-1d860548791a
f8c1a1fd-e561-40fe-8507-b1bc8a4b1d5d	realm roles	openid-connect	oidc-usermodel-realm-role-mapper	\N	eec61978-bb6d-4959-8df6-bbadf6d1bee7
65615e5b-4f35-499b-bd8a-3c870c49499f	client roles	openid-connect	oidc-usermodel-client-role-mapper	\N	eec61978-bb6d-4959-8df6-bbadf6d1bee7
47c74057-fbac-4c8a-b10c-5aa588a4d978	audience resolve	openid-connect	oidc-audience-resolve-mapper	\N	eec61978-bb6d-4959-8df6-bbadf6d1bee7
5244abc4-79e8-4bed-8031-1a2040b3e445	allowed web origins	openid-connect	oidc-allowed-origins-mapper	\N	e83e4acd-b6d4-4dbf-9e14-579fb796de1b
d7e7061d-742d-4ba0-8e7b-12cb2a2f8e96	upn	openid-connect	oidc-usermodel-property-mapper	\N	e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b
7075553b-ec91-4753-a660-f253810a56f5	groups	openid-connect	oidc-usermodel-realm-role-mapper	\N	e4dc5136-ae7e-482d-bd3f-c9f65f5bbf5b
55c61dfb-c119-4f49-9622-9a5801682784	locale	openid-connect	oidc-usermodel-attribute-mapper	a9b463b8-18bb-49f5-a3bd-9e399734c104	\N
65e08326-21af-44f1-b1ea-289c09dd0afc	Client ID	openid-connect	oidc-usersessionmodel-note-mapper	1d826890-822d-4456-8d3b-6fdbe01475ba	\N
57011c8a-cb7a-4d24-836f-6982b72c3c40	Client Host	openid-connect	oidc-usersessionmodel-note-mapper	1d826890-822d-4456-8d3b-6fdbe01475ba	\N
f528237b-6ad3-474d-a29d-3870b3ba5eb7	Client IP Address	openid-connect	oidc-usersessionmodel-note-mapper	1d826890-822d-4456-8d3b-6fdbe01475ba	\N
64084d9c-01f4-4066-90c9-c2987955fdd6	Username	openid-connect	oidc-usermodel-property-mapper	1d826890-822d-4456-8d3b-6fdbe01475ba	\N
\.


--
-- Data for Name: protocol_mapper_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.protocol_mapper_config (protocol_mapper_id, value, name) FROM stdin;
177b4619-5036-4586-8cb0-cf840881d801	true	userinfo.token.claim
177b4619-5036-4586-8cb0-cf840881d801	locale	user.attribute
177b4619-5036-4586-8cb0-cf840881d801	true	id.token.claim
177b4619-5036-4586-8cb0-cf840881d801	true	access.token.claim
177b4619-5036-4586-8cb0-cf840881d801	locale	claim.name
177b4619-5036-4586-8cb0-cf840881d801	String	jsonType.label
9a19c552-37b9-4ab0-863b-fd8d618d9123	false	single
9a19c552-37b9-4ab0-863b-fd8d618d9123	Basic	attribute.nameformat
9a19c552-37b9-4ab0-863b-fd8d618d9123	Role	attribute.name
705adc67-98f2-4077-929b-ac379bbe7ed1	true	userinfo.token.claim
705adc67-98f2-4077-929b-ac379bbe7ed1	true	id.token.claim
705adc67-98f2-4077-929b-ac379bbe7ed1	true	access.token.claim
a75f897c-2f40-4f9d-8b91-33a4b21428f8	true	userinfo.token.claim
a75f897c-2f40-4f9d-8b91-33a4b21428f8	lastName	user.attribute
a75f897c-2f40-4f9d-8b91-33a4b21428f8	true	id.token.claim
a75f897c-2f40-4f9d-8b91-33a4b21428f8	true	access.token.claim
a75f897c-2f40-4f9d-8b91-33a4b21428f8	family_name	claim.name
a75f897c-2f40-4f9d-8b91-33a4b21428f8	String	jsonType.label
d569c3bb-c5d5-4e8a-a486-a1f199bf23ac	true	userinfo.token.claim
d569c3bb-c5d5-4e8a-a486-a1f199bf23ac	firstName	user.attribute
d569c3bb-c5d5-4e8a-a486-a1f199bf23ac	true	id.token.claim
d569c3bb-c5d5-4e8a-a486-a1f199bf23ac	true	access.token.claim
d569c3bb-c5d5-4e8a-a486-a1f199bf23ac	given_name	claim.name
d569c3bb-c5d5-4e8a-a486-a1f199bf23ac	String	jsonType.label
45419442-4a67-4123-9e7d-1f1171cf5abd	true	userinfo.token.claim
45419442-4a67-4123-9e7d-1f1171cf5abd	middleName	user.attribute
45419442-4a67-4123-9e7d-1f1171cf5abd	true	id.token.claim
45419442-4a67-4123-9e7d-1f1171cf5abd	true	access.token.claim
45419442-4a67-4123-9e7d-1f1171cf5abd	middle_name	claim.name
45419442-4a67-4123-9e7d-1f1171cf5abd	String	jsonType.label
01ac5506-ad2c-4fef-93d7-00767745acf0	true	userinfo.token.claim
01ac5506-ad2c-4fef-93d7-00767745acf0	nickname	user.attribute
01ac5506-ad2c-4fef-93d7-00767745acf0	true	id.token.claim
01ac5506-ad2c-4fef-93d7-00767745acf0	true	access.token.claim
01ac5506-ad2c-4fef-93d7-00767745acf0	nickname	claim.name
01ac5506-ad2c-4fef-93d7-00767745acf0	String	jsonType.label
f1721fe2-4438-4154-af47-fa3efd86dfbd	true	userinfo.token.claim
f1721fe2-4438-4154-af47-fa3efd86dfbd	username	user.attribute
f1721fe2-4438-4154-af47-fa3efd86dfbd	true	id.token.claim
f1721fe2-4438-4154-af47-fa3efd86dfbd	true	access.token.claim
f1721fe2-4438-4154-af47-fa3efd86dfbd	preferred_username	claim.name
f1721fe2-4438-4154-af47-fa3efd86dfbd	String	jsonType.label
d88a49f0-54c2-4474-b3d0-6aae4744fbdc	true	userinfo.token.claim
d88a49f0-54c2-4474-b3d0-6aae4744fbdc	profile	user.attribute
d88a49f0-54c2-4474-b3d0-6aae4744fbdc	true	id.token.claim
d88a49f0-54c2-4474-b3d0-6aae4744fbdc	true	access.token.claim
d88a49f0-54c2-4474-b3d0-6aae4744fbdc	profile	claim.name
d88a49f0-54c2-4474-b3d0-6aae4744fbdc	String	jsonType.label
d4f34541-4cc2-4aa9-a6c6-882221ddb53a	true	userinfo.token.claim
d4f34541-4cc2-4aa9-a6c6-882221ddb53a	picture	user.attribute
d4f34541-4cc2-4aa9-a6c6-882221ddb53a	true	id.token.claim
d4f34541-4cc2-4aa9-a6c6-882221ddb53a	true	access.token.claim
d4f34541-4cc2-4aa9-a6c6-882221ddb53a	picture	claim.name
d4f34541-4cc2-4aa9-a6c6-882221ddb53a	String	jsonType.label
3802f904-159f-4282-8c41-5945f0354ffe	true	userinfo.token.claim
3802f904-159f-4282-8c41-5945f0354ffe	website	user.attribute
3802f904-159f-4282-8c41-5945f0354ffe	true	id.token.claim
3802f904-159f-4282-8c41-5945f0354ffe	true	access.token.claim
3802f904-159f-4282-8c41-5945f0354ffe	website	claim.name
3802f904-159f-4282-8c41-5945f0354ffe	String	jsonType.label
e62338cf-544f-40b5-a93e-68622c1e5bbd	true	userinfo.token.claim
e62338cf-544f-40b5-a93e-68622c1e5bbd	gender	user.attribute
e62338cf-544f-40b5-a93e-68622c1e5bbd	true	id.token.claim
e62338cf-544f-40b5-a93e-68622c1e5bbd	true	access.token.claim
e62338cf-544f-40b5-a93e-68622c1e5bbd	gender	claim.name
e62338cf-544f-40b5-a93e-68622c1e5bbd	String	jsonType.label
b2d69612-a218-4107-b040-1d8944e63c99	true	userinfo.token.claim
b2d69612-a218-4107-b040-1d8944e63c99	birthdate	user.attribute
b2d69612-a218-4107-b040-1d8944e63c99	true	id.token.claim
b2d69612-a218-4107-b040-1d8944e63c99	true	access.token.claim
b2d69612-a218-4107-b040-1d8944e63c99	birthdate	claim.name
b2d69612-a218-4107-b040-1d8944e63c99	String	jsonType.label
da9f7275-584e-4657-b84e-3374cfe68f97	true	userinfo.token.claim
da9f7275-584e-4657-b84e-3374cfe68f97	zoneinfo	user.attribute
da9f7275-584e-4657-b84e-3374cfe68f97	true	id.token.claim
da9f7275-584e-4657-b84e-3374cfe68f97	true	access.token.claim
da9f7275-584e-4657-b84e-3374cfe68f97	zoneinfo	claim.name
da9f7275-584e-4657-b84e-3374cfe68f97	String	jsonType.label
2e9f5627-4779-4bb2-9c00-e87d797d8456	true	userinfo.token.claim
2e9f5627-4779-4bb2-9c00-e87d797d8456	locale	user.attribute
2e9f5627-4779-4bb2-9c00-e87d797d8456	true	id.token.claim
2e9f5627-4779-4bb2-9c00-e87d797d8456	true	access.token.claim
2e9f5627-4779-4bb2-9c00-e87d797d8456	locale	claim.name
2e9f5627-4779-4bb2-9c00-e87d797d8456	String	jsonType.label
b6969ced-57c5-4e65-8d03-9086898f4138	true	userinfo.token.claim
b6969ced-57c5-4e65-8d03-9086898f4138	updatedAt	user.attribute
b6969ced-57c5-4e65-8d03-9086898f4138	true	id.token.claim
b6969ced-57c5-4e65-8d03-9086898f4138	true	access.token.claim
b6969ced-57c5-4e65-8d03-9086898f4138	updated_at	claim.name
b6969ced-57c5-4e65-8d03-9086898f4138	String	jsonType.label
5ecf31d6-4b71-4b07-8089-04f4094fd14f	true	userinfo.token.claim
5ecf31d6-4b71-4b07-8089-04f4094fd14f	email	user.attribute
5ecf31d6-4b71-4b07-8089-04f4094fd14f	true	id.token.claim
5ecf31d6-4b71-4b07-8089-04f4094fd14f	true	access.token.claim
5ecf31d6-4b71-4b07-8089-04f4094fd14f	email	claim.name
5ecf31d6-4b71-4b07-8089-04f4094fd14f	String	jsonType.label
34e79634-3cc2-438b-bcd2-221a48adbb91	true	userinfo.token.claim
34e79634-3cc2-438b-bcd2-221a48adbb91	emailVerified	user.attribute
34e79634-3cc2-438b-bcd2-221a48adbb91	true	id.token.claim
34e79634-3cc2-438b-bcd2-221a48adbb91	true	access.token.claim
34e79634-3cc2-438b-bcd2-221a48adbb91	email_verified	claim.name
34e79634-3cc2-438b-bcd2-221a48adbb91	boolean	jsonType.label
6c08166d-03b7-4147-9200-97493f09fb07	formatted	user.attribute.formatted
6c08166d-03b7-4147-9200-97493f09fb07	country	user.attribute.country
6c08166d-03b7-4147-9200-97493f09fb07	postal_code	user.attribute.postal_code
6c08166d-03b7-4147-9200-97493f09fb07	true	userinfo.token.claim
6c08166d-03b7-4147-9200-97493f09fb07	street	user.attribute.street
6c08166d-03b7-4147-9200-97493f09fb07	true	id.token.claim
6c08166d-03b7-4147-9200-97493f09fb07	region	user.attribute.region
6c08166d-03b7-4147-9200-97493f09fb07	true	access.token.claim
6c08166d-03b7-4147-9200-97493f09fb07	locality	user.attribute.locality
17abe44b-600c-4560-8bbd-c88362548d8d	true	userinfo.token.claim
17abe44b-600c-4560-8bbd-c88362548d8d	phoneNumber	user.attribute
17abe44b-600c-4560-8bbd-c88362548d8d	true	id.token.claim
17abe44b-600c-4560-8bbd-c88362548d8d	true	access.token.claim
17abe44b-600c-4560-8bbd-c88362548d8d	phone_number	claim.name
17abe44b-600c-4560-8bbd-c88362548d8d	String	jsonType.label
3beb283c-72a6-41b8-a527-f81d26bafb61	true	userinfo.token.claim
3beb283c-72a6-41b8-a527-f81d26bafb61	phoneNumberVerified	user.attribute
3beb283c-72a6-41b8-a527-f81d26bafb61	true	id.token.claim
3beb283c-72a6-41b8-a527-f81d26bafb61	true	access.token.claim
3beb283c-72a6-41b8-a527-f81d26bafb61	phone_number_verified	claim.name
3beb283c-72a6-41b8-a527-f81d26bafb61	boolean	jsonType.label
b69739b4-ed30-42b8-ae81-73d6b62c22ca	true	multivalued
b69739b4-ed30-42b8-ae81-73d6b62c22ca	foo	user.attribute
b69739b4-ed30-42b8-ae81-73d6b62c22ca	true	access.token.claim
b69739b4-ed30-42b8-ae81-73d6b62c22ca	realm_access.roles	claim.name
b69739b4-ed30-42b8-ae81-73d6b62c22ca	String	jsonType.label
83ae4f91-bb5d-4533-9a33-7feba1a1cedf	true	multivalued
83ae4f91-bb5d-4533-9a33-7feba1a1cedf	foo	user.attribute
83ae4f91-bb5d-4533-9a33-7feba1a1cedf	true	access.token.claim
83ae4f91-bb5d-4533-9a33-7feba1a1cedf	resource_access.${client_id}.roles	claim.name
83ae4f91-bb5d-4533-9a33-7feba1a1cedf	String	jsonType.label
d24a6fff-911b-498b-a4a0-f11916faedf8	true	userinfo.token.claim
d24a6fff-911b-498b-a4a0-f11916faedf8	username	user.attribute
d24a6fff-911b-498b-a4a0-f11916faedf8	true	id.token.claim
d24a6fff-911b-498b-a4a0-f11916faedf8	true	access.token.claim
d24a6fff-911b-498b-a4a0-f11916faedf8	upn	claim.name
d24a6fff-911b-498b-a4a0-f11916faedf8	String	jsonType.label
d616d411-ff3b-48ec-ad4d-ba5060d57740	true	multivalued
d616d411-ff3b-48ec-ad4d-ba5060d57740	foo	user.attribute
d616d411-ff3b-48ec-ad4d-ba5060d57740	true	id.token.claim
d616d411-ff3b-48ec-ad4d-ba5060d57740	true	access.token.claim
d616d411-ff3b-48ec-ad4d-ba5060d57740	groups	claim.name
d616d411-ff3b-48ec-ad4d-ba5060d57740	String	jsonType.label
c5e74724-df92-4595-9097-17430bbdf5d8	false	single
c5e74724-df92-4595-9097-17430bbdf5d8	Basic	attribute.nameformat
c5e74724-df92-4595-9097-17430bbdf5d8	Role	attribute.name
5786e340-844e-4f2d-b65c-0ae16847d745	true	userinfo.token.claim
5786e340-844e-4f2d-b65c-0ae16847d745	true	id.token.claim
5786e340-844e-4f2d-b65c-0ae16847d745	true	access.token.claim
78362876-15d6-4e15-b5c6-5e4da359ec4a	true	userinfo.token.claim
78362876-15d6-4e15-b5c6-5e4da359ec4a	lastName	user.attribute
78362876-15d6-4e15-b5c6-5e4da359ec4a	true	id.token.claim
78362876-15d6-4e15-b5c6-5e4da359ec4a	true	access.token.claim
78362876-15d6-4e15-b5c6-5e4da359ec4a	family_name	claim.name
78362876-15d6-4e15-b5c6-5e4da359ec4a	String	jsonType.label
bee9b862-76a4-4aed-8a9c-d41673cfc900	true	userinfo.token.claim
bee9b862-76a4-4aed-8a9c-d41673cfc900	firstName	user.attribute
bee9b862-76a4-4aed-8a9c-d41673cfc900	true	id.token.claim
bee9b862-76a4-4aed-8a9c-d41673cfc900	true	access.token.claim
bee9b862-76a4-4aed-8a9c-d41673cfc900	given_name	claim.name
bee9b862-76a4-4aed-8a9c-d41673cfc900	String	jsonType.label
86d95dc5-706a-423f-a6ee-b3af9161c2e9	true	userinfo.token.claim
86d95dc5-706a-423f-a6ee-b3af9161c2e9	middleName	user.attribute
86d95dc5-706a-423f-a6ee-b3af9161c2e9	true	id.token.claim
86d95dc5-706a-423f-a6ee-b3af9161c2e9	true	access.token.claim
86d95dc5-706a-423f-a6ee-b3af9161c2e9	middle_name	claim.name
86d95dc5-706a-423f-a6ee-b3af9161c2e9	String	jsonType.label
274dd6c2-f4c9-4a27-97c3-7109b4520b84	true	userinfo.token.claim
274dd6c2-f4c9-4a27-97c3-7109b4520b84	nickname	user.attribute
274dd6c2-f4c9-4a27-97c3-7109b4520b84	true	id.token.claim
274dd6c2-f4c9-4a27-97c3-7109b4520b84	true	access.token.claim
274dd6c2-f4c9-4a27-97c3-7109b4520b84	nickname	claim.name
274dd6c2-f4c9-4a27-97c3-7109b4520b84	String	jsonType.label
cdb5b38d-cff2-4286-800e-54347fefe6f7	true	userinfo.token.claim
cdb5b38d-cff2-4286-800e-54347fefe6f7	username	user.attribute
cdb5b38d-cff2-4286-800e-54347fefe6f7	true	id.token.claim
cdb5b38d-cff2-4286-800e-54347fefe6f7	true	access.token.claim
cdb5b38d-cff2-4286-800e-54347fefe6f7	preferred_username	claim.name
cdb5b38d-cff2-4286-800e-54347fefe6f7	String	jsonType.label
21b8a595-85fb-4db0-aef3-0c5515f8bcfb	true	userinfo.token.claim
21b8a595-85fb-4db0-aef3-0c5515f8bcfb	profile	user.attribute
21b8a595-85fb-4db0-aef3-0c5515f8bcfb	true	id.token.claim
21b8a595-85fb-4db0-aef3-0c5515f8bcfb	true	access.token.claim
21b8a595-85fb-4db0-aef3-0c5515f8bcfb	profile	claim.name
21b8a595-85fb-4db0-aef3-0c5515f8bcfb	String	jsonType.label
93b2fb27-d425-45b0-97c3-46a903b8b50c	true	userinfo.token.claim
93b2fb27-d425-45b0-97c3-46a903b8b50c	picture	user.attribute
93b2fb27-d425-45b0-97c3-46a903b8b50c	true	id.token.claim
93b2fb27-d425-45b0-97c3-46a903b8b50c	true	access.token.claim
93b2fb27-d425-45b0-97c3-46a903b8b50c	picture	claim.name
93b2fb27-d425-45b0-97c3-46a903b8b50c	String	jsonType.label
7a56defd-abbd-4283-98c8-77063c3cdf68	true	userinfo.token.claim
7a56defd-abbd-4283-98c8-77063c3cdf68	website	user.attribute
7a56defd-abbd-4283-98c8-77063c3cdf68	true	id.token.claim
7a56defd-abbd-4283-98c8-77063c3cdf68	true	access.token.claim
7a56defd-abbd-4283-98c8-77063c3cdf68	website	claim.name
7a56defd-abbd-4283-98c8-77063c3cdf68	String	jsonType.label
74bb5f01-487e-47b3-9e1e-d2c407a469b0	true	userinfo.token.claim
74bb5f01-487e-47b3-9e1e-d2c407a469b0	gender	user.attribute
74bb5f01-487e-47b3-9e1e-d2c407a469b0	true	id.token.claim
74bb5f01-487e-47b3-9e1e-d2c407a469b0	true	access.token.claim
74bb5f01-487e-47b3-9e1e-d2c407a469b0	gender	claim.name
74bb5f01-487e-47b3-9e1e-d2c407a469b0	String	jsonType.label
62c6edbb-fa4b-4237-a454-3c9f2c3d59a4	true	userinfo.token.claim
62c6edbb-fa4b-4237-a454-3c9f2c3d59a4	birthdate	user.attribute
62c6edbb-fa4b-4237-a454-3c9f2c3d59a4	true	id.token.claim
62c6edbb-fa4b-4237-a454-3c9f2c3d59a4	true	access.token.claim
62c6edbb-fa4b-4237-a454-3c9f2c3d59a4	birthdate	claim.name
62c6edbb-fa4b-4237-a454-3c9f2c3d59a4	String	jsonType.label
a3530b02-0a4a-4209-903c-df35dc141587	true	userinfo.token.claim
a3530b02-0a4a-4209-903c-df35dc141587	zoneinfo	user.attribute
a3530b02-0a4a-4209-903c-df35dc141587	true	id.token.claim
a3530b02-0a4a-4209-903c-df35dc141587	true	access.token.claim
a3530b02-0a4a-4209-903c-df35dc141587	zoneinfo	claim.name
a3530b02-0a4a-4209-903c-df35dc141587	String	jsonType.label
e0a94abe-fd58-4ed8-a766-0b606ded4fc1	true	userinfo.token.claim
e0a94abe-fd58-4ed8-a766-0b606ded4fc1	locale	user.attribute
e0a94abe-fd58-4ed8-a766-0b606ded4fc1	true	id.token.claim
e0a94abe-fd58-4ed8-a766-0b606ded4fc1	true	access.token.claim
e0a94abe-fd58-4ed8-a766-0b606ded4fc1	locale	claim.name
e0a94abe-fd58-4ed8-a766-0b606ded4fc1	String	jsonType.label
df2d5cf6-b340-4aac-8711-c759479bf5b6	true	userinfo.token.claim
df2d5cf6-b340-4aac-8711-c759479bf5b6	updatedAt	user.attribute
df2d5cf6-b340-4aac-8711-c759479bf5b6	true	id.token.claim
df2d5cf6-b340-4aac-8711-c759479bf5b6	true	access.token.claim
df2d5cf6-b340-4aac-8711-c759479bf5b6	updated_at	claim.name
df2d5cf6-b340-4aac-8711-c759479bf5b6	String	jsonType.label
60a25bee-37f3-4144-a5a9-8963e48498fa	true	userinfo.token.claim
60a25bee-37f3-4144-a5a9-8963e48498fa	email	user.attribute
60a25bee-37f3-4144-a5a9-8963e48498fa	true	id.token.claim
60a25bee-37f3-4144-a5a9-8963e48498fa	true	access.token.claim
60a25bee-37f3-4144-a5a9-8963e48498fa	email	claim.name
60a25bee-37f3-4144-a5a9-8963e48498fa	String	jsonType.label
9481b23d-c41a-4649-8b20-563d97bca837	true	userinfo.token.claim
9481b23d-c41a-4649-8b20-563d97bca837	emailVerified	user.attribute
9481b23d-c41a-4649-8b20-563d97bca837	true	id.token.claim
9481b23d-c41a-4649-8b20-563d97bca837	true	access.token.claim
9481b23d-c41a-4649-8b20-563d97bca837	email_verified	claim.name
9481b23d-c41a-4649-8b20-563d97bca837	boolean	jsonType.label
5fff92cf-008c-4265-a68f-d8027799d064	formatted	user.attribute.formatted
5fff92cf-008c-4265-a68f-d8027799d064	country	user.attribute.country
5fff92cf-008c-4265-a68f-d8027799d064	postal_code	user.attribute.postal_code
5fff92cf-008c-4265-a68f-d8027799d064	true	userinfo.token.claim
5fff92cf-008c-4265-a68f-d8027799d064	street	user.attribute.street
5fff92cf-008c-4265-a68f-d8027799d064	true	id.token.claim
5fff92cf-008c-4265-a68f-d8027799d064	region	user.attribute.region
5fff92cf-008c-4265-a68f-d8027799d064	true	access.token.claim
5fff92cf-008c-4265-a68f-d8027799d064	locality	user.attribute.locality
e9ecd319-62a7-47b3-aaac-1d2f75ca7dd5	true	userinfo.token.claim
e9ecd319-62a7-47b3-aaac-1d2f75ca7dd5	phoneNumber	user.attribute
e9ecd319-62a7-47b3-aaac-1d2f75ca7dd5	true	id.token.claim
e9ecd319-62a7-47b3-aaac-1d2f75ca7dd5	true	access.token.claim
e9ecd319-62a7-47b3-aaac-1d2f75ca7dd5	phone_number	claim.name
e9ecd319-62a7-47b3-aaac-1d2f75ca7dd5	String	jsonType.label
19f29476-f5b2-45aa-9e9b-7115136c4718	true	userinfo.token.claim
19f29476-f5b2-45aa-9e9b-7115136c4718	phoneNumberVerified	user.attribute
19f29476-f5b2-45aa-9e9b-7115136c4718	true	id.token.claim
19f29476-f5b2-45aa-9e9b-7115136c4718	true	access.token.claim
19f29476-f5b2-45aa-9e9b-7115136c4718	phone_number_verified	claim.name
19f29476-f5b2-45aa-9e9b-7115136c4718	boolean	jsonType.label
f8c1a1fd-e561-40fe-8507-b1bc8a4b1d5d	true	multivalued
f8c1a1fd-e561-40fe-8507-b1bc8a4b1d5d	foo	user.attribute
f8c1a1fd-e561-40fe-8507-b1bc8a4b1d5d	true	access.token.claim
f8c1a1fd-e561-40fe-8507-b1bc8a4b1d5d	realm_access.roles	claim.name
f8c1a1fd-e561-40fe-8507-b1bc8a4b1d5d	String	jsonType.label
65615e5b-4f35-499b-bd8a-3c870c49499f	true	multivalued
65615e5b-4f35-499b-bd8a-3c870c49499f	foo	user.attribute
65615e5b-4f35-499b-bd8a-3c870c49499f	true	access.token.claim
65615e5b-4f35-499b-bd8a-3c870c49499f	resource_access.${client_id}.roles	claim.name
65615e5b-4f35-499b-bd8a-3c870c49499f	String	jsonType.label
d7e7061d-742d-4ba0-8e7b-12cb2a2f8e96	true	userinfo.token.claim
d7e7061d-742d-4ba0-8e7b-12cb2a2f8e96	username	user.attribute
d7e7061d-742d-4ba0-8e7b-12cb2a2f8e96	true	id.token.claim
d7e7061d-742d-4ba0-8e7b-12cb2a2f8e96	true	access.token.claim
d7e7061d-742d-4ba0-8e7b-12cb2a2f8e96	upn	claim.name
d7e7061d-742d-4ba0-8e7b-12cb2a2f8e96	String	jsonType.label
7075553b-ec91-4753-a660-f253810a56f5	true	multivalued
7075553b-ec91-4753-a660-f253810a56f5	foo	user.attribute
7075553b-ec91-4753-a660-f253810a56f5	true	id.token.claim
7075553b-ec91-4753-a660-f253810a56f5	true	access.token.claim
7075553b-ec91-4753-a660-f253810a56f5	groups	claim.name
7075553b-ec91-4753-a660-f253810a56f5	String	jsonType.label
55c61dfb-c119-4f49-9622-9a5801682784	true	userinfo.token.claim
55c61dfb-c119-4f49-9622-9a5801682784	locale	user.attribute
55c61dfb-c119-4f49-9622-9a5801682784	true	id.token.claim
55c61dfb-c119-4f49-9622-9a5801682784	true	access.token.claim
55c61dfb-c119-4f49-9622-9a5801682784	locale	claim.name
55c61dfb-c119-4f49-9622-9a5801682784	String	jsonType.label
65e08326-21af-44f1-b1ea-289c09dd0afc	clientId	user.session.note
65e08326-21af-44f1-b1ea-289c09dd0afc	true	id.token.claim
65e08326-21af-44f1-b1ea-289c09dd0afc	true	access.token.claim
65e08326-21af-44f1-b1ea-289c09dd0afc	clientId	claim.name
65e08326-21af-44f1-b1ea-289c09dd0afc	String	jsonType.label
57011c8a-cb7a-4d24-836f-6982b72c3c40	clientHost	user.session.note
57011c8a-cb7a-4d24-836f-6982b72c3c40	true	id.token.claim
57011c8a-cb7a-4d24-836f-6982b72c3c40	true	access.token.claim
57011c8a-cb7a-4d24-836f-6982b72c3c40	clientHost	claim.name
57011c8a-cb7a-4d24-836f-6982b72c3c40	String	jsonType.label
f528237b-6ad3-474d-a29d-3870b3ba5eb7	clientAddress	user.session.note
f528237b-6ad3-474d-a29d-3870b3ba5eb7	true	id.token.claim
f528237b-6ad3-474d-a29d-3870b3ba5eb7	true	access.token.claim
f528237b-6ad3-474d-a29d-3870b3ba5eb7	clientAddress	claim.name
f528237b-6ad3-474d-a29d-3870b3ba5eb7	String	jsonType.label
64084d9c-01f4-4066-90c9-c2987955fdd6	true	userinfo.token.claim
64084d9c-01f4-4066-90c9-c2987955fdd6	username	user.attribute
64084d9c-01f4-4066-90c9-c2987955fdd6	true	id.token.claim
64084d9c-01f4-4066-90c9-c2987955fdd6	true	access.token.claim
64084d9c-01f4-4066-90c9-c2987955fdd6	user_name	claim.name
64084d9c-01f4-4066-90c9-c2987955fdd6	String	jsonType.label
\.


--
-- Data for Name: realm; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm (id, access_code_lifespan, user_action_lifespan, access_token_lifespan, account_theme, admin_theme, email_theme, enabled, events_enabled, events_expiration, login_theme, name, not_before, password_policy, registration_allowed, remember_me, reset_password_allowed, social, ssl_required, sso_idle_timeout, sso_max_lifespan, update_profile_on_soc_login, verify_email, master_admin_client, login_lifespan, internationalization_enabled, default_locale, reg_email_as_username, admin_events_enabled, admin_events_details_enabled, edit_username_allowed, otp_policy_counter, otp_policy_window, otp_policy_period, otp_policy_digits, otp_policy_alg, otp_policy_type, browser_flow, registration_flow, direct_grant_flow, reset_credentials_flow, client_auth_flow, offline_session_idle_timeout, revoke_refresh_token, access_token_life_implicit, login_with_email_allowed, duplicate_emails_allowed, docker_auth_flow, refresh_token_max_reuse, allow_user_managed_access, sso_max_lifespan_remember_me, sso_idle_timeout_remember_me) FROM stdin;
master	60	300	60	\N	\N	\N	t	f	0	\N	master	0	\N	f	f	f	f	EXTERNAL	1800	36000	f	f	b268de25-483d-4179-a250-b7597b3ddbe1	1800	f	\N	f	f	f	f	0	1	30	6	HmacSHA1	totp	458aca5a-6431-444b-8154-3a1ba163176b	1336a465-dc8d-4fc9-a22b-6a51b81d1d1d	d0ba8d6e-e5c5-4131-937c-ca6dfd6501c2	18abeaf9-9775-4c62-9195-2aba10a59dd8	4893e889-9bff-41a0-9509-e018fe690ffc	2592000	f	900	t	f	9349cbfc-d917-4665-b9fa-30648e034f7b	0	f	0	0
civil-add	60	300	300	\N	\N	\N	t	f	0	\N	civil-add	0	\N	f	f	f	f	EXTERNAL	1800	36000	f	f	b5b9bb1d-8598-4e60-b13b-39da766bda6b	1800	f	\N	f	f	f	f	0	1	30	6	HmacSHA1	totp	394d6d73-3f25-40e5-b7c5-b2c66c51e421	63fd6b7b-80d8-4268-a849-325d4f1f0e99	e144ee07-cc75-47f9-a2b5-e60a34f0dbfa	2b030fd4-db8d-4b8a-b320-5939e3e1725c	7561b83b-9a60-45f2-8383-fd1af2f0be69	2592000	f	900	t	f	d1bcce02-7635-4067-a40c-6f75fb03c4a7	0	f	0	0
\.


--
-- Data for Name: realm_attribute; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_attribute (name, value, realm_id) FROM stdin;
_browser_header.contentSecurityPolicyReportOnly		master
_browser_header.xContentTypeOptions	nosniff	master
_browser_header.xRobotsTag	none	master
_browser_header.xFrameOptions	SAMEORIGIN	master
_browser_header.contentSecurityPolicy	frame-src 'self'; frame-ancestors 'self'; object-src 'none';	master
_browser_header.xXSSProtection	1; mode=block	master
_browser_header.strictTransportSecurity	max-age=31536000; includeSubDomains	master
bruteForceProtected	false	master
permanentLockout	false	master
maxFailureWaitSeconds	900	master
minimumQuickLoginWaitSeconds	60	master
waitIncrementSeconds	60	master
quickLoginCheckMilliSeconds	1000	master
maxDeltaTimeSeconds	43200	master
failureFactor	30	master
displayName	Keycloak	master
displayNameHtml	<div class="kc-logo-text"><span>Keycloak</span></div>	master
offlineSessionMaxLifespanEnabled	false	master
offlineSessionMaxLifespan	5184000	master
bruteForceProtected	false	civil-add
permanentLockout	false	civil-add
maxFailureWaitSeconds	900	civil-add
minimumQuickLoginWaitSeconds	60	civil-add
waitIncrementSeconds	60	civil-add
quickLoginCheckMilliSeconds	1000	civil-add
maxDeltaTimeSeconds	43200	civil-add
failureFactor	30	civil-add
actionTokenGeneratedByAdminLifespan	43200	civil-add
actionTokenGeneratedByUserLifespan	300	civil-add
offlineSessionMaxLifespanEnabled	false	civil-add
offlineSessionMaxLifespan	5184000	civil-add
webAuthnPolicyRpEntityName	keycloak	civil-add
webAuthnPolicySignatureAlgorithms	ES256	civil-add
webAuthnPolicyRpId		civil-add
webAuthnPolicyAttestationConveyancePreference	not specified	civil-add
webAuthnPolicyAuthenticatorAttachment	not specified	civil-add
webAuthnPolicyRequireResidentKey	not specified	civil-add
webAuthnPolicyUserVerificationRequirement	not specified	civil-add
webAuthnPolicyCreateTimeout	0	civil-add
webAuthnPolicyAvoidSameAuthenticatorRegister	false	civil-add
webAuthnPolicyRpEntityNamePasswordless	keycloak	civil-add
webAuthnPolicySignatureAlgorithmsPasswordless	ES256	civil-add
webAuthnPolicyRpIdPasswordless		civil-add
webAuthnPolicyAttestationConveyancePreferencePasswordless	not specified	civil-add
webAuthnPolicyAuthenticatorAttachmentPasswordless	not specified	civil-add
webAuthnPolicyRequireResidentKeyPasswordless	not specified	civil-add
webAuthnPolicyUserVerificationRequirementPasswordless	not specified	civil-add
webAuthnPolicyCreateTimeoutPasswordless	0	civil-add
webAuthnPolicyAvoidSameAuthenticatorRegisterPasswordless	false	civil-add
_browser_header.contentSecurityPolicyReportOnly		civil-add
_browser_header.xContentTypeOptions	nosniff	civil-add
_browser_header.xRobotsTag	none	civil-add
_browser_header.xFrameOptions	SAMEORIGIN	civil-add
_browser_header.contentSecurityPolicy	frame-src 'self'; frame-ancestors 'self'; object-src 'none';	civil-add
_browser_header.xXSSProtection	1; mode=block	civil-add
_browser_header.strictTransportSecurity	max-age=31536000; includeSubDomains	civil-add
\.


--
-- Data for Name: realm_default_groups; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_default_groups (realm_id, group_id) FROM stdin;
\.


--
-- Data for Name: realm_default_roles; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_default_roles (realm_id, role_id) FROM stdin;
master	43df3975-e020-4a1b-8b70-3be499933685
master	05d5f5eb-1c19-4920-8002-7e42aaaa8a5e
civil-add	159d8b16-c522-4128-8e3a-c29b28e80d34
civil-add	9eeeb252-a3f2-485e-a87b-3dcdd240fe33
civil-add	6a226a88-7fa5-4d9e-ab87-7f3342ac05c1
\.


--
-- Data for Name: realm_enabled_event_types; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_enabled_event_types (realm_id, value) FROM stdin;
\.


--
-- Data for Name: realm_events_listeners; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_events_listeners (realm_id, value) FROM stdin;
master	jboss-logging
civil-add	jboss-logging
\.


--
-- Data for Name: realm_required_credential; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_required_credential (type, form_label, input, secret, realm_id) FROM stdin;
password	password	t	t	master
password	password	t	t	civil-add
\.


--
-- Data for Name: realm_smtp_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_smtp_config (realm_id, value, name) FROM stdin;
\.


--
-- Data for Name: realm_supported_locales; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_supported_locales (realm_id, value) FROM stdin;
\.


--
-- Data for Name: redirect_uris; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.redirect_uris (client_id, value) FROM stdin;
3ea086ad-f833-40cc-8aff-3612b7138d17	/realms/master/account/*
249c9f55-d8be-43d5-8e3d-7c744ded79f1	/realms/master/account/*
b03c1fb3-9455-4b87-a0eb-c25e1be32912	/admin/master/console/*
406cedd9-b0f8-4d0d-83f2-a5195cdfa910	/realms/civil-add/account/*
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	/realms/civil-add/account/*
a9b463b8-18bb-49f5-a3bd-9e399734c104	/admin/civil-add/console/*
1d826890-822d-4456-8d3b-6fdbe01475ba	*
\.


--
-- Data for Name: required_action_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.required_action_config (required_action_id, value, name) FROM stdin;
\.


--
-- Data for Name: required_action_provider; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.required_action_provider (id, alias, name, realm_id, enabled, default_action, provider_id, priority) FROM stdin;
938be30d-20c6-4c42-90c8-9e35ed75a31d	VERIFY_EMAIL	Verify Email	master	t	f	VERIFY_EMAIL	50
b6bbb9da-a921-4a57-8f76-4237fe350e24	UPDATE_PROFILE	Update Profile	master	t	f	UPDATE_PROFILE	40
77eb300b-271f-45e2-8b92-3939c07c87e6	CONFIGURE_TOTP	Configure OTP	master	t	f	CONFIGURE_TOTP	10
3d89b358-05a4-4612-adfb-f53cf314545f	UPDATE_PASSWORD	Update Password	master	t	f	UPDATE_PASSWORD	30
f4de34ce-6f92-479e-a059-f8ce9d93433c	terms_and_conditions	Terms and Conditions	master	f	f	terms_and_conditions	20
c8d54b70-6479-4c39-bd70-85122bcada85	update_user_locale	Update User Locale	master	t	f	update_user_locale	1000
da77ffec-a630-45df-a99e-956f241bb94b	VERIFY_EMAIL	Verify Email	civil-add	t	f	VERIFY_EMAIL	50
623de77e-7538-44ef-aaaa-dc45de12339a	UPDATE_PROFILE	Update Profile	civil-add	t	f	UPDATE_PROFILE	40
f0a73822-6535-41e7-876e-5e49aec519f4	CONFIGURE_TOTP	Configure OTP	civil-add	t	f	CONFIGURE_TOTP	10
ccc2ea06-1aeb-4958-a76b-95046de4319a	UPDATE_PASSWORD	Update Password	civil-add	t	f	UPDATE_PASSWORD	30
dc6a541e-1569-474d-83e0-377628f94dac	terms_and_conditions	Terms and Conditions	civil-add	f	f	terms_and_conditions	20
dbe86cd7-4936-4708-bff4-357642cba756	update_user_locale	Update User Locale	civil-add	t	f	update_user_locale	1000
\.


--
-- Data for Name: resource_attribute; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_attribute (id, name, value, resource_id) FROM stdin;
\.


--
-- Data for Name: resource_policy; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_policy (resource_id, policy_id) FROM stdin;
\.


--
-- Data for Name: resource_scope; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_scope (resource_id, scope_id) FROM stdin;
\.


--
-- Data for Name: resource_server; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_server (id, allow_rs_remote_mgmt, policy_enforce_mode, decision_strategy) FROM stdin;
1d826890-822d-4456-8d3b-6fdbe01475ba	t	0	1
\.


--
-- Data for Name: resource_server_perm_ticket; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_server_perm_ticket (id, owner, requester, created_timestamp, granted_timestamp, resource_id, scope_id, resource_server_id, policy_id) FROM stdin;
\.


--
-- Data for Name: resource_server_policy; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_server_policy (id, name, description, type, decision_strategy, logic, resource_server_id, owner) FROM stdin;
c5509373-62f1-438d-b14d-2bfbd629dec0	Default Policy	A policy that grants access only for users within this realm	js	0	0	1d826890-822d-4456-8d3b-6fdbe01475ba	\N
483cfade-75f6-42f8-aa9b-f262a04952c7	Default Permission	A permission that applies to the default resource type	resource	1	0	1d826890-822d-4456-8d3b-6fdbe01475ba	\N
\.


--
-- Data for Name: resource_server_resource; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_server_resource (id, name, type, icon_uri, owner, resource_server_id, owner_managed_access, display_name) FROM stdin;
fd35f410-52b0-434e-b8b8-d14ed4019181	Default Resource	urn:civil-add_back:resources:default	\N	1d826890-822d-4456-8d3b-6fdbe01475ba	1d826890-822d-4456-8d3b-6fdbe01475ba	f	\N
\.


--
-- Data for Name: resource_server_scope; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_server_scope (id, name, icon_uri, resource_server_id, display_name) FROM stdin;
\.


--
-- Data for Name: resource_uris; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_uris (resource_id, value) FROM stdin;
fd35f410-52b0-434e-b8b8-d14ed4019181	/*
\.


--
-- Data for Name: role_attribute; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.role_attribute (id, role_id, name, value) FROM stdin;
\.


--
-- Data for Name: scope_mapping; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.scope_mapping (client_id, role_id) FROM stdin;
249c9f55-d8be-43d5-8e3d-7c744ded79f1	1635476e-d04b-41db-8429-6c164d2b0255
9059b8a4-352b-4ab8-ba9d-28b739cd80b6	093235eb-d1b2-4732-bdaa-8db103645f63
\.


--
-- Data for Name: scope_policy; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.scope_policy (scope_id, policy_id) FROM stdin;
\.


--
-- Data for Name: user_attribute; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_attribute (name, value, user_id, id) FROM stdin;
\.


--
-- Data for Name: user_consent; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_consent (id, client_id, user_id, created_date, last_updated_date, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: user_consent_client_scope; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_consent_client_scope (user_consent_id, scope_id) FROM stdin;
\.


--
-- Data for Name: user_entity; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_entity (id, email, email_constraint, email_verified, enabled, federation_link, first_name, last_name, realm_id, username, created_timestamp, service_account_client_link, not_before) FROM stdin;
785cc4e2-b971-4a82-bcb6-b997646aaec5	\N	5c48f964-f48c-454f-a428-b0e9bc58221e	f	t	\N	\N	\N	master	user	1587384857496	\N	0
b9712f3a-5fce-4ed4-a2e8-07fbda63f20a	\N	a2b94442-c542-49fe-8899-5fda655250b3	f	t	\N	\N	\N	civil-add	service-account-civil-add_back	1587415603318	1d826890-822d-4456-8d3b-6fdbe01475ba	0
b7f5370c-02f9-4a55-8a44-43ab6c54f781	contact@tool-add.com	contact@tool-add.com	f	t	\N	\N	\N	civil-add	ta_user	1587416072836	\N	0
\.


--
-- Data for Name: user_federation_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_federation_config (user_federation_provider_id, value, name) FROM stdin;
\.


--
-- Data for Name: user_federation_mapper; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_federation_mapper (id, name, federation_provider_id, federation_mapper_type, realm_id) FROM stdin;
\.


--
-- Data for Name: user_federation_mapper_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_federation_mapper_config (user_federation_mapper_id, value, name) FROM stdin;
\.


--
-- Data for Name: user_federation_provider; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_federation_provider (id, changed_sync_period, display_name, full_sync_period, last_sync, priority, provider_name, realm_id) FROM stdin;
\.


--
-- Data for Name: user_group_membership; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_group_membership (group_id, user_id) FROM stdin;
\.


--
-- Data for Name: user_required_action; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_required_action (user_id, required_action) FROM stdin;
\.


--
-- Data for Name: user_role_mapping; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_role_mapping (role_id, user_id) FROM stdin;
43df3975-e020-4a1b-8b70-3be499933685	785cc4e2-b971-4a82-bcb6-b997646aaec5
59641392-3138-44db-b74f-e4876f3e0def	785cc4e2-b971-4a82-bcb6-b997646aaec5
1635476e-d04b-41db-8429-6c164d2b0255	785cc4e2-b971-4a82-bcb6-b997646aaec5
05d5f5eb-1c19-4920-8002-7e42aaaa8a5e	785cc4e2-b971-4a82-bcb6-b997646aaec5
7cef0a8f-b179-4cc3-8e59-92899865ef50	785cc4e2-b971-4a82-bcb6-b997646aaec5
159d8b16-c522-4128-8e3a-c29b28e80d34	b9712f3a-5fce-4ed4-a2e8-07fbda63f20a
9eeeb252-a3f2-485e-a87b-3dcdd240fe33	b9712f3a-5fce-4ed4-a2e8-07fbda63f20a
093235eb-d1b2-4732-bdaa-8db103645f63	b9712f3a-5fce-4ed4-a2e8-07fbda63f20a
c5c2aee8-1c25-45b3-8535-49bf502325f0	b9712f3a-5fce-4ed4-a2e8-07fbda63f20a
01a23297-519f-483d-882f-df599f518e58	b9712f3a-5fce-4ed4-a2e8-07fbda63f20a
159d8b16-c522-4128-8e3a-c29b28e80d34	b7f5370c-02f9-4a55-8a44-43ab6c54f781
9eeeb252-a3f2-485e-a87b-3dcdd240fe33	b7f5370c-02f9-4a55-8a44-43ab6c54f781
093235eb-d1b2-4732-bdaa-8db103645f63	b7f5370c-02f9-4a55-8a44-43ab6c54f781
c5c2aee8-1c25-45b3-8535-49bf502325f0	b7f5370c-02f9-4a55-8a44-43ab6c54f781
6a226a88-7fa5-4d9e-ab87-7f3342ac05c1	b7f5370c-02f9-4a55-8a44-43ab6c54f781
\.


--
-- Data for Name: user_session; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_session (id, auth_method, ip_address, last_session_refresh, login_username, realm_id, remember_me, started, user_id, user_session_state, broker_session_id, broker_user_id) FROM stdin;
\.


--
-- Data for Name: user_session_note; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_session_note (user_session, name, value) FROM stdin;
\.


--
-- Data for Name: username_login_failure; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.username_login_failure (realm_id, username, failed_login_not_before, last_failure, last_ip_failure, num_failures) FROM stdin;
\.


--
-- Data for Name: web_origins; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.web_origins (client_id, value) FROM stdin;
b03c1fb3-9455-4b87-a0eb-c25e1be32912	+
a9b463b8-18bb-49f5-a3bd-9e399734c104	+
\.


--
-- Name: username_login_failure CONSTRAINT_17-2; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.username_login_failure
    ADD CONSTRAINT "CONSTRAINT_17-2" PRIMARY KEY (realm_id, username);


--
-- Name: keycloak_role UK_J3RWUVD56ONTGSUHOGM184WW2-2; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT "UK_J3RWUVD56ONTGSUHOGM184WW2-2" UNIQUE (name, client_realm_constraint);


--
-- Name: client_auth_flow_bindings c_cli_flow_bind; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_auth_flow_bindings
    ADD CONSTRAINT c_cli_flow_bind PRIMARY KEY (client_id, binding_name);


--
-- Name: client_scope_client c_cli_scope_bind; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_client
    ADD CONSTRAINT c_cli_scope_bind PRIMARY KEY (client_id, scope_id);


--
-- Name: client_initial_access cnstr_client_init_acc_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_initial_access
    ADD CONSTRAINT cnstr_client_init_acc_pk PRIMARY KEY (id);


--
-- Name: realm_default_groups con_group_id_def_groups; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT con_group_id_def_groups UNIQUE (group_id);


--
-- Name: broker_link constr_broker_link_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.broker_link
    ADD CONSTRAINT constr_broker_link_pk PRIMARY KEY (identity_provider, user_id);


--
-- Name: client_user_session_note constr_cl_usr_ses_note; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_user_session_note
    ADD CONSTRAINT constr_cl_usr_ses_note PRIMARY KEY (client_session, name);


--
-- Name: client_default_roles constr_client_default_roles; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_default_roles
    ADD CONSTRAINT constr_client_default_roles PRIMARY KEY (client_id, role_id);


--
-- Name: component_config constr_component_config_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.component_config
    ADD CONSTRAINT constr_component_config_pk PRIMARY KEY (id);


--
-- Name: component constr_component_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT constr_component_pk PRIMARY KEY (id);


--
-- Name: fed_user_required_action constr_fed_required_action; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_required_action
    ADD CONSTRAINT constr_fed_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: fed_user_attribute constr_fed_user_attr_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_attribute
    ADD CONSTRAINT constr_fed_user_attr_pk PRIMARY KEY (id);


--
-- Name: fed_user_consent constr_fed_user_consent_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_consent
    ADD CONSTRAINT constr_fed_user_consent_pk PRIMARY KEY (id);


--
-- Name: fed_user_credential constr_fed_user_cred_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_credential
    ADD CONSTRAINT constr_fed_user_cred_pk PRIMARY KEY (id);


--
-- Name: fed_user_group_membership constr_fed_user_group; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_group_membership
    ADD CONSTRAINT constr_fed_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: fed_user_role_mapping constr_fed_user_role; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_role_mapping
    ADD CONSTRAINT constr_fed_user_role PRIMARY KEY (role_id, user_id);


--
-- Name: federated_user constr_federated_user; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.federated_user
    ADD CONSTRAINT constr_federated_user PRIMARY KEY (id);


--
-- Name: realm_default_groups constr_realm_default_groups; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT constr_realm_default_groups PRIMARY KEY (realm_id, group_id);


--
-- Name: realm_enabled_event_types constr_realm_enabl_event_types; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_enabled_event_types
    ADD CONSTRAINT constr_realm_enabl_event_types PRIMARY KEY (realm_id, value);


--
-- Name: realm_events_listeners constr_realm_events_listeners; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_events_listeners
    ADD CONSTRAINT constr_realm_events_listeners PRIMARY KEY (realm_id, value);


--
-- Name: realm_supported_locales constr_realm_supported_locales; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_supported_locales
    ADD CONSTRAINT constr_realm_supported_locales PRIMARY KEY (realm_id, value);


--
-- Name: identity_provider constraint_2b; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT constraint_2b PRIMARY KEY (internal_id);


--
-- Name: client_attributes constraint_3c; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_attributes
    ADD CONSTRAINT constraint_3c PRIMARY KEY (client_id, name);


--
-- Name: event_entity constraint_4; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.event_entity
    ADD CONSTRAINT constraint_4 PRIMARY KEY (id);


--
-- Name: federated_identity constraint_40; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.federated_identity
    ADD CONSTRAINT constraint_40 PRIMARY KEY (identity_provider, user_id);


--
-- Name: realm constraint_4a; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT constraint_4a PRIMARY KEY (id);


--
-- Name: client_session_role constraint_5; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_role
    ADD CONSTRAINT constraint_5 PRIMARY KEY (client_session, role_id);


--
-- Name: user_session constraint_57; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_session
    ADD CONSTRAINT constraint_57 PRIMARY KEY (id);


--
-- Name: user_federation_provider constraint_5c; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_provider
    ADD CONSTRAINT constraint_5c PRIMARY KEY (id);


--
-- Name: client_session_note constraint_5e; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_note
    ADD CONSTRAINT constraint_5e PRIMARY KEY (client_session, name);


--
-- Name: client constraint_7; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT constraint_7 PRIMARY KEY (id);


--
-- Name: client_session constraint_8; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session
    ADD CONSTRAINT constraint_8 PRIMARY KEY (id);


--
-- Name: scope_mapping constraint_81; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT constraint_81 PRIMARY KEY (client_id, role_id);


--
-- Name: client_node_registrations constraint_84; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_node_registrations
    ADD CONSTRAINT constraint_84 PRIMARY KEY (client_id, name);


--
-- Name: realm_attribute constraint_9; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_attribute
    ADD CONSTRAINT constraint_9 PRIMARY KEY (name, realm_id);


--
-- Name: realm_required_credential constraint_92; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_required_credential
    ADD CONSTRAINT constraint_92 PRIMARY KEY (realm_id, type);


--
-- Name: keycloak_role constraint_a; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT constraint_a PRIMARY KEY (id);


--
-- Name: admin_event_entity constraint_admin_event_entity; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.admin_event_entity
    ADD CONSTRAINT constraint_admin_event_entity PRIMARY KEY (id);


--
-- Name: authenticator_config_entry constraint_auth_cfg_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authenticator_config_entry
    ADD CONSTRAINT constraint_auth_cfg_pk PRIMARY KEY (authenticator_id, name);


--
-- Name: authentication_execution constraint_auth_exec_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT constraint_auth_exec_pk PRIMARY KEY (id);


--
-- Name: authentication_flow constraint_auth_flow_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authentication_flow
    ADD CONSTRAINT constraint_auth_flow_pk PRIMARY KEY (id);


--
-- Name: authenticator_config constraint_auth_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authenticator_config
    ADD CONSTRAINT constraint_auth_pk PRIMARY KEY (id);


--
-- Name: client_session_auth_status constraint_auth_status_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_auth_status
    ADD CONSTRAINT constraint_auth_status_pk PRIMARY KEY (client_session, authenticator);


--
-- Name: user_role_mapping constraint_c; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_role_mapping
    ADD CONSTRAINT constraint_c PRIMARY KEY (role_id, user_id);


--
-- Name: composite_role constraint_composite_role; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT constraint_composite_role PRIMARY KEY (composite, child_role);


--
-- Name: client_session_prot_mapper constraint_cs_pmp_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_prot_mapper
    ADD CONSTRAINT constraint_cs_pmp_pk PRIMARY KEY (client_session, protocol_mapper_id);


--
-- Name: identity_provider_config constraint_d; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider_config
    ADD CONSTRAINT constraint_d PRIMARY KEY (identity_provider_id, name);


--
-- Name: policy_config constraint_dpc; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.policy_config
    ADD CONSTRAINT constraint_dpc PRIMARY KEY (policy_id, name);


--
-- Name: realm_smtp_config constraint_e; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_smtp_config
    ADD CONSTRAINT constraint_e PRIMARY KEY (realm_id, name);


--
-- Name: credential constraint_f; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.credential
    ADD CONSTRAINT constraint_f PRIMARY KEY (id);


--
-- Name: user_federation_config constraint_f9; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_config
    ADD CONSTRAINT constraint_f9 PRIMARY KEY (user_federation_provider_id, name);


--
-- Name: resource_server_perm_ticket constraint_fapmt; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT constraint_fapmt PRIMARY KEY (id);


--
-- Name: resource_server_resource constraint_farsr; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT constraint_farsr PRIMARY KEY (id);


--
-- Name: resource_server_policy constraint_farsrp; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT constraint_farsrp PRIMARY KEY (id);


--
-- Name: associated_policy constraint_farsrpap; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT constraint_farsrpap PRIMARY KEY (policy_id, associated_policy_id);


--
-- Name: resource_policy constraint_farsrpp; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT constraint_farsrpp PRIMARY KEY (resource_id, policy_id);


--
-- Name: resource_server_scope constraint_farsrs; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT constraint_farsrs PRIMARY KEY (id);


--
-- Name: resource_scope constraint_farsrsp; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT constraint_farsrsp PRIMARY KEY (resource_id, scope_id);


--
-- Name: scope_policy constraint_farsrsps; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT constraint_farsrsps PRIMARY KEY (scope_id, policy_id);


--
-- Name: user_entity constraint_fb; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT constraint_fb PRIMARY KEY (id);


--
-- Name: user_federation_mapper_config constraint_fedmapper_cfg_pm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_mapper_config
    ADD CONSTRAINT constraint_fedmapper_cfg_pm PRIMARY KEY (user_federation_mapper_id, name);


--
-- Name: user_federation_mapper constraint_fedmapperpm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT constraint_fedmapperpm PRIMARY KEY (id);


--
-- Name: fed_user_consent_cl_scope constraint_fgrntcsnt_clsc_pm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_consent_cl_scope
    ADD CONSTRAINT constraint_fgrntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent_client_scope constraint_grntcsnt_clsc_pm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_consent_client_scope
    ADD CONSTRAINT constraint_grntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent constraint_grntcsnt_pm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT constraint_grntcsnt_pm PRIMARY KEY (id);


--
-- Name: keycloak_group constraint_group; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT constraint_group PRIMARY KEY (id);


--
-- Name: group_attribute constraint_group_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.group_attribute
    ADD CONSTRAINT constraint_group_attribute_pk PRIMARY KEY (id);


--
-- Name: group_role_mapping constraint_group_role; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT constraint_group_role PRIMARY KEY (role_id, group_id);


--
-- Name: identity_provider_mapper constraint_idpm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider_mapper
    ADD CONSTRAINT constraint_idpm PRIMARY KEY (id);


--
-- Name: idp_mapper_config constraint_idpmconfig; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.idp_mapper_config
    ADD CONSTRAINT constraint_idpmconfig PRIMARY KEY (idp_mapper_id, name);


--
-- Name: migration_model constraint_migmod; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.migration_model
    ADD CONSTRAINT constraint_migmod PRIMARY KEY (id);


--
-- Name: offline_client_session constraint_offl_cl_ses_pk3; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.offline_client_session
    ADD CONSTRAINT constraint_offl_cl_ses_pk3 PRIMARY KEY (user_session_id, client_id, client_storage_provider, external_client_id, offline_flag);


--
-- Name: offline_user_session constraint_offl_us_ses_pk2; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.offline_user_session
    ADD CONSTRAINT constraint_offl_us_ses_pk2 PRIMARY KEY (user_session_id, offline_flag);


--
-- Name: protocol_mapper constraint_pcm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT constraint_pcm PRIMARY KEY (id);


--
-- Name: protocol_mapper_config constraint_pmconfig; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.protocol_mapper_config
    ADD CONSTRAINT constraint_pmconfig PRIMARY KEY (protocol_mapper_id, name);


--
-- Name: realm_default_roles constraint_realm_default_roles; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_roles
    ADD CONSTRAINT constraint_realm_default_roles PRIMARY KEY (realm_id, role_id);


--
-- Name: redirect_uris constraint_redirect_uris; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.redirect_uris
    ADD CONSTRAINT constraint_redirect_uris PRIMARY KEY (client_id, value);


--
-- Name: required_action_config constraint_req_act_cfg_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.required_action_config
    ADD CONSTRAINT constraint_req_act_cfg_pk PRIMARY KEY (required_action_id, name);


--
-- Name: required_action_provider constraint_req_act_prv_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.required_action_provider
    ADD CONSTRAINT constraint_req_act_prv_pk PRIMARY KEY (id);


--
-- Name: user_required_action constraint_required_action; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_required_action
    ADD CONSTRAINT constraint_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: resource_uris constraint_resour_uris_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_uris
    ADD CONSTRAINT constraint_resour_uris_pk PRIMARY KEY (resource_id, value);


--
-- Name: role_attribute constraint_role_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.role_attribute
    ADD CONSTRAINT constraint_role_attribute_pk PRIMARY KEY (id);


--
-- Name: user_attribute constraint_user_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_attribute
    ADD CONSTRAINT constraint_user_attribute_pk PRIMARY KEY (id);


--
-- Name: user_group_membership constraint_user_group; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_group_membership
    ADD CONSTRAINT constraint_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: user_session_note constraint_usn_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_session_note
    ADD CONSTRAINT constraint_usn_pk PRIMARY KEY (user_session, name);


--
-- Name: web_origins constraint_web_origins; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.web_origins
    ADD CONSTRAINT constraint_web_origins PRIMARY KEY (client_id, value);


--
-- Name: client_scope_attributes pk_cl_tmpl_attr; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_attributes
    ADD CONSTRAINT pk_cl_tmpl_attr PRIMARY KEY (scope_id, name);


--
-- Name: client_scope pk_cli_template; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT pk_cli_template PRIMARY KEY (id);


--
-- Name: databasechangeloglock pk_databasechangeloglock; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);


--
-- Name: resource_server pk_resource_server; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server
    ADD CONSTRAINT pk_resource_server PRIMARY KEY (id);


--
-- Name: client_scope_role_mapping pk_template_scope; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT pk_template_scope PRIMARY KEY (scope_id, role_id);


--
-- Name: default_client_scope r_def_cli_scope_bind; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT r_def_cli_scope_bind PRIMARY KEY (realm_id, scope_id);


--
-- Name: resource_attribute res_attr_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_attribute
    ADD CONSTRAINT res_attr_pk PRIMARY KEY (id);


--
-- Name: keycloak_group sibling_names; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT sibling_names UNIQUE (realm_id, parent_group, name);


--
-- Name: identity_provider uk_2daelwnibji49avxsrtuf6xj33; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT uk_2daelwnibji49avxsrtuf6xj33 UNIQUE (provider_alias, realm_id);


--
-- Name: client_default_roles uk_8aelwnibji49avxsrtuf6xjow; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_default_roles
    ADD CONSTRAINT uk_8aelwnibji49avxsrtuf6xjow UNIQUE (role_id);


--
-- Name: client uk_b71cjlbenv945rb6gcon438at; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT uk_b71cjlbenv945rb6gcon438at UNIQUE (realm_id, client_id);


--
-- Name: client_scope uk_cli_scope; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT uk_cli_scope UNIQUE (realm_id, name);


--
-- Name: user_entity uk_dykn684sl8up1crfei6eckhd7; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT uk_dykn684sl8up1crfei6eckhd7 UNIQUE (realm_id, email_constraint);


--
-- Name: resource_server_resource uk_frsr6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5ha6 UNIQUE (name, owner, resource_server_id);


--
-- Name: resource_server_perm_ticket uk_frsr6t700s9v50bu18ws5pmt; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5pmt UNIQUE (owner, requester, resource_server_id, resource_id, scope_id);


--
-- Name: resource_server_policy uk_frsrpt700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT uk_frsrpt700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: resource_server_scope uk_frsrst700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT uk_frsrst700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: realm_default_roles uk_h4wpd7w4hsoolni3h0sw7btje; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_roles
    ADD CONSTRAINT uk_h4wpd7w4hsoolni3h0sw7btje UNIQUE (role_id);


--
-- Name: user_consent uk_jkuwuvd56ontgsuhogm8uewrt; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT uk_jkuwuvd56ontgsuhogm8uewrt UNIQUE (client_id, client_storage_provider, external_client_id, user_id);


--
-- Name: realm uk_orvsdmla56612eaefiq6wl5oi; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT uk_orvsdmla56612eaefiq6wl5oi UNIQUE (name);


--
-- Name: user_entity uk_ru8tt6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT uk_ru8tt6t700s9v50bu18ws5ha6 UNIQUE (realm_id, username);


--
-- Name: idx_assoc_pol_assoc_pol_id; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_assoc_pol_assoc_pol_id ON public.associated_policy USING btree (associated_policy_id);


--
-- Name: idx_auth_config_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_auth_config_realm ON public.authenticator_config USING btree (realm_id);


--
-- Name: idx_auth_exec_flow; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_auth_exec_flow ON public.authentication_execution USING btree (flow_id);


--
-- Name: idx_auth_exec_realm_flow; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_auth_exec_realm_flow ON public.authentication_execution USING btree (realm_id, flow_id);


--
-- Name: idx_auth_flow_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_auth_flow_realm ON public.authentication_flow USING btree (realm_id);


--
-- Name: idx_cl_clscope; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_cl_clscope ON public.client_scope_client USING btree (scope_id);


--
-- Name: idx_client_def_roles_client; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_client_def_roles_client ON public.client_default_roles USING btree (client_id);


--
-- Name: idx_client_id; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_client_id ON public.client USING btree (client_id);


--
-- Name: idx_client_init_acc_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_client_init_acc_realm ON public.client_initial_access USING btree (realm_id);


--
-- Name: idx_client_session_session; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_client_session_session ON public.client_session USING btree (session_id);


--
-- Name: idx_clscope_attrs; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_clscope_attrs ON public.client_scope_attributes USING btree (scope_id);


--
-- Name: idx_clscope_cl; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_clscope_cl ON public.client_scope_client USING btree (client_id);


--
-- Name: idx_clscope_protmap; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_clscope_protmap ON public.protocol_mapper USING btree (client_scope_id);


--
-- Name: idx_clscope_role; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_clscope_role ON public.client_scope_role_mapping USING btree (scope_id);


--
-- Name: idx_compo_config_compo; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_compo_config_compo ON public.component_config USING btree (component_id);


--
-- Name: idx_component_provider_type; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_component_provider_type ON public.component USING btree (provider_type);


--
-- Name: idx_component_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_component_realm ON public.component USING btree (realm_id);


--
-- Name: idx_composite; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_composite ON public.composite_role USING btree (composite);


--
-- Name: idx_composite_child; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_composite_child ON public.composite_role USING btree (child_role);


--
-- Name: idx_defcls_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_defcls_realm ON public.default_client_scope USING btree (realm_id);


--
-- Name: idx_defcls_scope; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_defcls_scope ON public.default_client_scope USING btree (scope_id);


--
-- Name: idx_event_time; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_event_time ON public.event_entity USING btree (realm_id, event_time);


--
-- Name: idx_fedidentity_feduser; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fedidentity_feduser ON public.federated_identity USING btree (federated_user_id);


--
-- Name: idx_fedidentity_user; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fedidentity_user ON public.federated_identity USING btree (user_id);


--
-- Name: idx_fu_attribute; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_attribute ON public.fed_user_attribute USING btree (user_id, realm_id, name);


--
-- Name: idx_fu_cnsnt_ext; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_cnsnt_ext ON public.fed_user_consent USING btree (user_id, client_storage_provider, external_client_id);


--
-- Name: idx_fu_consent; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_consent ON public.fed_user_consent USING btree (user_id, client_id);


--
-- Name: idx_fu_consent_ru; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_consent_ru ON public.fed_user_consent USING btree (realm_id, user_id);


--
-- Name: idx_fu_credential; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_credential ON public.fed_user_credential USING btree (user_id, type);


--
-- Name: idx_fu_credential_ru; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_credential_ru ON public.fed_user_credential USING btree (realm_id, user_id);


--
-- Name: idx_fu_group_membership; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_group_membership ON public.fed_user_group_membership USING btree (user_id, group_id);


--
-- Name: idx_fu_group_membership_ru; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_group_membership_ru ON public.fed_user_group_membership USING btree (realm_id, user_id);


--
-- Name: idx_fu_required_action; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_required_action ON public.fed_user_required_action USING btree (user_id, required_action);


--
-- Name: idx_fu_required_action_ru; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_required_action_ru ON public.fed_user_required_action USING btree (realm_id, user_id);


--
-- Name: idx_fu_role_mapping; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_role_mapping ON public.fed_user_role_mapping USING btree (user_id, role_id);


--
-- Name: idx_fu_role_mapping_ru; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_role_mapping_ru ON public.fed_user_role_mapping USING btree (realm_id, user_id);


--
-- Name: idx_group_attr_group; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_group_attr_group ON public.group_attribute USING btree (group_id);


--
-- Name: idx_group_role_mapp_group; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_group_role_mapp_group ON public.group_role_mapping USING btree (group_id);


--
-- Name: idx_id_prov_mapp_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_id_prov_mapp_realm ON public.identity_provider_mapper USING btree (realm_id);


--
-- Name: idx_ident_prov_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_ident_prov_realm ON public.identity_provider USING btree (realm_id);


--
-- Name: idx_keycloak_role_client; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_keycloak_role_client ON public.keycloak_role USING btree (client);


--
-- Name: idx_keycloak_role_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_keycloak_role_realm ON public.keycloak_role USING btree (realm);


--
-- Name: idx_offline_uss_createdon; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_offline_uss_createdon ON public.offline_user_session USING btree (created_on);


--
-- Name: idx_protocol_mapper_client; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_protocol_mapper_client ON public.protocol_mapper USING btree (client_id);


--
-- Name: idx_realm_attr_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_attr_realm ON public.realm_attribute USING btree (realm_id);


--
-- Name: idx_realm_clscope; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_clscope ON public.client_scope USING btree (realm_id);


--
-- Name: idx_realm_def_grp_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_def_grp_realm ON public.realm_default_groups USING btree (realm_id);


--
-- Name: idx_realm_def_roles_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_def_roles_realm ON public.realm_default_roles USING btree (realm_id);


--
-- Name: idx_realm_evt_list_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_evt_list_realm ON public.realm_events_listeners USING btree (realm_id);


--
-- Name: idx_realm_evt_types_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_evt_types_realm ON public.realm_enabled_event_types USING btree (realm_id);


--
-- Name: idx_realm_master_adm_cli; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_master_adm_cli ON public.realm USING btree (master_admin_client);


--
-- Name: idx_realm_supp_local_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_supp_local_realm ON public.realm_supported_locales USING btree (realm_id);


--
-- Name: idx_redir_uri_client; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_redir_uri_client ON public.redirect_uris USING btree (client_id);


--
-- Name: idx_req_act_prov_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_req_act_prov_realm ON public.required_action_provider USING btree (realm_id);


--
-- Name: idx_res_policy_policy; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_res_policy_policy ON public.resource_policy USING btree (policy_id);


--
-- Name: idx_res_scope_scope; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_res_scope_scope ON public.resource_scope USING btree (scope_id);


--
-- Name: idx_res_serv_pol_res_serv; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_res_serv_pol_res_serv ON public.resource_server_policy USING btree (resource_server_id);


--
-- Name: idx_res_srv_res_res_srv; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_res_srv_res_res_srv ON public.resource_server_resource USING btree (resource_server_id);


--
-- Name: idx_res_srv_scope_res_srv; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_res_srv_scope_res_srv ON public.resource_server_scope USING btree (resource_server_id);


--
-- Name: idx_role_attribute; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_role_attribute ON public.role_attribute USING btree (role_id);


--
-- Name: idx_role_clscope; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_role_clscope ON public.client_scope_role_mapping USING btree (role_id);


--
-- Name: idx_scope_mapping_role; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_scope_mapping_role ON public.scope_mapping USING btree (role_id);


--
-- Name: idx_scope_policy_policy; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_scope_policy_policy ON public.scope_policy USING btree (policy_id);


--
-- Name: idx_update_time; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_update_time ON public.migration_model USING btree (update_time);


--
-- Name: idx_us_sess_id_on_cl_sess; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_us_sess_id_on_cl_sess ON public.offline_client_session USING btree (user_session_id);


--
-- Name: idx_usconsent_clscope; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_usconsent_clscope ON public.user_consent_client_scope USING btree (user_consent_id);


--
-- Name: idx_user_attribute; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_attribute ON public.user_attribute USING btree (user_id);


--
-- Name: idx_user_consent; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_consent ON public.user_consent USING btree (user_id);


--
-- Name: idx_user_credential; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_credential ON public.credential USING btree (user_id);


--
-- Name: idx_user_email; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_email ON public.user_entity USING btree (email);


--
-- Name: idx_user_group_mapping; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_group_mapping ON public.user_group_membership USING btree (user_id);


--
-- Name: idx_user_reqactions; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_reqactions ON public.user_required_action USING btree (user_id);


--
-- Name: idx_user_role_mapping; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_role_mapping ON public.user_role_mapping USING btree (user_id);


--
-- Name: idx_usr_fed_map_fed_prv; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_usr_fed_map_fed_prv ON public.user_federation_mapper USING btree (federation_provider_id);


--
-- Name: idx_usr_fed_map_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_usr_fed_map_realm ON public.user_federation_mapper USING btree (realm_id);


--
-- Name: idx_usr_fed_prv_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_usr_fed_prv_realm ON public.user_federation_provider USING btree (realm_id);


--
-- Name: idx_web_orig_client; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_web_orig_client ON public.web_origins USING btree (client_id);


--
-- Name: client_session_auth_status auth_status_constraint; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_auth_status
    ADD CONSTRAINT auth_status_constraint FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: identity_provider fk2b4ebc52ae5c3b34; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT fk2b4ebc52ae5c3b34 FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_attributes fk3c47c64beacca966; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_attributes
    ADD CONSTRAINT fk3c47c64beacca966 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: federated_identity fk404288b92ef007a6; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.federated_identity
    ADD CONSTRAINT fk404288b92ef007a6 FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: client_node_registrations fk4129723ba992f594; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_node_registrations
    ADD CONSTRAINT fk4129723ba992f594 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_session_note fk5edfb00ff51c2736; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_note
    ADD CONSTRAINT fk5edfb00ff51c2736 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: user_session_note fk5edfb00ff51d3472; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_session_note
    ADD CONSTRAINT fk5edfb00ff51d3472 FOREIGN KEY (user_session) REFERENCES public.user_session(id);


--
-- Name: client_session_role fk_11b7sgqw18i532811v7o2dv76; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_role
    ADD CONSTRAINT fk_11b7sgqw18i532811v7o2dv76 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: redirect_uris fk_1burs8pb4ouj97h5wuppahv9f; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.redirect_uris
    ADD CONSTRAINT fk_1burs8pb4ouj97h5wuppahv9f FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: user_federation_provider fk_1fj32f6ptolw2qy60cd8n01e8; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_provider
    ADD CONSTRAINT fk_1fj32f6ptolw2qy60cd8n01e8 FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_session_prot_mapper fk_33a8sgqw18i532811v7o2dk89; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_prot_mapper
    ADD CONSTRAINT fk_33a8sgqw18i532811v7o2dk89 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: realm_required_credential fk_5hg65lybevavkqfki3kponh9v; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_required_credential
    ADD CONSTRAINT fk_5hg65lybevavkqfki3kponh9v FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: resource_attribute fk_5hrm2vlf9ql5fu022kqepovbr; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu022kqepovbr FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: user_attribute fk_5hrm2vlf9ql5fu043kqepovbr; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu043kqepovbr FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: user_required_action fk_6qj3w1jw9cvafhe19bwsiuvmd; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_required_action
    ADD CONSTRAINT fk_6qj3w1jw9cvafhe19bwsiuvmd FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: keycloak_role fk_6vyqfe4cn4wlq8r6kt5vdsj5c; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT fk_6vyqfe4cn4wlq8r6kt5vdsj5c FOREIGN KEY (realm) REFERENCES public.realm(id);


--
-- Name: realm_smtp_config fk_70ej8xdxgxd0b9hh6180irr0o; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_smtp_config
    ADD CONSTRAINT fk_70ej8xdxgxd0b9hh6180irr0o FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_default_roles fk_8aelwnibji49avxsrtuf6xjow; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_default_roles
    ADD CONSTRAINT fk_8aelwnibji49avxsrtuf6xjow FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: realm_attribute fk_8shxd6l3e9atqukacxgpffptw; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_attribute
    ADD CONSTRAINT fk_8shxd6l3e9atqukacxgpffptw FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: composite_role fk_a63wvekftu8jo1pnj81e7mce2; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT fk_a63wvekftu8jo1pnj81e7mce2 FOREIGN KEY (composite) REFERENCES public.keycloak_role(id);


--
-- Name: authentication_execution fk_auth_exec_flow; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT fk_auth_exec_flow FOREIGN KEY (flow_id) REFERENCES public.authentication_flow(id);


--
-- Name: authentication_execution fk_auth_exec_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT fk_auth_exec_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: authentication_flow fk_auth_flow_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authentication_flow
    ADD CONSTRAINT fk_auth_flow_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: authenticator_config fk_auth_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authenticator_config
    ADD CONSTRAINT fk_auth_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_session fk_b4ao2vcvat6ukau74wbwtfqo1; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session
    ADD CONSTRAINT fk_b4ao2vcvat6ukau74wbwtfqo1 FOREIGN KEY (session_id) REFERENCES public.user_session(id);


--
-- Name: user_role_mapping fk_c4fqv34p1mbylloxang7b1q3l; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_role_mapping
    ADD CONSTRAINT fk_c4fqv34p1mbylloxang7b1q3l FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: client_scope_client fk_c_cli_scope_client; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_client
    ADD CONSTRAINT fk_c_cli_scope_client FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_scope_client fk_c_cli_scope_scope; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_client
    ADD CONSTRAINT fk_c_cli_scope_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_scope_attributes fk_cl_scope_attr_scope; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_attributes
    ADD CONSTRAINT fk_cl_scope_attr_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_scope_role_mapping fk_cl_scope_rm_role; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT fk_cl_scope_rm_role FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: client_scope_role_mapping fk_cl_scope_rm_scope; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT fk_cl_scope_rm_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_user_session_note fk_cl_usr_ses_note; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_user_session_note
    ADD CONSTRAINT fk_cl_usr_ses_note FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: protocol_mapper fk_cli_scope_mapper; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT fk_cli_scope_mapper FOREIGN KEY (client_scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_initial_access fk_client_init_acc_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_initial_access
    ADD CONSTRAINT fk_client_init_acc_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: component_config fk_component_config; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.component_config
    ADD CONSTRAINT fk_component_config FOREIGN KEY (component_id) REFERENCES public.component(id);


--
-- Name: component fk_component_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT fk_component_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_default_groups fk_def_groups_group; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT fk_def_groups_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: realm_default_groups fk_def_groups_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT fk_def_groups_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_default_roles fk_evudb1ppw84oxfax2drs03icc; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_roles
    ADD CONSTRAINT fk_evudb1ppw84oxfax2drs03icc FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: user_federation_mapper_config fk_fedmapper_cfg; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_mapper_config
    ADD CONSTRAINT fk_fedmapper_cfg FOREIGN KEY (user_federation_mapper_id) REFERENCES public.user_federation_mapper(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_fedprv; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_fedprv FOREIGN KEY (federation_provider_id) REFERENCES public.user_federation_provider(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: associated_policy fk_frsr5s213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT fk_frsr5s213xcx4wnkog82ssrfy FOREIGN KEY (associated_policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrasp13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT fk_frsrasp13xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog82sspmt; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82sspmt FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_server_resource fk_frsrho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog83sspmt; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog83sspmt FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog84sspmt; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog84sspmt FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: associated_policy fk_frsrpas14xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT fk_frsrpas14xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrpass3xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT fk_frsrpass3xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: resource_server_perm_ticket fk_frsrpo2128cx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrpo2128cx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_server_policy fk_frsrpo213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT fk_frsrpo213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_scope fk_frsrpos13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT fk_frsrpos13xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpos53xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT fk_frsrpos53xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpp213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT fk_frsrpp213xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_scope fk_frsrps213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT fk_frsrps213xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: resource_server_scope fk_frsrso213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT fk_frsrso213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: composite_role fk_gr7thllb9lu8q4vqa4524jjy8; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT fk_gr7thllb9lu8q4vqa4524jjy8 FOREIGN KEY (child_role) REFERENCES public.keycloak_role(id);


--
-- Name: user_consent_client_scope fk_grntcsnt_clsc_usc; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_consent_client_scope
    ADD CONSTRAINT fk_grntcsnt_clsc_usc FOREIGN KEY (user_consent_id) REFERENCES public.user_consent(id);


--
-- Name: user_consent fk_grntcsnt_user; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT fk_grntcsnt_user FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: group_attribute fk_group_attribute_group; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.group_attribute
    ADD CONSTRAINT fk_group_attribute_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: keycloak_group fk_group_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT fk_group_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: group_role_mapping fk_group_role_group; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT fk_group_role_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: group_role_mapping fk_group_role_role; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT fk_group_role_role FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: realm_default_roles fk_h4wpd7w4hsoolni3h0sw7btje; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_roles
    ADD CONSTRAINT fk_h4wpd7w4hsoolni3h0sw7btje FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: realm_enabled_event_types fk_h846o4h0w8epx5nwedrf5y69j; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_enabled_event_types
    ADD CONSTRAINT fk_h846o4h0w8epx5nwedrf5y69j FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_events_listeners fk_h846o4h0w8epx5nxev9f5y69j; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_events_listeners
    ADD CONSTRAINT fk_h846o4h0w8epx5nxev9f5y69j FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: identity_provider_mapper fk_idpm_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider_mapper
    ADD CONSTRAINT fk_idpm_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: idp_mapper_config fk_idpmconfig; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.idp_mapper_config
    ADD CONSTRAINT fk_idpmconfig FOREIGN KEY (idp_mapper_id) REFERENCES public.identity_provider_mapper(id);


--
-- Name: keycloak_role fk_kjho5le2c0ral09fl8cm9wfw9; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT fk_kjho5le2c0ral09fl8cm9wfw9 FOREIGN KEY (client) REFERENCES public.client(id);


--
-- Name: web_origins fk_lojpho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.web_origins
    ADD CONSTRAINT fk_lojpho213xcx4wnkog82ssrfy FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_default_roles fk_nuilts7klwqw2h8m2b5joytky; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_default_roles
    ADD CONSTRAINT fk_nuilts7klwqw2h8m2b5joytky FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: scope_mapping fk_ouse064plmlr732lxjcn1q5f1; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT fk_ouse064plmlr732lxjcn1q5f1 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: scope_mapping fk_p3rh9grku11kqfrs4fltt7rnq; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT fk_p3rh9grku11kqfrs4fltt7rnq FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: client fk_p56ctinxxb9gsk57fo49f9tac; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT fk_p56ctinxxb9gsk57fo49f9tac FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: protocol_mapper fk_pcm_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT fk_pcm_realm FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: credential fk_pfyr0glasqyl0dei3kl69r6v0; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.credential
    ADD CONSTRAINT fk_pfyr0glasqyl0dei3kl69r6v0 FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: protocol_mapper_config fk_pmconfig; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.protocol_mapper_config
    ADD CONSTRAINT fk_pmconfig FOREIGN KEY (protocol_mapper_id) REFERENCES public.protocol_mapper(id);


--
-- Name: default_client_scope fk_r_def_cli_scope_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT fk_r_def_cli_scope_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: default_client_scope fk_r_def_cli_scope_scope; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT fk_r_def_cli_scope_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_scope fk_realm_cli_scope; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT fk_realm_cli_scope FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: required_action_provider fk_req_act_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.required_action_provider
    ADD CONSTRAINT fk_req_act_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: resource_uris fk_resource_server_uris; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_uris
    ADD CONSTRAINT fk_resource_server_uris FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: role_attribute fk_role_attribute_id; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.role_attribute
    ADD CONSTRAINT fk_role_attribute_id FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: realm_supported_locales fk_supported_locales_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_supported_locales
    ADD CONSTRAINT fk_supported_locales_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: user_federation_config fk_t13hpu1j94r2ebpekr39x5eu5; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_config
    ADD CONSTRAINT fk_t13hpu1j94r2ebpekr39x5eu5 FOREIGN KEY (user_federation_provider_id) REFERENCES public.user_federation_provider(id);


--
-- Name: realm fk_traf444kk6qrkms7n56aiwq5y; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT fk_traf444kk6qrkms7n56aiwq5y FOREIGN KEY (master_admin_client) REFERENCES public.client(id);


--
-- Name: user_group_membership fk_user_group_user; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_group_membership
    ADD CONSTRAINT fk_user_group_user FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: policy_config fkdc34197cf864c4e43; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.policy_config
    ADD CONSTRAINT fkdc34197cf864c4e43 FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: identity_provider_config fkdc4897cf864c4e43; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider_config
    ADD CONSTRAINT fkdc4897cf864c4e43 FOREIGN KEY (identity_provider_id) REFERENCES public.identity_provider(internal_id);


--
-- PostgreSQL database dump complete
--

