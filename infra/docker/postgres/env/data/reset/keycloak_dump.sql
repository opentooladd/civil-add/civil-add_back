--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Debian 12.2-2.pgdg100+1)
-- Dumped by pg_dump version 12.2 (Debian 12.2-2.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: admin_event_entity; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.admin_event_entity (
    id character varying(36) NOT NULL,
    admin_event_time bigint,
    realm_id character varying(255),
    operation_type character varying(255),
    auth_realm_id character varying(255),
    auth_client_id character varying(255),
    auth_user_id character varying(255),
    ip_address character varying(255),
    resource_path character varying(2550),
    representation text,
    error character varying(255),
    resource_type character varying(64)
);


ALTER TABLE public.admin_event_entity OWNER TO "kUser";

--
-- Name: associated_policy; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.associated_policy (
    policy_id character varying(36) NOT NULL,
    associated_policy_id character varying(36) NOT NULL
);


ALTER TABLE public.associated_policy OWNER TO "kUser";

--
-- Name: authentication_execution; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.authentication_execution (
    id character varying(36) NOT NULL,
    alias character varying(255),
    authenticator character varying(36),
    realm_id character varying(36),
    flow_id character varying(36),
    requirement integer,
    priority integer,
    authenticator_flow boolean DEFAULT false NOT NULL,
    auth_flow_id character varying(36),
    auth_config character varying(36)
);


ALTER TABLE public.authentication_execution OWNER TO "kUser";

--
-- Name: authentication_flow; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.authentication_flow (
    id character varying(36) NOT NULL,
    alias character varying(255),
    description character varying(255),
    realm_id character varying(36),
    provider_id character varying(36) DEFAULT 'basic-flow'::character varying NOT NULL,
    top_level boolean DEFAULT false NOT NULL,
    built_in boolean DEFAULT false NOT NULL
);


ALTER TABLE public.authentication_flow OWNER TO "kUser";

--
-- Name: authenticator_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.authenticator_config (
    id character varying(36) NOT NULL,
    alias character varying(255),
    realm_id character varying(36)
);


ALTER TABLE public.authenticator_config OWNER TO "kUser";

--
-- Name: authenticator_config_entry; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.authenticator_config_entry (
    authenticator_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.authenticator_config_entry OWNER TO "kUser";

--
-- Name: broker_link; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.broker_link (
    identity_provider character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL,
    broker_user_id character varying(255),
    broker_username character varying(255),
    token text,
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.broker_link OWNER TO "kUser";

--
-- Name: client; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client (
    id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    full_scope_allowed boolean DEFAULT false NOT NULL,
    client_id character varying(255),
    not_before integer,
    public_client boolean DEFAULT false NOT NULL,
    secret character varying(255),
    base_url character varying(255),
    bearer_only boolean DEFAULT false NOT NULL,
    management_url character varying(255),
    surrogate_auth_required boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    protocol character varying(255),
    node_rereg_timeout integer DEFAULT 0,
    frontchannel_logout boolean DEFAULT false NOT NULL,
    consent_required boolean DEFAULT false NOT NULL,
    name character varying(255),
    service_accounts_enabled boolean DEFAULT false NOT NULL,
    client_authenticator_type character varying(255),
    root_url character varying(255),
    description character varying(255),
    registration_token character varying(255),
    standard_flow_enabled boolean DEFAULT true NOT NULL,
    implicit_flow_enabled boolean DEFAULT false NOT NULL,
    direct_access_grants_enabled boolean DEFAULT false NOT NULL,
    always_display_in_console boolean DEFAULT false NOT NULL
);


ALTER TABLE public.client OWNER TO "kUser";

--
-- Name: client_attributes; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_attributes (
    client_id character varying(36) NOT NULL,
    value character varying(4000),
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_attributes OWNER TO "kUser";

--
-- Name: client_auth_flow_bindings; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_auth_flow_bindings (
    client_id character varying(36) NOT NULL,
    flow_id character varying(36),
    binding_name character varying(255) NOT NULL
);


ALTER TABLE public.client_auth_flow_bindings OWNER TO "kUser";

--
-- Name: client_default_roles; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_default_roles (
    client_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.client_default_roles OWNER TO "kUser";

--
-- Name: client_initial_access; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_initial_access (
    id character varying(36) NOT NULL,
    realm_id character varying(36) NOT NULL,
    "timestamp" integer,
    expiration integer,
    count integer,
    remaining_count integer
);


ALTER TABLE public.client_initial_access OWNER TO "kUser";

--
-- Name: client_node_registrations; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_node_registrations (
    client_id character varying(36) NOT NULL,
    value integer,
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_node_registrations OWNER TO "kUser";

--
-- Name: client_scope; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_scope (
    id character varying(36) NOT NULL,
    name character varying(255),
    realm_id character varying(36),
    description character varying(255),
    protocol character varying(255)
);


ALTER TABLE public.client_scope OWNER TO "kUser";

--
-- Name: client_scope_attributes; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_scope_attributes (
    scope_id character varying(36) NOT NULL,
    value character varying(2048),
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_scope_attributes OWNER TO "kUser";

--
-- Name: client_scope_client; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_scope_client (
    client_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE public.client_scope_client OWNER TO "kUser";

--
-- Name: client_scope_role_mapping; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_scope_role_mapping (
    scope_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.client_scope_role_mapping OWNER TO "kUser";

--
-- Name: client_session; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_session (
    id character varying(36) NOT NULL,
    client_id character varying(36),
    redirect_uri character varying(255),
    state character varying(255),
    "timestamp" integer,
    session_id character varying(36),
    auth_method character varying(255),
    realm_id character varying(255),
    auth_user_id character varying(36),
    current_action character varying(36)
);


ALTER TABLE public.client_session OWNER TO "kUser";

--
-- Name: client_session_auth_status; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_session_auth_status (
    authenticator character varying(36) NOT NULL,
    status integer,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_auth_status OWNER TO "kUser";

--
-- Name: client_session_note; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_session_note (
    name character varying(255) NOT NULL,
    value character varying(255),
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_note OWNER TO "kUser";

--
-- Name: client_session_prot_mapper; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_session_prot_mapper (
    protocol_mapper_id character varying(36) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_prot_mapper OWNER TO "kUser";

--
-- Name: client_session_role; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_session_role (
    role_id character varying(255) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_role OWNER TO "kUser";

--
-- Name: client_user_session_note; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.client_user_session_note (
    name character varying(255) NOT NULL,
    value character varying(2048),
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_user_session_note OWNER TO "kUser";

--
-- Name: component; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.component (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_id character varying(36),
    provider_id character varying(36),
    provider_type character varying(255),
    realm_id character varying(36),
    sub_type character varying(255)
);


ALTER TABLE public.component OWNER TO "kUser";

--
-- Name: component_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.component_config (
    id character varying(36) NOT NULL,
    component_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(4000)
);


ALTER TABLE public.component_config OWNER TO "kUser";

--
-- Name: composite_role; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.composite_role (
    composite character varying(36) NOT NULL,
    child_role character varying(36) NOT NULL
);


ALTER TABLE public.composite_role OWNER TO "kUser";

--
-- Name: credential; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.credential (
    id character varying(36) NOT NULL,
    salt bytea,
    type character varying(255),
    user_id character varying(36),
    created_date bigint,
    user_label character varying(255),
    secret_data text,
    credential_data text,
    priority integer
);


ALTER TABLE public.credential OWNER TO "kUser";

--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO "kUser";

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO "kUser";

--
-- Name: default_client_scope; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.default_client_scope (
    realm_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE public.default_client_scope OWNER TO "kUser";

--
-- Name: event_entity; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.event_entity (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    details_json character varying(2550),
    error character varying(255),
    ip_address character varying(255),
    realm_id character varying(255),
    session_id character varying(255),
    event_time bigint,
    type character varying(255),
    user_id character varying(255)
);


ALTER TABLE public.event_entity OWNER TO "kUser";

--
-- Name: fed_user_attribute; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_attribute (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    value character varying(2024)
);


ALTER TABLE public.fed_user_attribute OWNER TO "kUser";

--
-- Name: fed_user_consent; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE public.fed_user_consent OWNER TO "kUser";

--
-- Name: fed_user_consent_cl_scope; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_consent_cl_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.fed_user_consent_cl_scope OWNER TO "kUser";

--
-- Name: fed_user_credential; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_credential (
    id character varying(36) NOT NULL,
    salt bytea,
    type character varying(255),
    created_date bigint,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    user_label character varying(255),
    secret_data text,
    credential_data text,
    priority integer
);


ALTER TABLE public.fed_user_credential OWNER TO "kUser";

--
-- Name: fed_user_group_membership; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_group_membership OWNER TO "kUser";

--
-- Name: fed_user_required_action; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_required_action (
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_required_action OWNER TO "kUser";

--
-- Name: fed_user_role_mapping; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.fed_user_role_mapping (
    role_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_role_mapping OWNER TO "kUser";

--
-- Name: federated_identity; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.federated_identity (
    identity_provider character varying(255) NOT NULL,
    realm_id character varying(36),
    federated_user_id character varying(255),
    federated_username character varying(255),
    token text,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.federated_identity OWNER TO "kUser";

--
-- Name: federated_user; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.federated_user (
    id character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.federated_user OWNER TO "kUser";

--
-- Name: group_attribute; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.group_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.group_attribute OWNER TO "kUser";

--
-- Name: group_role_mapping; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.group_role_mapping (
    role_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.group_role_mapping OWNER TO "kUser";

--
-- Name: identity_provider; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.identity_provider (
    internal_id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    provider_alias character varying(255),
    provider_id character varying(255),
    store_token boolean DEFAULT false NOT NULL,
    authenticate_by_default boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    add_token_role boolean DEFAULT true NOT NULL,
    trust_email boolean DEFAULT false NOT NULL,
    first_broker_login_flow_id character varying(36),
    post_broker_login_flow_id character varying(36),
    provider_display_name character varying(255),
    link_only boolean DEFAULT false NOT NULL
);


ALTER TABLE public.identity_provider OWNER TO "kUser";

--
-- Name: identity_provider_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.identity_provider_config (
    identity_provider_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.identity_provider_config OWNER TO "kUser";

--
-- Name: identity_provider_mapper; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.identity_provider_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    idp_alias character varying(255) NOT NULL,
    idp_mapper_name character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.identity_provider_mapper OWNER TO "kUser";

--
-- Name: idp_mapper_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.idp_mapper_config (
    idp_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.idp_mapper_config OWNER TO "kUser";

--
-- Name: keycloak_group; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.keycloak_group (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_group character varying(36) NOT NULL,
    realm_id character varying(36)
);


ALTER TABLE public.keycloak_group OWNER TO "kUser";

--
-- Name: keycloak_role; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.keycloak_role (
    id character varying(36) NOT NULL,
    client_realm_constraint character varying(255),
    client_role boolean DEFAULT false NOT NULL,
    description character varying(255),
    name character varying(255),
    realm_id character varying(255),
    client character varying(36),
    realm character varying(36)
);


ALTER TABLE public.keycloak_role OWNER TO "kUser";

--
-- Name: migration_model; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.migration_model (
    id character varying(36) NOT NULL,
    version character varying(36),
    update_time bigint DEFAULT 0 NOT NULL
);


ALTER TABLE public.migration_model OWNER TO "kUser";

--
-- Name: offline_client_session; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.offline_client_session (
    user_session_id character varying(36) NOT NULL,
    client_id character varying(255) NOT NULL,
    offline_flag character varying(4) NOT NULL,
    "timestamp" integer,
    data text,
    client_storage_provider character varying(36) DEFAULT 'local'::character varying NOT NULL,
    external_client_id character varying(255) DEFAULT 'local'::character varying NOT NULL
);


ALTER TABLE public.offline_client_session OWNER TO "kUser";

--
-- Name: offline_user_session; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.offline_user_session (
    user_session_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    created_on integer NOT NULL,
    offline_flag character varying(4) NOT NULL,
    data text,
    last_session_refresh integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.offline_user_session OWNER TO "kUser";

--
-- Name: policy_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.policy_config (
    policy_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value text
);


ALTER TABLE public.policy_config OWNER TO "kUser";

--
-- Name: protocol_mapper; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.protocol_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    protocol character varying(255) NOT NULL,
    protocol_mapper_name character varying(255) NOT NULL,
    client_id character varying(36),
    client_scope_id character varying(36)
);


ALTER TABLE public.protocol_mapper OWNER TO "kUser";

--
-- Name: protocol_mapper_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.protocol_mapper_config (
    protocol_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.protocol_mapper_config OWNER TO "kUser";

--
-- Name: realm; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm (
    id character varying(36) NOT NULL,
    access_code_lifespan integer,
    user_action_lifespan integer,
    access_token_lifespan integer,
    account_theme character varying(255),
    admin_theme character varying(255),
    email_theme character varying(255),
    enabled boolean DEFAULT false NOT NULL,
    events_enabled boolean DEFAULT false NOT NULL,
    events_expiration bigint,
    login_theme character varying(255),
    name character varying(255),
    not_before integer,
    password_policy character varying(2550),
    registration_allowed boolean DEFAULT false NOT NULL,
    remember_me boolean DEFAULT false NOT NULL,
    reset_password_allowed boolean DEFAULT false NOT NULL,
    social boolean DEFAULT false NOT NULL,
    ssl_required character varying(255),
    sso_idle_timeout integer,
    sso_max_lifespan integer,
    update_profile_on_soc_login boolean DEFAULT false NOT NULL,
    verify_email boolean DEFAULT false NOT NULL,
    master_admin_client character varying(36),
    login_lifespan integer,
    internationalization_enabled boolean DEFAULT false NOT NULL,
    default_locale character varying(255),
    reg_email_as_username boolean DEFAULT false NOT NULL,
    admin_events_enabled boolean DEFAULT false NOT NULL,
    admin_events_details_enabled boolean DEFAULT false NOT NULL,
    edit_username_allowed boolean DEFAULT false NOT NULL,
    otp_policy_counter integer DEFAULT 0,
    otp_policy_window integer DEFAULT 1,
    otp_policy_period integer DEFAULT 30,
    otp_policy_digits integer DEFAULT 6,
    otp_policy_alg character varying(36) DEFAULT 'HmacSHA1'::character varying,
    otp_policy_type character varying(36) DEFAULT 'totp'::character varying,
    browser_flow character varying(36),
    registration_flow character varying(36),
    direct_grant_flow character varying(36),
    reset_credentials_flow character varying(36),
    client_auth_flow character varying(36),
    offline_session_idle_timeout integer DEFAULT 0,
    revoke_refresh_token boolean DEFAULT false NOT NULL,
    access_token_life_implicit integer DEFAULT 0,
    login_with_email_allowed boolean DEFAULT true NOT NULL,
    duplicate_emails_allowed boolean DEFAULT false NOT NULL,
    docker_auth_flow character varying(36),
    refresh_token_max_reuse integer DEFAULT 0,
    allow_user_managed_access boolean DEFAULT false NOT NULL,
    sso_max_lifespan_remember_me integer DEFAULT 0 NOT NULL,
    sso_idle_timeout_remember_me integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.realm OWNER TO "kUser";

--
-- Name: realm_attribute; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_attribute (
    name character varying(255) NOT NULL,
    value character varying(255),
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_attribute OWNER TO "kUser";

--
-- Name: realm_default_groups; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_default_groups (
    realm_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_default_groups OWNER TO "kUser";

--
-- Name: realm_default_roles; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_default_roles (
    realm_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_default_roles OWNER TO "kUser";

--
-- Name: realm_enabled_event_types; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_enabled_event_types (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_enabled_event_types OWNER TO "kUser";

--
-- Name: realm_events_listeners; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_events_listeners (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_events_listeners OWNER TO "kUser";

--
-- Name: realm_required_credential; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_required_credential (
    type character varying(255) NOT NULL,
    form_label character varying(255),
    input boolean DEFAULT false NOT NULL,
    secret boolean DEFAULT false NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_required_credential OWNER TO "kUser";

--
-- Name: realm_smtp_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_smtp_config (
    realm_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.realm_smtp_config OWNER TO "kUser";

--
-- Name: realm_supported_locales; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.realm_supported_locales (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_supported_locales OWNER TO "kUser";

--
-- Name: redirect_uris; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.redirect_uris (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.redirect_uris OWNER TO "kUser";

--
-- Name: required_action_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.required_action_config (
    required_action_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.required_action_config OWNER TO "kUser";

--
-- Name: required_action_provider; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.required_action_provider (
    id character varying(36) NOT NULL,
    alias character varying(255),
    name character varying(255),
    realm_id character varying(36),
    enabled boolean DEFAULT false NOT NULL,
    default_action boolean DEFAULT false NOT NULL,
    provider_id character varying(255),
    priority integer
);


ALTER TABLE public.required_action_provider OWNER TO "kUser";

--
-- Name: resource_attribute; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    resource_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_attribute OWNER TO "kUser";

--
-- Name: resource_policy; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_policy (
    resource_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_policy OWNER TO "kUser";

--
-- Name: resource_scope; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_scope (
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_scope OWNER TO "kUser";

--
-- Name: resource_server; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_server (
    id character varying(36) NOT NULL,
    allow_rs_remote_mgmt boolean DEFAULT false NOT NULL,
    policy_enforce_mode character varying(15) NOT NULL,
    decision_strategy smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.resource_server OWNER TO "kUser";

--
-- Name: resource_server_perm_ticket; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_server_perm_ticket (
    id character varying(36) NOT NULL,
    owner character varying(255) NOT NULL,
    requester character varying(255) NOT NULL,
    created_timestamp bigint NOT NULL,
    granted_timestamp bigint,
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36),
    resource_server_id character varying(36) NOT NULL,
    policy_id character varying(36)
);


ALTER TABLE public.resource_server_perm_ticket OWNER TO "kUser";

--
-- Name: resource_server_policy; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_server_policy (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    type character varying(255) NOT NULL,
    decision_strategy character varying(20),
    logic character varying(20),
    resource_server_id character varying(36) NOT NULL,
    owner character varying(255)
);


ALTER TABLE public.resource_server_policy OWNER TO "kUser";

--
-- Name: resource_server_resource; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_server_resource (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255),
    icon_uri character varying(255),
    owner character varying(255) NOT NULL,
    resource_server_id character varying(36) NOT NULL,
    owner_managed_access boolean DEFAULT false NOT NULL,
    display_name character varying(255)
);


ALTER TABLE public.resource_server_resource OWNER TO "kUser";

--
-- Name: resource_server_scope; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_server_scope (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    icon_uri character varying(255),
    resource_server_id character varying(36) NOT NULL,
    display_name character varying(255)
);


ALTER TABLE public.resource_server_scope OWNER TO "kUser";

--
-- Name: resource_uris; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.resource_uris (
    resource_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.resource_uris OWNER TO "kUser";

--
-- Name: role_attribute; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.role_attribute (
    id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255)
);


ALTER TABLE public.role_attribute OWNER TO "kUser";

--
-- Name: scope_mapping; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.scope_mapping (
    client_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.scope_mapping OWNER TO "kUser";

--
-- Name: scope_policy; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.scope_policy (
    scope_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE public.scope_policy OWNER TO "kUser";

--
-- Name: user_attribute; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_attribute (
    name character varying(255) NOT NULL,
    value character varying(255),
    user_id character varying(36) NOT NULL,
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL
);


ALTER TABLE public.user_attribute OWNER TO "kUser";

--
-- Name: user_consent; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    user_id character varying(36) NOT NULL,
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE public.user_consent OWNER TO "kUser";

--
-- Name: user_consent_client_scope; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_consent_client_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.user_consent_client_scope OWNER TO "kUser";

--
-- Name: user_entity; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_entity (
    id character varying(36) NOT NULL,
    email character varying(255),
    email_constraint character varying(255),
    email_verified boolean DEFAULT false NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    federation_link character varying(255),
    first_name character varying(255),
    last_name character varying(255),
    realm_id character varying(255),
    username character varying(255),
    created_timestamp bigint,
    service_account_client_link character varying(255),
    not_before integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.user_entity OWNER TO "kUser";

--
-- Name: user_federation_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_federation_config (
    user_federation_provider_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_federation_config OWNER TO "kUser";

--
-- Name: user_federation_mapper; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_federation_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    federation_provider_id character varying(36) NOT NULL,
    federation_mapper_type character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.user_federation_mapper OWNER TO "kUser";

--
-- Name: user_federation_mapper_config; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_federation_mapper_config (
    user_federation_mapper_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_federation_mapper_config OWNER TO "kUser";

--
-- Name: user_federation_provider; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_federation_provider (
    id character varying(36) NOT NULL,
    changed_sync_period integer,
    display_name character varying(255),
    full_sync_period integer,
    last_sync integer,
    priority integer,
    provider_name character varying(255),
    realm_id character varying(36)
);


ALTER TABLE public.user_federation_provider OWNER TO "kUser";

--
-- Name: user_group_membership; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.user_group_membership OWNER TO "kUser";

--
-- Name: user_required_action; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_required_action (
    user_id character varying(36) NOT NULL,
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL
);


ALTER TABLE public.user_required_action OWNER TO "kUser";

--
-- Name: user_role_mapping; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_role_mapping (
    role_id character varying(255) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.user_role_mapping OWNER TO "kUser";

--
-- Name: user_session; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_session (
    id character varying(36) NOT NULL,
    auth_method character varying(255),
    ip_address character varying(255),
    last_session_refresh integer,
    login_username character varying(255),
    realm_id character varying(255),
    remember_me boolean DEFAULT false NOT NULL,
    started integer,
    user_id character varying(255),
    user_session_state integer,
    broker_session_id character varying(255),
    broker_user_id character varying(255)
);


ALTER TABLE public.user_session OWNER TO "kUser";

--
-- Name: user_session_note; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.user_session_note (
    user_session character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(2048)
);


ALTER TABLE public.user_session_note OWNER TO "kUser";

--
-- Name: username_login_failure; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.username_login_failure (
    realm_id character varying(36) NOT NULL,
    username character varying(255) NOT NULL,
    failed_login_not_before integer,
    last_failure bigint,
    last_ip_failure character varying(255),
    num_failures integer
);


ALTER TABLE public.username_login_failure OWNER TO "kUser";

--
-- Name: web_origins; Type: TABLE; Schema: public; Owner: kUser
--

CREATE TABLE public.web_origins (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.web_origins OWNER TO "kUser";

--
-- Data for Name: admin_event_entity; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.admin_event_entity (id, admin_event_time, realm_id, operation_type, auth_realm_id, auth_client_id, auth_user_id, ip_address, resource_path, representation, error, resource_type) FROM stdin;
\.


--
-- Data for Name: associated_policy; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.associated_policy (policy_id, associated_policy_id) FROM stdin;
\.


--
-- Data for Name: authentication_execution; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.authentication_execution (id, alias, authenticator, realm_id, flow_id, requirement, priority, authenticator_flow, auth_flow_id, auth_config) FROM stdin;
01f5255b-14e0-4c29-b339-089c6f421356	\N	auth-cookie	master	444b300d-45af-457a-ba39-2b6ebed8fdef	2	10	f	\N	\N
809503ec-271b-497b-a4be-10138e61cd0d	\N	auth-spnego	master	444b300d-45af-457a-ba39-2b6ebed8fdef	3	20	f	\N	\N
e80f3fb2-80be-4537-bb27-e722848d95d4	\N	identity-provider-redirector	master	444b300d-45af-457a-ba39-2b6ebed8fdef	2	25	f	\N	\N
f2d439e2-db74-4622-8fdf-3ff3361905c0	\N	\N	master	444b300d-45af-457a-ba39-2b6ebed8fdef	2	30	t	de7ae12e-43b2-4b8e-acca-86e30873321a	\N
18f03c17-1c6c-4c38-9624-38fb549eddf1	\N	auth-username-password-form	master	de7ae12e-43b2-4b8e-acca-86e30873321a	0	10	f	\N	\N
c0fde716-f8a1-4218-9204-5edb09c4f97e	\N	\N	master	de7ae12e-43b2-4b8e-acca-86e30873321a	1	20	t	c14462a0-b77b-4290-9f05-be35bb2e7dcb	\N
518f5395-1be7-4bf0-bd6f-aec47b0a1b63	\N	conditional-user-configured	master	c14462a0-b77b-4290-9f05-be35bb2e7dcb	0	10	f	\N	\N
2acf3d49-8cc6-4ce0-81d5-1ffe7f44655d	\N	auth-otp-form	master	c14462a0-b77b-4290-9f05-be35bb2e7dcb	0	20	f	\N	\N
dff5f612-5f8f-4375-99a4-246dca1f61a5	\N	direct-grant-validate-username	master	d096bbf5-4c86-4d85-ae97-76be9e9a4511	0	10	f	\N	\N
b74b0db7-2d32-4dd2-b171-f62d0380a868	\N	direct-grant-validate-password	master	d096bbf5-4c86-4d85-ae97-76be9e9a4511	0	20	f	\N	\N
1434752e-e80d-4f4a-a712-a4fdd54547f2	\N	\N	master	d096bbf5-4c86-4d85-ae97-76be9e9a4511	1	30	t	1c44a6f4-b3d9-4414-98e9-c06e14ed06d9	\N
7d7aad54-4f16-4c99-abff-bf0f618f112c	\N	conditional-user-configured	master	1c44a6f4-b3d9-4414-98e9-c06e14ed06d9	0	10	f	\N	\N
37acbc63-38d8-4d19-9b02-c49a67d3cef2	\N	direct-grant-validate-otp	master	1c44a6f4-b3d9-4414-98e9-c06e14ed06d9	0	20	f	\N	\N
4866636e-6c1d-4657-b3bd-d6ae4c7b7801	\N	registration-page-form	master	10c15e38-1179-46c8-b98e-a1b34e293623	0	10	t	374d5f01-d5cc-4147-8e6c-ff5a2cc99b74	\N
daaeb1db-d3f3-4513-8a1a-fa4d761f2ffb	\N	registration-user-creation	master	374d5f01-d5cc-4147-8e6c-ff5a2cc99b74	0	20	f	\N	\N
17736364-5993-46f4-87a0-3143ebbf51ac	\N	registration-profile-action	master	374d5f01-d5cc-4147-8e6c-ff5a2cc99b74	0	40	f	\N	\N
18c246ef-e418-4882-b561-2b2f45cac37e	\N	registration-password-action	master	374d5f01-d5cc-4147-8e6c-ff5a2cc99b74	0	50	f	\N	\N
5f1ba7a7-c353-4394-948c-aeba1a026ac8	\N	registration-recaptcha-action	master	374d5f01-d5cc-4147-8e6c-ff5a2cc99b74	3	60	f	\N	\N
755b9eba-058a-45c1-82e8-f9a813923019	\N	reset-credentials-choose-user	master	a6978b30-60da-4368-a4dc-e74d6443019a	0	10	f	\N	\N
93f6f9dc-a3b1-4368-9a6c-be7cade818c3	\N	reset-credential-email	master	a6978b30-60da-4368-a4dc-e74d6443019a	0	20	f	\N	\N
2e18d1eb-60bd-4c5f-ace5-9be94f605d26	\N	reset-password	master	a6978b30-60da-4368-a4dc-e74d6443019a	0	30	f	\N	\N
832db61d-c3e8-4082-b4cb-fc5c31c56904	\N	\N	master	a6978b30-60da-4368-a4dc-e74d6443019a	1	40	t	2ba7caf1-c83d-404d-849f-82b4bbca35d6	\N
e856e9fc-55e8-41cd-9757-373289bbf19a	\N	conditional-user-configured	master	2ba7caf1-c83d-404d-849f-82b4bbca35d6	0	10	f	\N	\N
e8575885-d732-41ea-a48d-f0ed731a681e	\N	reset-otp	master	2ba7caf1-c83d-404d-849f-82b4bbca35d6	0	20	f	\N	\N
374749f0-acd0-4c08-b728-62c4e71fa8e5	\N	client-secret	master	0f4f02c1-6068-4d34-b994-4037ace5ccfc	2	10	f	\N	\N
574f000c-82cd-48a4-9f2e-4c398ba75f93	\N	client-jwt	master	0f4f02c1-6068-4d34-b994-4037ace5ccfc	2	20	f	\N	\N
5f5c524d-769b-493b-8324-98e15b4b8ba6	\N	client-secret-jwt	master	0f4f02c1-6068-4d34-b994-4037ace5ccfc	2	30	f	\N	\N
4eedec33-1849-4424-aeaa-ffc853c4d15a	\N	client-x509	master	0f4f02c1-6068-4d34-b994-4037ace5ccfc	2	40	f	\N	\N
a47b8670-6ed5-456a-bf57-2ce950ee5bbc	\N	idp-review-profile	master	b0fdb1ce-697b-4a08-9b52-16a4b11d16fc	0	10	f	\N	1eb810cd-0621-49ed-a002-64449e4fb8d0
856d745c-a989-4292-869e-58e1dd7ae0af	\N	\N	master	b0fdb1ce-697b-4a08-9b52-16a4b11d16fc	0	20	t	ee62d720-3608-4f89-9a6c-9f2fe76fca48	\N
295299d3-fa4e-41a7-9767-e1bb520cf1f0	\N	idp-create-user-if-unique	master	ee62d720-3608-4f89-9a6c-9f2fe76fca48	2	10	f	\N	d0f891da-1924-47b9-ade0-a23a3caa9f19
4d0beb23-17e4-4052-8669-33463f1d3611	\N	\N	master	ee62d720-3608-4f89-9a6c-9f2fe76fca48	2	20	t	d7865b28-e688-40e9-a88e-5040c5002bcc	\N
6c4ee247-2021-431d-836e-c2bda8e08f98	\N	idp-confirm-link	master	d7865b28-e688-40e9-a88e-5040c5002bcc	0	10	f	\N	\N
04f4de8c-72cd-4bd2-9736-ef885fc93844	\N	\N	master	d7865b28-e688-40e9-a88e-5040c5002bcc	0	20	t	7e2899a8-0c08-4eba-8da9-c62fe1aa6dc0	\N
b9e9af17-dec6-44dc-aa6f-6a852f7582dc	\N	idp-email-verification	master	7e2899a8-0c08-4eba-8da9-c62fe1aa6dc0	2	10	f	\N	\N
5a511413-ecfe-477e-8d83-8a0071a23481	\N	\N	master	7e2899a8-0c08-4eba-8da9-c62fe1aa6dc0	2	20	t	7310950f-74b2-40a4-8f06-5d31dd66dc78	\N
d433a878-11bf-4ce2-8b95-792b557f1763	\N	idp-username-password-form	master	7310950f-74b2-40a4-8f06-5d31dd66dc78	0	10	f	\N	\N
f17881d4-0efb-47cd-90ce-dc58d677c376	\N	\N	master	7310950f-74b2-40a4-8f06-5d31dd66dc78	1	20	t	d4799c8d-65f0-4b3c-9cf4-f463199bac83	\N
2ff04a99-caeb-4f8d-b312-44f6f246c2ff	\N	conditional-user-configured	master	d4799c8d-65f0-4b3c-9cf4-f463199bac83	0	10	f	\N	\N
00b9005d-c788-40ae-b707-ef6efc824618	\N	auth-otp-form	master	d4799c8d-65f0-4b3c-9cf4-f463199bac83	0	20	f	\N	\N
1db9ebf2-bc2e-4f9e-b15a-fcdcecd68f7d	\N	http-basic-authenticator	master	dfb3a701-0745-4cac-9ba4-669c068b0239	0	10	f	\N	\N
8c33dfe2-c437-4c17-b6e5-f8fca3d00df4	\N	docker-http-basic-authenticator	master	26b90fec-b105-4d32-a851-882424f64d13	0	10	f	\N	\N
764a65be-b367-43c6-8cdb-87b3251ce44e	\N	no-cookie-redirect	master	35cea52a-6e96-4876-960e-028fb9699230	0	10	f	\N	\N
382dae3d-179d-4492-93d9-dc50c7b3d600	\N	\N	master	35cea52a-6e96-4876-960e-028fb9699230	0	20	t	8456094c-049c-4ea9-bba4-624720e1b03a	\N
b359b26d-9259-4ca2-9940-ea2edf3dc8b4	\N	basic-auth	master	8456094c-049c-4ea9-bba4-624720e1b03a	0	10	f	\N	\N
40595210-9f05-4ced-bc3f-deffae06b368	\N	basic-auth-otp	master	8456094c-049c-4ea9-bba4-624720e1b03a	3	20	f	\N	\N
8372b927-2339-4745-b1e1-2a2afbe2ce87	\N	auth-spnego	master	8456094c-049c-4ea9-bba4-624720e1b03a	3	30	f	\N	\N
\.


--
-- Data for Name: authentication_flow; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.authentication_flow (id, alias, description, realm_id, provider_id, top_level, built_in) FROM stdin;
444b300d-45af-457a-ba39-2b6ebed8fdef	browser	browser based authentication	master	basic-flow	t	t
de7ae12e-43b2-4b8e-acca-86e30873321a	forms	Username, password, otp and other auth forms.	master	basic-flow	f	t
c14462a0-b77b-4290-9f05-be35bb2e7dcb	Browser - Conditional OTP	Flow to determine if the OTP is required for the authentication	master	basic-flow	f	t
d096bbf5-4c86-4d85-ae97-76be9e9a4511	direct grant	OpenID Connect Resource Owner Grant	master	basic-flow	t	t
1c44a6f4-b3d9-4414-98e9-c06e14ed06d9	Direct Grant - Conditional OTP	Flow to determine if the OTP is required for the authentication	master	basic-flow	f	t
10c15e38-1179-46c8-b98e-a1b34e293623	registration	registration flow	master	basic-flow	t	t
374d5f01-d5cc-4147-8e6c-ff5a2cc99b74	registration form	registration form	master	form-flow	f	t
a6978b30-60da-4368-a4dc-e74d6443019a	reset credentials	Reset credentials for a user if they forgot their password or something	master	basic-flow	t	t
2ba7caf1-c83d-404d-849f-82b4bbca35d6	Reset - Conditional OTP	Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.	master	basic-flow	f	t
0f4f02c1-6068-4d34-b994-4037ace5ccfc	clients	Base authentication for clients	master	client-flow	t	t
b0fdb1ce-697b-4a08-9b52-16a4b11d16fc	first broker login	Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account	master	basic-flow	t	t
ee62d720-3608-4f89-9a6c-9f2fe76fca48	User creation or linking	Flow for the existing/non-existing user alternatives	master	basic-flow	f	t
d7865b28-e688-40e9-a88e-5040c5002bcc	Handle Existing Account	Handle what to do if there is existing account with same email/username like authenticated identity provider	master	basic-flow	f	t
7e2899a8-0c08-4eba-8da9-c62fe1aa6dc0	Account verification options	Method with which to verity the existing account	master	basic-flow	f	t
7310950f-74b2-40a4-8f06-5d31dd66dc78	Verify Existing Account by Re-authentication	Reauthentication of existing account	master	basic-flow	f	t
d4799c8d-65f0-4b3c-9cf4-f463199bac83	First broker login - Conditional OTP	Flow to determine if the OTP is required for the authentication	master	basic-flow	f	t
dfb3a701-0745-4cac-9ba4-669c068b0239	saml ecp	SAML ECP Profile Authentication Flow	master	basic-flow	t	t
26b90fec-b105-4d32-a851-882424f64d13	docker auth	Used by Docker clients to authenticate against the IDP	master	basic-flow	t	t
35cea52a-6e96-4876-960e-028fb9699230	http challenge	An authentication flow based on challenge-response HTTP Authentication Schemes	master	basic-flow	t	t
8456094c-049c-4ea9-bba4-624720e1b03a	Authentication Options	Authentication options.	master	basic-flow	f	t
\.


--
-- Data for Name: authenticator_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.authenticator_config (id, alias, realm_id) FROM stdin;
1eb810cd-0621-49ed-a002-64449e4fb8d0	review profile config	master
d0f891da-1924-47b9-ade0-a23a3caa9f19	create unique user config	master
\.


--
-- Data for Name: authenticator_config_entry; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.authenticator_config_entry (authenticator_id, value, name) FROM stdin;
1eb810cd-0621-49ed-a002-64449e4fb8d0	missing	update.profile.on.first.login
d0f891da-1924-47b9-ade0-a23a3caa9f19	false	require.password.update.after.registration
\.


--
-- Data for Name: broker_link; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.broker_link (identity_provider, storage_provider_id, realm_id, broker_user_id, broker_username, token, user_id) FROM stdin;
\.


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client (id, enabled, full_scope_allowed, client_id, not_before, public_client, secret, base_url, bearer_only, management_url, surrogate_auth_required, realm_id, protocol, node_rereg_timeout, frontchannel_logout, consent_required, name, service_accounts_enabled, client_authenticator_type, root_url, description, registration_token, standard_flow_enabled, implicit_flow_enabled, direct_access_grants_enabled, always_display_in_console) FROM stdin;
2a07992e-4027-44de-ba50-f2c608090153	t	t	master-realm	0	f	7e73a6c3-f189-4645-9d55-30743ad13db2	\N	t	\N	f	master	\N	0	f	f	master Realm	f	client-secret	\N	\N	\N	t	f	f	f
93fad04f-e88d-4a85-82fd-b00c3cd09128	t	f	account	0	f	ff715e51-2c89-4c84-8ca2-e92aa670983d	/realms/master/account/	f	\N	f	master	openid-connect	0	f	f	${client_account}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
fb016a40-542c-411d-90e3-20ec9204dd99	t	f	account-console	0	t	41bf6550-f2e5-4602-85b8-9c6e2f595192	/realms/master/account/	f	\N	f	master	openid-connect	0	f	f	${client_account-console}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
6517b92e-7400-431b-a3a5-ea4e2fbde25c	t	f	broker	0	f	80b37c34-5858-467d-9890-b4d0aabc9365	\N	f	\N	f	master	openid-connect	0	f	f	${client_broker}	f	client-secret	\N	\N	\N	t	f	f	f
ea67977a-47af-4e65-aa4e-bcf35ea426ee	t	f	security-admin-console	0	t	91b60aa6-7597-403d-865e-6acc99e9baa0	/admin/master/console/	f	\N	f	master	openid-connect	0	f	f	${client_security-admin-console}	f	client-secret	${authAdminUrl}	\N	\N	t	f	f	f
20a36f74-fab6-4b1e-8e4b-59364b16ffb7	t	f	admin-cli	0	t	0f56dd48-678a-454e-8c9c-3f2c39197b39	\N	f	\N	f	master	openid-connect	0	f	f	${client_admin-cli}	f	client-secret	\N	\N	\N	f	f	t	f
\.


--
-- Data for Name: client_attributes; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_attributes (client_id, value, name) FROM stdin;
fb016a40-542c-411d-90e3-20ec9204dd99	S256	pkce.code.challenge.method
ea67977a-47af-4e65-aa4e-bcf35ea426ee	S256	pkce.code.challenge.method
\.


--
-- Data for Name: client_auth_flow_bindings; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_auth_flow_bindings (client_id, flow_id, binding_name) FROM stdin;
\.


--
-- Data for Name: client_default_roles; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_default_roles (client_id, role_id) FROM stdin;
93fad04f-e88d-4a85-82fd-b00c3cd09128	3daddb01-1bde-4f3c-bb1d-bef825700263
93fad04f-e88d-4a85-82fd-b00c3cd09128	2277bd23-8529-4ceb-8968-4b5355781066
\.


--
-- Data for Name: client_initial_access; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_initial_access (id, realm_id, "timestamp", expiration, count, remaining_count) FROM stdin;
\.


--
-- Data for Name: client_node_registrations; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_node_registrations (client_id, value, name) FROM stdin;
\.


--
-- Data for Name: client_scope; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_scope (id, name, realm_id, description, protocol) FROM stdin;
2a7ec7b7-4ea6-474c-8285-b5367f1c74d5	offline_access	master	OpenID Connect built-in scope: offline_access	openid-connect
e0c36b79-d5bb-4279-abc4-59af25565a36	role_list	master	SAML role list	saml
cefaca23-67d3-4fdf-8bd6-4b5be69107da	profile	master	OpenID Connect built-in scope: profile	openid-connect
3f25edd8-7f39-4ab7-b154-b001d5b8e743	email	master	OpenID Connect built-in scope: email	openid-connect
a392d362-f92c-4b69-baaa-6d2c9fb67345	address	master	OpenID Connect built-in scope: address	openid-connect
f942c3a6-f535-4c81-9c21-ec6538011f4e	phone	master	OpenID Connect built-in scope: phone	openid-connect
a6d6e178-5758-42e6-a057-02c00490b994	roles	master	OpenID Connect scope for add user roles to the access token	openid-connect
b1f71906-8978-4e47-82a6-1db23fbc7a98	web-origins	master	OpenID Connect scope for add allowed web origins to the access token	openid-connect
5aa0e2d9-302f-4694-b742-3199c7f637f5	microprofile-jwt	master	Microprofile - JWT built-in scope	openid-connect
\.


--
-- Data for Name: client_scope_attributes; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_scope_attributes (scope_id, value, name) FROM stdin;
2a7ec7b7-4ea6-474c-8285-b5367f1c74d5	true	display.on.consent.screen
2a7ec7b7-4ea6-474c-8285-b5367f1c74d5	${offlineAccessScopeConsentText}	consent.screen.text
e0c36b79-d5bb-4279-abc4-59af25565a36	true	display.on.consent.screen
e0c36b79-d5bb-4279-abc4-59af25565a36	${samlRoleListScopeConsentText}	consent.screen.text
cefaca23-67d3-4fdf-8bd6-4b5be69107da	true	display.on.consent.screen
cefaca23-67d3-4fdf-8bd6-4b5be69107da	${profileScopeConsentText}	consent.screen.text
cefaca23-67d3-4fdf-8bd6-4b5be69107da	true	include.in.token.scope
3f25edd8-7f39-4ab7-b154-b001d5b8e743	true	display.on.consent.screen
3f25edd8-7f39-4ab7-b154-b001d5b8e743	${emailScopeConsentText}	consent.screen.text
3f25edd8-7f39-4ab7-b154-b001d5b8e743	true	include.in.token.scope
a392d362-f92c-4b69-baaa-6d2c9fb67345	true	display.on.consent.screen
a392d362-f92c-4b69-baaa-6d2c9fb67345	${addressScopeConsentText}	consent.screen.text
a392d362-f92c-4b69-baaa-6d2c9fb67345	true	include.in.token.scope
f942c3a6-f535-4c81-9c21-ec6538011f4e	true	display.on.consent.screen
f942c3a6-f535-4c81-9c21-ec6538011f4e	${phoneScopeConsentText}	consent.screen.text
f942c3a6-f535-4c81-9c21-ec6538011f4e	true	include.in.token.scope
a6d6e178-5758-42e6-a057-02c00490b994	true	display.on.consent.screen
a6d6e178-5758-42e6-a057-02c00490b994	${rolesScopeConsentText}	consent.screen.text
a6d6e178-5758-42e6-a057-02c00490b994	false	include.in.token.scope
b1f71906-8978-4e47-82a6-1db23fbc7a98	false	display.on.consent.screen
b1f71906-8978-4e47-82a6-1db23fbc7a98		consent.screen.text
b1f71906-8978-4e47-82a6-1db23fbc7a98	false	include.in.token.scope
5aa0e2d9-302f-4694-b742-3199c7f637f5	false	display.on.consent.screen
5aa0e2d9-302f-4694-b742-3199c7f637f5	true	include.in.token.scope
\.


--
-- Data for Name: client_scope_client; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_scope_client (client_id, scope_id, default_scope) FROM stdin;
93fad04f-e88d-4a85-82fd-b00c3cd09128	e0c36b79-d5bb-4279-abc4-59af25565a36	t
fb016a40-542c-411d-90e3-20ec9204dd99	e0c36b79-d5bb-4279-abc4-59af25565a36	t
20a36f74-fab6-4b1e-8e4b-59364b16ffb7	e0c36b79-d5bb-4279-abc4-59af25565a36	t
6517b92e-7400-431b-a3a5-ea4e2fbde25c	e0c36b79-d5bb-4279-abc4-59af25565a36	t
2a07992e-4027-44de-ba50-f2c608090153	e0c36b79-d5bb-4279-abc4-59af25565a36	t
ea67977a-47af-4e65-aa4e-bcf35ea426ee	e0c36b79-d5bb-4279-abc4-59af25565a36	t
93fad04f-e88d-4a85-82fd-b00c3cd09128	cefaca23-67d3-4fdf-8bd6-4b5be69107da	t
93fad04f-e88d-4a85-82fd-b00c3cd09128	3f25edd8-7f39-4ab7-b154-b001d5b8e743	t
93fad04f-e88d-4a85-82fd-b00c3cd09128	a6d6e178-5758-42e6-a057-02c00490b994	t
93fad04f-e88d-4a85-82fd-b00c3cd09128	b1f71906-8978-4e47-82a6-1db23fbc7a98	t
93fad04f-e88d-4a85-82fd-b00c3cd09128	2a7ec7b7-4ea6-474c-8285-b5367f1c74d5	f
93fad04f-e88d-4a85-82fd-b00c3cd09128	a392d362-f92c-4b69-baaa-6d2c9fb67345	f
93fad04f-e88d-4a85-82fd-b00c3cd09128	f942c3a6-f535-4c81-9c21-ec6538011f4e	f
93fad04f-e88d-4a85-82fd-b00c3cd09128	5aa0e2d9-302f-4694-b742-3199c7f637f5	f
fb016a40-542c-411d-90e3-20ec9204dd99	cefaca23-67d3-4fdf-8bd6-4b5be69107da	t
fb016a40-542c-411d-90e3-20ec9204dd99	3f25edd8-7f39-4ab7-b154-b001d5b8e743	t
fb016a40-542c-411d-90e3-20ec9204dd99	a6d6e178-5758-42e6-a057-02c00490b994	t
fb016a40-542c-411d-90e3-20ec9204dd99	b1f71906-8978-4e47-82a6-1db23fbc7a98	t
fb016a40-542c-411d-90e3-20ec9204dd99	2a7ec7b7-4ea6-474c-8285-b5367f1c74d5	f
fb016a40-542c-411d-90e3-20ec9204dd99	a392d362-f92c-4b69-baaa-6d2c9fb67345	f
fb016a40-542c-411d-90e3-20ec9204dd99	f942c3a6-f535-4c81-9c21-ec6538011f4e	f
fb016a40-542c-411d-90e3-20ec9204dd99	5aa0e2d9-302f-4694-b742-3199c7f637f5	f
20a36f74-fab6-4b1e-8e4b-59364b16ffb7	cefaca23-67d3-4fdf-8bd6-4b5be69107da	t
20a36f74-fab6-4b1e-8e4b-59364b16ffb7	3f25edd8-7f39-4ab7-b154-b001d5b8e743	t
20a36f74-fab6-4b1e-8e4b-59364b16ffb7	a6d6e178-5758-42e6-a057-02c00490b994	t
20a36f74-fab6-4b1e-8e4b-59364b16ffb7	b1f71906-8978-4e47-82a6-1db23fbc7a98	t
20a36f74-fab6-4b1e-8e4b-59364b16ffb7	2a7ec7b7-4ea6-474c-8285-b5367f1c74d5	f
20a36f74-fab6-4b1e-8e4b-59364b16ffb7	a392d362-f92c-4b69-baaa-6d2c9fb67345	f
20a36f74-fab6-4b1e-8e4b-59364b16ffb7	f942c3a6-f535-4c81-9c21-ec6538011f4e	f
20a36f74-fab6-4b1e-8e4b-59364b16ffb7	5aa0e2d9-302f-4694-b742-3199c7f637f5	f
6517b92e-7400-431b-a3a5-ea4e2fbde25c	cefaca23-67d3-4fdf-8bd6-4b5be69107da	t
6517b92e-7400-431b-a3a5-ea4e2fbde25c	3f25edd8-7f39-4ab7-b154-b001d5b8e743	t
6517b92e-7400-431b-a3a5-ea4e2fbde25c	a6d6e178-5758-42e6-a057-02c00490b994	t
6517b92e-7400-431b-a3a5-ea4e2fbde25c	b1f71906-8978-4e47-82a6-1db23fbc7a98	t
6517b92e-7400-431b-a3a5-ea4e2fbde25c	2a7ec7b7-4ea6-474c-8285-b5367f1c74d5	f
6517b92e-7400-431b-a3a5-ea4e2fbde25c	a392d362-f92c-4b69-baaa-6d2c9fb67345	f
6517b92e-7400-431b-a3a5-ea4e2fbde25c	f942c3a6-f535-4c81-9c21-ec6538011f4e	f
6517b92e-7400-431b-a3a5-ea4e2fbde25c	5aa0e2d9-302f-4694-b742-3199c7f637f5	f
2a07992e-4027-44de-ba50-f2c608090153	cefaca23-67d3-4fdf-8bd6-4b5be69107da	t
2a07992e-4027-44de-ba50-f2c608090153	3f25edd8-7f39-4ab7-b154-b001d5b8e743	t
2a07992e-4027-44de-ba50-f2c608090153	a6d6e178-5758-42e6-a057-02c00490b994	t
2a07992e-4027-44de-ba50-f2c608090153	b1f71906-8978-4e47-82a6-1db23fbc7a98	t
2a07992e-4027-44de-ba50-f2c608090153	2a7ec7b7-4ea6-474c-8285-b5367f1c74d5	f
2a07992e-4027-44de-ba50-f2c608090153	a392d362-f92c-4b69-baaa-6d2c9fb67345	f
2a07992e-4027-44de-ba50-f2c608090153	f942c3a6-f535-4c81-9c21-ec6538011f4e	f
2a07992e-4027-44de-ba50-f2c608090153	5aa0e2d9-302f-4694-b742-3199c7f637f5	f
ea67977a-47af-4e65-aa4e-bcf35ea426ee	cefaca23-67d3-4fdf-8bd6-4b5be69107da	t
ea67977a-47af-4e65-aa4e-bcf35ea426ee	3f25edd8-7f39-4ab7-b154-b001d5b8e743	t
ea67977a-47af-4e65-aa4e-bcf35ea426ee	a6d6e178-5758-42e6-a057-02c00490b994	t
ea67977a-47af-4e65-aa4e-bcf35ea426ee	b1f71906-8978-4e47-82a6-1db23fbc7a98	t
ea67977a-47af-4e65-aa4e-bcf35ea426ee	2a7ec7b7-4ea6-474c-8285-b5367f1c74d5	f
ea67977a-47af-4e65-aa4e-bcf35ea426ee	a392d362-f92c-4b69-baaa-6d2c9fb67345	f
ea67977a-47af-4e65-aa4e-bcf35ea426ee	f942c3a6-f535-4c81-9c21-ec6538011f4e	f
ea67977a-47af-4e65-aa4e-bcf35ea426ee	5aa0e2d9-302f-4694-b742-3199c7f637f5	f
\.


--
-- Data for Name: client_scope_role_mapping; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_scope_role_mapping (scope_id, role_id) FROM stdin;
2a7ec7b7-4ea6-474c-8285-b5367f1c74d5	c5ab9c28-856d-44b5-a664-46921c327023
\.


--
-- Data for Name: client_session; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_session (id, client_id, redirect_uri, state, "timestamp", session_id, auth_method, realm_id, auth_user_id, current_action) FROM stdin;
\.


--
-- Data for Name: client_session_auth_status; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_session_auth_status (authenticator, status, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_note; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_session_note (name, value, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_prot_mapper; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_session_prot_mapper (protocol_mapper_id, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_role; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_session_role (role_id, client_session) FROM stdin;
\.


--
-- Data for Name: client_user_session_note; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.client_user_session_note (name, value, client_session) FROM stdin;
\.


--
-- Data for Name: component; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.component (id, name, parent_id, provider_id, provider_type, realm_id, sub_type) FROM stdin;
9b51117b-fa2a-4541-af98-d2688d506f62	Trusted Hosts	master	trusted-hosts	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
1573b8fe-a52a-448a-bdb6-67cd1e591b5b	Consent Required	master	consent-required	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
08e6b612-ac12-4b90-bd98-067131e640ea	Full Scope Disabled	master	scope	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
ae019fd9-b7df-4ab9-b0f2-a8de02523d43	Max Clients Limit	master	max-clients	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
2ee8159c-1193-49f0-9194-4d337caf9a9c	Allowed Protocol Mapper Types	master	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
c0ce36e8-8108-46a6-aa66-9eda94e23e84	Allowed Client Scopes	master	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
3a4a1d55-5383-4ad9-a502-89cc477d5eed	Allowed Protocol Mapper Types	master	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	authenticated
844555a7-b0a6-4b41-b3e5-5ca785b4291d	Allowed Client Scopes	master	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	authenticated
7bbdf7c0-37ef-4f13-9c52-71db20a042db	rsa-generated	master	rsa-generated	org.keycloak.keys.KeyProvider	master	\N
6cd93b8f-1629-4bec-8d89-862c49f38324	hmac-generated	master	hmac-generated	org.keycloak.keys.KeyProvider	master	\N
150730da-35b0-44cd-b847-db7e2004279b	aes-generated	master	aes-generated	org.keycloak.keys.KeyProvider	master	\N
\.


--
-- Data for Name: component_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.component_config (id, component_id, name, value) FROM stdin;
9f61995b-59c9-4575-8bdb-1ffbe9992cba	3a4a1d55-5383-4ad9-a502-89cc477d5eed	allowed-protocol-mapper-types	oidc-address-mapper
f3eb0637-46ae-478a-849a-df4f8da58975	3a4a1d55-5383-4ad9-a502-89cc477d5eed	allowed-protocol-mapper-types	saml-role-list-mapper
1855227f-96bd-48e0-a738-6a6e7b38adb3	3a4a1d55-5383-4ad9-a502-89cc477d5eed	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
79f54603-53b8-42b6-a5f9-9961976ac087	3a4a1d55-5383-4ad9-a502-89cc477d5eed	allowed-protocol-mapper-types	saml-user-property-mapper
4af4213a-56ad-4e4c-ae53-de3545690084	3a4a1d55-5383-4ad9-a502-89cc477d5eed	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
c6156472-596a-4ed8-afbc-43f3f554e3ff	3a4a1d55-5383-4ad9-a502-89cc477d5eed	allowed-protocol-mapper-types	oidc-full-name-mapper
92204be7-e1e2-431b-9ce2-0d858764bb69	3a4a1d55-5383-4ad9-a502-89cc477d5eed	allowed-protocol-mapper-types	saml-user-attribute-mapper
ac5e47ce-30b0-4c78-ab18-20cce55402c2	3a4a1d55-5383-4ad9-a502-89cc477d5eed	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
9a791f2c-855a-4ef8-8955-4b0d8d13acd8	c0ce36e8-8108-46a6-aa66-9eda94e23e84	allow-default-scopes	true
9f0c3d2d-666c-4ddf-9dd6-cdb9026dcfd0	2ee8159c-1193-49f0-9194-4d337caf9a9c	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
ae64e31c-2b31-4b3f-a0e9-852e0a576b32	2ee8159c-1193-49f0-9194-4d337caf9a9c	allowed-protocol-mapper-types	saml-user-attribute-mapper
a3a2c8f8-ad7b-47d8-a5f3-8829ecbc8378	2ee8159c-1193-49f0-9194-4d337caf9a9c	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
88178b93-fb2e-46c2-b5e4-fee31fe2d468	2ee8159c-1193-49f0-9194-4d337caf9a9c	allowed-protocol-mapper-types	saml-user-property-mapper
c4f2778b-8450-4c13-a9c3-bf04d9e5c2f8	2ee8159c-1193-49f0-9194-4d337caf9a9c	allowed-protocol-mapper-types	oidc-full-name-mapper
58906fae-dcef-43d3-86cb-ba56a018008b	2ee8159c-1193-49f0-9194-4d337caf9a9c	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
f78b4a0b-269f-4480-8ffa-10866fa8b723	2ee8159c-1193-49f0-9194-4d337caf9a9c	allowed-protocol-mapper-types	oidc-address-mapper
113f0305-80c3-4203-9dbe-ed2336898b06	2ee8159c-1193-49f0-9194-4d337caf9a9c	allowed-protocol-mapper-types	saml-role-list-mapper
788058a5-aa41-4745-a338-a052dd051319	9b51117b-fa2a-4541-af98-d2688d506f62	host-sending-registration-request-must-match	true
4dafd647-216b-4d96-958e-07cf2c63df08	9b51117b-fa2a-4541-af98-d2688d506f62	client-uris-must-match	true
48d01969-d7eb-4ef8-ba56-67685d6ecf23	844555a7-b0a6-4b41-b3e5-5ca785b4291d	allow-default-scopes	true
e34b8bd6-6c4d-4260-b3d6-1a90209e3b37	ae019fd9-b7df-4ab9-b0f2-a8de02523d43	max-clients	200
34321b92-1751-4015-8a70-6142d35a7e73	7bbdf7c0-37ef-4f13-9c52-71db20a042db	priority	100
e5fa995e-ee4c-4f1b-b06b-8b6f56648f7b	7bbdf7c0-37ef-4f13-9c52-71db20a042db	certificate	MIICmzCCAYMCBgFxkghquTANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjAwNDE5MTA0MDU3WhcNMzAwNDE5MTA0MjM3WjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCG68/atTf/DqQGNvr/oroq9B/+/GAawlDo1RSqpgPmzb9ho0MO0nrU77kfrC559kLWxX8fO+2slNpZ2XpgBp1Nyv+gEq8ujJlHUaq1+0L0W6UdRpW3l+wtpddh1kc+Z5d3fUl5lrBNQ+0jaRyP0wtEZPpFY2ZNIPLwdrmkcRkagJNACSnoryzprqy+4Tcx8as1MFRL2mARP42mK2Qwc5Rn1MBEpKqCP0nsJl2JcVY4+JFcyV4YJPwOX/3m5r1+/JZW3R56qqov5f50aYuXL/9FofTYzkLPFqwmS5kb1pbQWBo1LC2yBsALQ5ifOhvUEf3RcnXXmubFUTycQCZ9hdXVAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAA96StWtKjNZf2bRCLmdDQJ1zj3GjLra23CoiH2VPysCnCZYFvTaOSEWEq2hj9LT/+3oM7VWn/yzcW/ZFmTMqnHWPSAQ3mxGlgSJ55oqQ44EmVvGJivn6VsQjbpEMmYNrElu+hOitw+/2P79BvGRFGQgCGFg8miiQpGgG0n1YyAUXC+emPsNUC1ZshTFajdb8Vh/FOVzxNvRpIA7GkNasuKjj9DTiAc5NVf8RP7gyFcXyBQxypmkou0GdlEWJvmCS0cg0OUJdqrXX4SvnWqVj4eywVAPOPmkdi3nI5OMYF0S90HdRwHikrW51cpIbeCPb8cB2V9yoo/4kbDvVf0qn1w=
dfef9326-e7af-41dc-8cdb-b4878b124e74	7bbdf7c0-37ef-4f13-9c52-71db20a042db	privateKey	MIIEowIBAAKCAQEAhuvP2rU3/w6kBjb6/6K6KvQf/vxgGsJQ6NUUqqYD5s2/YaNDDtJ61O+5H6wuefZC1sV/HzvtrJTaWdl6YAadTcr/oBKvLoyZR1GqtftC9FulHUaVt5fsLaXXYdZHPmeXd31JeZawTUPtI2kcj9MLRGT6RWNmTSDy8Ha5pHEZGoCTQAkp6K8s6a6svuE3MfGrNTBUS9pgET+NpitkMHOUZ9TARKSqgj9J7CZdiXFWOPiRXMleGCT8Dl/95ua9fvyWVt0eeqqqL+X+dGmLly//RaH02M5CzxasJkuZG9aW0FgaNSwtsgbAC0OYnzob1BH90XJ115rmxVE8nEAmfYXV1QIDAQABAoIBAF/P3g9mWZmbAAgZyyz2llYifwZImsbl7ycpCkAVHPgubBYeJmTEO84zXreLQoYBx31VSu0XVbUVD+p2clz21YaE/friydy4o7Dt4djR0b5Nxnd/xJpYHLRd7RHhZT5/uiR0qFTcGd8EucZHwqywJnQvcTsNV3y2RU9WQIbyee1DDAWcNSaSmN/XA2j+TTq6WUV3TzRfJv5ArY57lqQYierX7bciXFP6kzVz/W+4DP3izdrTL+pFpXr0yeVHubPo+a4ulqGWw0AqhLirYNswXtclT2vb5rMXWE3LPcFVTxNRts9QA9rdGlg3FnBk79+lMLQl4VNKYFW6mwZ0VubSgIECgYEA4x77cBzQFCgAcawGRYiYm2cdclCWlmvk0rS0zLdjbUF8xzK89Gm3khNzPAR6IsNtg8JmHoVBTjyPqCE8xjQoIg6H+whl5lkXwsO7KsfQtSy1jTE0D/nlqR7BYhNEw2C6iICSsRZNkWVn2peJ9yYcvNJdHfw4vMnl6YWBS3Y0kQkCgYEAmBOhetqMb6GfmmkpdX7MF1pWRd/9mlrKG7y3fjzYt6Tkby9YGnsQUItSMZU3pIUOXa7582/Upw+9E/66zbWdnXQ56rNjedmtntaXT6JIPdg2ihqzmPPJKbnJCOm0hZC/Pk9fRcMUqYwKnX22nE3pMc6p2jzg5qGy1CwRzD9ZrW0CgYEAzeF369kyhhL81A4sYTvBmp2Og+GpoBGOC7Ljce+Loa6Lfcbwol5ymWgoPW+Rh5G/5ICEMYZ5/IHJd48pmEIPX0/s1BJ4zKZWc6caM12OQ62TpbnEh/bvyK6ofk8ZNkKo1LQ4RW8CQZq62dwkpHVX95J59oR8qiYa0+TBn7Y3UIECgYBfLJH36ATR7C2Nnlji/prN1qx9iIQXcNBh8YZIy925eMm5x8B0uiXosnibLU6oeNkXymUFdCx67T+g3t+b1BizLMT+XIxZ+uBMsvH5VY+unLztS26wv4lawrnNCS4AFSmK3nO6ni81OL5/9+gsBBp4IGVufoEl9XaMkJQG5nLCNQKBgHTqXqgblfmZ4fywk456GWJJq5C8SoP4NLyvrbjpRDROydsIpxSbEQser7R7aBLba1B1lxDTc/NMPx1+A4qTGckIsOBPnr7BJxvmdUQf4lMtf8OdrGUnpcR8C7YBfUm9+NDSz0SYZ0nsxEZg7CbTwmqOawyZFE94jadrIZN5RykQ
a40c6993-69f4-4ae0-bb42-e1f32b0e7645	150730da-35b0-44cd-b847-db7e2004279b	secret	8jgNwTcoulGq87hvOw6VGg
40a70ad2-2fd0-4b8c-91d0-ce356331ad78	150730da-35b0-44cd-b847-db7e2004279b	priority	100
2e916bfb-5c87-46bb-be49-a5bde453be54	150730da-35b0-44cd-b847-db7e2004279b	kid	7fb5a718-90a6-4af9-8741-7fd248720789
57e76859-3322-4613-b88a-0dd836e99cfb	6cd93b8f-1629-4bec-8d89-862c49f38324	algorithm	HS256
090ff25e-e74c-474f-93df-1e5d03ad84a7	6cd93b8f-1629-4bec-8d89-862c49f38324	secret	hQF7fmfjuzLxdxhsoGIZEKAcE65J3OVECI-Ui3FjfX6-VMQOBXBY4JeI_QmoUXloZi4FAwoPTSiuymLJtmQZJw
aab6b7f0-1308-4422-93bd-8078524bf156	6cd93b8f-1629-4bec-8d89-862c49f38324	priority	100
b7c6a318-e4eb-4836-8fe5-7bc76265c637	6cd93b8f-1629-4bec-8d89-862c49f38324	kid	02413a15-0091-47e4-adf7-3eb9b3bb0574
\.


--
-- Data for Name: composite_role; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.composite_role (composite, child_role) FROM stdin;
2734cd0d-17af-40e6-bd46-865c8fa66a2d	574bb134-480a-4892-89b2-d1e918f32b04
2734cd0d-17af-40e6-bd46-865c8fa66a2d	7a467c72-b420-4ce9-a53b-3fb75602d591
2734cd0d-17af-40e6-bd46-865c8fa66a2d	7c84e35a-646b-4cee-ba62-f92860c1f432
2734cd0d-17af-40e6-bd46-865c8fa66a2d	182a5064-6b8c-45ae-b795-0e02f00e6775
2734cd0d-17af-40e6-bd46-865c8fa66a2d	c2b5bbe6-7203-407e-8336-74686b2bf623
2734cd0d-17af-40e6-bd46-865c8fa66a2d	cb9a90b4-6702-4307-b6cc-08cd6e54cc8c
2734cd0d-17af-40e6-bd46-865c8fa66a2d	d874989b-34f0-44f8-8545-4ec9ad16edd6
2734cd0d-17af-40e6-bd46-865c8fa66a2d	aaa797fa-6916-4ae4-8391-52f4c2306eee
2734cd0d-17af-40e6-bd46-865c8fa66a2d	20bf613d-59e2-4e76-846f-1faf77c46fbf
2734cd0d-17af-40e6-bd46-865c8fa66a2d	b27cdeca-7541-4c76-8eb5-d8a8583eefd6
2734cd0d-17af-40e6-bd46-865c8fa66a2d	64cc6d28-f953-4e63-95d8-c9eea4cb58db
2734cd0d-17af-40e6-bd46-865c8fa66a2d	deb3cacd-5ee6-49db-87b8-47570c8604e0
2734cd0d-17af-40e6-bd46-865c8fa66a2d	69f5ae3f-78d8-48b5-a933-ce4f0d6624be
2734cd0d-17af-40e6-bd46-865c8fa66a2d	11a1efda-1e42-4cd9-b57b-fde06d6096c3
2734cd0d-17af-40e6-bd46-865c8fa66a2d	1891355c-0313-42cc-b54e-434f562f496e
2734cd0d-17af-40e6-bd46-865c8fa66a2d	64d9e42c-05d0-46f9-b0ca-9da5d4f87c3d
2734cd0d-17af-40e6-bd46-865c8fa66a2d	9a9dccbe-27ed-4f50-975b-6bb561cc8752
2734cd0d-17af-40e6-bd46-865c8fa66a2d	15410f55-634a-4dcd-8caf-5886f244db74
182a5064-6b8c-45ae-b795-0e02f00e6775	1891355c-0313-42cc-b54e-434f562f496e
182a5064-6b8c-45ae-b795-0e02f00e6775	15410f55-634a-4dcd-8caf-5886f244db74
c2b5bbe6-7203-407e-8336-74686b2bf623	64d9e42c-05d0-46f9-b0ca-9da5d4f87c3d
2277bd23-8529-4ceb-8968-4b5355781066	e154df14-921f-4480-a38f-0e1bded303dd
155fd8fc-f9de-4834-b648-af4fd0baeed0	6c52cc9b-2276-48de-b3f4-698cb9952c60
2734cd0d-17af-40e6-bd46-865c8fa66a2d	fc29d850-88ef-4e6b-b4fd-05ebd77dc61b
\.


--
-- Data for Name: credential; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.credential (id, salt, type, user_id, created_date, user_label, secret_data, credential_data, priority) FROM stdin;
c95ab7a8-66dc-4716-8b23-f0faef7bbdb5	\N	password	1cc8ec8b-50e6-4a1a-9fa4-79550b23939b	1587292958046	\N	{"value":"Oy+H3vr6CP/l9ztrqxjampPF8Dqiaky+2IJR3/MtprBi/Zi2dP/3BGVlDGKxc+TV/Oid5JfS+4SxN5uvG24MUA==","salt":"fCLdGY5kw4zWe2+9+TZO/g=="}	{"hashIterations":100000,"algorithm":"pbkdf2-sha256"}	10
\.


--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
1.0.0.Final-KEYCLOAK-5461	sthorger@redhat.com	META-INF/jpa-changelog-1.0.0.Final.xml	2020-04-19 10:42:25.766583	1	EXECUTED	7:4e70412f24a3f382c82183742ec79317	createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...		\N	3.5.4	\N	\N	7292945056
1.0.0.Final-KEYCLOAK-5461	sthorger@redhat.com	META-INF/db2-jpa-changelog-1.0.0.Final.xml	2020-04-19 10:42:25.805866	2	MARK_RAN	7:cb16724583e9675711801c6875114f28	createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...		\N	3.5.4	\N	\N	7292945056
1.1.0.Beta1	sthorger@redhat.com	META-INF/jpa-changelog-1.1.0.Beta1.xml	2020-04-19 10:42:25.968718	3	EXECUTED	7:0310eb8ba07cec616460794d42ade0fa	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=CLIENT_ATTRIBUTES; createTable tableName=CLIENT_SESSION_NOTE; createTable tableName=APP_NODE_REGISTRATIONS; addColumn table...		\N	3.5.4	\N	\N	7292945056
1.1.0.Final	sthorger@redhat.com	META-INF/jpa-changelog-1.1.0.Final.xml	2020-04-19 10:42:25.992407	4	EXECUTED	7:5d25857e708c3233ef4439df1f93f012	renameColumn newColumnName=EVENT_TIME, oldColumnName=TIME, tableName=EVENT_ENTITY		\N	3.5.4	\N	\N	7292945056
1.2.0.Beta1	psilva@redhat.com	META-INF/jpa-changelog-1.2.0.Beta1.xml	2020-04-19 10:42:26.203675	5	EXECUTED	7:c7a54a1041d58eb3817a4a883b4d4e84	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...		\N	3.5.4	\N	\N	7292945056
1.2.0.Beta1	psilva@redhat.com	META-INF/db2-jpa-changelog-1.2.0.Beta1.xml	2020-04-19 10:42:26.211231	6	MARK_RAN	7:2e01012df20974c1c2a605ef8afe25b7	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...		\N	3.5.4	\N	\N	7292945056
1.2.0.RC1	bburke@redhat.com	META-INF/jpa-changelog-1.2.0.CR1.xml	2020-04-19 10:42:26.368452	7	EXECUTED	7:0f08df48468428e0f30ee59a8ec01a41	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...		\N	3.5.4	\N	\N	7292945056
1.2.0.RC1	bburke@redhat.com	META-INF/db2-jpa-changelog-1.2.0.CR1.xml	2020-04-19 10:42:26.439773	8	MARK_RAN	7:a77ea2ad226b345e7d689d366f185c8c	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...		\N	3.5.4	\N	\N	7292945056
1.2.0.Final	keycloak	META-INF/jpa-changelog-1.2.0.Final.xml	2020-04-19 10:42:26.454486	9	EXECUTED	7:a3377a2059aefbf3b90ebb4c4cc8e2ab	update tableName=CLIENT; update tableName=CLIENT; update tableName=CLIENT		\N	3.5.4	\N	\N	7292945056
1.3.0	bburke@redhat.com	META-INF/jpa-changelog-1.3.0.xml	2020-04-19 10:42:26.668044	10	EXECUTED	7:04c1dbedc2aa3e9756d1a1668e003451	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=ADMI...		\N	3.5.4	\N	\N	7292945056
1.4.0	bburke@redhat.com	META-INF/jpa-changelog-1.4.0.xml	2020-04-19 10:42:26.784866	11	EXECUTED	7:36ef39ed560ad07062d956db861042ba	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	3.5.4	\N	\N	7292945056
1.4.0	bburke@redhat.com	META-INF/db2-jpa-changelog-1.4.0.xml	2020-04-19 10:42:26.7912	12	MARK_RAN	7:d909180b2530479a716d3f9c9eaea3d7	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	3.5.4	\N	\N	7292945056
1.5.0	bburke@redhat.com	META-INF/jpa-changelog-1.5.0.xml	2020-04-19 10:42:26.824432	13	EXECUTED	7:cf12b04b79bea5152f165eb41f3955f6	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	3.5.4	\N	\N	7292945056
1.6.1_from15	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2020-04-19 10:42:26.858222	14	EXECUTED	7:7e32c8f05c755e8675764e7d5f514509	addColumn tableName=REALM; addColumn tableName=KEYCLOAK_ROLE; addColumn tableName=CLIENT; createTable tableName=OFFLINE_USER_SESSION; createTable tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_US_SES_PK2, tableName=...		\N	3.5.4	\N	\N	7292945056
1.6.1_from16-pre	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2020-04-19 10:42:26.862254	15	MARK_RAN	7:980ba23cc0ec39cab731ce903dd01291	delete tableName=OFFLINE_CLIENT_SESSION; delete tableName=OFFLINE_USER_SESSION		\N	3.5.4	\N	\N	7292945056
1.6.1_from16	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2020-04-19 10:42:26.866366	16	MARK_RAN	7:2fa220758991285312eb84f3b4ff5336	dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_US_SES_PK, tableName=OFFLINE_USER_SESSION; dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_CL_SES_PK, tableName=OFFLINE_CLIENT_SESSION; addColumn tableName=OFFLINE_USER_SESSION; update tableName=OF...		\N	3.5.4	\N	\N	7292945056
1.6.1	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2020-04-19 10:42:26.870048	17	EXECUTED	7:d41d8cd98f00b204e9800998ecf8427e	empty		\N	3.5.4	\N	\N	7292945056
1.7.0	bburke@redhat.com	META-INF/jpa-changelog-1.7.0.xml	2020-04-19 10:42:26.972167	18	EXECUTED	7:91ace540896df890cc00a0490ee52bbc	createTable tableName=KEYCLOAK_GROUP; createTable tableName=GROUP_ROLE_MAPPING; createTable tableName=GROUP_ATTRIBUTE; createTable tableName=USER_GROUP_MEMBERSHIP; createTable tableName=REALM_DEFAULT_GROUPS; addColumn tableName=IDENTITY_PROVIDER; ...		\N	3.5.4	\N	\N	7292945056
1.8.0	mposolda@redhat.com	META-INF/jpa-changelog-1.8.0.xml	2020-04-19 10:42:27.079089	19	EXECUTED	7:c31d1646dfa2618a9335c00e07f89f24	addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...		\N	3.5.4	\N	\N	7292945056
1.8.0-2	keycloak	META-INF/jpa-changelog-1.8.0.xml	2020-04-19 10:42:27.087638	20	EXECUTED	7:df8bc21027a4f7cbbb01f6344e89ce07	dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL		\N	3.5.4	\N	\N	7292945056
authz-3.4.0.CR1-resource-server-pk-change-part1	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2020-04-19 10:42:27.936744	45	EXECUTED	7:6a48ce645a3525488a90fbf76adf3bb3	addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_RESOURCE; addColumn tableName=RESOURCE_SERVER_SCOPE		\N	3.5.4	\N	\N	7292945056
1.8.0	mposolda@redhat.com	META-INF/db2-jpa-changelog-1.8.0.xml	2020-04-19 10:42:27.095864	21	MARK_RAN	7:f987971fe6b37d963bc95fee2b27f8df	addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...		\N	3.5.4	\N	\N	7292945056
1.8.0-2	keycloak	META-INF/db2-jpa-changelog-1.8.0.xml	2020-04-19 10:42:27.100647	22	MARK_RAN	7:df8bc21027a4f7cbbb01f6344e89ce07	dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL		\N	3.5.4	\N	\N	7292945056
1.9.0	mposolda@redhat.com	META-INF/jpa-changelog-1.9.0.xml	2020-04-19 10:42:27.154748	23	EXECUTED	7:ed2dc7f799d19ac452cbcda56c929e47	update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=REALM; update tableName=REALM; customChange; dr...		\N	3.5.4	\N	\N	7292945056
1.9.1	keycloak	META-INF/jpa-changelog-1.9.1.xml	2020-04-19 10:42:27.163045	24	EXECUTED	7:80b5db88a5dda36ece5f235be8757615	modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=PUBLIC_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM		\N	3.5.4	\N	\N	7292945056
1.9.1	keycloak	META-INF/db2-jpa-changelog-1.9.1.xml	2020-04-19 10:42:27.166809	25	MARK_RAN	7:1437310ed1305a9b93f8848f301726ce	modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM		\N	3.5.4	\N	\N	7292945056
1.9.2	keycloak	META-INF/jpa-changelog-1.9.2.xml	2020-04-19 10:42:27.232923	26	EXECUTED	7:b82ffb34850fa0836be16deefc6a87c4	createIndex indexName=IDX_USER_EMAIL, tableName=USER_ENTITY; createIndex indexName=IDX_USER_ROLE_MAPPING, tableName=USER_ROLE_MAPPING; createIndex indexName=IDX_USER_GROUP_MAPPING, tableName=USER_GROUP_MEMBERSHIP; createIndex indexName=IDX_USER_CO...		\N	3.5.4	\N	\N	7292945056
authz-2.0.0	psilva@redhat.com	META-INF/jpa-changelog-authz-2.0.0.xml	2020-04-19 10:42:27.370456	27	EXECUTED	7:9cc98082921330d8d9266decdd4bd658	createTable tableName=RESOURCE_SERVER; addPrimaryKey constraintName=CONSTRAINT_FARS, tableName=RESOURCE_SERVER; addUniqueConstraint constraintName=UK_AU8TT6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER; createTable tableName=RESOURCE_SERVER_RESOU...		\N	3.5.4	\N	\N	7292945056
authz-2.5.1	psilva@redhat.com	META-INF/jpa-changelog-authz-2.5.1.xml	2020-04-19 10:42:27.377814	28	EXECUTED	7:03d64aeed9cb52b969bd30a7ac0db57e	update tableName=RESOURCE_SERVER_POLICY		\N	3.5.4	\N	\N	7292945056
2.1.0-KEYCLOAK-5461	bburke@redhat.com	META-INF/jpa-changelog-2.1.0.xml	2020-04-19 10:42:27.49746	29	EXECUTED	7:f1f9fd8710399d725b780f463c6b21cd	createTable tableName=BROKER_LINK; createTable tableName=FED_USER_ATTRIBUTE; createTable tableName=FED_USER_CONSENT; createTable tableName=FED_USER_CONSENT_ROLE; createTable tableName=FED_USER_CONSENT_PROT_MAPPER; createTable tableName=FED_USER_CR...		\N	3.5.4	\N	\N	7292945056
2.2.0	bburke@redhat.com	META-INF/jpa-changelog-2.2.0.xml	2020-04-19 10:42:27.523725	30	EXECUTED	7:53188c3eb1107546e6f765835705b6c1	addColumn tableName=ADMIN_EVENT_ENTITY; createTable tableName=CREDENTIAL_ATTRIBUTE; createTable tableName=FED_CREDENTIAL_ATTRIBUTE; modifyDataType columnName=VALUE, tableName=CREDENTIAL; addForeignKeyConstraint baseTableName=FED_CREDENTIAL_ATTRIBU...		\N	3.5.4	\N	\N	7292945056
2.3.0	bburke@redhat.com	META-INF/jpa-changelog-2.3.0.xml	2020-04-19 10:42:27.549862	31	EXECUTED	7:d6e6f3bc57a0c5586737d1351725d4d4	createTable tableName=FEDERATED_USER; addPrimaryKey constraintName=CONSTR_FEDERATED_USER, tableName=FEDERATED_USER; dropDefaultValue columnName=TOTP, tableName=USER_ENTITY; dropColumn columnName=TOTP, tableName=USER_ENTITY; addColumn tableName=IDE...		\N	3.5.4	\N	\N	7292945056
2.4.0	bburke@redhat.com	META-INF/jpa-changelog-2.4.0.xml	2020-04-19 10:42:27.557273	32	EXECUTED	7:454d604fbd755d9df3fd9c6329043aa5	customChange		\N	3.5.4	\N	\N	7292945056
2.5.0	bburke@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2020-04-19 10:42:27.567516	33	EXECUTED	7:57e98a3077e29caf562f7dbf80c72600	customChange; modifyDataType columnName=USER_ID, tableName=OFFLINE_USER_SESSION		\N	3.5.4	\N	\N	7292945056
2.5.0-unicode-oracle	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2020-04-19 10:42:27.57129	34	MARK_RAN	7:e4c7e8f2256210aee71ddc42f538b57a	modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...		\N	3.5.4	\N	\N	7292945056
2.5.0-unicode-other-dbs	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2020-04-19 10:42:27.610387	35	EXECUTED	7:09a43c97e49bc626460480aa1379b522	modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...		\N	3.5.4	\N	\N	7292945056
2.5.0-duplicate-email-support	slawomir@dabek.name	META-INF/jpa-changelog-2.5.0.xml	2020-04-19 10:42:27.618014	36	EXECUTED	7:26bfc7c74fefa9126f2ce702fb775553	addColumn tableName=REALM		\N	3.5.4	\N	\N	7292945056
2.5.0-unique-group-names	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2020-04-19 10:42:27.627314	37	EXECUTED	7:a161e2ae671a9020fff61e996a207377	addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	7292945056
2.5.1	bburke@redhat.com	META-INF/jpa-changelog-2.5.1.xml	2020-04-19 10:42:27.632816	38	EXECUTED	7:37fc1781855ac5388c494f1442b3f717	addColumn tableName=FED_USER_CONSENT		\N	3.5.4	\N	\N	7292945056
3.0.0	bburke@redhat.com	META-INF/jpa-changelog-3.0.0.xml	2020-04-19 10:42:27.638419	39	EXECUTED	7:13a27db0dae6049541136adad7261d27	addColumn tableName=IDENTITY_PROVIDER		\N	3.5.4	\N	\N	7292945056
3.2.0-fix	keycloak	META-INF/jpa-changelog-3.2.0.xml	2020-04-19 10:42:27.641486	40	MARK_RAN	7:550300617e3b59e8af3a6294df8248a3	addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS		\N	3.5.4	\N	\N	7292945056
3.2.0-fix-with-keycloak-5416	keycloak	META-INF/jpa-changelog-3.2.0.xml	2020-04-19 10:42:27.647599	41	MARK_RAN	7:e3a9482b8931481dc2772a5c07c44f17	dropIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS; addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS; createIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS		\N	3.5.4	\N	\N	7292945056
3.2.0-fix-offline-sessions	hmlnarik	META-INF/jpa-changelog-3.2.0.xml	2020-04-19 10:42:27.660011	42	EXECUTED	7:72b07d85a2677cb257edb02b408f332d	customChange		\N	3.5.4	\N	\N	7292945056
3.2.0-fixed	keycloak	META-INF/jpa-changelog-3.2.0.xml	2020-04-19 10:42:27.923121	43	EXECUTED	7:a72a7858967bd414835d19e04d880312	addColumn tableName=REALM; dropPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_PK2, tableName=OFFLINE_CLIENT_SESSION; dropColumn columnName=CLIENT_SESSION_ID, tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_P...		\N	3.5.4	\N	\N	7292945056
3.3.0	keycloak	META-INF/jpa-changelog-3.3.0.xml	2020-04-19 10:42:27.930339	44	EXECUTED	7:94edff7cf9ce179e7e85f0cd78a3cf2c	addColumn tableName=USER_ENTITY		\N	3.5.4	\N	\N	7292945056
authz-3.4.0.CR1-resource-server-pk-change-part2-KEYCLOAK-6095	hmlnarik@redhat.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2020-04-19 10:42:27.943679	46	EXECUTED	7:e64b5dcea7db06077c6e57d3b9e5ca14	customChange		\N	3.5.4	\N	\N	7292945056
authz-3.4.0.CR1-resource-server-pk-change-part3-fixed	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2020-04-19 10:42:27.946701	47	MARK_RAN	7:fd8cf02498f8b1e72496a20afc75178c	dropIndex indexName=IDX_RES_SERV_POL_RES_SERV, tableName=RESOURCE_SERVER_POLICY; dropIndex indexName=IDX_RES_SRV_RES_RES_SRV, tableName=RESOURCE_SERVER_RESOURCE; dropIndex indexName=IDX_RES_SRV_SCOPE_RES_SRV, tableName=RESOURCE_SERVER_SCOPE		\N	3.5.4	\N	\N	7292945056
authz-3.4.0.CR1-resource-server-pk-change-part3-fixed-nodropindex	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2020-04-19 10:42:28.003511	48	EXECUTED	7:542794f25aa2b1fbabb7e577d6646319	addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_POLICY; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_RESOURCE; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, ...		\N	3.5.4	\N	\N	7292945056
authn-3.4.0.CR1-refresh-token-max-reuse	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2020-04-19 10:42:28.010468	49	EXECUTED	7:edad604c882df12f74941dac3cc6d650	addColumn tableName=REALM		\N	3.5.4	\N	\N	7292945056
3.4.0	keycloak	META-INF/jpa-changelog-3.4.0.xml	2020-04-19 10:42:28.082369	50	EXECUTED	7:0f88b78b7b46480eb92690cbf5e44900	addPrimaryKey constraintName=CONSTRAINT_REALM_DEFAULT_ROLES, tableName=REALM_DEFAULT_ROLES; addPrimaryKey constraintName=CONSTRAINT_COMPOSITE_ROLE, tableName=COMPOSITE_ROLE; addPrimaryKey constraintName=CONSTR_REALM_DEFAULT_GROUPS, tableName=REALM...		\N	3.5.4	\N	\N	7292945056
3.4.0-KEYCLOAK-5230	hmlnarik@redhat.com	META-INF/jpa-changelog-3.4.0.xml	2020-04-19 10:42:28.140283	51	EXECUTED	7:d560e43982611d936457c327f872dd59	createIndex indexName=IDX_FU_ATTRIBUTE, tableName=FED_USER_ATTRIBUTE; createIndex indexName=IDX_FU_CONSENT, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CONSENT_RU, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CREDENTIAL, t...		\N	3.5.4	\N	\N	7292945056
3.4.1	psilva@redhat.com	META-INF/jpa-changelog-3.4.1.xml	2020-04-19 10:42:28.145511	52	EXECUTED	7:c155566c42b4d14ef07059ec3b3bbd8e	modifyDataType columnName=VALUE, tableName=CLIENT_ATTRIBUTES		\N	3.5.4	\N	\N	7292945056
3.4.2	keycloak	META-INF/jpa-changelog-3.4.2.xml	2020-04-19 10:42:28.149752	53	EXECUTED	7:b40376581f12d70f3c89ba8ddf5b7dea	update tableName=REALM		\N	3.5.4	\N	\N	7292945056
3.4.2-KEYCLOAK-5172	mkanis@redhat.com	META-INF/jpa-changelog-3.4.2.xml	2020-04-19 10:42:28.153805	54	EXECUTED	7:a1132cc395f7b95b3646146c2e38f168	update tableName=CLIENT		\N	3.5.4	\N	\N	7292945056
4.0.0-KEYCLOAK-6335	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2020-04-19 10:42:28.163787	55	EXECUTED	7:d8dc5d89c789105cfa7ca0e82cba60af	createTable tableName=CLIENT_AUTH_FLOW_BINDINGS; addPrimaryKey constraintName=C_CLI_FLOW_BIND, tableName=CLIENT_AUTH_FLOW_BINDINGS		\N	3.5.4	\N	\N	7292945056
4.0.0-CLEANUP-UNUSED-TABLE	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2020-04-19 10:42:28.171494	56	EXECUTED	7:7822e0165097182e8f653c35517656a3	dropTable tableName=CLIENT_IDENTITY_PROV_MAPPING		\N	3.5.4	\N	\N	7292945056
4.0.0-KEYCLOAK-6228	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2020-04-19 10:42:28.202703	57	EXECUTED	7:c6538c29b9c9a08f9e9ea2de5c2b6375	dropUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHOGM8UEWRT, tableName=USER_CONSENT; dropNotNullConstraint columnName=CLIENT_ID, tableName=USER_CONSENT; addColumn tableName=USER_CONSENT; addUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHO...		\N	3.5.4	\N	\N	7292945056
4.0.0-KEYCLOAK-5579-fixed	mposolda@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2020-04-19 10:42:28.344488	58	EXECUTED	7:6d4893e36de22369cf73bcb051ded875	dropForeignKeyConstraint baseTableName=CLIENT_TEMPLATE_ATTRIBUTES, constraintName=FK_CL_TEMPL_ATTR_TEMPL; renameTable newTableName=CLIENT_SCOPE_ATTRIBUTES, oldTableName=CLIENT_TEMPLATE_ATTRIBUTES; renameColumn newColumnName=SCOPE_ID, oldColumnName...		\N	3.5.4	\N	\N	7292945056
authz-4.0.0.CR1	psilva@redhat.com	META-INF/jpa-changelog-authz-4.0.0.CR1.xml	2020-04-19 10:42:28.385799	59	EXECUTED	7:57960fc0b0f0dd0563ea6f8b2e4a1707	createTable tableName=RESOURCE_SERVER_PERM_TICKET; addPrimaryKey constraintName=CONSTRAINT_FAPMT, tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRHO213XCX4WNKOG82SSPMT...		\N	3.5.4	\N	\N	7292945056
authz-4.0.0.Beta3	psilva@redhat.com	META-INF/jpa-changelog-authz-4.0.0.Beta3.xml	2020-04-19 10:42:28.394589	60	EXECUTED	7:2b4b8bff39944c7097977cc18dbceb3b	addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRPO2128CX4WNKOG82SSRFY, referencedTableName=RESOURCE_SERVER_POLICY		\N	3.5.4	\N	\N	7292945056
authz-4.2.0.Final	mhajas@redhat.com	META-INF/jpa-changelog-authz-4.2.0.Final.xml	2020-04-19 10:42:28.406669	61	EXECUTED	7:2aa42a964c59cd5b8ca9822340ba33a8	createTable tableName=RESOURCE_URIS; addForeignKeyConstraint baseTableName=RESOURCE_URIS, constraintName=FK_RESOURCE_SERVER_URIS, referencedTableName=RESOURCE_SERVER_RESOURCE; customChange; dropColumn columnName=URI, tableName=RESOURCE_SERVER_RESO...		\N	3.5.4	\N	\N	7292945056
authz-4.2.0.Final-KEYCLOAK-9944	hmlnarik@redhat.com	META-INF/jpa-changelog-authz-4.2.0.Final.xml	2020-04-19 10:42:28.416774	62	EXECUTED	7:9ac9e58545479929ba23f4a3087a0346	addPrimaryKey constraintName=CONSTRAINT_RESOUR_URIS_PK, tableName=RESOURCE_URIS		\N	3.5.4	\N	\N	7292945056
4.2.0-KEYCLOAK-6313	wadahiro@gmail.com	META-INF/jpa-changelog-4.2.0.xml	2020-04-19 10:42:28.421469	63	EXECUTED	7:14d407c35bc4fe1976867756bcea0c36	addColumn tableName=REQUIRED_ACTION_PROVIDER		\N	3.5.4	\N	\N	7292945056
4.3.0-KEYCLOAK-7984	wadahiro@gmail.com	META-INF/jpa-changelog-4.3.0.xml	2020-04-19 10:42:28.425226	64	EXECUTED	7:241a8030c748c8548e346adee548fa93	update tableName=REQUIRED_ACTION_PROVIDER		\N	3.5.4	\N	\N	7292945056
4.6.0-KEYCLOAK-7950	psilva@redhat.com	META-INF/jpa-changelog-4.6.0.xml	2020-04-19 10:42:28.428818	65	EXECUTED	7:7d3182f65a34fcc61e8d23def037dc3f	update tableName=RESOURCE_SERVER_RESOURCE		\N	3.5.4	\N	\N	7292945056
4.6.0-KEYCLOAK-8377	keycloak	META-INF/jpa-changelog-4.6.0.xml	2020-04-19 10:42:28.448289	66	EXECUTED	7:b30039e00a0b9715d430d1b0636728fa	createTable tableName=ROLE_ATTRIBUTE; addPrimaryKey constraintName=CONSTRAINT_ROLE_ATTRIBUTE_PK, tableName=ROLE_ATTRIBUTE; addForeignKeyConstraint baseTableName=ROLE_ATTRIBUTE, constraintName=FK_ROLE_ATTRIBUTE_ID, referencedTableName=KEYCLOAK_ROLE...		\N	3.5.4	\N	\N	7292945056
4.6.0-KEYCLOAK-8555	gideonray@gmail.com	META-INF/jpa-changelog-4.6.0.xml	2020-04-19 10:42:28.456733	67	EXECUTED	7:3797315ca61d531780f8e6f82f258159	createIndex indexName=IDX_COMPONENT_PROVIDER_TYPE, tableName=COMPONENT		\N	3.5.4	\N	\N	7292945056
4.7.0-KEYCLOAK-1267	sguilhen@redhat.com	META-INF/jpa-changelog-4.7.0.xml	2020-04-19 10:42:28.462989	68	EXECUTED	7:c7aa4c8d9573500c2d347c1941ff0301	addColumn tableName=REALM		\N	3.5.4	\N	\N	7292945056
4.7.0-KEYCLOAK-7275	keycloak	META-INF/jpa-changelog-4.7.0.xml	2020-04-19 10:42:28.47732	69	EXECUTED	7:b207faee394fc074a442ecd42185a5dd	renameColumn newColumnName=CREATED_ON, oldColumnName=LAST_SESSION_REFRESH, tableName=OFFLINE_USER_SESSION; addNotNullConstraint columnName=CREATED_ON, tableName=OFFLINE_USER_SESSION; addColumn tableName=OFFLINE_USER_SESSION; customChange; createIn...		\N	3.5.4	\N	\N	7292945056
4.8.0-KEYCLOAK-8835	sguilhen@redhat.com	META-INF/jpa-changelog-4.8.0.xml	2020-04-19 10:42:28.484322	70	EXECUTED	7:ab9a9762faaba4ddfa35514b212c4922	addNotNullConstraint columnName=SSO_MAX_LIFESPAN_REMEMBER_ME, tableName=REALM; addNotNullConstraint columnName=SSO_IDLE_TIMEOUT_REMEMBER_ME, tableName=REALM		\N	3.5.4	\N	\N	7292945056
authz-7.0.0-KEYCLOAK-10443	psilva@redhat.com	META-INF/jpa-changelog-authz-7.0.0.xml	2020-04-19 10:42:28.489288	71	EXECUTED	7:b9710f74515a6ccb51b72dc0d19df8c4	addColumn tableName=RESOURCE_SERVER		\N	3.5.4	\N	\N	7292945056
8.0.0-adding-credential-columns	keycloak	META-INF/jpa-changelog-8.0.0.xml	2020-04-19 10:42:28.49681	72	EXECUTED	7:ec9707ae4d4f0b7452fee20128083879	addColumn tableName=CREDENTIAL; addColumn tableName=FED_USER_CREDENTIAL		\N	3.5.4	\N	\N	7292945056
8.0.0-updating-credential-data-not-oracle	keycloak	META-INF/jpa-changelog-8.0.0.xml	2020-04-19 10:42:28.502999	73	EXECUTED	7:03b3f4b264c3c68ba082250a80b74216	update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL		\N	3.5.4	\N	\N	7292945056
8.0.0-updating-credential-data-oracle	keycloak	META-INF/jpa-changelog-8.0.0.xml	2020-04-19 10:42:28.505266	74	MARK_RAN	7:64c5728f5ca1f5aa4392217701c4fe23	update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL		\N	3.5.4	\N	\N	7292945056
8.0.0-credential-cleanup-fixed	keycloak	META-INF/jpa-changelog-8.0.0.xml	2020-04-19 10:42:28.5221	75	EXECUTED	7:b48da8c11a3d83ddd6b7d0c8c2219345	dropDefaultValue columnName=COUNTER, tableName=CREDENTIAL; dropDefaultValue columnName=DIGITS, tableName=CREDENTIAL; dropDefaultValue columnName=PERIOD, tableName=CREDENTIAL; dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; dropColumn ...		\N	3.5.4	\N	\N	7292945056
8.0.0-resource-tag-support	keycloak	META-INF/jpa-changelog-8.0.0.xml	2020-04-19 10:42:28.531534	76	EXECUTED	7:a73379915c23bfad3e8f5c6d5c0aa4bd	addColumn tableName=MIGRATION_MODEL; createIndex indexName=IDX_UPDATE_TIME, tableName=MIGRATION_MODEL		\N	3.5.4	\N	\N	7292945056
9.0.0-always-display-client	keycloak	META-INF/jpa-changelog-9.0.0.xml	2020-04-19 10:42:28.537037	77	EXECUTED	7:39e0073779aba192646291aa2332493d	addColumn tableName=CLIENT		\N	3.5.4	\N	\N	7292945056
9.0.0-drop-constraints-for-column-increase	keycloak	META-INF/jpa-changelog-9.0.0.xml	2020-04-19 10:42:28.540043	78	MARK_RAN	7:81f87368f00450799b4bf42ea0b3ec34	dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5PMT, tableName=RESOURCE_SERVER_PERM_TICKET; dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER_RESOURCE; dropPrimaryKey constraintName=CONSTRAINT_O...		\N	3.5.4	\N	\N	7292945056
9.0.0-increase-column-size-federated-fk	keycloak	META-INF/jpa-changelog-9.0.0.xml	2020-04-19 10:42:28.566628	79	EXECUTED	7:20b37422abb9fb6571c618148f013a15	modifyDataType columnName=CLIENT_ID, tableName=FED_USER_CONSENT; modifyDataType columnName=CLIENT_REALM_CONSTRAINT, tableName=KEYCLOAK_ROLE; modifyDataType columnName=OWNER, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=CLIENT_ID, ta...		\N	3.5.4	\N	\N	7292945056
9.0.0-recreate-constraints-after-column-increase	keycloak	META-INF/jpa-changelog-9.0.0.xml	2020-04-19 10:42:28.570139	80	MARK_RAN	7:1970bb6cfb5ee800736b95ad3fb3c78a	addNotNullConstraint columnName=CLIENT_ID, tableName=OFFLINE_CLIENT_SESSION; addNotNullConstraint columnName=OWNER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNullConstraint columnName=REQUESTER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNull...		\N	3.5.4	\N	\N	7292945056
9.0.1-add-index-to-client.client_id	keycloak	META-INF/jpa-changelog-9.0.1.xml	2020-04-19 10:42:28.578983	81	EXECUTED	7:45d9b25fc3b455d522d8dcc10a0f4c80	createIndex indexName=IDX_CLIENT_ID, tableName=CLIENT		\N	3.5.4	\N	\N	7292945056
9.0.1-KEYCLOAK-12579-drop-constraints	keycloak	META-INF/jpa-changelog-9.0.1.xml	2020-04-19 10:42:28.581827	82	MARK_RAN	7:890ae73712bc187a66c2813a724d037f	dropUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	7292945056
9.0.1-KEYCLOAK-12579-add-not-null-constraint	keycloak	META-INF/jpa-changelog-9.0.1.xml	2020-04-19 10:42:28.587012	83	EXECUTED	7:0a211980d27fafe3ff50d19a3a29b538	addNotNullConstraint columnName=PARENT_GROUP, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	7292945056
9.0.1-KEYCLOAK-12579-recreate-constraints	keycloak	META-INF/jpa-changelog-9.0.1.xml	2020-04-19 10:42:28.589852	84	MARK_RAN	7:a161e2ae671a9020fff61e996a207377	addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	7292945056
9.0.1-add-index-to-events	keycloak	META-INF/jpa-changelog-9.0.1.xml	2020-04-19 10:42:28.597954	85	EXECUTED	7:01c49302201bdf815b0a18d1f98a55dc	createIndex indexName=IDX_EVENT_TIME, tableName=EVENT_ENTITY		\N	3.5.4	\N	\N	7292945056
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
1000	f	\N	\N
1001	f	\N	\N
\.


--
-- Data for Name: default_client_scope; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.default_client_scope (realm_id, scope_id, default_scope) FROM stdin;
master	2a7ec7b7-4ea6-474c-8285-b5367f1c74d5	f
master	e0c36b79-d5bb-4279-abc4-59af25565a36	t
master	cefaca23-67d3-4fdf-8bd6-4b5be69107da	t
master	3f25edd8-7f39-4ab7-b154-b001d5b8e743	t
master	a392d362-f92c-4b69-baaa-6d2c9fb67345	f
master	f942c3a6-f535-4c81-9c21-ec6538011f4e	f
master	a6d6e178-5758-42e6-a057-02c00490b994	t
master	b1f71906-8978-4e47-82a6-1db23fbc7a98	t
master	5aa0e2d9-302f-4694-b742-3199c7f637f5	f
\.


--
-- Data for Name: event_entity; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.event_entity (id, client_id, details_json, error, ip_address, realm_id, session_id, event_time, type, user_id) FROM stdin;
\.


--
-- Data for Name: fed_user_attribute; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_attribute (id, name, user_id, realm_id, storage_provider_id, value) FROM stdin;
\.


--
-- Data for Name: fed_user_consent; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_consent (id, client_id, user_id, realm_id, storage_provider_id, created_date, last_updated_date, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: fed_user_consent_cl_scope; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_consent_cl_scope (user_consent_id, scope_id) FROM stdin;
\.


--
-- Data for Name: fed_user_credential; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_credential (id, salt, type, created_date, user_id, realm_id, storage_provider_id, user_label, secret_data, credential_data, priority) FROM stdin;
\.


--
-- Data for Name: fed_user_group_membership; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_group_membership (group_id, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: fed_user_required_action; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_required_action (required_action, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: fed_user_role_mapping; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.fed_user_role_mapping (role_id, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: federated_identity; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.federated_identity (identity_provider, realm_id, federated_user_id, federated_username, token, user_id) FROM stdin;
\.


--
-- Data for Name: federated_user; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.federated_user (id, storage_provider_id, realm_id) FROM stdin;
\.


--
-- Data for Name: group_attribute; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.group_attribute (id, name, value, group_id) FROM stdin;
\.


--
-- Data for Name: group_role_mapping; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.group_role_mapping (role_id, group_id) FROM stdin;
\.


--
-- Data for Name: identity_provider; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.identity_provider (internal_id, enabled, provider_alias, provider_id, store_token, authenticate_by_default, realm_id, add_token_role, trust_email, first_broker_login_flow_id, post_broker_login_flow_id, provider_display_name, link_only) FROM stdin;
\.


--
-- Data for Name: identity_provider_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.identity_provider_config (identity_provider_id, value, name) FROM stdin;
\.


--
-- Data for Name: identity_provider_mapper; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.identity_provider_mapper (id, name, idp_alias, idp_mapper_name, realm_id) FROM stdin;
\.


--
-- Data for Name: idp_mapper_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.idp_mapper_config (idp_mapper_id, value, name) FROM stdin;
\.


--
-- Data for Name: keycloak_group; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.keycloak_group (id, name, parent_group, realm_id) FROM stdin;
\.


--
-- Data for Name: keycloak_role; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.keycloak_role (id, client_realm_constraint, client_role, description, name, realm_id, client, realm) FROM stdin;
2734cd0d-17af-40e6-bd46-865c8fa66a2d	master	f	${role_admin}	admin	master	\N	master
574bb134-480a-4892-89b2-d1e918f32b04	master	f	${role_create-realm}	create-realm	master	\N	master
7a467c72-b420-4ce9-a53b-3fb75602d591	2a07992e-4027-44de-ba50-f2c608090153	t	${role_create-client}	create-client	master	2a07992e-4027-44de-ba50-f2c608090153	\N
7c84e35a-646b-4cee-ba62-f92860c1f432	2a07992e-4027-44de-ba50-f2c608090153	t	${role_view-realm}	view-realm	master	2a07992e-4027-44de-ba50-f2c608090153	\N
182a5064-6b8c-45ae-b795-0e02f00e6775	2a07992e-4027-44de-ba50-f2c608090153	t	${role_view-users}	view-users	master	2a07992e-4027-44de-ba50-f2c608090153	\N
c2b5bbe6-7203-407e-8336-74686b2bf623	2a07992e-4027-44de-ba50-f2c608090153	t	${role_view-clients}	view-clients	master	2a07992e-4027-44de-ba50-f2c608090153	\N
cb9a90b4-6702-4307-b6cc-08cd6e54cc8c	2a07992e-4027-44de-ba50-f2c608090153	t	${role_view-events}	view-events	master	2a07992e-4027-44de-ba50-f2c608090153	\N
d874989b-34f0-44f8-8545-4ec9ad16edd6	2a07992e-4027-44de-ba50-f2c608090153	t	${role_view-identity-providers}	view-identity-providers	master	2a07992e-4027-44de-ba50-f2c608090153	\N
aaa797fa-6916-4ae4-8391-52f4c2306eee	2a07992e-4027-44de-ba50-f2c608090153	t	${role_view-authorization}	view-authorization	master	2a07992e-4027-44de-ba50-f2c608090153	\N
20bf613d-59e2-4e76-846f-1faf77c46fbf	2a07992e-4027-44de-ba50-f2c608090153	t	${role_manage-realm}	manage-realm	master	2a07992e-4027-44de-ba50-f2c608090153	\N
b27cdeca-7541-4c76-8eb5-d8a8583eefd6	2a07992e-4027-44de-ba50-f2c608090153	t	${role_manage-users}	manage-users	master	2a07992e-4027-44de-ba50-f2c608090153	\N
64cc6d28-f953-4e63-95d8-c9eea4cb58db	2a07992e-4027-44de-ba50-f2c608090153	t	${role_manage-clients}	manage-clients	master	2a07992e-4027-44de-ba50-f2c608090153	\N
deb3cacd-5ee6-49db-87b8-47570c8604e0	2a07992e-4027-44de-ba50-f2c608090153	t	${role_manage-events}	manage-events	master	2a07992e-4027-44de-ba50-f2c608090153	\N
69f5ae3f-78d8-48b5-a933-ce4f0d6624be	2a07992e-4027-44de-ba50-f2c608090153	t	${role_manage-identity-providers}	manage-identity-providers	master	2a07992e-4027-44de-ba50-f2c608090153	\N
11a1efda-1e42-4cd9-b57b-fde06d6096c3	2a07992e-4027-44de-ba50-f2c608090153	t	${role_manage-authorization}	manage-authorization	master	2a07992e-4027-44de-ba50-f2c608090153	\N
1891355c-0313-42cc-b54e-434f562f496e	2a07992e-4027-44de-ba50-f2c608090153	t	${role_query-users}	query-users	master	2a07992e-4027-44de-ba50-f2c608090153	\N
64d9e42c-05d0-46f9-b0ca-9da5d4f87c3d	2a07992e-4027-44de-ba50-f2c608090153	t	${role_query-clients}	query-clients	master	2a07992e-4027-44de-ba50-f2c608090153	\N
9a9dccbe-27ed-4f50-975b-6bb561cc8752	2a07992e-4027-44de-ba50-f2c608090153	t	${role_query-realms}	query-realms	master	2a07992e-4027-44de-ba50-f2c608090153	\N
15410f55-634a-4dcd-8caf-5886f244db74	2a07992e-4027-44de-ba50-f2c608090153	t	${role_query-groups}	query-groups	master	2a07992e-4027-44de-ba50-f2c608090153	\N
3daddb01-1bde-4f3c-bb1d-bef825700263	93fad04f-e88d-4a85-82fd-b00c3cd09128	t	${role_view-profile}	view-profile	master	93fad04f-e88d-4a85-82fd-b00c3cd09128	\N
2277bd23-8529-4ceb-8968-4b5355781066	93fad04f-e88d-4a85-82fd-b00c3cd09128	t	${role_manage-account}	manage-account	master	93fad04f-e88d-4a85-82fd-b00c3cd09128	\N
e154df14-921f-4480-a38f-0e1bded303dd	93fad04f-e88d-4a85-82fd-b00c3cd09128	t	${role_manage-account-links}	manage-account-links	master	93fad04f-e88d-4a85-82fd-b00c3cd09128	\N
8911d268-d16c-4a07-9bdd-216f61cf7c2d	93fad04f-e88d-4a85-82fd-b00c3cd09128	t	${role_view-applications}	view-applications	master	93fad04f-e88d-4a85-82fd-b00c3cd09128	\N
6c52cc9b-2276-48de-b3f4-698cb9952c60	93fad04f-e88d-4a85-82fd-b00c3cd09128	t	${role_view-consent}	view-consent	master	93fad04f-e88d-4a85-82fd-b00c3cd09128	\N
155fd8fc-f9de-4834-b648-af4fd0baeed0	93fad04f-e88d-4a85-82fd-b00c3cd09128	t	${role_manage-consent}	manage-consent	master	93fad04f-e88d-4a85-82fd-b00c3cd09128	\N
26793120-c0ea-47b5-b482-bdc9520dc51d	6517b92e-7400-431b-a3a5-ea4e2fbde25c	t	${role_read-token}	read-token	master	6517b92e-7400-431b-a3a5-ea4e2fbde25c	\N
fc29d850-88ef-4e6b-b4fd-05ebd77dc61b	2a07992e-4027-44de-ba50-f2c608090153	t	${role_impersonation}	impersonation	master	2a07992e-4027-44de-ba50-f2c608090153	\N
c5ab9c28-856d-44b5-a664-46921c327023	master	f	${role_offline-access}	offline_access	master	\N	master
aef73a02-f50a-43ba-b95f-f48feeddb3be	master	f	${role_uma_authorization}	uma_authorization	master	\N	master
\.


--
-- Data for Name: migration_model; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.migration_model (id, version, update_time) FROM stdin;
yo812	9.0.2	1587292954
\.


--
-- Data for Name: offline_client_session; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.offline_client_session (user_session_id, client_id, offline_flag, "timestamp", data, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: offline_user_session; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.offline_user_session (user_session_id, user_id, realm_id, created_on, offline_flag, data, last_session_refresh) FROM stdin;
\.


--
-- Data for Name: policy_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.policy_config (policy_id, name, value) FROM stdin;
\.


--
-- Data for Name: protocol_mapper; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.protocol_mapper (id, name, protocol, protocol_mapper_name, client_id, client_scope_id) FROM stdin;
cc34c82b-660c-47e3-a804-26779fe6f8ed	audience resolve	openid-connect	oidc-audience-resolve-mapper	fb016a40-542c-411d-90e3-20ec9204dd99	\N
d950594b-acca-41f0-8348-6631b5a5e983	locale	openid-connect	oidc-usermodel-attribute-mapper	ea67977a-47af-4e65-aa4e-bcf35ea426ee	\N
50c325a0-aec6-4d3f-9348-f8175accf623	role list	saml	saml-role-list-mapper	\N	e0c36b79-d5bb-4279-abc4-59af25565a36
b8614d68-f9c3-4cc8-8b45-abe5f084b385	full name	openid-connect	oidc-full-name-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
02b2cb0d-495a-4303-97ca-4abb9fd1d592	family name	openid-connect	oidc-usermodel-property-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
81e1a2b4-0d42-4dab-850f-e6d46c34e636	given name	openid-connect	oidc-usermodel-property-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
42fd6bbb-35d2-4ac4-9db4-7224a0304bd3	middle name	openid-connect	oidc-usermodel-attribute-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
5591eb4c-7295-43c4-a49d-49e719730958	nickname	openid-connect	oidc-usermodel-attribute-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
e16f680b-5b70-4594-8580-47d69b664f3f	username	openid-connect	oidc-usermodel-property-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
0a25b58f-b1e9-4139-8ddb-9da8cf4a45a3	profile	openid-connect	oidc-usermodel-attribute-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
6165fabb-9d79-4783-b11e-bcc5f99969c5	picture	openid-connect	oidc-usermodel-attribute-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
f7dcc01a-ebd9-4487-87a9-16ad8fe0af50	website	openid-connect	oidc-usermodel-attribute-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
887c73d7-fb11-4c7a-8362-54c9c15603fd	gender	openid-connect	oidc-usermodel-attribute-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
85572b30-a14e-40df-9662-e23a4184381e	birthdate	openid-connect	oidc-usermodel-attribute-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
71a21d44-3b92-48a8-b343-3af926aa0ba1	zoneinfo	openid-connect	oidc-usermodel-attribute-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
8caca93e-953f-4da0-8fdd-80c8b4041d44	locale	openid-connect	oidc-usermodel-attribute-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
a6f004a0-a6a4-4d27-8490-7ff6ee4e7f3a	updated at	openid-connect	oidc-usermodel-attribute-mapper	\N	cefaca23-67d3-4fdf-8bd6-4b5be69107da
c4ade8a1-f970-4df3-9d84-cfc90f53e6d2	email	openid-connect	oidc-usermodel-property-mapper	\N	3f25edd8-7f39-4ab7-b154-b001d5b8e743
4aa8d7c5-4236-46ab-b994-4f34b1c4a700	email verified	openid-connect	oidc-usermodel-property-mapper	\N	3f25edd8-7f39-4ab7-b154-b001d5b8e743
9a55b94a-3e60-427a-92fd-d315749b1dad	address	openid-connect	oidc-address-mapper	\N	a392d362-f92c-4b69-baaa-6d2c9fb67345
ab879f60-07eb-43c0-b449-337695f790d1	phone number	openid-connect	oidc-usermodel-attribute-mapper	\N	f942c3a6-f535-4c81-9c21-ec6538011f4e
734a8b2a-4071-482c-8a16-00f9938693a6	phone number verified	openid-connect	oidc-usermodel-attribute-mapper	\N	f942c3a6-f535-4c81-9c21-ec6538011f4e
d0cf25ad-12cf-4379-bf9b-3de1e2ed8c8e	realm roles	openid-connect	oidc-usermodel-realm-role-mapper	\N	a6d6e178-5758-42e6-a057-02c00490b994
4f5d5eaf-b709-415a-8d99-f8381bf71f71	client roles	openid-connect	oidc-usermodel-client-role-mapper	\N	a6d6e178-5758-42e6-a057-02c00490b994
877755d2-f69f-4cf6-9c5c-cd2d32bd7579	audience resolve	openid-connect	oidc-audience-resolve-mapper	\N	a6d6e178-5758-42e6-a057-02c00490b994
ba262d09-1e5f-4c4b-9fdf-9508d4645d63	allowed web origins	openid-connect	oidc-allowed-origins-mapper	\N	b1f71906-8978-4e47-82a6-1db23fbc7a98
827d328c-82e6-463a-9601-fc11ee62f577	upn	openid-connect	oidc-usermodel-property-mapper	\N	5aa0e2d9-302f-4694-b742-3199c7f637f5
f7d6fff8-899f-43a4-8ee8-a7bbed897426	groups	openid-connect	oidc-usermodel-realm-role-mapper	\N	5aa0e2d9-302f-4694-b742-3199c7f637f5
\.


--
-- Data for Name: protocol_mapper_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.protocol_mapper_config (protocol_mapper_id, value, name) FROM stdin;
d950594b-acca-41f0-8348-6631b5a5e983	true	userinfo.token.claim
d950594b-acca-41f0-8348-6631b5a5e983	locale	user.attribute
d950594b-acca-41f0-8348-6631b5a5e983	true	id.token.claim
d950594b-acca-41f0-8348-6631b5a5e983	true	access.token.claim
d950594b-acca-41f0-8348-6631b5a5e983	locale	claim.name
d950594b-acca-41f0-8348-6631b5a5e983	String	jsonType.label
50c325a0-aec6-4d3f-9348-f8175accf623	false	single
50c325a0-aec6-4d3f-9348-f8175accf623	Basic	attribute.nameformat
50c325a0-aec6-4d3f-9348-f8175accf623	Role	attribute.name
b8614d68-f9c3-4cc8-8b45-abe5f084b385	true	userinfo.token.claim
b8614d68-f9c3-4cc8-8b45-abe5f084b385	true	id.token.claim
b8614d68-f9c3-4cc8-8b45-abe5f084b385	true	access.token.claim
02b2cb0d-495a-4303-97ca-4abb9fd1d592	true	userinfo.token.claim
02b2cb0d-495a-4303-97ca-4abb9fd1d592	lastName	user.attribute
02b2cb0d-495a-4303-97ca-4abb9fd1d592	true	id.token.claim
02b2cb0d-495a-4303-97ca-4abb9fd1d592	true	access.token.claim
02b2cb0d-495a-4303-97ca-4abb9fd1d592	family_name	claim.name
02b2cb0d-495a-4303-97ca-4abb9fd1d592	String	jsonType.label
81e1a2b4-0d42-4dab-850f-e6d46c34e636	true	userinfo.token.claim
81e1a2b4-0d42-4dab-850f-e6d46c34e636	firstName	user.attribute
81e1a2b4-0d42-4dab-850f-e6d46c34e636	true	id.token.claim
81e1a2b4-0d42-4dab-850f-e6d46c34e636	true	access.token.claim
81e1a2b4-0d42-4dab-850f-e6d46c34e636	given_name	claim.name
81e1a2b4-0d42-4dab-850f-e6d46c34e636	String	jsonType.label
42fd6bbb-35d2-4ac4-9db4-7224a0304bd3	true	userinfo.token.claim
42fd6bbb-35d2-4ac4-9db4-7224a0304bd3	middleName	user.attribute
42fd6bbb-35d2-4ac4-9db4-7224a0304bd3	true	id.token.claim
42fd6bbb-35d2-4ac4-9db4-7224a0304bd3	true	access.token.claim
42fd6bbb-35d2-4ac4-9db4-7224a0304bd3	middle_name	claim.name
42fd6bbb-35d2-4ac4-9db4-7224a0304bd3	String	jsonType.label
5591eb4c-7295-43c4-a49d-49e719730958	true	userinfo.token.claim
5591eb4c-7295-43c4-a49d-49e719730958	nickname	user.attribute
5591eb4c-7295-43c4-a49d-49e719730958	true	id.token.claim
5591eb4c-7295-43c4-a49d-49e719730958	true	access.token.claim
5591eb4c-7295-43c4-a49d-49e719730958	nickname	claim.name
5591eb4c-7295-43c4-a49d-49e719730958	String	jsonType.label
e16f680b-5b70-4594-8580-47d69b664f3f	true	userinfo.token.claim
e16f680b-5b70-4594-8580-47d69b664f3f	username	user.attribute
e16f680b-5b70-4594-8580-47d69b664f3f	true	id.token.claim
e16f680b-5b70-4594-8580-47d69b664f3f	true	access.token.claim
e16f680b-5b70-4594-8580-47d69b664f3f	preferred_username	claim.name
e16f680b-5b70-4594-8580-47d69b664f3f	String	jsonType.label
0a25b58f-b1e9-4139-8ddb-9da8cf4a45a3	true	userinfo.token.claim
0a25b58f-b1e9-4139-8ddb-9da8cf4a45a3	profile	user.attribute
0a25b58f-b1e9-4139-8ddb-9da8cf4a45a3	true	id.token.claim
0a25b58f-b1e9-4139-8ddb-9da8cf4a45a3	true	access.token.claim
0a25b58f-b1e9-4139-8ddb-9da8cf4a45a3	profile	claim.name
0a25b58f-b1e9-4139-8ddb-9da8cf4a45a3	String	jsonType.label
6165fabb-9d79-4783-b11e-bcc5f99969c5	true	userinfo.token.claim
6165fabb-9d79-4783-b11e-bcc5f99969c5	picture	user.attribute
6165fabb-9d79-4783-b11e-bcc5f99969c5	true	id.token.claim
6165fabb-9d79-4783-b11e-bcc5f99969c5	true	access.token.claim
6165fabb-9d79-4783-b11e-bcc5f99969c5	picture	claim.name
6165fabb-9d79-4783-b11e-bcc5f99969c5	String	jsonType.label
f7dcc01a-ebd9-4487-87a9-16ad8fe0af50	true	userinfo.token.claim
f7dcc01a-ebd9-4487-87a9-16ad8fe0af50	website	user.attribute
f7dcc01a-ebd9-4487-87a9-16ad8fe0af50	true	id.token.claim
f7dcc01a-ebd9-4487-87a9-16ad8fe0af50	true	access.token.claim
f7dcc01a-ebd9-4487-87a9-16ad8fe0af50	website	claim.name
f7dcc01a-ebd9-4487-87a9-16ad8fe0af50	String	jsonType.label
887c73d7-fb11-4c7a-8362-54c9c15603fd	true	userinfo.token.claim
887c73d7-fb11-4c7a-8362-54c9c15603fd	gender	user.attribute
887c73d7-fb11-4c7a-8362-54c9c15603fd	true	id.token.claim
887c73d7-fb11-4c7a-8362-54c9c15603fd	true	access.token.claim
887c73d7-fb11-4c7a-8362-54c9c15603fd	gender	claim.name
887c73d7-fb11-4c7a-8362-54c9c15603fd	String	jsonType.label
85572b30-a14e-40df-9662-e23a4184381e	true	userinfo.token.claim
85572b30-a14e-40df-9662-e23a4184381e	birthdate	user.attribute
85572b30-a14e-40df-9662-e23a4184381e	true	id.token.claim
85572b30-a14e-40df-9662-e23a4184381e	true	access.token.claim
85572b30-a14e-40df-9662-e23a4184381e	birthdate	claim.name
85572b30-a14e-40df-9662-e23a4184381e	String	jsonType.label
71a21d44-3b92-48a8-b343-3af926aa0ba1	true	userinfo.token.claim
71a21d44-3b92-48a8-b343-3af926aa0ba1	zoneinfo	user.attribute
71a21d44-3b92-48a8-b343-3af926aa0ba1	true	id.token.claim
71a21d44-3b92-48a8-b343-3af926aa0ba1	true	access.token.claim
71a21d44-3b92-48a8-b343-3af926aa0ba1	zoneinfo	claim.name
71a21d44-3b92-48a8-b343-3af926aa0ba1	String	jsonType.label
8caca93e-953f-4da0-8fdd-80c8b4041d44	true	userinfo.token.claim
8caca93e-953f-4da0-8fdd-80c8b4041d44	locale	user.attribute
8caca93e-953f-4da0-8fdd-80c8b4041d44	true	id.token.claim
8caca93e-953f-4da0-8fdd-80c8b4041d44	true	access.token.claim
8caca93e-953f-4da0-8fdd-80c8b4041d44	locale	claim.name
8caca93e-953f-4da0-8fdd-80c8b4041d44	String	jsonType.label
a6f004a0-a6a4-4d27-8490-7ff6ee4e7f3a	true	userinfo.token.claim
a6f004a0-a6a4-4d27-8490-7ff6ee4e7f3a	updatedAt	user.attribute
a6f004a0-a6a4-4d27-8490-7ff6ee4e7f3a	true	id.token.claim
a6f004a0-a6a4-4d27-8490-7ff6ee4e7f3a	true	access.token.claim
a6f004a0-a6a4-4d27-8490-7ff6ee4e7f3a	updated_at	claim.name
a6f004a0-a6a4-4d27-8490-7ff6ee4e7f3a	String	jsonType.label
c4ade8a1-f970-4df3-9d84-cfc90f53e6d2	true	userinfo.token.claim
c4ade8a1-f970-4df3-9d84-cfc90f53e6d2	email	user.attribute
c4ade8a1-f970-4df3-9d84-cfc90f53e6d2	true	id.token.claim
c4ade8a1-f970-4df3-9d84-cfc90f53e6d2	true	access.token.claim
c4ade8a1-f970-4df3-9d84-cfc90f53e6d2	email	claim.name
c4ade8a1-f970-4df3-9d84-cfc90f53e6d2	String	jsonType.label
4aa8d7c5-4236-46ab-b994-4f34b1c4a700	true	userinfo.token.claim
4aa8d7c5-4236-46ab-b994-4f34b1c4a700	emailVerified	user.attribute
4aa8d7c5-4236-46ab-b994-4f34b1c4a700	true	id.token.claim
4aa8d7c5-4236-46ab-b994-4f34b1c4a700	true	access.token.claim
4aa8d7c5-4236-46ab-b994-4f34b1c4a700	email_verified	claim.name
4aa8d7c5-4236-46ab-b994-4f34b1c4a700	boolean	jsonType.label
9a55b94a-3e60-427a-92fd-d315749b1dad	formatted	user.attribute.formatted
9a55b94a-3e60-427a-92fd-d315749b1dad	country	user.attribute.country
9a55b94a-3e60-427a-92fd-d315749b1dad	postal_code	user.attribute.postal_code
9a55b94a-3e60-427a-92fd-d315749b1dad	true	userinfo.token.claim
9a55b94a-3e60-427a-92fd-d315749b1dad	street	user.attribute.street
9a55b94a-3e60-427a-92fd-d315749b1dad	true	id.token.claim
9a55b94a-3e60-427a-92fd-d315749b1dad	region	user.attribute.region
9a55b94a-3e60-427a-92fd-d315749b1dad	true	access.token.claim
9a55b94a-3e60-427a-92fd-d315749b1dad	locality	user.attribute.locality
ab879f60-07eb-43c0-b449-337695f790d1	true	userinfo.token.claim
ab879f60-07eb-43c0-b449-337695f790d1	phoneNumber	user.attribute
ab879f60-07eb-43c0-b449-337695f790d1	true	id.token.claim
ab879f60-07eb-43c0-b449-337695f790d1	true	access.token.claim
ab879f60-07eb-43c0-b449-337695f790d1	phone_number	claim.name
ab879f60-07eb-43c0-b449-337695f790d1	String	jsonType.label
734a8b2a-4071-482c-8a16-00f9938693a6	true	userinfo.token.claim
734a8b2a-4071-482c-8a16-00f9938693a6	phoneNumberVerified	user.attribute
734a8b2a-4071-482c-8a16-00f9938693a6	true	id.token.claim
734a8b2a-4071-482c-8a16-00f9938693a6	true	access.token.claim
734a8b2a-4071-482c-8a16-00f9938693a6	phone_number_verified	claim.name
734a8b2a-4071-482c-8a16-00f9938693a6	boolean	jsonType.label
d0cf25ad-12cf-4379-bf9b-3de1e2ed8c8e	true	multivalued
d0cf25ad-12cf-4379-bf9b-3de1e2ed8c8e	foo	user.attribute
d0cf25ad-12cf-4379-bf9b-3de1e2ed8c8e	true	access.token.claim
d0cf25ad-12cf-4379-bf9b-3de1e2ed8c8e	realm_access.roles	claim.name
d0cf25ad-12cf-4379-bf9b-3de1e2ed8c8e	String	jsonType.label
4f5d5eaf-b709-415a-8d99-f8381bf71f71	true	multivalued
4f5d5eaf-b709-415a-8d99-f8381bf71f71	foo	user.attribute
4f5d5eaf-b709-415a-8d99-f8381bf71f71	true	access.token.claim
4f5d5eaf-b709-415a-8d99-f8381bf71f71	resource_access.${client_id}.roles	claim.name
4f5d5eaf-b709-415a-8d99-f8381bf71f71	String	jsonType.label
827d328c-82e6-463a-9601-fc11ee62f577	true	userinfo.token.claim
827d328c-82e6-463a-9601-fc11ee62f577	username	user.attribute
827d328c-82e6-463a-9601-fc11ee62f577	true	id.token.claim
827d328c-82e6-463a-9601-fc11ee62f577	true	access.token.claim
827d328c-82e6-463a-9601-fc11ee62f577	upn	claim.name
827d328c-82e6-463a-9601-fc11ee62f577	String	jsonType.label
f7d6fff8-899f-43a4-8ee8-a7bbed897426	true	multivalued
f7d6fff8-899f-43a4-8ee8-a7bbed897426	foo	user.attribute
f7d6fff8-899f-43a4-8ee8-a7bbed897426	true	id.token.claim
f7d6fff8-899f-43a4-8ee8-a7bbed897426	true	access.token.claim
f7d6fff8-899f-43a4-8ee8-a7bbed897426	groups	claim.name
f7d6fff8-899f-43a4-8ee8-a7bbed897426	String	jsonType.label
\.


--
-- Data for Name: realm; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm (id, access_code_lifespan, user_action_lifespan, access_token_lifespan, account_theme, admin_theme, email_theme, enabled, events_enabled, events_expiration, login_theme, name, not_before, password_policy, registration_allowed, remember_me, reset_password_allowed, social, ssl_required, sso_idle_timeout, sso_max_lifespan, update_profile_on_soc_login, verify_email, master_admin_client, login_lifespan, internationalization_enabled, default_locale, reg_email_as_username, admin_events_enabled, admin_events_details_enabled, edit_username_allowed, otp_policy_counter, otp_policy_window, otp_policy_period, otp_policy_digits, otp_policy_alg, otp_policy_type, browser_flow, registration_flow, direct_grant_flow, reset_credentials_flow, client_auth_flow, offline_session_idle_timeout, revoke_refresh_token, access_token_life_implicit, login_with_email_allowed, duplicate_emails_allowed, docker_auth_flow, refresh_token_max_reuse, allow_user_managed_access, sso_max_lifespan_remember_me, sso_idle_timeout_remember_me) FROM stdin;
master	60	300	60	\N	\N	\N	t	f	0	\N	master	0	\N	f	f	f	f	EXTERNAL	1800	36000	f	f	2a07992e-4027-44de-ba50-f2c608090153	1800	f	\N	f	f	f	f	0	1	30	6	HmacSHA1	totp	444b300d-45af-457a-ba39-2b6ebed8fdef	10c15e38-1179-46c8-b98e-a1b34e293623	d096bbf5-4c86-4d85-ae97-76be9e9a4511	a6978b30-60da-4368-a4dc-e74d6443019a	0f4f02c1-6068-4d34-b994-4037ace5ccfc	2592000	f	900	t	f	26b90fec-b105-4d32-a851-882424f64d13	0	f	0	0
\.


--
-- Data for Name: realm_attribute; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_attribute (name, value, realm_id) FROM stdin;
_browser_header.contentSecurityPolicyReportOnly		master
_browser_header.xContentTypeOptions	nosniff	master
_browser_header.xRobotsTag	none	master
_browser_header.xFrameOptions	SAMEORIGIN	master
_browser_header.contentSecurityPolicy	frame-src 'self'; frame-ancestors 'self'; object-src 'none';	master
_browser_header.xXSSProtection	1; mode=block	master
_browser_header.strictTransportSecurity	max-age=31536000; includeSubDomains	master
bruteForceProtected	false	master
permanentLockout	false	master
maxFailureWaitSeconds	900	master
minimumQuickLoginWaitSeconds	60	master
waitIncrementSeconds	60	master
quickLoginCheckMilliSeconds	1000	master
maxDeltaTimeSeconds	43200	master
failureFactor	30	master
displayName	Keycloak	master
displayNameHtml	<div class="kc-logo-text"><span>Keycloak</span></div>	master
offlineSessionMaxLifespanEnabled	false	master
offlineSessionMaxLifespan	5184000	master
\.


--
-- Data for Name: realm_default_groups; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_default_groups (realm_id, group_id) FROM stdin;
\.


--
-- Data for Name: realm_default_roles; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_default_roles (realm_id, role_id) FROM stdin;
master	c5ab9c28-856d-44b5-a664-46921c327023
master	aef73a02-f50a-43ba-b95f-f48feeddb3be
\.


--
-- Data for Name: realm_enabled_event_types; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_enabled_event_types (realm_id, value) FROM stdin;
\.


--
-- Data for Name: realm_events_listeners; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_events_listeners (realm_id, value) FROM stdin;
master	jboss-logging
\.


--
-- Data for Name: realm_required_credential; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_required_credential (type, form_label, input, secret, realm_id) FROM stdin;
password	password	t	t	master
\.


--
-- Data for Name: realm_smtp_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_smtp_config (realm_id, value, name) FROM stdin;
\.


--
-- Data for Name: realm_supported_locales; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.realm_supported_locales (realm_id, value) FROM stdin;
\.


--
-- Data for Name: redirect_uris; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.redirect_uris (client_id, value) FROM stdin;
93fad04f-e88d-4a85-82fd-b00c3cd09128	/realms/master/account/*
fb016a40-542c-411d-90e3-20ec9204dd99	/realms/master/account/*
ea67977a-47af-4e65-aa4e-bcf35ea426ee	/admin/master/console/*
\.


--
-- Data for Name: required_action_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.required_action_config (required_action_id, value, name) FROM stdin;
\.


--
-- Data for Name: required_action_provider; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.required_action_provider (id, alias, name, realm_id, enabled, default_action, provider_id, priority) FROM stdin;
5cbd79a5-143a-43a5-8733-8552e24b3efc	VERIFY_EMAIL	Verify Email	master	t	f	VERIFY_EMAIL	50
f81e3714-b0ec-48df-982d-7e286c5c9a9c	UPDATE_PROFILE	Update Profile	master	t	f	UPDATE_PROFILE	40
30e5c0ef-099a-4146-849a-677963e4b420	CONFIGURE_TOTP	Configure OTP	master	t	f	CONFIGURE_TOTP	10
10dffe40-d735-4702-a3a1-00bb9053be84	UPDATE_PASSWORD	Update Password	master	t	f	UPDATE_PASSWORD	30
586cf87d-f26b-40c0-bcac-feaaa3ecf65d	terms_and_conditions	Terms and Conditions	master	f	f	terms_and_conditions	20
d550503e-b883-405e-abb8-3dc30992b7de	update_user_locale	Update User Locale	master	t	f	update_user_locale	1000
\.


--
-- Data for Name: resource_attribute; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_attribute (id, name, value, resource_id) FROM stdin;
\.


--
-- Data for Name: resource_policy; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_policy (resource_id, policy_id) FROM stdin;
\.


--
-- Data for Name: resource_scope; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_scope (resource_id, scope_id) FROM stdin;
\.


--
-- Data for Name: resource_server; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_server (id, allow_rs_remote_mgmt, policy_enforce_mode, decision_strategy) FROM stdin;
\.


--
-- Data for Name: resource_server_perm_ticket; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_server_perm_ticket (id, owner, requester, created_timestamp, granted_timestamp, resource_id, scope_id, resource_server_id, policy_id) FROM stdin;
\.


--
-- Data for Name: resource_server_policy; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_server_policy (id, name, description, type, decision_strategy, logic, resource_server_id, owner) FROM stdin;
\.


--
-- Data for Name: resource_server_resource; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_server_resource (id, name, type, icon_uri, owner, resource_server_id, owner_managed_access, display_name) FROM stdin;
\.


--
-- Data for Name: resource_server_scope; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_server_scope (id, name, icon_uri, resource_server_id, display_name) FROM stdin;
\.


--
-- Data for Name: resource_uris; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.resource_uris (resource_id, value) FROM stdin;
\.


--
-- Data for Name: role_attribute; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.role_attribute (id, role_id, name, value) FROM stdin;
\.


--
-- Data for Name: scope_mapping; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.scope_mapping (client_id, role_id) FROM stdin;
fb016a40-542c-411d-90e3-20ec9204dd99	2277bd23-8529-4ceb-8968-4b5355781066
\.


--
-- Data for Name: scope_policy; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.scope_policy (scope_id, policy_id) FROM stdin;
\.


--
-- Data for Name: user_attribute; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_attribute (name, value, user_id, id) FROM stdin;
\.


--
-- Data for Name: user_consent; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_consent (id, client_id, user_id, created_date, last_updated_date, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: user_consent_client_scope; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_consent_client_scope (user_consent_id, scope_id) FROM stdin;
\.


--
-- Data for Name: user_entity; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_entity (id, email, email_constraint, email_verified, enabled, federation_link, first_name, last_name, realm_id, username, created_timestamp, service_account_client_link, not_before) FROM stdin;
1cc8ec8b-50e6-4a1a-9fa4-79550b23939b	\N	e21b6715-e22e-4ed1-9490-6243df18bf93	f	t	\N	\N	\N	master	user	1587292957838	\N	0
\.


--
-- Data for Name: user_federation_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_federation_config (user_federation_provider_id, value, name) FROM stdin;
\.


--
-- Data for Name: user_federation_mapper; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_federation_mapper (id, name, federation_provider_id, federation_mapper_type, realm_id) FROM stdin;
\.


--
-- Data for Name: user_federation_mapper_config; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_federation_mapper_config (user_federation_mapper_id, value, name) FROM stdin;
\.


--
-- Data for Name: user_federation_provider; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_federation_provider (id, changed_sync_period, display_name, full_sync_period, last_sync, priority, provider_name, realm_id) FROM stdin;
\.


--
-- Data for Name: user_group_membership; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_group_membership (group_id, user_id) FROM stdin;
\.


--
-- Data for Name: user_required_action; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_required_action (user_id, required_action) FROM stdin;
\.


--
-- Data for Name: user_role_mapping; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_role_mapping (role_id, user_id) FROM stdin;
c5ab9c28-856d-44b5-a664-46921c327023	1cc8ec8b-50e6-4a1a-9fa4-79550b23939b
aef73a02-f50a-43ba-b95f-f48feeddb3be	1cc8ec8b-50e6-4a1a-9fa4-79550b23939b
2277bd23-8529-4ceb-8968-4b5355781066	1cc8ec8b-50e6-4a1a-9fa4-79550b23939b
3daddb01-1bde-4f3c-bb1d-bef825700263	1cc8ec8b-50e6-4a1a-9fa4-79550b23939b
2734cd0d-17af-40e6-bd46-865c8fa66a2d	1cc8ec8b-50e6-4a1a-9fa4-79550b23939b
\.


--
-- Data for Name: user_session; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_session (id, auth_method, ip_address, last_session_refresh, login_username, realm_id, remember_me, started, user_id, user_session_state, broker_session_id, broker_user_id) FROM stdin;
\.


--
-- Data for Name: user_session_note; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.user_session_note (user_session, name, value) FROM stdin;
\.


--
-- Data for Name: username_login_failure; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.username_login_failure (realm_id, username, failed_login_not_before, last_failure, last_ip_failure, num_failures) FROM stdin;
\.


--
-- Data for Name: web_origins; Type: TABLE DATA; Schema: public; Owner: kUser
--

COPY public.web_origins (client_id, value) FROM stdin;
ea67977a-47af-4e65-aa4e-bcf35ea426ee	+
\.


--
-- Name: username_login_failure CONSTRAINT_17-2; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.username_login_failure
    ADD CONSTRAINT "CONSTRAINT_17-2" PRIMARY KEY (realm_id, username);


--
-- Name: keycloak_role UK_J3RWUVD56ONTGSUHOGM184WW2-2; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT "UK_J3RWUVD56ONTGSUHOGM184WW2-2" UNIQUE (name, client_realm_constraint);


--
-- Name: client_auth_flow_bindings c_cli_flow_bind; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_auth_flow_bindings
    ADD CONSTRAINT c_cli_flow_bind PRIMARY KEY (client_id, binding_name);


--
-- Name: client_scope_client c_cli_scope_bind; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_client
    ADD CONSTRAINT c_cli_scope_bind PRIMARY KEY (client_id, scope_id);


--
-- Name: client_initial_access cnstr_client_init_acc_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_initial_access
    ADD CONSTRAINT cnstr_client_init_acc_pk PRIMARY KEY (id);


--
-- Name: realm_default_groups con_group_id_def_groups; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT con_group_id_def_groups UNIQUE (group_id);


--
-- Name: broker_link constr_broker_link_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.broker_link
    ADD CONSTRAINT constr_broker_link_pk PRIMARY KEY (identity_provider, user_id);


--
-- Name: client_user_session_note constr_cl_usr_ses_note; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_user_session_note
    ADD CONSTRAINT constr_cl_usr_ses_note PRIMARY KEY (client_session, name);


--
-- Name: client_default_roles constr_client_default_roles; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_default_roles
    ADD CONSTRAINT constr_client_default_roles PRIMARY KEY (client_id, role_id);


--
-- Name: component_config constr_component_config_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.component_config
    ADD CONSTRAINT constr_component_config_pk PRIMARY KEY (id);


--
-- Name: component constr_component_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT constr_component_pk PRIMARY KEY (id);


--
-- Name: fed_user_required_action constr_fed_required_action; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_required_action
    ADD CONSTRAINT constr_fed_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: fed_user_attribute constr_fed_user_attr_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_attribute
    ADD CONSTRAINT constr_fed_user_attr_pk PRIMARY KEY (id);


--
-- Name: fed_user_consent constr_fed_user_consent_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_consent
    ADD CONSTRAINT constr_fed_user_consent_pk PRIMARY KEY (id);


--
-- Name: fed_user_credential constr_fed_user_cred_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_credential
    ADD CONSTRAINT constr_fed_user_cred_pk PRIMARY KEY (id);


--
-- Name: fed_user_group_membership constr_fed_user_group; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_group_membership
    ADD CONSTRAINT constr_fed_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: fed_user_role_mapping constr_fed_user_role; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_role_mapping
    ADD CONSTRAINT constr_fed_user_role PRIMARY KEY (role_id, user_id);


--
-- Name: federated_user constr_federated_user; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.federated_user
    ADD CONSTRAINT constr_federated_user PRIMARY KEY (id);


--
-- Name: realm_default_groups constr_realm_default_groups; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT constr_realm_default_groups PRIMARY KEY (realm_id, group_id);


--
-- Name: realm_enabled_event_types constr_realm_enabl_event_types; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_enabled_event_types
    ADD CONSTRAINT constr_realm_enabl_event_types PRIMARY KEY (realm_id, value);


--
-- Name: realm_events_listeners constr_realm_events_listeners; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_events_listeners
    ADD CONSTRAINT constr_realm_events_listeners PRIMARY KEY (realm_id, value);


--
-- Name: realm_supported_locales constr_realm_supported_locales; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_supported_locales
    ADD CONSTRAINT constr_realm_supported_locales PRIMARY KEY (realm_id, value);


--
-- Name: identity_provider constraint_2b; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT constraint_2b PRIMARY KEY (internal_id);


--
-- Name: client_attributes constraint_3c; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_attributes
    ADD CONSTRAINT constraint_3c PRIMARY KEY (client_id, name);


--
-- Name: event_entity constraint_4; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.event_entity
    ADD CONSTRAINT constraint_4 PRIMARY KEY (id);


--
-- Name: federated_identity constraint_40; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.federated_identity
    ADD CONSTRAINT constraint_40 PRIMARY KEY (identity_provider, user_id);


--
-- Name: realm constraint_4a; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT constraint_4a PRIMARY KEY (id);


--
-- Name: client_session_role constraint_5; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_role
    ADD CONSTRAINT constraint_5 PRIMARY KEY (client_session, role_id);


--
-- Name: user_session constraint_57; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_session
    ADD CONSTRAINT constraint_57 PRIMARY KEY (id);


--
-- Name: user_federation_provider constraint_5c; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_provider
    ADD CONSTRAINT constraint_5c PRIMARY KEY (id);


--
-- Name: client_session_note constraint_5e; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_note
    ADD CONSTRAINT constraint_5e PRIMARY KEY (client_session, name);


--
-- Name: client constraint_7; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT constraint_7 PRIMARY KEY (id);


--
-- Name: client_session constraint_8; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session
    ADD CONSTRAINT constraint_8 PRIMARY KEY (id);


--
-- Name: scope_mapping constraint_81; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT constraint_81 PRIMARY KEY (client_id, role_id);


--
-- Name: client_node_registrations constraint_84; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_node_registrations
    ADD CONSTRAINT constraint_84 PRIMARY KEY (client_id, name);


--
-- Name: realm_attribute constraint_9; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_attribute
    ADD CONSTRAINT constraint_9 PRIMARY KEY (name, realm_id);


--
-- Name: realm_required_credential constraint_92; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_required_credential
    ADD CONSTRAINT constraint_92 PRIMARY KEY (realm_id, type);


--
-- Name: keycloak_role constraint_a; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT constraint_a PRIMARY KEY (id);


--
-- Name: admin_event_entity constraint_admin_event_entity; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.admin_event_entity
    ADD CONSTRAINT constraint_admin_event_entity PRIMARY KEY (id);


--
-- Name: authenticator_config_entry constraint_auth_cfg_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authenticator_config_entry
    ADD CONSTRAINT constraint_auth_cfg_pk PRIMARY KEY (authenticator_id, name);


--
-- Name: authentication_execution constraint_auth_exec_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT constraint_auth_exec_pk PRIMARY KEY (id);


--
-- Name: authentication_flow constraint_auth_flow_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authentication_flow
    ADD CONSTRAINT constraint_auth_flow_pk PRIMARY KEY (id);


--
-- Name: authenticator_config constraint_auth_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authenticator_config
    ADD CONSTRAINT constraint_auth_pk PRIMARY KEY (id);


--
-- Name: client_session_auth_status constraint_auth_status_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_auth_status
    ADD CONSTRAINT constraint_auth_status_pk PRIMARY KEY (client_session, authenticator);


--
-- Name: user_role_mapping constraint_c; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_role_mapping
    ADD CONSTRAINT constraint_c PRIMARY KEY (role_id, user_id);


--
-- Name: composite_role constraint_composite_role; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT constraint_composite_role PRIMARY KEY (composite, child_role);


--
-- Name: client_session_prot_mapper constraint_cs_pmp_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_prot_mapper
    ADD CONSTRAINT constraint_cs_pmp_pk PRIMARY KEY (client_session, protocol_mapper_id);


--
-- Name: identity_provider_config constraint_d; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider_config
    ADD CONSTRAINT constraint_d PRIMARY KEY (identity_provider_id, name);


--
-- Name: policy_config constraint_dpc; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.policy_config
    ADD CONSTRAINT constraint_dpc PRIMARY KEY (policy_id, name);


--
-- Name: realm_smtp_config constraint_e; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_smtp_config
    ADD CONSTRAINT constraint_e PRIMARY KEY (realm_id, name);


--
-- Name: credential constraint_f; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.credential
    ADD CONSTRAINT constraint_f PRIMARY KEY (id);


--
-- Name: user_federation_config constraint_f9; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_config
    ADD CONSTRAINT constraint_f9 PRIMARY KEY (user_federation_provider_id, name);


--
-- Name: resource_server_perm_ticket constraint_fapmt; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT constraint_fapmt PRIMARY KEY (id);


--
-- Name: resource_server_resource constraint_farsr; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT constraint_farsr PRIMARY KEY (id);


--
-- Name: resource_server_policy constraint_farsrp; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT constraint_farsrp PRIMARY KEY (id);


--
-- Name: associated_policy constraint_farsrpap; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT constraint_farsrpap PRIMARY KEY (policy_id, associated_policy_id);


--
-- Name: resource_policy constraint_farsrpp; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT constraint_farsrpp PRIMARY KEY (resource_id, policy_id);


--
-- Name: resource_server_scope constraint_farsrs; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT constraint_farsrs PRIMARY KEY (id);


--
-- Name: resource_scope constraint_farsrsp; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT constraint_farsrsp PRIMARY KEY (resource_id, scope_id);


--
-- Name: scope_policy constraint_farsrsps; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT constraint_farsrsps PRIMARY KEY (scope_id, policy_id);


--
-- Name: user_entity constraint_fb; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT constraint_fb PRIMARY KEY (id);


--
-- Name: user_federation_mapper_config constraint_fedmapper_cfg_pm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_mapper_config
    ADD CONSTRAINT constraint_fedmapper_cfg_pm PRIMARY KEY (user_federation_mapper_id, name);


--
-- Name: user_federation_mapper constraint_fedmapperpm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT constraint_fedmapperpm PRIMARY KEY (id);


--
-- Name: fed_user_consent_cl_scope constraint_fgrntcsnt_clsc_pm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.fed_user_consent_cl_scope
    ADD CONSTRAINT constraint_fgrntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent_client_scope constraint_grntcsnt_clsc_pm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_consent_client_scope
    ADD CONSTRAINT constraint_grntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent constraint_grntcsnt_pm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT constraint_grntcsnt_pm PRIMARY KEY (id);


--
-- Name: keycloak_group constraint_group; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT constraint_group PRIMARY KEY (id);


--
-- Name: group_attribute constraint_group_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.group_attribute
    ADD CONSTRAINT constraint_group_attribute_pk PRIMARY KEY (id);


--
-- Name: group_role_mapping constraint_group_role; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT constraint_group_role PRIMARY KEY (role_id, group_id);


--
-- Name: identity_provider_mapper constraint_idpm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider_mapper
    ADD CONSTRAINT constraint_idpm PRIMARY KEY (id);


--
-- Name: idp_mapper_config constraint_idpmconfig; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.idp_mapper_config
    ADD CONSTRAINT constraint_idpmconfig PRIMARY KEY (idp_mapper_id, name);


--
-- Name: migration_model constraint_migmod; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.migration_model
    ADD CONSTRAINT constraint_migmod PRIMARY KEY (id);


--
-- Name: offline_client_session constraint_offl_cl_ses_pk3; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.offline_client_session
    ADD CONSTRAINT constraint_offl_cl_ses_pk3 PRIMARY KEY (user_session_id, client_id, client_storage_provider, external_client_id, offline_flag);


--
-- Name: offline_user_session constraint_offl_us_ses_pk2; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.offline_user_session
    ADD CONSTRAINT constraint_offl_us_ses_pk2 PRIMARY KEY (user_session_id, offline_flag);


--
-- Name: protocol_mapper constraint_pcm; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT constraint_pcm PRIMARY KEY (id);


--
-- Name: protocol_mapper_config constraint_pmconfig; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.protocol_mapper_config
    ADD CONSTRAINT constraint_pmconfig PRIMARY KEY (protocol_mapper_id, name);


--
-- Name: realm_default_roles constraint_realm_default_roles; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_roles
    ADD CONSTRAINT constraint_realm_default_roles PRIMARY KEY (realm_id, role_id);


--
-- Name: redirect_uris constraint_redirect_uris; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.redirect_uris
    ADD CONSTRAINT constraint_redirect_uris PRIMARY KEY (client_id, value);


--
-- Name: required_action_config constraint_req_act_cfg_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.required_action_config
    ADD CONSTRAINT constraint_req_act_cfg_pk PRIMARY KEY (required_action_id, name);


--
-- Name: required_action_provider constraint_req_act_prv_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.required_action_provider
    ADD CONSTRAINT constraint_req_act_prv_pk PRIMARY KEY (id);


--
-- Name: user_required_action constraint_required_action; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_required_action
    ADD CONSTRAINT constraint_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: resource_uris constraint_resour_uris_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_uris
    ADD CONSTRAINT constraint_resour_uris_pk PRIMARY KEY (resource_id, value);


--
-- Name: role_attribute constraint_role_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.role_attribute
    ADD CONSTRAINT constraint_role_attribute_pk PRIMARY KEY (id);


--
-- Name: user_attribute constraint_user_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_attribute
    ADD CONSTRAINT constraint_user_attribute_pk PRIMARY KEY (id);


--
-- Name: user_group_membership constraint_user_group; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_group_membership
    ADD CONSTRAINT constraint_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: user_session_note constraint_usn_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_session_note
    ADD CONSTRAINT constraint_usn_pk PRIMARY KEY (user_session, name);


--
-- Name: web_origins constraint_web_origins; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.web_origins
    ADD CONSTRAINT constraint_web_origins PRIMARY KEY (client_id, value);


--
-- Name: client_scope_attributes pk_cl_tmpl_attr; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_attributes
    ADD CONSTRAINT pk_cl_tmpl_attr PRIMARY KEY (scope_id, name);


--
-- Name: client_scope pk_cli_template; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT pk_cli_template PRIMARY KEY (id);


--
-- Name: databasechangeloglock pk_databasechangeloglock; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);


--
-- Name: resource_server pk_resource_server; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server
    ADD CONSTRAINT pk_resource_server PRIMARY KEY (id);


--
-- Name: client_scope_role_mapping pk_template_scope; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT pk_template_scope PRIMARY KEY (scope_id, role_id);


--
-- Name: default_client_scope r_def_cli_scope_bind; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT r_def_cli_scope_bind PRIMARY KEY (realm_id, scope_id);


--
-- Name: resource_attribute res_attr_pk; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_attribute
    ADD CONSTRAINT res_attr_pk PRIMARY KEY (id);


--
-- Name: keycloak_group sibling_names; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT sibling_names UNIQUE (realm_id, parent_group, name);


--
-- Name: identity_provider uk_2daelwnibji49avxsrtuf6xj33; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT uk_2daelwnibji49avxsrtuf6xj33 UNIQUE (provider_alias, realm_id);


--
-- Name: client_default_roles uk_8aelwnibji49avxsrtuf6xjow; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_default_roles
    ADD CONSTRAINT uk_8aelwnibji49avxsrtuf6xjow UNIQUE (role_id);


--
-- Name: client uk_b71cjlbenv945rb6gcon438at; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT uk_b71cjlbenv945rb6gcon438at UNIQUE (realm_id, client_id);


--
-- Name: client_scope uk_cli_scope; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT uk_cli_scope UNIQUE (realm_id, name);


--
-- Name: user_entity uk_dykn684sl8up1crfei6eckhd7; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT uk_dykn684sl8up1crfei6eckhd7 UNIQUE (realm_id, email_constraint);


--
-- Name: resource_server_resource uk_frsr6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5ha6 UNIQUE (name, owner, resource_server_id);


--
-- Name: resource_server_perm_ticket uk_frsr6t700s9v50bu18ws5pmt; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5pmt UNIQUE (owner, requester, resource_server_id, resource_id, scope_id);


--
-- Name: resource_server_policy uk_frsrpt700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT uk_frsrpt700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: resource_server_scope uk_frsrst700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT uk_frsrst700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: realm_default_roles uk_h4wpd7w4hsoolni3h0sw7btje; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_roles
    ADD CONSTRAINT uk_h4wpd7w4hsoolni3h0sw7btje UNIQUE (role_id);


--
-- Name: user_consent uk_jkuwuvd56ontgsuhogm8uewrt; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT uk_jkuwuvd56ontgsuhogm8uewrt UNIQUE (client_id, client_storage_provider, external_client_id, user_id);


--
-- Name: realm uk_orvsdmla56612eaefiq6wl5oi; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT uk_orvsdmla56612eaefiq6wl5oi UNIQUE (name);


--
-- Name: user_entity uk_ru8tt6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT uk_ru8tt6t700s9v50bu18ws5ha6 UNIQUE (realm_id, username);


--
-- Name: idx_assoc_pol_assoc_pol_id; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_assoc_pol_assoc_pol_id ON public.associated_policy USING btree (associated_policy_id);


--
-- Name: idx_auth_config_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_auth_config_realm ON public.authenticator_config USING btree (realm_id);


--
-- Name: idx_auth_exec_flow; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_auth_exec_flow ON public.authentication_execution USING btree (flow_id);


--
-- Name: idx_auth_exec_realm_flow; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_auth_exec_realm_flow ON public.authentication_execution USING btree (realm_id, flow_id);


--
-- Name: idx_auth_flow_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_auth_flow_realm ON public.authentication_flow USING btree (realm_id);


--
-- Name: idx_cl_clscope; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_cl_clscope ON public.client_scope_client USING btree (scope_id);


--
-- Name: idx_client_def_roles_client; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_client_def_roles_client ON public.client_default_roles USING btree (client_id);


--
-- Name: idx_client_id; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_client_id ON public.client USING btree (client_id);


--
-- Name: idx_client_init_acc_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_client_init_acc_realm ON public.client_initial_access USING btree (realm_id);


--
-- Name: idx_client_session_session; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_client_session_session ON public.client_session USING btree (session_id);


--
-- Name: idx_clscope_attrs; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_clscope_attrs ON public.client_scope_attributes USING btree (scope_id);


--
-- Name: idx_clscope_cl; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_clscope_cl ON public.client_scope_client USING btree (client_id);


--
-- Name: idx_clscope_protmap; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_clscope_protmap ON public.protocol_mapper USING btree (client_scope_id);


--
-- Name: idx_clscope_role; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_clscope_role ON public.client_scope_role_mapping USING btree (scope_id);


--
-- Name: idx_compo_config_compo; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_compo_config_compo ON public.component_config USING btree (component_id);


--
-- Name: idx_component_provider_type; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_component_provider_type ON public.component USING btree (provider_type);


--
-- Name: idx_component_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_component_realm ON public.component USING btree (realm_id);


--
-- Name: idx_composite; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_composite ON public.composite_role USING btree (composite);


--
-- Name: idx_composite_child; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_composite_child ON public.composite_role USING btree (child_role);


--
-- Name: idx_defcls_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_defcls_realm ON public.default_client_scope USING btree (realm_id);


--
-- Name: idx_defcls_scope; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_defcls_scope ON public.default_client_scope USING btree (scope_id);


--
-- Name: idx_event_time; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_event_time ON public.event_entity USING btree (realm_id, event_time);


--
-- Name: idx_fedidentity_feduser; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fedidentity_feduser ON public.federated_identity USING btree (federated_user_id);


--
-- Name: idx_fedidentity_user; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fedidentity_user ON public.federated_identity USING btree (user_id);


--
-- Name: idx_fu_attribute; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_attribute ON public.fed_user_attribute USING btree (user_id, realm_id, name);


--
-- Name: idx_fu_cnsnt_ext; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_cnsnt_ext ON public.fed_user_consent USING btree (user_id, client_storage_provider, external_client_id);


--
-- Name: idx_fu_consent; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_consent ON public.fed_user_consent USING btree (user_id, client_id);


--
-- Name: idx_fu_consent_ru; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_consent_ru ON public.fed_user_consent USING btree (realm_id, user_id);


--
-- Name: idx_fu_credential; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_credential ON public.fed_user_credential USING btree (user_id, type);


--
-- Name: idx_fu_credential_ru; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_credential_ru ON public.fed_user_credential USING btree (realm_id, user_id);


--
-- Name: idx_fu_group_membership; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_group_membership ON public.fed_user_group_membership USING btree (user_id, group_id);


--
-- Name: idx_fu_group_membership_ru; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_group_membership_ru ON public.fed_user_group_membership USING btree (realm_id, user_id);


--
-- Name: idx_fu_required_action; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_required_action ON public.fed_user_required_action USING btree (user_id, required_action);


--
-- Name: idx_fu_required_action_ru; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_required_action_ru ON public.fed_user_required_action USING btree (realm_id, user_id);


--
-- Name: idx_fu_role_mapping; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_role_mapping ON public.fed_user_role_mapping USING btree (user_id, role_id);


--
-- Name: idx_fu_role_mapping_ru; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_fu_role_mapping_ru ON public.fed_user_role_mapping USING btree (realm_id, user_id);


--
-- Name: idx_group_attr_group; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_group_attr_group ON public.group_attribute USING btree (group_id);


--
-- Name: idx_group_role_mapp_group; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_group_role_mapp_group ON public.group_role_mapping USING btree (group_id);


--
-- Name: idx_id_prov_mapp_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_id_prov_mapp_realm ON public.identity_provider_mapper USING btree (realm_id);


--
-- Name: idx_ident_prov_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_ident_prov_realm ON public.identity_provider USING btree (realm_id);


--
-- Name: idx_keycloak_role_client; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_keycloak_role_client ON public.keycloak_role USING btree (client);


--
-- Name: idx_keycloak_role_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_keycloak_role_realm ON public.keycloak_role USING btree (realm);


--
-- Name: idx_offline_uss_createdon; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_offline_uss_createdon ON public.offline_user_session USING btree (created_on);


--
-- Name: idx_protocol_mapper_client; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_protocol_mapper_client ON public.protocol_mapper USING btree (client_id);


--
-- Name: idx_realm_attr_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_attr_realm ON public.realm_attribute USING btree (realm_id);


--
-- Name: idx_realm_clscope; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_clscope ON public.client_scope USING btree (realm_id);


--
-- Name: idx_realm_def_grp_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_def_grp_realm ON public.realm_default_groups USING btree (realm_id);


--
-- Name: idx_realm_def_roles_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_def_roles_realm ON public.realm_default_roles USING btree (realm_id);


--
-- Name: idx_realm_evt_list_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_evt_list_realm ON public.realm_events_listeners USING btree (realm_id);


--
-- Name: idx_realm_evt_types_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_evt_types_realm ON public.realm_enabled_event_types USING btree (realm_id);


--
-- Name: idx_realm_master_adm_cli; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_master_adm_cli ON public.realm USING btree (master_admin_client);


--
-- Name: idx_realm_supp_local_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_realm_supp_local_realm ON public.realm_supported_locales USING btree (realm_id);


--
-- Name: idx_redir_uri_client; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_redir_uri_client ON public.redirect_uris USING btree (client_id);


--
-- Name: idx_req_act_prov_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_req_act_prov_realm ON public.required_action_provider USING btree (realm_id);


--
-- Name: idx_res_policy_policy; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_res_policy_policy ON public.resource_policy USING btree (policy_id);


--
-- Name: idx_res_scope_scope; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_res_scope_scope ON public.resource_scope USING btree (scope_id);


--
-- Name: idx_res_serv_pol_res_serv; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_res_serv_pol_res_serv ON public.resource_server_policy USING btree (resource_server_id);


--
-- Name: idx_res_srv_res_res_srv; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_res_srv_res_res_srv ON public.resource_server_resource USING btree (resource_server_id);


--
-- Name: idx_res_srv_scope_res_srv; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_res_srv_scope_res_srv ON public.resource_server_scope USING btree (resource_server_id);


--
-- Name: idx_role_attribute; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_role_attribute ON public.role_attribute USING btree (role_id);


--
-- Name: idx_role_clscope; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_role_clscope ON public.client_scope_role_mapping USING btree (role_id);


--
-- Name: idx_scope_mapping_role; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_scope_mapping_role ON public.scope_mapping USING btree (role_id);


--
-- Name: idx_scope_policy_policy; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_scope_policy_policy ON public.scope_policy USING btree (policy_id);


--
-- Name: idx_update_time; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_update_time ON public.migration_model USING btree (update_time);


--
-- Name: idx_us_sess_id_on_cl_sess; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_us_sess_id_on_cl_sess ON public.offline_client_session USING btree (user_session_id);


--
-- Name: idx_usconsent_clscope; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_usconsent_clscope ON public.user_consent_client_scope USING btree (user_consent_id);


--
-- Name: idx_user_attribute; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_attribute ON public.user_attribute USING btree (user_id);


--
-- Name: idx_user_consent; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_consent ON public.user_consent USING btree (user_id);


--
-- Name: idx_user_credential; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_credential ON public.credential USING btree (user_id);


--
-- Name: idx_user_email; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_email ON public.user_entity USING btree (email);


--
-- Name: idx_user_group_mapping; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_group_mapping ON public.user_group_membership USING btree (user_id);


--
-- Name: idx_user_reqactions; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_reqactions ON public.user_required_action USING btree (user_id);


--
-- Name: idx_user_role_mapping; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_user_role_mapping ON public.user_role_mapping USING btree (user_id);


--
-- Name: idx_usr_fed_map_fed_prv; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_usr_fed_map_fed_prv ON public.user_federation_mapper USING btree (federation_provider_id);


--
-- Name: idx_usr_fed_map_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_usr_fed_map_realm ON public.user_federation_mapper USING btree (realm_id);


--
-- Name: idx_usr_fed_prv_realm; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_usr_fed_prv_realm ON public.user_federation_provider USING btree (realm_id);


--
-- Name: idx_web_orig_client; Type: INDEX; Schema: public; Owner: kUser
--

CREATE INDEX idx_web_orig_client ON public.web_origins USING btree (client_id);


--
-- Name: client_session_auth_status auth_status_constraint; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_auth_status
    ADD CONSTRAINT auth_status_constraint FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: identity_provider fk2b4ebc52ae5c3b34; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT fk2b4ebc52ae5c3b34 FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_attributes fk3c47c64beacca966; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_attributes
    ADD CONSTRAINT fk3c47c64beacca966 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: federated_identity fk404288b92ef007a6; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.federated_identity
    ADD CONSTRAINT fk404288b92ef007a6 FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: client_node_registrations fk4129723ba992f594; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_node_registrations
    ADD CONSTRAINT fk4129723ba992f594 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_session_note fk5edfb00ff51c2736; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_note
    ADD CONSTRAINT fk5edfb00ff51c2736 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: user_session_note fk5edfb00ff51d3472; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_session_note
    ADD CONSTRAINT fk5edfb00ff51d3472 FOREIGN KEY (user_session) REFERENCES public.user_session(id);


--
-- Name: client_session_role fk_11b7sgqw18i532811v7o2dv76; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_role
    ADD CONSTRAINT fk_11b7sgqw18i532811v7o2dv76 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: redirect_uris fk_1burs8pb4ouj97h5wuppahv9f; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.redirect_uris
    ADD CONSTRAINT fk_1burs8pb4ouj97h5wuppahv9f FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: user_federation_provider fk_1fj32f6ptolw2qy60cd8n01e8; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_provider
    ADD CONSTRAINT fk_1fj32f6ptolw2qy60cd8n01e8 FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_session_prot_mapper fk_33a8sgqw18i532811v7o2dk89; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session_prot_mapper
    ADD CONSTRAINT fk_33a8sgqw18i532811v7o2dk89 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: realm_required_credential fk_5hg65lybevavkqfki3kponh9v; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_required_credential
    ADD CONSTRAINT fk_5hg65lybevavkqfki3kponh9v FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: resource_attribute fk_5hrm2vlf9ql5fu022kqepovbr; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu022kqepovbr FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: user_attribute fk_5hrm2vlf9ql5fu043kqepovbr; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu043kqepovbr FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: user_required_action fk_6qj3w1jw9cvafhe19bwsiuvmd; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_required_action
    ADD CONSTRAINT fk_6qj3w1jw9cvafhe19bwsiuvmd FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: keycloak_role fk_6vyqfe4cn4wlq8r6kt5vdsj5c; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT fk_6vyqfe4cn4wlq8r6kt5vdsj5c FOREIGN KEY (realm) REFERENCES public.realm(id);


--
-- Name: realm_smtp_config fk_70ej8xdxgxd0b9hh6180irr0o; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_smtp_config
    ADD CONSTRAINT fk_70ej8xdxgxd0b9hh6180irr0o FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_default_roles fk_8aelwnibji49avxsrtuf6xjow; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_default_roles
    ADD CONSTRAINT fk_8aelwnibji49avxsrtuf6xjow FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: realm_attribute fk_8shxd6l3e9atqukacxgpffptw; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_attribute
    ADD CONSTRAINT fk_8shxd6l3e9atqukacxgpffptw FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: composite_role fk_a63wvekftu8jo1pnj81e7mce2; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT fk_a63wvekftu8jo1pnj81e7mce2 FOREIGN KEY (composite) REFERENCES public.keycloak_role(id);


--
-- Name: authentication_execution fk_auth_exec_flow; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT fk_auth_exec_flow FOREIGN KEY (flow_id) REFERENCES public.authentication_flow(id);


--
-- Name: authentication_execution fk_auth_exec_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT fk_auth_exec_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: authentication_flow fk_auth_flow_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authentication_flow
    ADD CONSTRAINT fk_auth_flow_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: authenticator_config fk_auth_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.authenticator_config
    ADD CONSTRAINT fk_auth_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_session fk_b4ao2vcvat6ukau74wbwtfqo1; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_session
    ADD CONSTRAINT fk_b4ao2vcvat6ukau74wbwtfqo1 FOREIGN KEY (session_id) REFERENCES public.user_session(id);


--
-- Name: user_role_mapping fk_c4fqv34p1mbylloxang7b1q3l; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_role_mapping
    ADD CONSTRAINT fk_c4fqv34p1mbylloxang7b1q3l FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: client_scope_client fk_c_cli_scope_client; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_client
    ADD CONSTRAINT fk_c_cli_scope_client FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_scope_client fk_c_cli_scope_scope; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_client
    ADD CONSTRAINT fk_c_cli_scope_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_scope_attributes fk_cl_scope_attr_scope; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_attributes
    ADD CONSTRAINT fk_cl_scope_attr_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_scope_role_mapping fk_cl_scope_rm_role; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT fk_cl_scope_rm_role FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: client_scope_role_mapping fk_cl_scope_rm_scope; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT fk_cl_scope_rm_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_user_session_note fk_cl_usr_ses_note; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_user_session_note
    ADD CONSTRAINT fk_cl_usr_ses_note FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: protocol_mapper fk_cli_scope_mapper; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT fk_cli_scope_mapper FOREIGN KEY (client_scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_initial_access fk_client_init_acc_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_initial_access
    ADD CONSTRAINT fk_client_init_acc_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: component_config fk_component_config; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.component_config
    ADD CONSTRAINT fk_component_config FOREIGN KEY (component_id) REFERENCES public.component(id);


--
-- Name: component fk_component_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT fk_component_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_default_groups fk_def_groups_group; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT fk_def_groups_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: realm_default_groups fk_def_groups_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT fk_def_groups_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_default_roles fk_evudb1ppw84oxfax2drs03icc; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_roles
    ADD CONSTRAINT fk_evudb1ppw84oxfax2drs03icc FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: user_federation_mapper_config fk_fedmapper_cfg; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_mapper_config
    ADD CONSTRAINT fk_fedmapper_cfg FOREIGN KEY (user_federation_mapper_id) REFERENCES public.user_federation_mapper(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_fedprv; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_fedprv FOREIGN KEY (federation_provider_id) REFERENCES public.user_federation_provider(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: associated_policy fk_frsr5s213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT fk_frsr5s213xcx4wnkog82ssrfy FOREIGN KEY (associated_policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrasp13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT fk_frsrasp13xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog82sspmt; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82sspmt FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_server_resource fk_frsrho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog83sspmt; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog83sspmt FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog84sspmt; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog84sspmt FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: associated_policy fk_frsrpas14xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT fk_frsrpas14xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrpass3xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT fk_frsrpass3xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: resource_server_perm_ticket fk_frsrpo2128cx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrpo2128cx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_server_policy fk_frsrpo213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT fk_frsrpo213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_scope fk_frsrpos13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT fk_frsrpos13xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpos53xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT fk_frsrpos53xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpp213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT fk_frsrpp213xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_scope fk_frsrps213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT fk_frsrps213xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: resource_server_scope fk_frsrso213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT fk_frsrso213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: composite_role fk_gr7thllb9lu8q4vqa4524jjy8; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT fk_gr7thllb9lu8q4vqa4524jjy8 FOREIGN KEY (child_role) REFERENCES public.keycloak_role(id);


--
-- Name: user_consent_client_scope fk_grntcsnt_clsc_usc; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_consent_client_scope
    ADD CONSTRAINT fk_grntcsnt_clsc_usc FOREIGN KEY (user_consent_id) REFERENCES public.user_consent(id);


--
-- Name: user_consent fk_grntcsnt_user; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT fk_grntcsnt_user FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: group_attribute fk_group_attribute_group; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.group_attribute
    ADD CONSTRAINT fk_group_attribute_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: keycloak_group fk_group_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT fk_group_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: group_role_mapping fk_group_role_group; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT fk_group_role_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: group_role_mapping fk_group_role_role; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT fk_group_role_role FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: realm_default_roles fk_h4wpd7w4hsoolni3h0sw7btje; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_default_roles
    ADD CONSTRAINT fk_h4wpd7w4hsoolni3h0sw7btje FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: realm_enabled_event_types fk_h846o4h0w8epx5nwedrf5y69j; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_enabled_event_types
    ADD CONSTRAINT fk_h846o4h0w8epx5nwedrf5y69j FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_events_listeners fk_h846o4h0w8epx5nxev9f5y69j; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_events_listeners
    ADD CONSTRAINT fk_h846o4h0w8epx5nxev9f5y69j FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: identity_provider_mapper fk_idpm_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider_mapper
    ADD CONSTRAINT fk_idpm_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: idp_mapper_config fk_idpmconfig; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.idp_mapper_config
    ADD CONSTRAINT fk_idpmconfig FOREIGN KEY (idp_mapper_id) REFERENCES public.identity_provider_mapper(id);


--
-- Name: keycloak_role fk_kjho5le2c0ral09fl8cm9wfw9; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT fk_kjho5le2c0ral09fl8cm9wfw9 FOREIGN KEY (client) REFERENCES public.client(id);


--
-- Name: web_origins fk_lojpho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.web_origins
    ADD CONSTRAINT fk_lojpho213xcx4wnkog82ssrfy FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_default_roles fk_nuilts7klwqw2h8m2b5joytky; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_default_roles
    ADD CONSTRAINT fk_nuilts7klwqw2h8m2b5joytky FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: scope_mapping fk_ouse064plmlr732lxjcn1q5f1; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT fk_ouse064plmlr732lxjcn1q5f1 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: scope_mapping fk_p3rh9grku11kqfrs4fltt7rnq; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT fk_p3rh9grku11kqfrs4fltt7rnq FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: client fk_p56ctinxxb9gsk57fo49f9tac; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT fk_p56ctinxxb9gsk57fo49f9tac FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: protocol_mapper fk_pcm_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT fk_pcm_realm FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: credential fk_pfyr0glasqyl0dei3kl69r6v0; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.credential
    ADD CONSTRAINT fk_pfyr0glasqyl0dei3kl69r6v0 FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: protocol_mapper_config fk_pmconfig; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.protocol_mapper_config
    ADD CONSTRAINT fk_pmconfig FOREIGN KEY (protocol_mapper_id) REFERENCES public.protocol_mapper(id);


--
-- Name: default_client_scope fk_r_def_cli_scope_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT fk_r_def_cli_scope_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: default_client_scope fk_r_def_cli_scope_scope; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT fk_r_def_cli_scope_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_scope fk_realm_cli_scope; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT fk_realm_cli_scope FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: required_action_provider fk_req_act_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.required_action_provider
    ADD CONSTRAINT fk_req_act_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: resource_uris fk_resource_server_uris; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.resource_uris
    ADD CONSTRAINT fk_resource_server_uris FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: role_attribute fk_role_attribute_id; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.role_attribute
    ADD CONSTRAINT fk_role_attribute_id FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: realm_supported_locales fk_supported_locales_realm; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm_supported_locales
    ADD CONSTRAINT fk_supported_locales_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: user_federation_config fk_t13hpu1j94r2ebpekr39x5eu5; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_federation_config
    ADD CONSTRAINT fk_t13hpu1j94r2ebpekr39x5eu5 FOREIGN KEY (user_federation_provider_id) REFERENCES public.user_federation_provider(id);


--
-- Name: realm fk_traf444kk6qrkms7n56aiwq5y; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT fk_traf444kk6qrkms7n56aiwq5y FOREIGN KEY (master_admin_client) REFERENCES public.client(id);


--
-- Name: user_group_membership fk_user_group_user; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.user_group_membership
    ADD CONSTRAINT fk_user_group_user FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: policy_config fkdc34197cf864c4e43; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.policy_config
    ADD CONSTRAINT fkdc34197cf864c4e43 FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: identity_provider_config fkdc4897cf864c4e43; Type: FK CONSTRAINT; Schema: public; Owner: kUser
--

ALTER TABLE ONLY public.identity_provider_config
    ADD CONSTRAINT fkdc4897cf864c4e43 FOREIGN KEY (identity_provider_id) REFERENCES public.identity_provider(internal_id);


--
-- PostgreSQL database dump complete
--

