use chrono::NaiveDateTime;
use crate::db::models::user_profile::UserProfile;
use crate::db::schema::*;

#[derive(Serialize, Deserialize, Queryable)]
pub struct Task {
    pub id: i64,
    pub title: String,
    pub body: String,
}

#[derive(Serialize, Deserialize, Insertable)]
#[table_name = "tasks"]
pub struct NewTask<'a> {
    pub title: &'a str,
    pub body: &'a str,
}

#[derive(Serialize, Deserialize)]
pub struct DetailedTask {
    pub task: Task,
    pub status: TaskStatus,
    pub team: Vec<(UserProfile, TeamRole)>,
}

#[derive(Serialize, Deserialize, Insertable, Queryable)]
#[table_name = "task_teams"]
pub struct TaskTeamMember {
    pub task_id: i64,
    pub user_id: String,
    pub team_role_id: i32,
}

#[derive(/*Serialize, Deserialize,*/ Insertable, Queryable)]
#[table_name = "task_status_history"]
pub struct TaskStatusHistoryEntry {
    pub task_id: i64,
    pub status_id: i32,
    pub author_id: String,
    pub transition_timestamp: NaiveDateTime,
}

// TODO : TaskStatus & TeamRoles are currently hard inserted in database
//      at initialization (no CRUD).

#[derive(Serialize, Deserialize, Queryable)]
pub struct TaskStatus {
    pub id: i32,
    pub name: String,
}

#[derive(Serialize, Deserialize, Queryable)]
pub struct TeamRole {
    pub id: i32,
    pub name: String,
}
