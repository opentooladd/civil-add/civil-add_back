use crate::db::schema::user_profiles;

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Insertable)]
#[table_name = "user_profiles"]
pub struct UserProfile {
    pub id: String,
    pub name: String,
}

impl Clone for UserProfile {
    fn clone(&self) -> Self {
        UserProfile {
            id: String::from(self.id.clone()),
            name: String::from(self.name.clone()),
        }
    }
}
