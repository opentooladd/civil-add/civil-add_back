extern crate diesel;

pub mod models;
pub mod schema;
pub mod commands;
pub mod queries;

use diesel::prelude::*;
use crate::SETTINGS;

pub fn establish_connection() -> PgConnection {
    let database_url: &str = &SETTINGS.database.url;
    PgConnection::establish(database_url)
        .expect(&format!("Error connecting to {}", database_url))
}
