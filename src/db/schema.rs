table! {
    task_status (id) {
        id -> Int4,
        name -> Varchar,
    }
}

table! {
    task_status_history (task_id, status_id, author_id, transition_timestamp) {
        task_id -> Int8,
        status_id -> Int4,
        author_id -> Varchar,
        transition_timestamp -> Timestamp,
    }
}

table! {
    task_teams (task_id, user_id, team_role_id) {
        task_id -> Int8,
        user_id -> Varchar,
        team_role_id -> Int4,
    }
}

table! {
    tasks (id) {
        id -> Int8,
        title -> Varchar,
        body -> Text,
    }
}

table! {
    team_roles (id) {
        id -> Int4,
        name -> Varchar,
    }
}

table! {
    user_profiles (id) {
        id -> Varchar,
        name -> Varchar,
    }
}

joinable!(task_status_history -> task_status (status_id));
joinable!(task_status_history -> tasks (task_id));
joinable!(task_teams -> tasks (task_id));
joinable!(task_teams -> team_roles (team_role_id));
joinable!(task_teams -> user_profiles (user_id));

allow_tables_to_appear_in_same_query!(
    task_status,
    task_status_history,
    task_teams,
    tasks,
    team_roles,
    user_profiles,
);
