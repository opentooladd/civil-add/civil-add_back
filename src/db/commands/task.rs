extern crate diesel;

use diesel::prelude::*;
use chrono::offset::Utc;

use crate::db::models::task::*;
use crate::db::models::user_profile::UserProfile;
use crate::db::schema::task_status_history;
use crate::db::schema::task_teams;
use crate::db::schema::tasks;
use crate::db::schema::tasks::dsl::*;


pub fn create(conn: &PgConnection, new_task: &NewTask, owner: UserProfile) -> DetailedTask {
    // register task
    let task: Task = diesel::insert_into(tasks::table)
        .values(new_task)
        .get_result(conn)
        .expect("Error saving new task");

    let draft_status = TaskStatus { id: 1, name: "DRAFT".to_string() }; // 1 is hard coded DRAFT status id
    let owner_role = TeamRole{ id: 1, name: "OWNER".to_string() }; // 1 is hard coded OWNER role id

    // register task team member
    diesel::insert_into(task_teams::table)
        .values( TaskTeamMember{ task_id: task.id, user_id: owner.id.clone(), team_role_id: owner_role.id})
        .get_result::<TaskTeamMember>(conn)
        .expect("Error updating new task team member.");

    // update task status
    diesel::insert_into(task_status_history::table)
        .values( TaskStatusHistoryEntry{ task_id: task.id, status_id: draft_status.id, author_id: owner.id.clone(), transition_timestamp: Utc::now().naive_utc() } )
        .get_result::<TaskStatusHistoryEntry>(conn)
        .expect("Error updating new task status.");

    DetailedTask {
        task,
        status: draft_status,
        team: vec![( owner, owner_role )]
    }
}

pub fn delete(conn: &PgConnection, pattern: String) -> usize {
    diesel::delete(tasks.filter(title.like(pattern)))
        .execute(conn)
        .expect("Error deleting tasks")
}


/** Tests
TODO : implement proper tests
***********/

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
