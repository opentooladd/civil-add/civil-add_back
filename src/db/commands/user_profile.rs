extern crate diesel;

use diesel::prelude::*;

use crate::db::schema::user_profiles::dsl::*;
use crate::db::schema::user_profiles;
use crate::db::models::user_profile::*;

/// Insert profile if not exists, else update it
pub fn insert(conn: &PgConnection, user_profile: &UserProfile) -> UserProfile {
    diesel::insert_into(user_profiles::table)
        .values(user_profile)
        .on_conflict(id)
        .do_update()
        .set(user_profile)
        .get_result(conn)
        .expect("Error inserting/updating user profile")
}

