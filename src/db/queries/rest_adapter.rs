// TODO : review project architecture :
//      * move some of those structures (// aiguilleur ?)
//      * enhance api response generic format : { data: ..., query_params: ..., errors: [...], ... }
//      * add default query values to server configuration

use std::str::FromStr;

use diesel::pg::Pg;
use diesel::prelude::*;
use diesel::query_builder::*;
use diesel::query_dsl::methods::LoadQuery;
use diesel::sql_types::{BigInt, HasSqlType};
use rocket::request::{FromQuery, Query as RestQuery};
use serde::export::fmt::Debug;

use crate::errors::api_error::*;

#[derive(Debug)]
pub struct Pagination {
    pub page: i64,
    pub page_size: i64,
}

// pub trait Filter

// pub trait OrderBy

#[derive(Debug)]
pub struct QueryParams {
    pub pagination: Pagination,
    // filters: Vec<Filter>,
    // orderBy: Option<OrderBy>,
}

impl QueryParams {
    // TODO : add configuration & hierarchical default values definition (QueryParams -> Pagination -> ...)
    pub fn default(key: String) -> Result<String, ApiError> {
        match &key[..] {
            "page" => Ok(String::from("1")),
            "page_size" => Ok(String::from("10")),
            _ => Err(
                ApiError::new(
                    400,
                    String::from("Unknown query parameter key given for default value.")
                )
            )
        }

    }
}


impl<'q> FromQuery<'q> for QueryParams {
    /// The number of `key`s we actually saw.
    type Error = usize;

    fn from_query(query: RestQuery) -> Result<Self, Self::Error> {

        // TODO : make this method generic and move it to the proper place.
        fn get_value<T>(mut query: RestQuery, key: String) -> T
            where
                T: Clone + FromStr,
                <T as std::str::FromStr>::Err: std::fmt::Debug
        {
            let default_value: T = match QueryParams::default(key.clone()) {
                Ok(r) => r.parse::<T>().expect("Wrong server configuration. Contact admin"),
                Err(_error) => panic!("Wrong server configuration. Contact admin")
            };

            query
                .find( |i| i.key == key.clone() )
                .map( |i| i.value.as_str().parse::<T>().unwrap_or(default_value.clone()) )
                .unwrap_or(default_value.clone())
        }

        let page_size: i64 = get_value(query.clone(), String::from("page_size"));
        let page: i64 = get_value(query.clone(), String::from("page"));
        let pagination = Pagination { page, page_size, };

        Ok(QueryParams { pagination, })
    }
}


#[derive(QueryId)]
pub struct Parametrized<T> {
    query: T,
    parameters: QueryParams,
}

pub trait Parametrize: Sized {
    fn parametrize(self, parameters: QueryParams) -> Parametrized<Self>;
}

impl<T> Parametrize for T {
    fn parametrize(self, parameters: QueryParams) -> Parametrized<Self> {
        Parametrized {
            query: self,
            parameters,
        }
    }
}

impl<T> QueryFragment<Pg> for Parametrized<T>
    where
        T: QueryFragment<Pg>,
{
    fn walk_ast(&self, mut out: AstPass<Pg>) -> QueryResult<()> {
        // TODO : review the sql request construction. Won't work for custom dataset / filters / sort parameters
        out.push_sql("SELECT *, COUNT(*) OVER () FROM (");
        self.query.walk_ast(out.reborrow())?;
        out.push_sql(") t LIMIT ");
        out.push_bind_param::<BigInt, _>(&self.parameters.pagination.page_size)?;
        out.push_sql(" OFFSET ");
        let offset = (self.parameters.pagination.page - 1) * self.parameters.pagination.page_size;
        out.push_bind_param::<BigInt, _>(&offset)?;
        Ok(())
    }
}

impl<T: Query> Query for Parametrized<T> {
    type SqlType = (T::SqlType, BigInt);
}

impl<T> RunQueryDsl<PgConnection> for Parametrized<T> {}

impl<T> Parametrized<T> {
    pub fn load_and_count_pages<U>(self, conn: &PgConnection) -> QueryResult<(Vec<U>, i64)>
        where
            Self: LoadQuery<PgConnection, (U, i64)>,
    {
        let page_size = self.parameters.pagination.page_size;
        let results = self.load::<(U, i64)>(conn)?;
        let total = results.get(0).map(|x| x.1).unwrap_or(0);
        let records = results.into_iter().map(|x| x.0).collect();
        let total_pages = (total as f64 / page_size as f64).ceil() as i64;
        Ok((records, total_pages))
    }
}

pub trait LoadPaginated<U>: Query + QueryId + QueryFragment<Pg> + LoadQuery<PgConnection, U> {
    fn load_with_parameters(self, conn: &PgConnection, parameters: QueryParams) -> QueryResult<(Vec<U>, i64)>;
}

impl<T, U> LoadPaginated<U> for T
    where
        Self: Query + QueryId + QueryFragment<Pg> + LoadQuery<PgConnection, U>,
        U: Queryable<Self::SqlType, Pg>,
        Pg: HasSqlType<Self::SqlType>,
{
    fn load_with_parameters(self, conn: &PgConnection, parameters: QueryParams) -> QueryResult<(Vec<U>, i64)> {
        let (records, total_pages) = {
            let query = self.parametrize(parameters);
            query.load_and_count_pages::<U>(conn)?
        };

        Ok((records, total_pages))
    }
}