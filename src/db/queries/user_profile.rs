extern crate diesel;

use diesel::prelude::*;

use crate::db::models::user_profile::UserProfile;
use crate::db::schema::user_profiles::dsl::*;


pub fn read_list(conn: &PgConnection) -> Vec<UserProfile> {
    user_profiles.limit(5)
        .load::<UserProfile>(conn)
        .expect("Error loading user profiles")
}

pub fn read_by_id(conn: &PgConnection, requested_id: String) -> UserProfile {
    user_profiles.find(requested_id)
        .first::<UserProfile>(conn)
        .expect("Error loading user profiles")
}
