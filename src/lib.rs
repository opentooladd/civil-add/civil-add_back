#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate diesel;
#[macro_use] extern crate lazy_static;


use settings::app::Settings;
lazy_static! {
    static ref SETTINGS: Settings = Settings::new();
}

pub mod db;
// TODO : remove routes from lib (currently needed for integration tests)
pub mod routes;
pub mod errors;
pub mod settings;
pub mod authentication;