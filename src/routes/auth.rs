use rocket::get;
use serde_json;
use serde_json::json;
use rocket::http::{Cookie, Cookies};
use rocket::response::Redirect;
use crate::authentication::session::UserSession;

#[get("/logout")]
pub fn logout(mut cookies: Cookies) -> String {
    // TODO : WARN : It looks like removing local cookie isn't enough, Keycloak keep session available.
    // We have to find a way to close Keycloak session.
    // cookies.remove_private(Cookie::named("rocket_oauth2_state"));
    cookies.remove_private(Cookie::named("user-session"));
    format!("Logged out !")
}

#[get("/details")]
pub fn details(us: UserSession) -> String {

    // TODO : display user info using http://localhost:8888/auth/realms/civil-add/protocol/openid-connect/userinfo
    // let https = HttpsConnector::new(hyper_sync_rustls::TlsClient::new());
    // let client = Client::with_connector(https);
    //
    // // Use the token to retrieve the user's GitHub account information.
    // let mime: Mime = "application/vnd.github.v3+json"
    //     .parse()
    //     .expect("parse GitHub MIME type");
    // let response = client
    //     .get("https://api.github.com/user")
    //     .header(Authorization(format!("token {}", token.access_token())))
    //     .header(Accept(vec![qitem(mime)]))
    //     .header(UserAgent("rocket_oauth2 demo application".into()))
    //     .send()?;
    //
    // // if !response.status.is_success() {
    // //     return Err(format!("got non-success status {}", response.status).into());
    // // }
    //
    // let user_info: GitHubUserInfo = serde_json::from_reader(response.take(2 * 1024 * 1024))?;

    format!("{}", json!(us).to_string() )
}

#[get("/details", rank=2)]
pub fn details_not_logged() -> Redirect {
    Redirect::to("/auth/login")
}


