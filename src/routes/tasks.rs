extern crate diesel;

use rocket::http::Status;
use rocket::response::Redirect;
use rocket::response::status::Custom;
use rocket_contrib::json::Json;

use crate::authentication::session::{LoggedUserSession, UserSession};
use crate::db::commands::task::create as create_task;
use crate::db::establish_connection;
use crate::db::models::task::*;
use crate::db::queries::task::read;
use crate::db::queries::rest_adapter::QueryParams;

#[post("/", format = "application/json", data = "<new_task>")]
pub fn create(
    user_session: UserSession,
    new_task: Json<NewTask>,
) -> Result<Json<DetailedTask>, Custom<String>> {
    let connection = establish_connection();
    let detailed_task = create_task(
        &connection,
        &new_task.0,
        user_session.get_user_profile().clone()
    );
    Ok(Json(detailed_task))
}

#[post("/", format = "application/json", data = "<_new_task>", rank=2)]
pub fn create_not_logged( _new_task: Json<NewTask> ) -> Status {
    Status::Forbidden
}

#[get("/?<query_params..>")]
pub fn get_list(_user_session: UserSession,query_params: QueryParams) -> Result<Json<Vec<Task>>, Custom<String>> {

    // TODO : add logging system
    // println!("{:?}", query_params);

    let connection = establish_connection();
    let task_list = read(&connection, query_params);
    Ok(Json(task_list))
}

#[get("/", rank=2)]
pub fn get_list_not_logged() -> Redirect {
    Redirect::to("/auth/login")
}

#[get("/<id>")]
pub fn get(_user_session: UserSession, id: i64) -> String {
    format!("TODO : get task {} !", id)
}

#[get("/<_id>", rank=2)]
pub fn get_not_logged(_id: i64) -> Redirect {
    Redirect::to("/auth/login")
}

#[put("/<id>")]
pub fn update(_user_session: UserSession, id: i64) -> String {
    format!("TODO : update task {} !", id)
}

#[delete("/<id>")]
pub fn delete(_user_session: UserSession, id: i64) -> String {
    format!("TODO : delete task {} !", id)
}

#[delete("/<_id>", rank=2)]
pub fn delete_not_logged(_id: i64) -> Status {
    Status::Forbidden
}
