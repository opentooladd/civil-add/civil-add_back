extern crate diesel;

use rocket::response::Redirect;
use rocket::response::status::Custom;
use rocket_contrib::json::Json;

use crate::db::establish_connection;
use crate::db::models::user_profile::UserProfile;
use crate::db::queries::user_profile::read_list;
use crate::authentication::session::UserSession;

#[get("/")]
pub fn get_list(_user_session: UserSession) -> Result<Json<Vec<UserProfile>>, Custom<String>> {
    let connection = establish_connection();
    let user_list = read_list(&connection);
    Ok(Json(user_list))
}

#[get("/", rank=2)]
pub fn get_list_not_logged() -> Redirect {
    Redirect::to("/auth/login")
}
