#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate diesel;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate rocket;
#[macro_use] extern crate serde_derive;
extern crate chrono;

use settings::app::Settings;

mod routes;
mod authentication;
pub mod errors;
pub mod db;
pub mod settings;

lazy_static! {
    static ref SETTINGS: Settings = Settings::new();
}

fn main() {
    rocket::ignite()
        .mount("/", routes![routes::base::index])
        .mount(
            "/tasks",
            routes![
                routes::tasks::create,
                routes::tasks::create_not_logged,
                routes::tasks::get,
                routes::tasks::get_not_logged,
                routes::tasks::get_list,
                routes::tasks::get_list_not_logged,
                routes::tasks::update,
                routes::tasks::delete,
                routes::tasks::delete_not_logged,
            ],
        )
        .mount(
            "/users",
            routes![
                routes::users::get_list,
                routes::users::get_list_not_logged,
            ],
        )
        .mount(
            "/auth",
            routes![
                routes::auth::logout,
                routes::auth::details,
                routes::auth::details_not_logged,
            ],
        )
        .attach(authentication::keycloak::fairing())
        .launch();
}