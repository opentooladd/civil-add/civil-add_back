extern crate civil_add;

use civil_add::settings::app::Settings;

fn main() {

    let settings = Settings::new();

    // Print out our settings
    println!("{:?}", settings);

}