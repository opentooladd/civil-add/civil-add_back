extern crate civil_add;
extern crate diesel;

use diesel::prelude::*;

use civil_add::db::establish_connection;
use civil_add::db::models::task::*;
use civil_add::db::queries::task::read;

fn main() {

    let connection: PgConnection = establish_connection();
    let results : Vec<Task> = read(&connection);

    println!("Displaying {} tasks", results.len());
    for task in results {
        println!("{}", task.title);
        println!("----------\n");
        println!("{}", task.body);
    }
}