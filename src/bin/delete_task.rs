extern crate diesel;
extern crate civil_add;

use std::env::args;

use civil_add::db::establish_connection;
use civil_add::db::commands::task::delete;

// TODO : delete by task_id
fn main() {
    let target = args().nth(1).expect("Expected a target to match against");
    let pattern = format!("%{}%", target);

    let connection = establish_connection();

    let num_deleted: usize = delete(&connection, pattern);

    println!("Deleted {} tasks", num_deleted);
}
