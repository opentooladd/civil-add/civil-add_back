extern crate civil_add;
extern crate diesel;

use std::io::{Read, stdin};

use civil_add::db::commands::task::create;
use civil_add::db::establish_connection;
use civil_add::db::models::task::*;

fn main() {
    let connection = establish_connection();

    let mut title = String::new();
    let mut body = String::new();

    println!("What would you like your title to be?");
    stdin().read_line(&mut title).unwrap();
    let title = title.trim_end(); // Remove the trailing newline

    println!(
        "\nOk! Let's write {} description (Press {} when finished)\n",
        title, EOF
    );
    stdin().read_to_string(&mut body).unwrap();

    let new_task = NewTask { title, body: &body, };
    let task = create(&connection, &new_task);
    println!("\nSaved draft {} with id {}", title, task.id);
}

#[cfg(not(windows))]
const EOF: &str = "CTRL+D";

#[cfg(windows)]
const EOF: &str = "CTRL+Z";
