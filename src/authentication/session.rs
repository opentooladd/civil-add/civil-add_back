
// TODO : create session_service to manage sessions / rights

use rocket::http::{Cookies, Cookie};
use rocket::Outcome;
use rocket::request::{self, FromRequest, Request};
use serde_json::Number;
use crate::db::models::user_profile::UserProfile;
use jsonwebtoken::dangerous_unsafe_decode;

// Todo : create a dedicated object for oauth provider answer
#[derive(Serialize, Deserialize)]
pub struct OauthSession {
    pub access_token: String,
    pub token_type: String,
    pub expires_in: Option<Number>,
    pub refresh_token: Option<String>,
    pub scope: Option<String>,
}


// TODO : take this cookie name from config
pub fn get_cookie_name() -> String {
    "cvad-user-session".to_string()
}

pub trait LoggedUserSession {

    fn get_oauth_session(&self) -> &OauthSession;

    fn get_user_profile(&self) -> &UserProfile;

    // TODO mutualize with keycloak
    fn get_cookie_from_request<'a, 'r> (request: &'a Request<'r>) -> Option<Cookie<'a>> {
        let mut cookies = request.guard::<Cookies<'_>>().expect(&get_cookie_name());
        cookies.get_private(&get_cookie_name())
    }
}

#[derive(Serialize, Deserialize)]
pub struct UserSession {
    oauth_session: OauthSession,
    user_profile: UserProfile,
}

impl LoggedUserSession for UserSession {
    fn get_oauth_session(&self) -> &OauthSession { &self.oauth_session }
    fn get_user_profile(&self) -> &UserProfile { &self.user_profile }
}


// TODO : move this code, review project structure
pub fn user_profile_from_token(access_token: &str) -> UserProfile {

    #[derive(Debug, Serialize, Deserialize)]
    struct Claims {
        sub: String,
        user_name: String,
    }

    // TODO : remove "unsafe" method call
    let decode_token_rslt = dangerous_unsafe_decode::<Claims>(access_token);

    decode_token_rslt.map(|token_data| UserProfile {
        id: token_data.claims.sub,
        name: token_data.claims.user_name
    } ).expect("Can't build user profile from given token." )
}


impl<'a, 'r> FromRequest<'a, 'r> for UserSession {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<UserSession, ()> {
        if let Some(cookie) = Self::get_cookie_from_request(request) {
            let os : OauthSession = serde_json::from_str(cookie.value() ).expect("Can't parse user session.");
            let up : UserProfile = user_profile_from_token(&os.access_token);

            return Outcome::Success( UserSession {
                oauth_session: os,
                user_profile: up
            });
        }
        Outcome::Forward(())
    }
}
