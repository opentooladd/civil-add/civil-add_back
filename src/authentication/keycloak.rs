use std::borrow::Cow;


use rocket::fairing::Fairing;
use rocket::http::{Cookie, Cookies, SameSite};
use rocket::request::Request;
use rocket::response::Redirect;
use rocket_oauth2::{OAuth2, OAuthConfig, StaticProvider, TokenResponse};
use rocket_oauth2::hyper_sync_rustls_adapter::HyperSyncRustlsAdapter;
use serde_json;

use crate::db::commands::user_profile::insert as insert_user;
use crate::db::establish_connection;
use crate::db::models::user_profile::UserProfile;
use crate::SETTINGS;
use crate::authentication::session::{user_profile_from_token, get_cookie_name};

/// Rocket fairing for managing the Keycloak OAuth2 flow
/// TODO : review config process / use Rocket.toml and OAuth2::fairing instead ?
pub fn fairing() -> impl Fairing {

    let provider_auth_uri: &str = &SETTINGS.keycloak.provider_auth_uri;
    let provider_token_uri: &str = &SETTINGS.keycloak.provider_token_uri;

    let provider = StaticProvider {
        auth_uri: Cow::Borrowed(provider_auth_uri),
        token_uri: Cow::Borrowed(provider_token_uri)
    };

    // TODO : add config
    let client_id: String = SETTINGS.keycloak.client_id.clone();
    let client_secret: String = SETTINGS.keycloak.client_secret.clone();

    // define callback route used in redirect_uri of provider
    let callback_uri = "/auth/redirect";
    let api_base_path = SETTINGS.civil_add.api_base_path.clone();
    let redirect_uri: String = api_base_path + callback_uri;

    // define login route that will request the 'user' scope to Keycloak
    let login = Some(("/auth/login", vec![String::from("user")]));

    let config = OAuthConfig::new(
        provider,
        client_id,
        client_secret,
        redirect_uri
    );

    OAuth2::custom(
        HyperSyncRustlsAdapter,
        post_login_callback,
        config,
        callback_uri,
        login
    )
}

/// Callback to handle the authenticated token received from Keycloak
/// and store it as a private cookie
fn post_login_callback(
    request: &Request<'_>,
    token: TokenResponse,
) -> Result<Redirect, Box<dyn (::std::error::Error)>> {

    // TODO : create a dedicated services for user_profile / user_session handling
    {
        let user_profile: UserProfile = user_profile_from_token(token.access_token());
        let connection = establish_connection();
        insert_user(&connection, &user_profile);
    }

    // Set a private cookie with the user's token, and redirect to the 'auth/details' page.
    // TODO : use conf for civil-add cookies
    let mut cookies = request.guard::<Cookies<'_>>().expect("Can't access cookies from request.");
    cookies.add_private(
        Cookie::build(get_cookie_name(), serde_json::to_string(token.as_value()).expect("Can not serialize token response.") )
            // TODO : ensure security (redirect url, review keycloak setup, ...)
            .path("/")
            .same_site(SameSite::Lax)
            // .same_site(SameSite::Strict)
            .finish(),
    );


    // println!("{}", request.headers().get_one("Location").expect(""));
    // println!("{}", request.headers().get_one("Referer").expect(""));

    // this is the front url in dev
    // the end redirect should be set by the front
    // because it determines the cookie domain.
    // Here it is hardcoded but it should come from the front in the
    // auth request in the form ?redirectUrl=url or whatever.
    // @TODO: replace that with initial query parameter
    let front_base_path = SETTINGS.civil_add.front_base_path.clone();
    Ok(Redirect::to(front_base_path))
}
