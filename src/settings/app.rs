use std::env;
use config::{ConfigError, Config, File};
use std::path::PathBuf;

#[derive(Debug, Deserialize)]
pub struct Database {
    pub url: String,
}

#[derive(Debug, Deserialize)]
pub struct Keycloak {
    pub provider_auth_uri: String,
    pub provider_token_uri: String,
    pub client_id: String,
    pub client_secret: String,
    pub redirect_uri: String,
}

#[derive(Debug, Deserialize)]
pub struct CivilAdd {
    pub front_base_path: String,
    pub api_base_path: String,
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub civil_add: CivilAdd,
    pub database: Database,
    pub keycloak: Keycloak,
}

// look at : https://github.com/mehcode/config-rs
impl Settings {

    fn get_app_path() -> PathBuf {
        let cvad_path_str = env::var("CVAD_PATH").expect("CVAD_PATH required");
        let mut cvad_path: PathBuf = PathBuf::new();
        cvad_path.push(cvad_path_str);
        cvad_path
    }

    fn get_config_path() -> PathBuf {
        let mut cvad_config_path = Self::get_app_path();
        cvad_config_path.push("configuration/civil-add.toml");
        cvad_config_path
    }

    pub fn generate() -> Result<Self, ConfigError> {
        let mut s = Config::new();

        // Start off by merging in the "default" configuration file
        s.merge(File::from(Self::get_config_path()))?;

        // TODO
        // s.set( "cvad_path", Self::getAppPath());


        // TODO : review environment usage
        // s.merge(Environment::with_prefix("civil-add"))?;
        //
        // let db_env_name: String = String::from("DATABASE_URL");
        // #[allow(unused_must_use)] {
        //     match env::var(db_env_name.to_owned()) {
        //         Ok(url)  => s.set(
        //             "database.url",
        //             url
        //         ),
        //         Err(e) => Err(ConfigError::NotFound(e.to_string())),
        //     };
        // }

        // Deserialize (and thus freeze) the entire configuration
        s.try_into()
    }

    pub fn new() -> Self { Self::generate().expect("Can't load Civil-Add configuration") }
}
